#!/bin/bash



if [ "$CI_COMMIT_REF_SLUG" != "dev" ]; then

    flutter pub run sentry_dart_plugin

    export SENTRY_RELEASE="diap_stash@$(yq -r .version pubspec.yaml)"
    sentry-cli --url="https://sentry.diapstash.com" --auth-token="${SENTRY_AUTH_TOKEN}" sourcemaps upload --release="$SENTRY_RELEASE" lib/ --url-prefix 'https://app.diapstash.com/lib' --ext dart || exit 0
    export SENTRY_RELEASE="fr.diapstash.diap_stash@$(yq -r .version pubspec.yaml)"

fi
