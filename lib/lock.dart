// ignore_for_file: use_build_context_synchronously, depend_on_referenced_packages

import 'dart:developer';

import 'package:diap_stash/common.dart';
import 'package:diap_stash/main.dart';
import 'package:diap_stash/providers/settings_provider.dart';
import 'package:flutter_screen_lock/flutter_screen_lock.dart';
import 'package:local_auth/local_auth.dart';
import 'package:local_auth_android/local_auth_android.dart';
import 'package:local_auth_ios/local_auth_ios.dart';

bool isAuthenticated = false;
bool lockIsOpen = false;

mixin Lock {
  Future<void> localAuth(
      BuildContext context, LocalAuthentication localAuth) async {
    if (await localAuth.isDeviceSupported()) {
      final didAuthenticate = await localAuth.authenticate(
          localizedReason: I18N.current.lock_bio_content,
          options: const AuthenticationOptions(biometricOnly: true),
          authMessages: [
            AndroidAuthMessages(
              signInTitle: I18N.current.lock_bio_title,
              biometricRequiredTitle: I18N.current.lock_bio_title,
              cancelButton: I18N.current.cancel,
            ),
            IOSAuthMessages(cancelButton: I18N.current.cancel),
          ]);
      if (didAuthenticate) {
        unlock();
        Navigator.pop(context);
      }
    } else {
      log("No bio supported");
    }
  }

  relock() {
    isAuthenticated = false;
  }

  Future tryShowScreenLock() async {
    log(" ============== LOCK ============== ");
    SettingsProvider sp = SettingsProvider.instance;
    var lock = sp.spInstance.getBool("lock");
    log("app locked ? $lock $isAuthenticated $lockIsOpen");
    if (lock == true && isAuthenticated == false && lockIsOpen == false) {
      log("Lock screen ?");
      await showScreenLock(sp.spInstance.getString("lock-code")!);
    } else {
      unlock();
    }
  }

  Future<bool> canUseBiometrics(LocalAuthentication localAuth) async {
    if (!await localAuth.isDeviceSupported()) {
      return false;
    }
    var availables = await localAuth.getAvailableBiometrics();
    return availables.contains(BiometricType.face) ||
        availables.contains(BiometricType.fingerprint) ||
        availables.contains(BiometricType.strong) ||
        availables.contains(BiometricType.iris);
  }

  showScreenLock(code) async {
    final localAuthentification = LocalAuthentication();
    if (navigatorKey.currentContext == null) {
      log("No nav key context");
      return;
    }
    lockIsOpen = true;
    var context = navigatorKey.currentContext!;

    await screenLock(
        context: context,
        correctString: code,
        canCancel: false,
        useBlur: true,
        config: ScreenLockConfig.defaultConfig.copyWith(backgroundColor: Colors.grey),
        title: Text(I18N.current.lock_title),
        customizedButtonChild: FutureBuilder(
            future: canUseBiometrics(localAuthentification),
            builder: (ctx, snap) {
              return snap.connectionState == ConnectionState.done && snap.data!
                  ? const Icon(Icons.fingerprint)
                  : Container();
            }),
        customizedButtonTap: () async =>
            await localAuth(context, localAuthentification),
        onError: (err) => log("Error $err"),
        onOpened: () async {
          if (await canUseBiometrics(localAuthentification)) {
            await localAuth(context, localAuthentification);
          }
        },
        onUnlocked: () {
          unlock();
          Navigator.of(context).pop();
        });
  }

  void unlock() {
    log("Unlock");
    isAuthenticated = true;
    lockIsOpen = false;
  }
}
