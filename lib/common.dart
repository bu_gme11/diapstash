import 'package:flutter_cache_manager/flutter_cache_manager.dart';

export 'package:diap_stash/constants.dart';
export 'package:flutter/material.dart';
export 'package:provider/provider.dart';
export 'package:flutter_gen/gen_l10n/app_localizations.dart';
export 'package:diap_stash/l10n/i18n.dart';
// ignore: unused_import, depend_on_referenced_packages
export 'package:intl/intl.dart' hide TextDirection;




CacheManager cacheManager = DefaultCacheManager();
