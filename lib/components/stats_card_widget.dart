import 'package:diap_stash/common.dart';

class StatsCard extends StatelessWidget {
  final String title;
  final String? stats;
  final String? subStats;
  final Widget? statsWidget;
  final Color bgColor;

  StatsCard(
      {super.key,
      required this.title,
      this.stats,
      this.statsWidget,
      this.subStats,
      this.bgColor = ColorDS}) {
    assert((stats != null || statsWidget != null),
        'Stats or stats widget are required');
  }

  Widget get contentWidget {
    if (statsWidget != null) return statsWidget!;
    return Text(stats!,
        textAlign: TextAlign.right,
        style: const TextStyle(
            fontSize: 24, color: Colors.white, fontWeight: FontWeight.bold));
  }

  @override
  Widget build(BuildContext context) {
    return Card(
        color: bgColor,
        child: LayoutBuilder(
            builder: (context, constraints) => SizedBox(
                  width: (constraints.maxWidth / 2) - 20,
                  child: Padding(
                    padding: const EdgeInsets.all(15),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(title,
                            textAlign: TextAlign.right,
                            style: const TextStyle(
                                color: Colors.white, fontSize: 18)),
                        const SizedBox(height: 10),
                        SizedBox(width: double.infinity, child: contentWidget),
                        if (subStats != null)
                          SizedBox(
                              width: double.infinity,
                              child: Text(subStats!,
                                  textAlign: TextAlign.right,
                                  style: const TextStyle(
                                      fontSize: 16,
                                      color: Colors.white,
                                      fontStyle: FontStyle.italic))),
                      ],
                    ),
                  ),
                )));
  }
}
