import 'package:diap_stash/common.dart';

enum StatsPeriod { all, custom }

var _initialTime = DateTime(2023, 1, 1);

class StatsPeriodWidget extends StatefulWidget {
  final StatsPeriod statsPeriod;
  final DateTimeRange? range;
  final Function(StatsPeriod, DateTimeRange?) onChange;

  const StatsPeriodWidget(
      {super.key,
      required this.statsPeriod,
      required this.range,
      required this.onChange});

  @override
  State<StatsPeriodWidget> createState() => _StatsPeriodWidgetState();
}

class _StatsPeriodWidgetState extends State<StatsPeriodWidget> {
  var showSelection = false;

  final _dateTimeFormat = DateFormat.yMd();

  bool get arrowEnabled => widget.statsPeriod != StatsPeriod.all;

  DateTimeRange? range;
  late StatsPeriod statsPeriod;

  @override
  void initState() {
    super.initState();
    if (widget.range == null) {
      range = DateTimeRange(
          start: DateTime.now()
              .subtract(const Duration(days: 7))
              .copyWith(hour: 00, minute: 00, second: 00),
          end: DateTime.now().copyWith(hour: 23, minute: 59, second: 59));
    } else {
      range = widget.range;
    }
    statsPeriod = widget.statsPeriod;
  }

  bool get hasNextPeriod {
    if (range == null) return false;
    var nextEnd = range!.end.add(range!.duration);
    return !nextEnd.isAfter(DateTime.now());
  }

  bool get hasPreviousPeriod {
    if (range == null) return false;
    var nextEnd = range!.start.subtract(range!.duration);
    return !nextEnd.isBefore(_initialTime);
  }

  previousPeriod() {
    if (range == null) return;
    setState(() {
      var duration = range!.end.copyWith(hour: 24, minute: 0, second: 0).difference(range!.start);
      range = DateTimeRange(
          start: range!.start
              .subtract(duration)
              .copyWith(hour: 00, minute: 00, second: 00),
          end: range!.end
              .subtract(duration)
              .copyWith(hour: 23, minute: 59, second: 59));
      widget.onChange(statsPeriod, range);
    });
  }

  nextPeriod() {
    setState(() {
      var duration = range!.end.copyWith(hour: 24, minute: 0, second: 0).difference(range!.start);
      range = DateTimeRange(
          start: range!.start
              .add(duration)
              .copyWith(hour: 00, minute: 00, second: 00),
          end: range!.end
              .add(duration)
              .copyWith(hour: 23, minute: 59, second: 59));
      widget.onChange(statsPeriod, range);
    });
  }

  double _buttonWidth(BuildContext context) {
    const maxWidth = 120.0;
    final buttonCount = StatsPeriod.values.length;
    final width = (MediaQuery.sizeOf(context).width - 100) / buttonCount;
    if (width < maxWidth) {
      return width;
    } else {
      return maxWidth;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(20, 0, 20, 5),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              IconButton(
                  onPressed:
                      arrowEnabled && hasPreviousPeriod ? previousPeriod : null,
                  icon: const Icon(Icons.arrow_back)),
              TextButton(
                onPressed: () {
                  setState(() {
                    showSelection = !showSelection;
                  });
                },
                child: Row(
                  children: [
                    if (statsPeriod == StatsPeriod.all)
                      Text(
                        I18N.current.stats_period_all_time,
                        style: const TextStyle(fontSize: 20),
                      )
                    else
                      Wrap(
                          spacing: 10,
                          alignment: WrapAlignment.center,
                          crossAxisAlignment: WrapCrossAlignment.center,
                          children: [
                            if (range != null)
                              Text(_dateTimeFormat.format(range!.start))
                            else
                              const Text("???"),
                            const Icon(Icons.arrow_forward),
                            if (range != null)
                              Text(_dateTimeFormat.format(range!.end))
                            else
                              const Text("???")
                          ]),
                    Icon(showSelection
                        ? Icons.arrow_drop_up
                        : Icons.arrow_drop_down)
                  ],
                ),
              ),
              IconButton(
                  onPressed: arrowEnabled && hasNextPeriod ? nextPeriod : null,
                  icon: const Icon(Icons.arrow_forward)),
            ],
          ),
          if (showSelection)
            Column(
              children: [
                ToggleButtons(
                  isSelected: [
                    for (var val in StatsPeriod.values) val == statsPeriod
                  ],
                  children: [
                    Container(
                      width: _buttonWidth(context),
                      alignment: Alignment.center,
                      child: Text(I18N.current.stats_period_all_time),
                    ),
                    Container(
                      width: _buttonWidth(context),
                      alignment: Alignment.center,
                      child: Text(I18N.current.stats_period_custom),
                    )
                  ],
                  onPressed: (index) {
                    var val = StatsPeriod.values[index];
                    setState(() {
                      if (val == StatsPeriod.custom && range == null) {
                        var currentDate = DateTime.now();
                        var start =
                            currentDate.subtract(const Duration(days: 7));
                        range = DateTimeRange(
                            start: start.copyWith(
                                hour: 00, minute: 00, second: 00),
                            end: currentDate.copyWith(
                                hour: 23, minute: 59, second: 59));
                      }
                      statsPeriod = val;
                      widget.onChange(statsPeriod, range);
                    });
                  },
                ),
                if (statsPeriod == StatsPeriod.custom)
                  buildCustomSelector(context)
              ],
            )
        ],
      ),
    );
  }

  Widget buildCustomSelector(BuildContext context) {
    var currentDate = DateTime.now();
    return Column(
      children: [
        TextButton(
            onPressed: () async {
              var nRange = await showDateRangePicker(
                  context: context,
                  firstDate: _initialTime,
                  lastDate: currentDate,
                  currentDate: currentDate,
                  initialDateRange: range);
              if (mounted && nRange != null) {
                setState(() {
                  range = DateTimeRange(
                      start: nRange.start
                          .copyWith(hour: 00, minute: 00, second: 00),
                      end: nRange.end
                          .copyWith(hour: 23, minute: 59, second: 59));
                  widget.onChange(statsPeriod, range);
                });
              }
            },
            child: Wrap(
              spacing: 20,
              direction: Axis.horizontal,
              alignment: WrapAlignment.center,
              children: [
                const Icon(null), //for center ;)
                if (range != null)
                  Text(_dateTimeFormat.format(range!.start))
                else
                  const Text("???"),
                const Icon(Icons.arrow_forward),
                if (range != null)
                  Text(_dateTimeFormat.format(range!.end))
                else
                  const Text("???"),
                const Icon(Icons.edit)
              ],
            ))
      ],
    );
  }
}
