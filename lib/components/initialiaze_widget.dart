import 'dart:async';
import 'dart:developer';

import 'package:diap_stash/constants.dart';
import 'package:diap_stash/lock.dart';
import 'package:diap_stash/main.dart';
import 'package:diap_stash/pages/main_page.dart';
import 'package:flutter/material.dart';

class InitializeWidget extends StatefulWidget {
  static const String routeName= "init";
  const InitializeWidget({super.key});

  @override
  State<StatefulWidget> createState() => _InitializeWidgetState();
}

class _InitializeWidgetState extends State<InitializeWidget>
    with Lock{



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(appBarTitle),
      ),
      primary: false,
      body: const Column(children: []),
    );
  }

  @override
  void initState() {
    super.initState();
    scheduleMicrotask(() async {
      log("Builder ?");
      await tryShowScreenLock();
      Navigator.of(navigatorKey.currentContext!).pushReplacementNamed(MainPage.routeName);
    });
  }
}
