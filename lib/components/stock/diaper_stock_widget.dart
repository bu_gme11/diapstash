import 'package:diap_stash/common.dart';
import 'package:diap_stash/components/stock/edit_stock_dialog_widget.dart';
import 'package:diap_stash/components/type_card_widget.dart';
import 'package:diap_stash/components/types/button_size_type_widget.dart';
import 'package:diap_stash/object/interfaces.dart';
import 'package:diap_stash/providers/diaper_provider.dart';
import 'package:diap_stash/providers/settings_provider.dart';
import 'package:diap_stash/util.dart';
import 'package:diap_stash/utils/stocks.dart';
import 'package:sentry_flutter/sentry_flutter.dart';

class DiaperStockWidget extends StatefulWidget {
  const DiaperStockWidget(
      {super.key,
      required this.stock,
      required this.readOnly,
      this.ssa,
      this.onSelect,
      this.imageSize = 100, this.nameSize, this.brandSize});

  final DiaperStock stock;
  final bool readOnly;
  final SharedStockAccess? ssa;
  final Function(String size)? onSelect;
  final double imageSize;
  final double? nameSize;
  final double? brandSize;


  @override
  State<DiaperStockWidget> createState() => _DiaperSockState();
}

class _DiaperSockState extends State<DiaperStockWidget> {
  @override
  Widget build(BuildContext context) {
    SettingsProvider sp = Provider.of<SettingsProvider>(context);
    DiaperProvider dp = Provider.of<DiaperProvider>(context);
    List<Widget> buttonsSize = [];
    var info = dp.getTypeInfo(widget.stock.type);
    var thresholdLowStock = info.thresholdLowStock ??
        sp.spInstance.getInt(SettingsProvider.keyThresholdLowStock) ??
        10;

    var sizes = widget.stock.count.keys.toList();
    sizes.sort(compareSize);
    for (var size in sizes) {
      var count = widget.stock.count[size]!;
      if (count <= 0 && count != DiaperStock.infinity) {
        count = 0;
        if (sp.keepZeroStockDiaper == 'never' ||
            (sp.keepZeroStockDiaper == 'favorite' &&
                !dp.isFavoriteType(widget.stock.type))) {
          continue;
        }
      }
      Color? bgcolor = Colors.purple;
      if (!widget.readOnly) {
        if (count == 0) {
          bgcolor = Colors.black38;
        } else if (count == DiaperStock.infinity) {
          bgcolor = Colors.purple;
        } else if (count >= thresholdLowStock) {
          bgcolor = const Color.fromRGBO(101, 154, 240, 1);
        } else {
          bgcolor = const Color.fromRGBO(212, 55, 55, 1);
        }
      } else if (count <= 0 && count != DiaperStock.infinity) {
        bgcolor = Colors.black38;
      }

      var sizeLabel = size;
      if (widget.stock.type.type == DTType.booster && size == "b") {
        sizeLabel = count > 1 ? I18N.current.boosters : I18N.current.booster;
        if (dp.currentChange != null) {
          sizeLabel = I18N.current.add;
        }
      }
      bool canUse = (!widget.readOnly || widget.onSelect != null) &&
          (count > 0 || count == DiaperStock.infinity);
      buttonsSize.add(GestureDetector(
          onTap: canUse
              ? () async {
                  if (widget.onSelect != null) {
                    widget.onSelect!(size);
                  } else {
                    await useStock(
                        context: context,
                        stock: widget.stock,
                        ssa: widget.ssa,
                        size: size,
                        onDone: () {
                          if (mounted) {
                            setState(() {});
                          }
                        });
                  }
                }
              : null,
          child: ButtonSizeTypeWidget(
              size: sizeLabel, count: count, color: bgcolor)));
    }

    return GestureDetector(
        onTap: !widget.readOnly
            ? () {
                openDialogEditStock();
              }
            : null,
        child: TypeCardWidget(
          type: widget.stock.type,
          buttonsSize: buttonsSize,
          onInfoPressed: openDialogEditStock,
          imageSize: widget.imageSize,
          nameSize: widget.nameSize,
          brandSize: widget.brandSize,
        ));
  }

  void openDialogEditStock() {
    Sentry.addBreadcrumb(Breadcrumb(category: "navigation", data: const {
      "openDialog": "dialogEditStock",
    }));
    showDialog(
        context: context,
        builder: (ctx) => EditStockDialogWidget(stock: widget.stock, readOnly: widget.readOnly));
  }
}
