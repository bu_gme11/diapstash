import 'package:diap_stash/common.dart';
import 'package:diap_stash/components/catalog/rating_type_widget.dart';
import 'package:diap_stash/components/dialogs/dialog_count.dart';
import 'package:diap_stash/components/types/button_size_type_widget.dart';
import 'package:diap_stash/object/interfaces.dart';
import 'package:diap_stash/providers/diaper_provider.dart';
import 'package:diap_stash/util.dart';
import 'package:sentry_flutter/sentry_flutter.dart';
import 'package:diap_stash/pages/main_page.dart';
import 'package:universal_io/io.dart';

class EditStockDialogWidget extends StatefulWidget {
  final DiaperStock stock;
  final bool readOnly;

  const EditStockDialogWidget({super.key, required this.stock, required this.readOnly});

  @override
  State<StatefulWidget> createState() {
    return _StateEditStockDialogWidget();
  }
}

class _StateEditStockDialogWidget extends State<EditStockDialogWidget> {

  bool showRating = false;

  Widget? buildPriceText(BuildContext context, String size) {
    if (!widget.stock.prices.containsKey(size)) return null;
    NumberFormat format = NumberFormat.simpleCurrency();
    try {
      format = NumberFormat.simpleCurrency(locale: Platform.localeName);
    } catch (_) {}
    var priceStr = format.format(widget.stock.prices[size]);
    return Text(
      "($priceStr)",
      style: const TextStyle(color: Colors.white, fontStyle: FontStyle.italic),
    );
  }

  @override
  Widget build(BuildContext context) {
    TextEditingController textFieldController = TextEditingController();
    var type = widget.stock.type;
    var sizes = <String>{};
    sizes.addAll(type.availableSizes);
    sizes.addAll(widget.stock.count.keys);
    var sizesList = sizes.toList();
    sizesList.sort(compareSize);

    DiaperProvider dp = Provider.of(context, listen: true);
    var info = dp.getTypeInfo(widget.stock.type);
    var isFavorite = dp.favoritesTypes.contains(widget.stock.type);

    return AlertDialog(
      title: Text(widget.stock.type.name),
      content: SingleChildScrollView(
        child: Padding(
            padding: const EdgeInsets.all(5),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                if (type.image != null) Image.network(type.image!, height: 200),
                Wrap(
                  direction: Axis.horizontal,
                  children: [
                    if (type.type == DTType.booster && sizesList.isEmpty)
                      GestureDetector(
                          onTap: !widget.readOnly ? () {
                            textFieldController.clear();
                            var count = widget.stock.count["b"];
                            var price = widget.stock.prices["b"];
                            dialogCount(context, "b", count, price);
                          } : null,
                          child: ButtonSizeTypeWidget(
                            size: I18N.current.booster,
                            color: Colors.purple,
                            count: widget.stock.count["b"],
                            bottom: buildPriceText(context, "b"),
                          ))
                    else
                      for (var size in sizesList)
                        GestureDetector(
                            onTap: !widget.readOnly ? () {
                              textFieldController.clear();
                              var count = widget.stock.count[size];
                              var price = widget.stock.prices[size];
                              dialogCount(context, size, count, price);
                            } : null,
                            child: ButtonSizeTypeWidget(
                              size: (size == "b" && type.type == DTType.booster)
                                  ? I18N.current.boosters
                                  : size,
                              color: Colors.purple,
                              count: widget.stock.count[size] ?? 0,
                              bottom: buildPriceText(context, size),
                            ))
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(0, 15, 0, 0),
                  child: ElevatedButton(
                      onPressed: () {
                        if (isFavorite) {
                          dp.removeFromFavorite(widget.stock.type, doNotifyListeners: true);
                        } else {
                          dp.addToFavorite(widget.stock.type, doNotifyListeners: true);
                        }
                      },
                      child: Row(
                        children: [
                          Icon(
                            isFavorite ? Icons.star : Icons.star_border,
                            color: Colors.orangeAccent,
                            size: 48,
                          ),
                          Text(isFavorite
                              ? I18N.current.catalog_remove_favorite
                              : I18N.current.catalog_add_favorite)
                        ],
                      )),
                ),
                if(type.official)
                TextButton(
                    onPressed: () {
                      setState(() {
                        showRating = !showRating;
                      });
                    },
                    child: Text.rich(TextSpan(children: [
                      TextSpan(
                          text: showRating
                              ? I18N.current.hide_rating
                              : I18N.current.show_rating),
                      WidgetSpan(
                          child: Icon(
                              showRating ? Icons.arrow_drop_up : Icons.arrow_drop_down, size: 16))
                    ]))),
                if(type.official && showRating)
                RatingTypeWidget(type: type),
                if (info.note != null)
                  Padding(
                      padding: const EdgeInsets.all(5),
                      child: SizedBox(
                        width: double.infinity,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Center(
                              child: Text(
                                I18N.current.type_note,
                                textAlign: TextAlign.center,
                                style: const TextStyle(
                                    fontSize: 16, fontWeight: FontWeight.bold),
                              ),
                            ),
                            Text(info.note!)
                          ],
                        ),
                      ))
              ],
            )),
      ),
      actions: [
        if (widget.stock.totalSort() == 0)
          TextButton(
              onPressed: () async {
                var nav = Navigator.of(context);
                await dp.removeStock(widget.stock, doNotifyListeners: true);
                nav.pop();
              },
              child: Text(I18N.current.remove)),
        TextButton(
            onPressed: () {
              if (mounted) {
                Navigator.pop(context);
              }
            },
            child: Text(I18N.current.close))
      ],
    );
  }

  Future<String?> dialogCount(
      BuildContext context, String size, int? count, double? price) {
    Sentry.addBreadcrumb(Breadcrumb(category: "navigation", data: const {
      "openDialog": "dialogCountEditStock",
    }));
    return showDialog<String>(
        context: context,
        builder: (var context) => DialogCount(
              title: I18N.current.edit_stock_number_of_diapers_in_stock,
              onUpdate: (count, price) async {
                var diaperProvider =
                    Provider.of<DiaperProvider>(context, listen: false);
                var navigatorState = Navigator.of(context);

                await diaperProvider.updateStock(
                    widget.stock.type, size, count, doNotifyListeners: true);
                await diaperProvider.setPrice(widget.stock.type, size, price, doNotifyListeners: true);

                if (widget.stock.total() == 0) {
                  navigatorState.popUntil(
                      (route) => route.settings.name == MainPage.routeName);
                } else {
                  navigatorState.pop();
                }
              },
              initalValue: count,
              okLabel: I18N.current.edit,
              askPrice: true,
              price: price,
              canTotalDiaper: false,
            ));
  }
}
