import 'package:diap_stash/common.dart';

class RadioSettingsWdiget extends StatelessWidget {
  final String group;
  final String value;

  //final Widget text;
  final Map<String, Widget> options;
  final ValueChanged<String?>? onChanged;

  const RadioSettingsWdiget(
      {super.key,
      required this.group,
      required this.value,
      required this.options,
      this.onChanged});

  @override
  Widget build(BuildContext context) {
    return Column(
        children: [
      for (var entry in options.entries)
        Container(
          //color: Colors.blue,
          constraints: const BoxConstraints(minWidth: 50, maxWidth: 200),
          child: RadioListTile(
              title: entry.value,
              value: entry.key,
              groupValue: value,
              onChanged: onChanged),
        ),
    ]);
  }
}
