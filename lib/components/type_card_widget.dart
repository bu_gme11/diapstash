import 'package:diap_stash/common.dart';
import 'package:diap_stash/components/type_image_widget.dart';
import 'package:diap_stash/object/interfaces.dart';
import 'package:diap_stash/providers/diaper_provider.dart';

class TypeCardWidget extends StatelessWidget {
  final DiaperType type;
  final List<Widget> buttonsSize;
  final void Function()? onInfoPressed;
  final double imageSize;
  final double? nameSize;
  final double? brandSize;

  const TypeCardWidget(
      {super.key,
      required this.type,
      required this.buttonsSize,
      this.onInfoPressed,
      this.imageSize = 100,
      this.nameSize,
      this.brandSize
      });

  @override
  Widget build(BuildContext context) {
    DiaperProvider dp = Provider.of<DiaperProvider>(context, listen: false);
    bool isFavorite = dp.favoritesTypes.contains(type);
    return Card(
        child: Padding(
            padding: const EdgeInsets.all(5),
            child: IntrinsicHeight(
              child: Row(
                mainAxisSize: MainAxisSize.max,
                children: [
                  TypeImageWidget(type: type, width: imageSize, height: imageSize),
                  Expanded(
                    child: Padding(
                      padding:
                          const EdgeInsetsDirectional.fromSTEB(10, 5, 5, 5),
                      child: Column(
                        mainAxisSize: MainAxisSize.max,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            type.name,
                            textAlign: TextAlign.left,
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: nameSize ?? 24),
                          ),
                          if (type.brand != null)
                            Text(
                              type.brand!.name,
                              style: TextStyle(
                                  fontSize: brandSize ?? 16, color: Colors.grey),
                            ),
                          Expanded(
                            child: Wrap(
                              direction: Axis.horizontal,
                              children: buttonsSize,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Column(
                    children: [
                      if (onInfoPressed != null)
                        TextButton(
                            onPressed: onInfoPressed,
                            child: const Icon(Icons.info_outline)),
                      if (isFavorite)
                        Container(
                            alignment: Alignment.topRight,
                            child: const Icon(Icons.star,
                                color: Colors.orange, size: 36))
                    ],
                  )
                ],
              ),
            )));
  }
}
