import 'package:diap_stash/common.dart';

class FilledIcon extends StatelessWidget {
  final Widget Function(Color) itemBuilder;
  final Color color;
  final double percent;


  const FilledIcon({super.key, required this.itemBuilder, required this.color, this.percent = 0.5});

  @override
  Widget build(BuildContext context) {
    return ShaderMask(
      blendMode: BlendMode.srcATop,
      shaderCallback: (Rect rect) {
        return LinearGradient(
          stops: [0, percent, percent],
          colors: [color, color, color.withOpacity(0)],
        ).createShader(rect);
      },
      child: itemBuilder(Colors.grey[350] ?? Colors.grey),
    );
  }
}