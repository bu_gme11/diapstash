import 'package:diap_stash/common.dart';
import 'package:diap_stash/object/interfaces.dart';

class ButtonSizeTypeWidget extends StatelessWidget {
  final String size;
  final Color color;
  final IconData? icon;
  final int? count;
  final bool small;
  final Widget? bottom;

  const ButtonSizeTypeWidget(
      {super.key,
      required this.size,
      required this.color,
      this.icon,
      this.count,
      this.small = false,
      this.bottom
      });

  @override
  Widget build(BuildContext context) {

    Widget? countWidget;

    if(count != null){

      var countString = "$count";

      if(count == DiaperStock.infinity){
        countString = "∞";
      }

      countWidget = Text(countString,
          style: const TextStyle(
              color: Colors.white, fontWeight: FontWeight.normal));
    }

    return Container(
      constraints: BoxConstraints(minWidth: (small ? 35 : 65)),
      padding: EdgeInsets.fromLTRB(5, small ? 2 : 2, 5, small ? 2 : 5),
      margin: EdgeInsets.all(small ? 1 : 5),
      decoration: BoxDecoration(
          border: Border.all(
              color: const Color.fromRGBO(255, 255, 255, 0.5), width: 1),
          borderRadius: BorderRadius.circular(small ? 5 : 10),
          color: color),
      child: Column(children: [
        Text(size,
            style: TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.bold,
                fontSize: small ? null : 24)),
        if (countWidget != null)
          countWidget,
        if (icon != null) Icon(icon, color: Colors.white),
        if(bottom != null)
          bottom!,
      ]),
    );
  }
}
