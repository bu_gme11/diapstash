import 'dart:developer';

import 'package:diap_stash/common.dart';
import 'package:diap_stash/components/half_filled_icon.dart';
import 'package:diap_stash/object/interfaces.dart';
import 'package:diap_stash/object/rating.dart';
import 'package:diap_stash/providers/diaper_provider.dart';
import 'package:diap_stash/providers/diaper_provider/diaper_provider_rating.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class RatingTypeWidget extends StatefulWidget {
  final DiaperType type;

  const RatingTypeWidget({super.key, required this.type});

  @override
  State<RatingTypeWidget> createState() => _RatingTypeWidgetState();
}

class _RatingTypeWidgetState extends State<RatingTypeWidget> {
  bool tryLoad = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var dp = Provider.of<DiaperProvider>(context, listen: true);

    var overallCommunityRating = dp.getCommunityRating(
        type: widget.type, criteria: RatingCriteria.overall);

    if (!tryLoad && overallCommunityRating == null) {
      tryLoad = true;
      dp.loadCommunityRatingForType(type: widget.type, doNotifyListeners: true).then((_) async {
          //nothing
      }, onError: (error) async {
        log("Error on load community type rating", error: error);
      });
    }
    return Column(
      children: [
        buildItem(
            diaperProvider: dp,
            criteria: RatingCriteria.overall,
            icon: FontAwesomeIcons.solidStar,
            iconHalf: FontAwesomeIcons.starHalfStroke,
            iconEmpty: FontAwesomeIcons.star,
            color: Colors.orange,
            size: 36,
            titleStyle: Theme.of(context).textTheme.titleLarge),
        Wrap(
          alignment: WrapAlignment.spaceAround,
          crossAxisAlignment: WrapCrossAlignment.start,
          spacing: 15,
          runSpacing: 15,
          children: [
            buildItem(
                diaperProvider: dp,
                criteria: RatingCriteria.comfort,
                icon: FontAwesomeIcons.feather,
                titleStyle: Theme.of(context).textTheme.titleMedium),
            buildItem(
                diaperProvider: dp,
                criteria: RatingCriteria.durability,
                icon: FontAwesomeIcons.hammer,
                titleStyle: Theme.of(context).textTheme.titleMedium),
            buildItem(
                diaperProvider: dp,
                criteria: RatingCriteria.noise,
                icon: FontAwesomeIcons.volumeHigh,
                titleStyle: Theme.of(context).textTheme.titleMedium),
            buildItem(
                diaperProvider: dp,
                criteria: RatingCriteria.thickness,
                icon: FontAwesomeIcons.upRightAndDownLeftFromCenter,
                titleStyle: Theme.of(context).textTheme.titleMedium),
          ],
        )
      ],
    );
  }

  Widget buildItem(
      {required DiaperProvider diaperProvider,
      required RatingCriteria criteria,
      required IconData icon,
      IconData? iconHalf,
      IconData? iconEmpty,
      Color color = ColorDS,
      double size = 24,
      required TextStyle? titleStyle}) {
    var grey = Colors.grey[300] ?? Colors.grey;

    var communityScore = diaperProvider.getCommunityRating(
        type: widget.type, criteria: criteria);
    var userScore =
        diaperProvider.getUserRating(type: widget.type, criteria: criteria);

    Widget iHalf;
    if (iconHalf != null) {
      iHalf = FaIcon(iconHalf, color: color);
    } else {
      iHalf = FilledIcon(
        itemBuilder: (color) => FaIcon(icon, color: color),
        color: color,
        percent: 0.5,
      );
    }

    return buildRatingBar(
        title: Text(criteria.title, style: titleStyle),
        ratingWidget: RatingWidget(
            full: FaIcon(icon, color: color),
            half: iHalf,
            empty: FaIcon(iconEmpty ?? icon, color: grey)),
        bottomLegend: criteria.bottomLegend,
        onRatingUpdate: (value) async {
          await diaperProvider.sendRating(
              type: widget.type,
              criteria: criteria,
              note: value,
              doNotifyListeners: true);
        },
        itemSize: size,
        value: userScore?.note ?? 0,
        communityValue: communityScore?.note);
  }

  Widget buildRatingBar(
      {required Widget title,
      required RatingWidget ratingWidget,
      required ValueChanged<double>? onRatingUpdate,
      required double value,
      double? communityValue,
      RatingBottomLegend? bottomLegend,
      required double itemSize}) {
    var bar = RatingBar(
      ratingWidget: ratingWidget,
      onRatingUpdate: onRatingUpdate ?? (_) {},
      itemSize: itemSize,
      glow: false,
      itemCount: 5,
      direction: Axis.horizontal,
      updateOnDrag: false,
      initialRating: value,
      itemPadding: const EdgeInsets.only(left: 2, right: 2),
      minRating: 1,
      maxRating: 5,
      ignoreGestures: false,
      allowHalfRating: false,
    );

    return IntrinsicWidth(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Center(child: title),
          Center(child: bar),
          if (bottomLegend != null)
            Wrap(
              alignment: WrapAlignment.spaceBetween,
              children: [
                Text(bottomLegend.left,
                    style: Theme.of(context)
                        .textTheme
                        .labelMedium
                        ?.copyWith(fontStyle: FontStyle.italic)),
                Text(bottomLegend.right,
                    style: Theme.of(context)
                        .textTheme
                        .labelMedium
                        ?.copyWith(fontStyle: FontStyle.italic))
              ],
            ),
          if (communityValue != null)
            Center(
                child: Text(
              I18N.current.rating_community,
              style: Theme.of(context)
                  .textTheme
                  .bodyMedium
                  ?.copyWith(fontStyle: FontStyle.italic),
            )),
          if (communityValue != null)
            Center(
              child: RatingBarIndicator(
                itemBuilder: (context, index) {
                  if (communityValue == index + 0.5) {
                    return ratingWidget.half;
                  } else if (communityValue > index) {
                    return ratingWidget.full;
                  }
                  return ratingWidget.empty;
                },
                itemSize: itemSize / 2,
                itemCount: 5,
                direction: Axis.horizontal,
                itemPadding: const EdgeInsets.only(left: 2, right: 2),
                rating: communityValue,
                unratedColor: Colors.grey[300] ?? Colors.grey,
              ),
            )
        ],
      ),
    );
  }
}
