import 'package:diap_stash/common.dart';
import 'package:diap_stash/components/dialogs/dialog_count.dart';
import 'package:diap_stash/components/type_image_widget.dart';
import 'package:diap_stash/components/types/button_size_type_widget.dart';
import 'package:diap_stash/object/interfaces.dart';
import 'package:diap_stash/providers/diaper_provider.dart';
import 'package:diap_stash/providers/settings_provider.dart';
import 'package:diap_stash/util.dart';
import 'package:sentry_flutter/sentry_flutter.dart';
import 'package:universal_io/io.dart';

class DiaperTypeDialogWidget extends StatefulWidget {
  final DiaperType type;
  final Function(DiaperType type, String size)? selectDiaper;

  const DiaperTypeDialogWidget(
      {super.key, required this.type, this.selectDiaper});

  @override
  State<DiaperTypeDialogWidget> createState() => _DiaperTypeDialogWidgetState();
}

class _DiaperTypeDialogWidgetState extends State<DiaperTypeDialogWidget> {
  final TextEditingController _thresholdCtrl = TextEditingController();
  final TextEditingController _noteCtrl = TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  Widget? buildPriceText(
      BuildContext context, DiaperStock? stock, String size) {
    if (stock == null) return null;
    if (!stock.prices.containsKey(size)) return null;
    NumberFormat format = NumberFormat.simpleCurrency();
    try {
      format = NumberFormat.simpleCurrency(locale: Platform.localeName);
    } catch (_) {}
    var priceStr = format.format(stock.prices[size]);
    return Text(
      "($priceStr)",
      style: const TextStyle(color: Colors.white, fontStyle: FontStyle.italic),
    );
  }

  @override
  Widget build(BuildContext context) {
    TextEditingController textFieldController = TextEditingController();
    var sizes = widget.type.availableSizes.toList();
    sizes.sort(compareSize);

    DiaperProvider dp = Provider.of(context, listen: true);
    SettingsProvider sp = Provider.of(context, listen: false);

    var info = dp.getTypeInfo(widget.type);

    if (_thresholdCtrl.text.isEmpty) {
      var t = info.thresholdLowStock ?? sp.thresholdLowStock;
      _thresholdCtrl.text = "$t";
    }

    if (_noteCtrl.text.isEmpty && info.note != null) {
      _noteCtrl.text = info.note!;
    }

    return AlertDialog(
        actions: [
          TextButton(
            onPressed: () async {
              var info = dp.getTypeInfo(widget.type);
              var thresholdLowStock = int.tryParse(_thresholdCtrl.text);
              info.thresholdLowStock = thresholdLowStock;
              var note = _noteCtrl.text;
              info.note = note.isNotEmpty ? note : null;
              if (mounted) {
                Navigator.of(context).pop('Close');
              }
              await dp.saveTypeInfo(info);
            },
            child: Text(I18N.current.close),
          )
        ],
        scrollable: true,
        title: Text(widget.type.name),
        content: Padding(
            padding: const EdgeInsets.all(5),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                if (widget.type.image != null)
                  TypeImageWidget(
                      type: widget.type,
                      width: 150,
                      height: 150,
                      border: false),
                //Text(widget.type.name, style: const TextStyle(fontSize: 24)),
                Wrap(
                  direction: Axis.horizontal,
                  children: [
                    if (widget.type.type == DTType.booster && sizes.isEmpty)
                      GestureDetector(
                          child: ButtonSizeTypeWidget(
                            size: I18N.current.booster,
                            icon: Icons.add,
                            color: Colors.purple,
                          ),
                          onTap: () {
                            textFieldController.clear();
                            if (widget.selectDiaper != null) {
                              widget.selectDiaper!(widget.type, "b");
                              Navigator.of(context).pop();
                            } else {
                              dialogCount(context, textFieldController, "b");
                            }
                          })
                    else
                      for (var size in sizes)
                        GestureDetector(
                            child: ButtonSizeTypeWidget(
                                size: widget.type.type == DTType.booster &&
                                        size == "b"
                                    ? I18N.current.boosters
                                    : size,
                                icon: Icons.add,
                                color: Colors.purple),
                            onTap: () {
                              textFieldController.clear();
                              if (widget.selectDiaper != null) {
                                widget.selectDiaper!(widget.type, size);
                              } else {
                                dialogCount(context, textFieldController, size);
                              }
                            }),
                    //if (widget.type.type == DTType.diaper)
                    GestureDetector(
                        child: const ButtonSizeTypeWidget(
                            size: "?", icon: Icons.add, color: Colors.purple),
                        onTap: () {
                          textFieldController.clear();
                          dialogName(context, textFieldController, (sizeName) {
                            textFieldController.clear();
                            if (widget.selectDiaper != null) {
                              widget.selectDiaper!(widget.type, sizeName);
                            } else {
                              dialogCount(
                                  context, textFieldController, sizeName);
                            }
                          });
                        })
                  ],
                ),
                if (info.note != null && info.note!.isNotEmpty)
                  Column(
                    children: [
                      Text(I18N.current.type_note,
                          style: const TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 14)),
                      SizedBox(
                          width: double.infinity,
                          child: Text(info.note!, textAlign: TextAlign.left)),
                    ],
                  ),
              ],
            )));
  }

  Future<String?> dialogCount(BuildContext context,
      TextEditingController textFieldController, String size) {
    Sentry.addBreadcrumb(Breadcrumb(category: "navigation", data: const {
      "openDialog": "dialogCount",
    }));
    return showDialog<String>(
        context: context,
        builder: (var builder) => DialogCount(
              title: I18N.current.catalog_number_of_diapers_add,
              onUpdate: (count, price) async {
                var navigatorState = Navigator.of(context);
                var diaperProvider =
                    Provider.of<DiaperProvider>(context, listen: false);
                await diaperProvider.addStock(widget.type, size, count,
                    doNotifyListeners: true);
                if (price != null) {
                  await diaperProvider.setPrice(widget.type, size, price,
                      doNotifyListeners: true);
                }
                navigatorState.popUntil((r) =>
                    (r.settings.name != null && r.settings.name == "MainPage"));
              },
              predefinedValues: SettingsProvider.instance.predefinedBagSize,
              okLabel: I18N.current.add,
              askPrice: true,
            ));
  }

  Future<String?> dialogName(
      BuildContext context,
      TextEditingController textFieldController,
      void Function(String) callback) {
    Sentry.addBreadcrumb(Breadcrumb(category: "navigation", data: const {
      "openDialog": "dialogName",
    }));
    return showDialog<String>(
        context: context,
        builder: (var builder) => AlertDialog(
              title: Text(I18N.current.catalog_dialog_indicate_size_name),
              actions: [
                TextButton(
                    onPressed: () =>
                        Navigator.of(context).pop(),
                    child: Text(I18N.current.cancel)),
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                      callback(textFieldController.value.text);
                    },
                    child: Text(I18N.current.add))
              ],
              content: TextField(
                onChanged: (value) {},
                controller: textFieldController,
                keyboardType: TextInputType.name,
                decoration: InputDecoration(
                    hintText:
                        I18N.current.catalog_dialog_hint_custom_size_name),
              ),
            ));
  }
}
