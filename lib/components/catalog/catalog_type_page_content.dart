import 'package:diap_stash/common.dart';
import 'package:diap_stash/components/catalog/rating_type_widget.dart';
import 'package:diap_stash/components/dialogs/dialog_ask_text.dart';
import 'package:diap_stash/components/dialogs/dialog_count.dart';
import 'package:diap_stash/components/type_image_widget.dart';
import 'package:diap_stash/components/types/button_size_type_widget.dart';
import 'package:diap_stash/object/interfaces.dart';
import 'package:diap_stash/pages/catalog/customs/custom_type_page.dart';
import 'package:diap_stash/providers/diaper_provider.dart';
import 'package:diap_stash/providers/settings_provider.dart';
import 'package:diap_stash/util.dart';
import 'package:flutter/services.dart';
import 'package:sentry_flutter/sentry_flutter.dart';

class CatalogTypePageContent extends StatefulWidget {

  final DiaperType type;
  final bool isPage;

  const CatalogTypePageContent({super.key, required this.type, required this.isPage});

  @override
  State<CatalogTypePageContent> createState() => _CatalogTypePageContentState();
}

class _CatalogTypePageContentState extends State<CatalogTypePageContent> {
  TextEditingController textFieldController = TextEditingController();
  final TextEditingController _thresholdCtrl = TextEditingController();
  final TextEditingController _noteCtrl = TextEditingController();
  bool customThresholdLowStock = false;

  @override
  Widget build(BuildContext context) {
    DiaperProvider dp = Provider.of<DiaperProvider>(context, listen: true);
    SettingsProvider sp = Provider.of<SettingsProvider>(context, listen: true);
    var size = MediaQuery.sizeOf(context);
    var imageSize = size.width / 3;
    if(!widget.isPage){
      imageSize = 150;
    }

    return Column(
      children: [
        if(!widget.isPage)
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Center(child: Text(widget.type.name, style: Theme.of(context).textTheme.headlineLarge)),
          ),
        Center(
            child: TypeImageWidget(
                type: widget.type, width: imageSize, height: imageSize)),
        if (widget.type.brand?.image != null)
          Container(
              constraints: BoxConstraints(
                  maxWidth: size.width / 0.75, maxHeight: 100),
              child: Image.network(widget.type.brand!.image!,
                  fit: BoxFit.contain))
        else if (widget.type.brand != null)
          Text(
            widget.type.brand!.name,
            style: const TextStyle(fontSize: 18),
          ),
        buildSizesButtons(),
        if(!widget.isPage)
        buildFavoriteButton(dp),
        if(widget.type.official)
        buildRatings(context: context),
        buildUserInfo(dp: dp, sp: sp),
        if(!widget.isPage && !widget.type.official)
          Wrap(
            direction: Axis.horizontal,
            children: [
              ElevatedButton(onPressed: () async {
                Navigator.of(context).pushNamed(CustomTypePage.routeArgsName,
                    arguments: CustomTypePageArgs(type: widget.type));
              }, child: Text(I18N.current.edit))
            ],
          )
      ],
    );
  }

  AppBar buildAppBar(
      {required BuildContext context, required DiaperProvider dp}) {
    var actions = <Widget>[];

    if (!widget.type.official) {
      actions.add(IconButton(
          onPressed: () {
            Navigator.of(context).pushNamed(CustomTypePage.routeArgsName,
                arguments: CustomTypePageArgs(type: widget.type));
          },
          icon: const Icon(Icons.edit),
          tooltip: I18N.current.custom_type_edit));
    }

    var isFavorite = dp.isFavoriteType(widget.type);
    actions.add(IconButton(
      onPressed: () async {
        if (isFavorite) {
          await dp.removeFromFavorite(widget.type, doNotifyListeners: true);
        } else {
          await dp.addToFavorite(widget.type, doNotifyListeners: true);
        }
      },
      icon:
          isFavorite ? const Icon(Icons.star) : const Icon(Icons.star_outline),
      tooltip: I18N.current.favorite,
    ));

    return AppBar(
      title: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(widget.type.name),
          if (widget.type.brand != null)
            Text(widget.type.brand!.name,
                style: Theme.of(context).textTheme.bodyMedium)
        ],
      ),
      actions: actions,
    );
  }

  Widget buildSizesButtons() {
    var sizes = widget.type.availableSizes.toList();
    sizes.sort(compareSize);

    return Wrap(children: [
      if (widget.type.type == DTType.booster && sizes.isEmpty)
        GestureDetector(
            child: ButtonSizeTypeWidget(
              size: I18N.current.booster,
              icon: Icons.add,
              color: Colors.purple,
            ),
            onTap: () {
              textFieldController.clear();
              dialogCount(context, textFieldController, "b");
            })
      else
        for (var size in sizes)
          GestureDetector(
              child: ButtonSizeTypeWidget(
                  size: widget.type.type == DTType.booster && size == "b"
                      ? I18N.current.boosters
                      : size,
                  icon: Icons.add,
                  color: Colors.purple),
              onTap: () {
                textFieldController.clear();
                dialogCount(context, textFieldController, size);
              }),
      //if (widget.type.type == DTType.diaper)
      GestureDetector(
          child: const ButtonSizeTypeWidget(
              size: "?", icon: Icons.add, color: Colors.purple),
          onTap: () {
            textFieldController.clear();
            dialogName(context, textFieldController, (sizeName) {
              dialogCount(context, textFieldController, sizeName);
            });
          })
    ]);
  }

  Widget buildFavoriteButton(DiaperProvider dp) {
    var isFavorite = dp.isFavoriteType(widget.type);

    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: ElevatedButton(
          onPressed: () {
            if (isFavorite) {
              dp.removeFromFavorite(widget.type, doNotifyListeners: true);
            } else {
              dp.addToFavorite(widget.type, doNotifyListeners: true);
            }
          },
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              Icon(
                isFavorite ? Icons.star : Icons.star_border,
                color: Colors.orangeAccent,
                size: 48,
              ),
              Text(isFavorite
                  ? I18N.current.catalog_remove_favorite
                  : I18N.current.catalog_add_favorite)
            ],
          )),
    );
  }

  Widget buildUserInfo(
      {required DiaperProvider dp,
      required SettingsProvider sp,
      bool infoEditable = true}) {
    var info = dp.getTypeInfo(widget.type);

    if (_thresholdCtrl.text.isEmpty) {
      var t = info.thresholdLowStock ?? sp.thresholdLowStock;
      _thresholdCtrl.text = "$t";
      if (info.thresholdLowStock != null) {
        customThresholdLowStock = true;
      }
    }

    if (_noteCtrl.text.isEmpty && info.note != null) {
      _noteCtrl.text = info.note!;
    }

    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        children: [
          Text(
            I18N.current.catalog_type_page_informations,
            style: Theme.of(context).textTheme.headlineMedium,
          ),
          Focus(
            onFocusChange: (focus) async {
              if(!focus){
                var note = _noteCtrl.text;
                if(info.note != note) {
                  info.note = note;
                  await dp.saveTypeInfo(info);
                }
              }
            },
            child: TextFormField(
              controller: _noteCtrl,
              decoration: InputDecoration(label: Text(I18N.current.type_note)),
              maxLines: 3,
              minLines: 3,
              onEditingComplete: () async {
                var note = _noteCtrl.text;
                if(info.note != note) {
                  info.note = note;
                  await dp.saveTypeInfo(info);
                }
              }
            ),
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              SwitchListTile(
                title: Text(
                    I18N.current.catalog_type_page_custom_threshhold_stock),
                value: customThresholdLowStock,
                onChanged: (value) async {
                  setState(() {
                    customThresholdLowStock = value;
                  });
                  if (value == false) {
                    info.thresholdLowStock = null;
                    await dp.saveTypeInfo(info);
                  }
                },
              ),
              if (customThresholdLowStock)
                TextFormField(
                  controller: _thresholdCtrl,
                  keyboardType: TextInputType.number,
                  inputFormatters: [FilteringTextInputFormatter.digitsOnly],
                  decoration: InputDecoration(
                      label: Text(I18N.current.threshold_low_stock)),
                  onChanged: (_) async {
                    var val = int.tryParse(_thresholdCtrl.text);
                    if (val != null) {
                      info.thresholdLowStock = val;
                      await dp.saveTypeInfo(info);
                    }
                  },
                ),
            ],
          )
        ],
      ),
    );
  }

  Widget buildRatings({required BuildContext context}){
    return Column(
      children: [
        Text(I18N.current.rating, style: Theme.of(context).textTheme.headlineMedium),
        RatingTypeWidget(type: widget.type),
      ],
    );
  }

  Future<String?> dialogCount(BuildContext context,
      TextEditingController textFieldController, String size) {
    Sentry.addBreadcrumb(Breadcrumb(category: "navigation", data: const {
      "openDialog": "dialogCount",
    }));
    return showDialog<String>(
        context: context,
        builder: (var context) => DialogCount(
              title: I18N.current.catalog_number_of_diapers_add,
              onUpdate: (count, price) async {
                var navigatorState = Navigator.of(context);
                var diaperProvider =
                    Provider.of<DiaperProvider>(context, listen: false);
                await diaperProvider.addStock(widget.type, size, count,
                    doNotifyListeners: true);
                if (price != null) {
                  await diaperProvider.setPrice(widget.type, size, price,
                      doNotifyListeners: true);
                }
                navigatorState.pop();
              },
              predefinedValues: SettingsProvider.instance.predefinedBagSize,
              okLabel: I18N.current.add,
              askPrice: true,
            ));
  }

  dialogName(
      BuildContext context,
      TextEditingController textFieldController,
      void Function(String) callback) {
    Sentry.addBreadcrumb(Breadcrumb(category: "navigation", data: const {
      "openDialog": "dialogName",
    }));
    return DialogAskText(
      title: Text(I18N.current.catalog_dialog_indicate_size_name),
      cancelButton: Text(I18N.current.cancel),
      okButton: Text(I18N.current.add),
      cancelCallback: (context, _) {
        Navigator.of(context).pop();
      },
      okCallback: (context, ctrl) {
        Navigator.of(context).pop();
        callback(ctrl.value.text);
      },
    ).showAskTextDialog(context);
  }
}
