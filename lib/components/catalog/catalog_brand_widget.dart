import 'package:cached_network_image/cached_network_image.dart';
import 'package:diap_stash/common.dart';
import 'package:diap_stash/components/catalog/catalog_type_widget.dart';
import 'package:diap_stash/components/tutorials/submit_official_type.dart';
import 'package:diap_stash/object/interfaces.dart';
import 'package:diap_stash/providers/diaper_provider.dart';

class CatalogBrandWidget extends StatefulWidget {
  final DiaperBrand brand;
  final List<DiaperType> types;
  final bool official;
  final bool openSizeDialog;
  final Function(DiaperType type, String size)? selectDiaperWithSize;
  final Function(DiaperType type, )? selectDiaper;
  final bool all;
  final double typeSize;

  const CatalogBrandWidget({super.key,
    required this.brand,
    required this.official,
    required this.types,
    this.all = false,
    this.selectDiaperWithSize,
    this.selectDiaper,
    required this.openSizeDialog,
    this.typeSize = 100
  });

  @override
  State<StatefulWidget> createState() => _CatalogBrandWidgetState();
}

class _CatalogBrandWidgetState extends State<CatalogBrandWidget> {
  @override
  Widget build(BuildContext context) {
    var dp = Provider.of<DiaperProvider>(context, listen: false);

    List<DiaperType> types = widget.types;

    if (widget.brand.code == "OTHER") {
      types = types
          .where((element) =>
      element.brand == null &&
          (element.official == widget.official || widget.all))
          .toList();
    } else if (widget.brand.code == "_FAVORITE") {
      types = types.where((type) => dp.favoritesTypes.contains(type)).toList();
    } else {
      types = types
          .where((element) =>
      element.brand == widget.brand &&
          (element.official == widget.official || widget.all))
          .toList();
    }
    types.sort((t1, t2) => t1.name.compareTo(t2.name));

    List<Widget> buttonSize = [];
    for (var i = 0; i < types.length; i++) {
      GlobalKey? key;
      if ((!widget.official || widget.all) &&
          i == 0 &&
          keySubmitOfficialDiaperFirstCustomDiaper.currentWidget == null) {
        key = keySubmitOfficialDiaperFirstCustomDiaper;
      }
      buttonSize.add(CatalogTypeWidget(
          key: key,
          type: types[i],
          openSizeDialog: widget.openSizeDialog,
          selectDiaper: widget.selectDiaper,
          canEditCustomDiaper: true,
          selectDiaperWithSize: widget.selectDiaperWithSize,
          typeSize: widget.typeSize,
      ));
    }

    return Column(
      children: [
        if (widget.brand.image != null)
          ClipRRect(
              borderRadius: BorderRadius.circular(10),
              child: CachedNetworkImage(
                  imageUrl: widget.brand.image!,
                  height: 100,
                  width: MediaQuery
                      .of(context)
                      .size
                      .width * 0.5,
                  fit: BoxFit.contain)),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            if (widget.brand.code == "_FAVORITE")
              const Icon(Icons.star, color: Colors.orange, size: 48),
            Text(
              widget.brand.name,
              style: const TextStyle(fontSize: 24),
            ),
          ],
        ),
        Wrap(direction: Axis.horizontal, children: buttonSize)
      ],
    );
  }
}
