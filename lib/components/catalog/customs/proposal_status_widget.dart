import 'package:diap_stash/common.dart';
import 'package:diap_stash/components/type_card_widget.dart';
import 'package:diap_stash/components/types/button_size_type_widget.dart';
import 'package:diap_stash/object/proposals.dart';
import 'package:diap_stash/providers/diaper_provider.dart';

class ProposalStatusWidget extends StatefulWidget {
  final String proposalId;

  const ProposalStatusWidget({super.key, required this.proposalId});

  @override
  State<ProposalStatusWidget> createState() => _ProposalStatusWidgetState();
}

class _ProposalStatusWidgetState extends State<ProposalStatusWidget> {

  @override
  Widget build(BuildContext context) {
    DiaperProvider dp = Provider.of<DiaperProvider>(context, listen: false);
    return FutureBuilder(
        future: dp.getProposal(widget.proposalId),
        builder: ((context, snapshot) {
          if (snapshot.data == null) {
            return Container();
          }

          Widget chip = Chip(label: Text(I18N.current.proposal_pending));
          if (snapshot.data!.state == ProposalState.APPROVED) {
            chip = Chip(label: Text(I18N.current.proposal_approved), backgroundColor: Colors.greenAccent);
          } else if (snapshot.data!.state == ProposalState.REJECTED) {
            chip = Chip(label: Text(I18N.current.proposal_rejected), backgroundColor: Colors.redAccent);
          }

          var canMerge = snapshot.data!.state == ProposalState.APPROVED && snapshot.data!.original_type != null && snapshot.data!.result_type != null;
          var newType = snapshot.data!.result_type;
          return Column(children: [
            Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
              Text(I18N.current.proposal_status),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: chip,
              )
            ]),
            if (snapshot.data!.reason != null)
              Row(children: [
                Padding(
                  padding: const EdgeInsets.fromLTRB(5, 0, 10, 0),
                  child: Text(I18N.current.reason),
                ),
                Flexible(child: Text(snapshot.data!.reason!))
              ]),
            if(canMerge)
              ElevatedButton(onPressed: () async {
                await dp.transfertTypeToNewType(snapshot.data!.original_type!, newType!, doNotifyListeners: false);
                await (dp.database.delete(dp.database.diaperTypeProposalTable)..where((tbl) => tbl.proposal_id.equals(widget.proposalId))).go();
                await dp.removeType(snapshot.data!.original_type!, doNotifyListeners: false);
                dp.doNotifyListeners();
                setState(() {
                  Navigator.pop(context);
                });
              }, child: Text(I18N.current.proposal_merge_type)),
            if(canMerge)
              TypeCardWidget(type: newType!, buttonsSize: newType.availableSizes.map((size) => ButtonSizeTypeWidget(size: size, color: Colors.purple)).toList())
          ]);
        }));
  }
}
