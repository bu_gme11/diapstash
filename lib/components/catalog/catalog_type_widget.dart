import 'package:diap_stash/components/catalog/diaper_type_dialog_widget.dart';
import 'package:diap_stash/components/type_image_widget.dart';
import 'package:diap_stash/object/interfaces.dart';
import 'package:diap_stash/common.dart';
import 'package:diap_stash/pages/catalog/catalog_type_page.dart';
import 'package:diap_stash/pages/catalog/customs/custom_type_page.dart';
import 'package:sentry_flutter/sentry_flutter.dart';

class CatalogTypeWidget extends StatefulWidget {
  final DiaperType type;
  final bool openSizeDialog;
  final bool canEditCustomDiaper;
  final Function(DiaperType type, String size)? selectDiaperWithSize;
  final Function(DiaperType type)? selectDiaper;
  final double typeSize;
  final bool clickable;

  const CatalogTypeWidget(
      {super.key,
      required this.type,
      required this.openSizeDialog,
      required this.canEditCustomDiaper,
      this.selectDiaperWithSize,
      this.selectDiaper,
      this.typeSize = 100,
      this.clickable = true});

  @override
  State<StatefulWidget> createState() => CatalogTypeWidgetState();
}

class CatalogTypeWidgetState extends State<CatalogTypeWidget> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: widget.clickable ? () {
          if (widget.openSizeDialog || widget.selectDiaperWithSize != null) {
            showDialogType(context);
          } else if (widget.selectDiaper != null) {
            widget.selectDiaper!(widget.type);
          }else{
            Navigator.of(context).pushNamed(CatalogTypePage.routeName, arguments: CatalogTypePageArgs(type: widget.type));
          }
        } : null,
        onLongPress: widget.clickable && !widget.type.official && widget.canEditCustomDiaper ?
            () {
                Navigator.of(context).pushNamed(CustomTypePage.routeArgsName,
                    arguments: CustomTypePageArgs(type: widget.type));
              } : null,
        child: Container(
            padding: const EdgeInsets.all(10),
            width: widget.typeSize + 5,
            child: Column(
              children: [
                TypeImageWidget(type: widget.type, width: widget.typeSize, height: widget.typeSize),
                Text(
                  widget.type.name,
                  textAlign: TextAlign.center,
                ),
                if (widget.type.type == DTType.booster)
                  Text("(${I18N.current.booster.toLowerCase()})",
                      style: const TextStyle(
                          color: Colors.grey,
                          fontSize: 12,
                          fontStyle: FontStyle.italic)),
              ],
            )));
  }

  Future<String?> showDialogType(BuildContext context) {
    Sentry.addBreadcrumb(Breadcrumb(category: "navigation", data: const {
      "openDialog": "showDialogCatalogType",
    }));
    return showDialog<String>(
        context: context,
        builder: (BuildContext context) => DiaperTypeDialogWidget(
            type: widget.type,
            selectDiaper: widget.selectDiaperWithSize));
  }
}
