import 'package:diap_stash/common.dart';
import 'package:diap_stash/components/catalog/catalog_type_widget.dart';
import 'package:diap_stash/components/dialogs/catalog_list_dialog.dart';
import 'package:diap_stash/object/interfaces.dart';
import 'package:diap_stash/providers/diaper_provider.dart';

class SelectTypeWidget extends StatefulWidget {
  final DiaperType? type;
  final Function(DiaperType?) onChange;

  const SelectTypeWidget({required this.type, required this.onChange, super.key});

  @override
  State<SelectTypeWidget> createState() => _SelectTypeWidgetState();
}

class _SelectTypeWidgetState extends State<SelectTypeWidget> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        if (widget.type != null)
          CatalogTypeWidget(
              type: widget.type!,
              openSizeDialog: false,
              canEditCustomDiaper: false,
              clickable: false,
              typeSize: 150),
        ElevatedButton(
            onPressed: () {
              showDialogSelectType();
            },
            child: Text(I18N.current.select_type))
      ],
    );
  }

  showDialogSelectType() {
    DiaperProvider dp = Provider.of(context, listen: false);
    showDialog(
      context: context,
      builder: (context) => AlertDialog(
        title: Text(I18N.current.select_type),
        actions: [
          TextButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: Text(I18N.current.cancel))
        ],
        content: SizedBox(
          width: MediaQuery.sizeOf(context).width * 0.9,
          child: buildCatalog(dp, selectDiaper: (type) {
            Navigator.of(context).pop();
            widget.onChange(type);
          }, typeSize: 80, openSizeDialog: false),
        ),
      ),
    );
  }
}
