import 'package:diap_stash/common.dart';
import 'package:diap_stash/object/interfaces.dart';
import 'package:diap_stash/providers/settings_provider.dart';

Widget getCatalogFilter() {
  return StatefulBuilder(
    builder: (context, setState) {
      SettingsProvider sp = Provider.of<SettingsProvider>(context);
      var targets = sp.filterDiaperTarget.toList();
      var styles = sp.filterDiaperStyle.toList();
      var types = sp.filterDiaperType.toList();
      var discontinueds = sp.filterDiscontinued.toList();
      var stateArray = discontinuedArray;
      return Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Text(I18N.current.type,
              style: const TextStyle(fontSize: 20)),
          ToggleButtons(
              isSelected: [
                for (var s in DTType.values) types.contains(s)
              ],
              children: [
                for (var s in DTType.values)
                  Padding(
                    padding: const EdgeInsets.all(8),
                    child: Text(s.localizedNamed()),
                  )
              ],
              onPressed: (index) {
                setState(() {
                  var type = DTType.values[index];
                  if (types.contains(type)) {
                    types.remove(type);
                  } else {
                    types.add(type);
                  }
                  sp.setFilterDiaperType(types);
                });
              }),
          Text(I18N.current.diaper_target,
              style: const TextStyle(fontSize: 20)),
          ToggleButtons(
              isSelected: [
                for (var s in DiaperTarget.values) targets.contains(s)
              ],
              children: [
                for (var s in DiaperTarget.values)
                  Padding(
                    padding: const EdgeInsets.all(8),
                    child: Text(s.localizedNamed()),
                  )
              ],
              onPressed: (index) {
                setState(() {
                  var target = DiaperTarget.values[index];
                  if (targets.contains(target)) {
                    targets.remove(target);
                  } else {
                    targets.add(target);
                  }
                  sp.setFilterDiaperTarget(targets);
                });
              }),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(
              I18N.current.diaper_style,
              style: const TextStyle(fontSize: 20),
            ),
          ),
          ToggleButtons(
              isSelected: [
                for (var s in DiaperStyle.values) styles.contains(s)
              ],
              children: [
                for (var s in DiaperStyle.values)
                  Padding(
                    padding: const EdgeInsets.all(8),
                    child: Text(s.localizedNamed()),
                  )
              ],
              onPressed: (index) {
                setState(() {
                  var style = DiaperStyle.values[index];
                  if (styles.contains(style)) {
                    styles.remove(style);
                  } else {
                    styles.add(style);
                  }
                  sp.setFilterDiaperStyle(styles);
                });
              }),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(
              I18N.current.filter_state,
              style: const TextStyle(fontSize: 20),
            ),
          ),
          ToggleButtons(
              isSelected: [
                for (var s in stateArray) discontinueds.contains(s)
              ],
              children: [
                for (var s in stateArray)
                  Padding(
                    padding: const EdgeInsets.all(8),
                    child: Text(s ? I18N.current.state_discontinued : I18N.current.state_current  ),
                  )
              ],
              onPressed: (index) {
                setState(() {
                  var discontinued = stateArray[index];
                  if (discontinueds.contains(discontinued)) {
                    discontinueds.remove(discontinued);
                  } else {
                    discontinueds.add(discontinued);
                  }
                  sp.setFilterDiscontinued(discontinueds);
                });
              })
        ],
      );
    },
  );
}
