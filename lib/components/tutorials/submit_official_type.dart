import 'package:diap_stash/common.dart';
import 'package:diap_stash/components/catalog/catalog_type_widget.dart';
import 'package:diap_stash/providers/settings_provider.dart';
import 'package:tutorial_coach_mark/tutorial_coach_mark.dart';

GlobalKey keySubmitOfficialDiaperTabBar = GlobalKey();
GlobalKey keySubmitOfficialDiaperTab = GlobalKey();
GlobalKey keySubmitOfficialDiaperFirstCustomDiaper = GlobalKey();
GlobalKey keySubmitOfficialDiaperEditButton = GlobalKey();
GlobalKey keySubmitOfficialDiaperSubmitButton = GlobalKey();

late TutorialCoachMark tutorialSubmitOfficialDiaperMark;

void createTutorialSubmitOfficialDiaper(BuildContext context) {
  tutorialSubmitOfficialDiaperMark = TutorialCoachMark(
      textSkip: I18N.current.skip_tutorial,
      targets: [
        TargetFocus(
            identify: "tab",
            keyTarget: keySubmitOfficialDiaperTab,
            //enableTargetTab: true,
            //enableOverlayTab: true,
            radius: 1,
            paddingFocus: 1,
            contents: [
              TargetContent(
                  align: ContentAlign.bottom,
                  builder: (ctx, _) => Column(
                        children: [
                          Text(
                            I18N.current.submit_tuto_title,
                            style: const TextStyle(
                                fontSize: 18, color: Colors.white),
                          ),
                          Text(
                            I18N.current
                                .submit_tuto_open_custom_tab,
                            style: const TextStyle(
                              color: Colors.white,
                            ),
                          ),
                        ],
                      ))
            ]),
        TargetFocus(
            identify: "type",
            keyTarget: keySubmitOfficialDiaperFirstCustomDiaper,
            contents: [
              TargetContent(
                  align: ContentAlign.bottom,
                  builder: (ctx, _) => Text(
                        I18N.current.submit_tuto_select_type,
                        style: const TextStyle(
                          color: Colors.white,
                        ),
                      ))
            ]),
        TargetFocus(
            identify: "edit",
            keyTarget: keySubmitOfficialDiaperEditButton,
            contents: [
              TargetContent(
                  align: ContentAlign.bottom,
                  builder: (ctx, _) => Text(
                        I18N.current.submit_tuto_edit_type,
                        style: const TextStyle(
                          color: Colors.white,
                        ),
                      ))
            ]),
        TargetFocus(
            identify: "submit",
            keyTarget: keySubmitOfficialDiaperSubmitButton,
            contents: [
              TargetContent(
                  align: ContentAlign.custom,
                  customPosition:
                      CustomTargetContentPosition(top: 300, left: 10),
                  builder: (ctx, _) => Text(
                        I18N.current.submit_tuto_submit_button,
                        style: const TextStyle(
                          color: Colors.white,
                        ),
                      ))
            ])
      ],
      onClickTarget: (target) {
        if (target.identify == "tab") {
          (keySubmitOfficialDiaperTabBar.currentWidget! as TabBar)
              .controller!
              .index = 1;
          return;
        } else if (target.identify == "type" && keySubmitOfficialDiaperFirstCustomDiaper.currentState != null) {
          (keySubmitOfficialDiaperFirstCustomDiaper.currentState
                  as CatalogTypeWidgetState)
              .showDialogType(context);
        } else if (target.identify == "edit") {
          if (keySubmitOfficialDiaperEditButton.currentWidget != null) {
            (keySubmitOfficialDiaperEditButton.currentWidget as TextButton)
                .onPressed!();
          }
        } else if (target.identify == "submit") {
          if (keySubmitOfficialDiaperSubmitButton.currentWidget != null) {
            (keySubmitOfficialDiaperSubmitButton.currentWidget
                    as ElevatedButton)
                .onPressed!();
          }
        }
      },
      onSkip: () {
        SettingsProvider.instance.spInstance.setBool("tuto_submit_custom", true);
        return false;
      },
      onFinish: () {
        SettingsProvider.instance.spInstance.setBool("tuto_submit_custom", true);
      });
}
