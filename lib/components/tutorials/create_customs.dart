import 'package:diap_stash/common.dart';
import 'package:diap_stash/providers/settings_provider.dart';
import 'package:tutorial_coach_mark/tutorial_coach_mark.dart';

GlobalKey createCustomsFBKey = GlobalKey();
GlobalKey createCustomsNameKey = GlobalKey();
GlobalKey createCustomsImgKey = GlobalKey();
GlobalKey createSaveImgKey = GlobalKey();

late TutorialCoachMark tutorialCreateCustomDiaperMark;


void createTutorialCreateCustomDiaper(BuildContext context) {
  tutorialCreateCustomDiaperMark = TutorialCoachMark(
    textSkip: I18N.current.skip_tutorial,
      targets: [
        TargetFocus(
            identify: "create_customs_fb",
            keyTarget: createCustomsFBKey,
            enableTargetTab: true,
            enableOverlayTab: true,
            paddingFocus: 15,
            contents: [
              TargetContent(
                  align: ContentAlign.left,
                  child: Text(
                    I18N.current.custom_tuto_add,
                    style: const TextStyle(
                      color: Colors.white,
                    ),
                  ))
            ])
      ],
      onClickTarget: (target) {
        if (target.identify == "create_customs_fb") {
          (target.keyTarget!.currentWidget! as FloatingActionButton)
              .onPressed!();
          return;
        }
      },
    onSkip: () {
        SettingsProvider sp = SettingsProvider.instance;
        sp.spInstance.setBool("tuto_create_customs", true);
        return false;
    },
    onFinish: (){
      SettingsProvider sp = SettingsProvider.instance;
      sp.spInstance.setBool("tuto_create_customs", true);
    }
  );
}