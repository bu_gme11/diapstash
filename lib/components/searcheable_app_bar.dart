import 'package:diap_stash/common.dart';

class SearcheableAppBar extends StatefulWidget implements PreferredSizeWidget {
  final Widget title;
  final bool enableSearch;
  final TextEditingController? searchController;
  final List<Widget> actions;

  const SearcheableAppBar(
      {super.key,
      required this.title,
      required this.enableSearch,
      this.searchController,
      this.actions = const []});

  @override
  State<SearcheableAppBar> createState() => _SearcheableAppBarState();

  @override
  Size get preferredSize {
    return const Size.fromHeight(50);
  }
}

class _SearcheableAppBarState extends State<SearcheableAppBar> {
  bool searchOpened = false;

  get canSearch => widget.enableSearch && widget.searchController != null;

  @override
  Widget build(BuildContext context) {
    Widget title = widget.title;
    if (searchOpened && canSearch) {
      var closeIcon = IconButton(
          onPressed: () {
            setState(() {
              searchOpened = false;
              if(widget.searchController != null){
                widget.searchController!.text = "";
              }
            });
          },
          icon: const Icon(Icons.close));
      title = TextField(
        controller: widget.searchController,
        decoration: InputDecoration(
        hintText: I18N.current.search,
        suffixIcon: closeIcon),
      );
    }

    List<Widget> actions = [];

    if (canSearch) {
      if (!searchOpened) {
        actions.addAll(widget.actions);
        actions.add(IconButton(
            onPressed: () {
              setState(() {
                searchOpened = true;
              });
            },
            icon: const Icon(Icons.search)));
      }
    } else {
      actions.addAll(widget.actions);
    }
    return AppBar(
      title: title,
      actions: actions,
    );
  }
}
