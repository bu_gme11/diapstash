import 'dart:async';

import 'package:diap_stash/pages/history_page.dart';

import 'package:diap_stash/util.dart';
import 'package:diap_stash/common.dart';

class HistoryNoChangeWidget extends StatefulWidget {
  final HistoryData history;

  const HistoryNoChangeWidget({super.key, required this.history});

  @override
  State<StatefulWidget> createState() => _HistoryNoChangeWidgetState();
}

class _HistoryNoChangeWidgetState extends State<HistoryNoChangeWidget> {
  Timer? timer;

  @override
  void initState() {
    super.initState();
    if (widget.history.endTime == null) {
      timer = Timer.periodic(
          const Duration(seconds: 10), (timer){
            if(mounted) {
              setState(() => {});
            }else{
              timer.cancel();
            }
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    String sDuration = durationToString(widget.history.duration());

    bool showDate = false;

    return Card(
        elevation: 0,
        child: Padding(
            padding: const EdgeInsets.all(5),
            child: Column(children: [
              Column(
                children: [
                  const Icon(Icons.baby_changing_station),
                  Text(sDuration, style: const TextStyle(fontSize: 16))
                ],
              ),
              Text(
                I18N.current.history_no_wearing,
                textAlign: TextAlign.center,
              ),
              if (showDate)
                // ignore: dead_code
                Row(
                  children: [
                    Expanded(
                        child: Text(
                      dateTimeToString(widget.history.startTime),
                      style: const TextStyle(
                          fontStyle: FontStyle.italic, fontSize: 12),
                      textAlign: TextAlign.center,
                    )),
                    const Icon(
                      Icons.arrow_right_alt,
                      size: 12,
                    ),
                    if (widget.history.endTime != null)
                      Expanded(
                          child: Text(
                        dateTimeToString(widget.history.endTime!),
                        style: const TextStyle(
                            fontStyle: FontStyle.italic, fontSize: 12),
                        textAlign: TextAlign.center,
                      ))
                    else
                      const Expanded(
                          child: Text(
                        "Now",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontStyle: FontStyle.italic, fontSize: 12),
                      )),
                  ],
                ),
            ])));
  }
}
