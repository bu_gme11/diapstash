import 'dart:developer';

import 'package:diap_stash/common.dart';
import 'package:diap_stash/components/dialogs/dialog_yes_no.dart';
import 'package:diap_stash/components/history/history_change_widget.dart';
import 'package:diap_stash/object/interfaces.dart';
import 'package:diap_stash/pages/history/history_edit_change_page.dart';
import 'package:diap_stash/providers/diaper_provider.dart';
import 'package:diap_stash/providers/notification_provider.dart';
import 'package:diap_stash/util.dart';
import 'package:diap_stash/utils/water_drop_icons.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';

_propagateDateChange(
    {required BuildContext context,
    required DiaperProvider dp,
    required Change change,
    required DateTime startTime,
    required DateTime? endTime}) async {
  var changes = dp.changes.toList();
  changes.sort((a, b) => a.startTime.compareTo(b.startTime));
  List<Change> changesToUpdate = [];

  var index = changes.indexOf(change);

  var changeStartTime =
      change.startTime.copyWith(second: 0, millisecond: 0, microsecond: 0);
  if (index > 0) {
    var previous = changes[index - 1];
    var previousEndTime =
        previous.endTime?.copyWith(second: 0, millisecond: 0, microsecond: 0);

    var needEndTimePropagation = false;
    if (previousEndTime != null) {
      if (previousEndTime.isAfter(changeStartTime)) {
        needEndTimePropagation = true;
      } else if (previousEndTime.isAtSameMomentAs(changeStartTime) &&
          startTime != changeStartTime) {
        needEndTimePropagation = true;
      }
    }

    if (needEndTimePropagation) {
      log("Required to update endTime of ${index - 1} ($previous)");
      var formatedStartTime = dateTimeToString(changeStartTime);
      var res = await _showDialogAskPropagateTime(
          context: context,
          change: previous,
          title: I18N.current.change_ask_propagate_date_previous_title,
          text: I18N.current
              .change_ask_propagate_date_previous_text(formatedStartTime));
      if (res) {
        previous.endTime = changeStartTime;
        previous.updatedAt = DateTime.now();
        changesToUpdate.add(previous);
      }
    }
  }

  var changeEndTime =
      change.endTime?.copyWith(second: 0, millisecond: 0, microsecond: 0);

  if (index < changes.length - 1 &&
      endTime != null &&
      changeEndTime != null &&
      changeEndTime != endTime) {
    var next = changes[index + 1];
    var nextStartTime =
        next.startTime.copyWith(second: 0, millisecond: 0, microsecond: 0);
    if (nextStartTime.isAtSameMomentAs(endTime) ||
        nextStartTime.isBefore(endTime)) {
      log("Required to update startTime of ${index + 1} ($next)");
      var formatedEndTime = dateTimeToString(changeEndTime);
      // ignore: use_build_context_synchronously
      var res = await _showDialogAskPropagateTime(
          context: context,
          change: next,
          title: I18N.current.change_ask_propagate_date_next_title,
          text: I18N.current
              .change_ask_propagate_date_next_text(formatedEndTime));
      if (res) {
        next.startTime = change.endTime!;
        next.updatedAt = DateTime.now();
        changesToUpdate.add(next);
      }
    }
  }

  if (changesToUpdate.isNotEmpty) {
    await dp.saveSpecificChanges(
        specificChanges: changesToUpdate, doNotifyListeners: true);
  }
}

Future<bool> _showDialogAskPropagateTime(
    {required BuildContext context,
    required Change change,
    required String title,
    required String text}) async {
  var res = await showDialog<bool>(
      context: context,
      builder: (context) => AlertDialog(
            title: Text(title),
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Text(text),
                HistoryChangeWidget(
                    change: change,
                    imageSize: 50,
                    diaperFontSize: 18,
                    compact: true)
              ],
            ),
            actions: [
              TextButton(
                  onPressed: () {
                    Navigator.of(context).pop(false);
                  },
                  child: Text(I18N.current.no)),
              TextButton(
                  onPressed: () {
                    Navigator.of(context).pop(true);
                  },
                  child: Text(I18N.current.yes))
            ],
          ));
  return res ?? false;
}

showEditChangePage(
    {required BuildContext context,
    required final Change change,
    Function()? onDone}) {
  DiaperProvider dp = Provider.of<DiaperProvider>(context, listen: false);
  NotificationProvider np =
      Provider.of<NotificationProvider>(context, listen: false);

  DateTime startTime =
      change.startTime.copyWith(second: 0, millisecond: 0, microsecond: 0);
  DateTime? endTime = change.endTime?.copyWith(second: 0, millisecond: 0, microsecond: 0);

  Navigator.of(context).push(MaterialPageRoute(builder: (context) {
    return HistoryEditChangePage(change: change);
  })).then((value) async {
    var index = dp.changes.indexOf(change);
    if (index >= 0) {
      dp.changes[index] = change;
      await _propagateDateChange(
          context: context,
          dp: dp,
          change: change,
          startTime: startTime,
          endTime: endTime);
      await dp.saveSpecificChanges(
          specificChanges: [change], doNotifyListeners: true);
    }

    if (onDone != null) {
      await onDone();
    }
    await np.computeNotification(dp.currentChange);
  });
}

dialogAskEditChange(
    {required BuildContext context,
    required Change change,
    void Function()? onDone,
    Change? previousChange,
    bool canUndo = false,
    Future Function()? onUndo}) async {
  DiaperProvider dp = Provider.of<DiaperProvider>(context, listen: false);
  Widget? content;
  if (previousChange != null) {
    content = StatefulBuilder(builder: (context, setState) {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          Text(I18N.current.change_state_previous,
              textAlign: TextAlign.right,
              style: TextStyle(color: Colors.grey.shade700)),
          ToggleButtons(
              isSelected: [
                for (var s in ChangeState.values) previousChange.state == s
              ],
              children: [
                for (var s in ChangeState.values)
                  Padding(
                    padding: const EdgeInsets.fromLTRB(4, 2, 4, 2),
                    child: Text(s.localizedNamed()),
                  )
              ],
              onPressed: (index) async {
                previousChange.state = ChangeState.values[index];
                previousChange.updatedAt = DateTime.now();
                if (context.mounted) {
                  setState(() {});
                }
                await dp.saveSpecificChanges(
                    specificChanges: [previousChange],
                    doRemote: false,
                    doNotifyListeners: true);
              }),
          if (previousChange.state?.isWet ?? false)
            Text(I18N.current.change_wetness_level,
                textAlign: TextAlign.right,
                style: TextStyle(color: Colors.grey.shade700)),
          if (previousChange.state?.isWet ?? false)
            Align(
              alignment: Alignment.center,
              child: RatingBar.builder(
                glow: false,
                itemBuilder: (context, index) => Icon(WaterDrop.water_drop_index(index + 1), color: _ratingColor(index, previousChange.wetness)),
                direction: Axis.horizontal,
                wrapAlignment: WrapAlignment.center,
                minRating: 0,
                maxRating: 5,
                itemCount: 5,
                itemSize: 36,
                itemPadding:
                    const EdgeInsets.symmetric(horizontal: 2.0, vertical: 5),
                allowHalfRating: false,
                initialRating: previousChange.wetness ?? 0,
                onRatingUpdate: (value) async {
                  previousChange.wetness = value == 0 ? null : value;
                  previousChange.updatedAt = DateTime.now();
                  if (context.mounted) {
                    setState(() {});
                  }
                  await dp.saveSpecificChanges(
                      specificChanges: [previousChange],
                      doRemote: false,
                      doNotifyListeners: true);
                },
              ),
            )
        ],
      );
    });
  }
  ResultDialogYesNo? result = await showDialog(
      context: context,
      builder: (context) => DialogYesNo(
          title: Text(I18N.current.change_ask_edit),
          content: content,
          autoNoDelay: const Duration(seconds: 15),
          cancelButton: canUndo,
          cancelButtonText: Text(I18N.current.undo)));
  switch (result) {
    case ResultDialogYesNo.yes:
      // ignore: use_build_context_synchronously
      await showEditChangePage(
          context: context, change: change, onDone: onDone);
      break;
    case ResultDialogYesNo.cancel:
      if (onUndo != null) {
        onUndo();
      }
      break;
    default:
      if (onDone != null) {
        onDone();
      }
      break;
  }


}

Color _ratingColor(int index, double? value){
  if(kIsWeb){
    return ((value ?? 0) > index) ? Colors.blueAccent: Colors.grey;
  }
  return Colors.blueAccent;
}
