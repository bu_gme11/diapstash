import 'dart:async';

import 'package:diap_stash/common.dart';
import 'package:diap_stash/components/type_image_widget.dart';
import 'package:diap_stash/object/interfaces.dart';
import 'package:diap_stash/util.dart';
import 'package:diap_stash/utils/water_drop_icons.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class HistoryChangeWidget extends StatefulWidget {
  final Change change;
  final bool errorDate;
  final double imageSize;
  final double diaperFontSize;
  final bool compact;

  const HistoryChangeWidget(
      {super.key,
      required this.change,
      this.errorDate = false,
      this.imageSize = 100,
      this.diaperFontSize = 24,
      this.compact = false});

  @override
  State<StatefulWidget> createState() => _HistoryChangeWidgetState();
}

class _HistoryChangeWidgetState extends State<HistoryChangeWidget> {
  Timer? timer;

  @override
  void initState() {
    super.initState();
    if (widget.change.endTime == null) {
      timer = Timer.periodic(
          const Duration(seconds: 10),
          (timer) => {
                if (mounted && widget.change.endTime == null)
                  {setState(() => {})}
                else
                  {timer.cancel()}
              });
    }
  }

  @override
  Widget build(BuildContext context) {
    late Duration duration;
    if (widget.change.endTime != null) {
      duration = widget.change.endTime!.difference(widget.change.startTime);
      if (timer != null) {
        timer!.cancel();
        timer = null;
      }
    } else {
      duration = DateTime.now().difference(widget.change.startTime);
    }

    String sDuration = durationToString(duration);

    var firstDiaper =
        widget.change.diapers.isNotEmpty ? widget.change.diapers.first : null;
    var otherDiaperName = widget.change.diapers.length > 1
        ? widget.change.diapers.skip(1).map((e) => e.type.name).join(", ")
        : "";

    var currently = widget.change.endTime == null;

    int wetness = widget.change.wetness?.toInt() ?? 5;

    return Card(
        child: Padding(
            padding: const EdgeInsets.all(5),
            child: Column(children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  if (firstDiaper != null)
                    Column(
                      children: [
                        TypeImageWidget(
                            type: firstDiaper.type,
                            width: widget.imageSize,
                            height: widget.imageSize),
                        if (widget.change.diapers.length > 1)
                          Wrap(
                            children: [
                              for (var d in widget.change.diapers.skip(1).take(
                                  widget.change.diapers.length == 4 ? 3 : 2))
                                Padding(
                                  padding: const EdgeInsets.all(5),
                                  child: TypeImageWidget(
                                    type: d.type,
                                    width: 25,
                                    height: 25,
                                    radius: 5,
                                  ),
                                ),
                              if (widget.change.diapers.length > 4)
                                Padding(
                                    padding: const EdgeInsets.all(5),
                                    child: Container(
                                      width: 25,
                                      height: 25,
                                      decoration: const BoxDecoration(
                                        color: Colors.purple,
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(5)),
                                      ),
                                      child: const Icon(
                                        Icons.add,
                                        color: Colors.white,
                                      ),
                                    ))
                            ],
                          )
                      ],
                    ),
                  if (firstDiaper != null)
                    Expanded(
                        child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(5),
                          child: Text(firstDiaper.type.name,
                              style: TextStyle(
                                  fontSize: widget.diaperFontSize)),
                        ),
                        if (otherDiaperName.isNotEmpty)
                          Text(
                            otherDiaperName,
                            style: const TextStyle(
                                fontStyle: FontStyle.italic, fontSize: 10),
                          ),
                        Container(
                          decoration: const BoxDecoration(
                              color: Colors.purple,
                              borderRadius:
                                  BorderRadius.all(Radius.circular(5))),
                          child: Padding(
                              padding: const EdgeInsets.fromLTRB(10, 5, 10, 5),
                              child: Text(
                                firstDiaper.size,
                                textAlign: TextAlign.center,
                                style: const TextStyle(color: Colors.white),
                              )),
                        ),
                      ],
                    )),
                  if (!widget.compact)
                    Padding(
                      padding: const EdgeInsets.all(15),
                      child: Column(
                        children: [
                          if (currently)
                            const Icon(Icons.hourglass_top)
                          else
                            const Icon(Icons.access_time),
                          Text(sDuration, style: const TextStyle(fontSize: 16)),
                          Row(
                            children: [
                              if (widget.change.state?.isWet ?? false)
                                Icon(
                                  WaterDrop.water_drop_index(wetness),
                                  color: Theme.of(context).iconTheme.color ?? Colors.black,
                                  size: 24
                                ),
                              if (widget.change.state?.isMessy ?? false)
                                const Padding(
                                  padding: EdgeInsets.all(5),
                                  child: FaIcon(
                                    FontAwesomeIcons.poop,
                                    size: 20,
                                  ),
                                )
                            ],
                          ),
                          if (widget.change.tags != null &&
                              widget.change.tags!.isNotEmpty)
                            Container(
                              constraints: const BoxConstraints(maxWidth: 75),
                              child: Wrap(
                                alignment: WrapAlignment.spaceAround,
                                children: [
                                  for (var tag in widget.change.tags!)
                                    Padding(
                                      padding:
                                          const EdgeInsets.fromLTRB(2, 0, 2, 0),
                                      child: Text(tag,
                                          style: TextStyle(
                                              color: HexColor.fromText(tag))),
                                    ),
                                ],
                              ),
                            )
                        ],
                      ),
                    )
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Text(
                    dateTimeToString(widget.change.startTime),
                    style: const TextStyle(
                        fontStyle: FontStyle.italic, fontSize: 12),
                    textAlign: TextAlign.center,
                  ),
                  if (widget.change.endTime != null)
                    const Icon(Icons.arrow_right_alt, size: 12),
                  if (widget.change.endTime != null)
                    Text(
                      dateTimeToString(widget.change.endTime!),
                      style: const TextStyle(
                          fontStyle: FontStyle.italic, fontSize: 12),
                      textAlign: TextAlign.center,
                    )
                ],
              ),
              if (widget.errorDate)
                Text(I18N.current.history_date_conflit,
                    style: const TextStyle(
                        color: Colors.redAccent,
                        fontSize: 10,
                        fontStyle: FontStyle.italic))
            ])));
  }
}
