import 'dart:async';
import 'dart:developer';
import 'package:diap_stash/common.dart';
import 'package:diap_stash/components/history/edit_change.dart';
import 'package:diap_stash/components/type_image_widget.dart';
import 'package:diap_stash/providers/notification_provider.dart';
import 'package:diap_stash/util.dart';

import 'package:diap_stash/providers/diaper_provider.dart';
import 'package:sentry_flutter/sentry_flutter.dart';

class CurrentWearingWidget extends StatefulWidget {
  const CurrentWearingWidget({super.key});

  @override
  State<CurrentWearingWidget> createState() => _CurrentWearingWidgetState();
}

class _CurrentWearingWidgetState extends State<CurrentWearingWidget> {
  @override
  void initState() {
    super.initState();
    Timer.periodic(const Duration(seconds: 10), (Timer t) {
      if (mounted) {
        setState(() => {});
      } else {
        t.cancel();
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    var diaperProvider = Provider.of<DiaperProvider>(context, listen: true);

    if (diaperProvider.currentChange == null ||
        diaperProvider.currentChange!.diapers.isEmpty) {
      return Container();
    }

    var change = diaperProvider.currentChange!;

    var duration = DateTime.now().difference(change.startTime);
    String sDuration = durationToString(duration);

    DateTime? startTime;
    try {
      var changes = diaperProvider.changes.toList(growable: false);
      changes.sort((a, b) => b.startTime.compareTo(a.startTime));
      for (var i = 1; i < changes.length; i++) {
        var prev = changes[i];
        var c = changes[i - 1];
        if(prev.endTime == null){
          break;
        }
        if (c.startTime
            .difference(prev.endTime!)
            .inSeconds > 59) {
          if (i > 1) {
            startTime = c.startTime;
          }
          break;
        }
      }
    }catch(e){
      log("Error on calc streak", error: e);
    }

    Duration? inStreak =
        startTime != null ? DateTime.now().difference(startTime) : null;

    return SizedBox(
      child: Card(
          elevation: 1,
          child: Padding(
            padding: const EdgeInsets.all(5),
            child: Row(
              children: [
                Stack(
                  clipBehavior: Clip.none,
                  alignment: Alignment.bottomRight,
                  children: [
                    TypeImageWidget(
                        type: change.diapers.first.type, width: 50, height: 50),
                    if (change.diapers.length > 1)
                      Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: Container(
                          decoration: BoxDecoration(
                              color: Colors.purple,
                              borderRadius: BorderRadius.circular(5)),
                          child: const Padding(
                            padding: EdgeInsets.all(2.0),
                            child: Icon(
                              Icons.add,
                              color: Colors.white,
                              size: 14,
                            ),
                          ),
                        ),
                      ),
                  ],
                ),
                Expanded(
                    child: Text(change.diapers.first.type.name,
                        textAlign: TextAlign.center,
                        softWrap: true,
                        overflow: TextOverflow.ellipsis,
                        maxLines: 2,
                        style: const TextStyle(fontSize: 16))),
                Padding(
                  padding: const EdgeInsets.fromLTRB(15, 5, 15, 5),
                  child: Column(
                    children: [
                      const Icon(Icons.hourglass_top),
                      Text(sDuration),
                      if (inStreak != null)
                        Text(
                          durationToString(inStreak),
                          style: const TextStyle(fontSize: 12),
                        )
                    ],
                  ),
                ),
                ElevatedButton(
                  onPressed: () async {
                    Sentry.addBreadcrumb(Breadcrumb(
                        category: "ui.lick",
                        data: const {
                          "action": "removeCurrentDiaper",
                        }
                    ));
                    NotificationProvider np =
                        Provider.of(context, listen: false);
                    change.endTime = DateTime.now();
                    change.updatedAt = DateTime.now();
                    await diaperProvider.saveSpecificChanges(
                        specificChanges: [change],
                        doNotifyListeners: false);
                    await np.cancelChangeNotification();
                    await np.setupNotificationNightMode();
                    // ignore: use_build_context_synchronously
                    await dialogAskEditChange(
                        context: context,
                        change: change,
                        previousChange: change,
                        canUndo: true,
                        onUndo: () async {
                          change.endTime = null;
                          change.updatedAt = DateTime.now();
                          await diaperProvider.saveSpecificChanges(
                              specificChanges: [change],
                              doNotifyListeners: false);
                        },
                    );
                    diaperProvider.doNotifyListeners();
                  },
                  child: Text(I18N.current.removeDiaper),
                )
              ],
            ),
          )),
    );
  }
}
