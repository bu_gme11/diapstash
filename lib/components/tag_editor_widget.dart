import 'package:diap_stash/common.dart';
import 'package:diap_stash/providers/diaper_provider.dart';
import 'package:diap_stash/util.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';

// ignore: must_be_immutable
class TagEditorWidget extends StatefulWidget {
  List<String>? tags;
  final void Function(List<String>) onChanged;

  TagEditorWidget({super.key, required this.tags, required this.onChanged});

  @override
  State<TagEditorWidget> createState() => _TagEditorWidgetState();
}

class _TagEditorWidgetState extends State<TagEditorWidget> {
  final TextEditingController _tagsController = TextEditingController();
  List<String> tags = [];


  @override
  void initState() {
    super.initState();
    if(widget.tags != null){
      tags = widget.tags!;
    }
  }

  @override
  Widget build(BuildContext context) {
    DiaperProvider dp = Provider.of<DiaperProvider>(context, listen: false);

    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(I18N.current.tags),
          TypeAheadField(
            textFieldConfiguration: TextFieldConfiguration(
                controller: _tagsController,
                onSubmitted: (val) {
                  addTag(tag: val);
                },
                decoration: InputDecoration(
                    labelText: I18N.current.add_tags,
                    suffixIcon: TextButton(
                        onPressed: () {
                          addTag();
                        },
                        child: const Icon(Icons.add)))),
            suggestionsCallback: (pattern) {
              return dp.tags
                  .where((element) => !tags.contains(element))
                  .where((element) => element.startsWith(pattern))
                  .toList();
            },
            itemBuilder: (context, suggestion) {
              return Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(suggestion, style: TextStyle(color: HexColor.fromText(suggestion))),
              );
            },
            onSuggestionSelected: (suggestion) {
              addTag(tag: suggestion);
            },
            hideOnEmpty: true,
          ),
          Wrap(
            direction: Axis.horizontal,
            children: (tags)
                .map(
                  (e) => Chip(
                    label:
                        Text(e, style: TextStyle(color: HexColor.fromText(e))),
                    deleteIcon: const Icon(Icons.delete_outline),
                    onDeleted: () {
                      setState(() {
                        tags.remove(e);
                      });
                    },
                  ),
                )
                .toList(),
          ),
        ],
      ),
    );
  }

  void addTag({String? tag}) {
    setState(() {
      if (tag != null) {
        if (!tags.contains(tag)) {
          tags.add(tag);
          widget.onChanged(tags);
        }
      } else if (_tagsController.text.isNotEmpty) {
        if (!tags.contains(_tagsController.text)) {
          tags.add(_tagsController.text);
          widget.onChanged(tags);
        }
      }
      _tagsController.text = "";
    });
  }
}
