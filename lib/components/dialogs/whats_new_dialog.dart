import 'dart:convert';
import 'dart:developer';

import 'package:diap_stash/common.dart';
import 'package:diap_stash/object/interfaces.dart';
import 'package:diap_stash/providers/diaper_provider.dart';
import 'package:diap_stash/providers/settings_provider.dart';
import 'package:logging/logging.dart';
import 'package:http/http.dart' as http;
import 'package:package_info_plus/package_info_plus.dart';
import 'package:sentry_flutter/sentry_flutter.dart';

showReleaseDialog(BuildContext context, List<WhatsNewLog> logs) {
  if(!context.mounted){
    return;
  }
  Locale currentLocal = SettingsProvider.instance.getCurrentLocale(context);
  int currentIndex = 0;

  showDialog(
      context: context,
      builder: (context) {
        return StatefulBuilder(
          builder: (context, setState) {
            SettingsProvider sp = Provider.of(context, listen: false);
            var log = logs[currentIndex];
            var title = log.title;
            var description = log.description;

            if(log.messages.containsKey(currentLocal.languageCode)){
              var messages = log.messages[currentLocal.languageCode]!;
              if(messages.containsKey("title")){
                title = messages["title"]!;
              }
              if(messages.containsKey("description")){
                description = messages["description"]!;
              }
            }

            return AlertDialog(
              title:
                  Text(I18N.current.whats_news_hide_release(log.releaseName)),
              content: SingleChildScrollView(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Text(
                      title,
                      textAlign: TextAlign.center,
                      style: const TextStyle(
                          fontSize: 24, fontWeight: FontWeight.bold),
                    ),
                    if (log.image != null)
                      Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: Image.network(log.image!),
                      ),
                    Text(
                      description,
                      textAlign: TextAlign.left,
                    ),
                    const SizedBox(height: 20),
                    CheckboxListTile(
                      value: sp.whatNewHide,
                      onChanged: (val) {
                        setState((){
                          sp.setWhatsNewHide(val ?? false);
                        });
                      },
                      title: Text(I18N.current.whats_news_hide_next_time),
                      controlAffinity: ListTileControlAffinity.leading,
                      dense: true,
                    )
                  ],
                ),
              ),
              contentPadding: const EdgeInsets.fromLTRB(16, 16, 16, 0),
              actions: [
                if (currentIndex + 1 != logs.length)
                  TextButton(
                      onPressed: () {
                        setState(() {
                          currentIndex++;
                        });
                      },
                      child: Text(
                          "${I18N.current.next} (${currentIndex + 1}/${logs.length})")),
                TextButton(
                    onPressed: () {
                      sp.setWhatsNewLast(logs.last.buildCode);
                      Navigator.of(context).pop();
                    },
                    child: Text(I18N.current.close))
              ],
            );
          },
        );
      });
}

Future<void> generateReleaseDialog(BuildContext context, {bool showLast = false, bool force = false}) async {
  SettingsProvider sp = Provider.of(context, listen: false);

  if (sp.whatNewHide && !force) {
    return;
  }

  var info = await PackageInfo.fromPlatform();

  var from = sp.whatNewLast;
  List<WhatsNewLog> logs;
  if(showLast || from == null){
    logs = await loadLastWhatsNews();
  }else{
    logs = await loadWhatsNews(from, int.parse(info.buildNumber));
  }

  log("release dialog");
  if (logs.isNotEmpty) {
    // ignore: use_build_context_synchronously
    showReleaseDialog(context, logs);
  }else{
    log("No release log");
  }
}

Future<List<WhatsNewLog>> loadWhatsNews(int from, int to) async {
  List<WhatsNewLog> logs = [];
  var span = Sentry.startTransaction("loadWhatsNews", "loadWhatsNews");
  try {
    Uri uri = Uri.parse("$baseUrl/rest/whats-new?from=$from&to=$to");
    var res = await http.get(uri, headers: DiaperProvider.instance.cloudHeaders(headers: {}, span: span));

    if(res.statusCode == 200) {
      parseWhatsNewsLog(res, logs);
    }
  } catch (err, s) {
    Sentry.captureException(err, stackTrace: s, withScope: (scope) {
      scope.span = span;
    });
    log("Fail load whats news", level: Level.SEVERE.value, error: err);
  } finally {
    span.finish();
  }

  return logs;
}

Future<List<WhatsNewLog>> loadLastWhatsNews() async {
  List<WhatsNewLog> logs = [];
  var info = await PackageInfo.fromPlatform();
  var span = Sentry.startTransaction("loadLastWhatsNews", "loadLastWhatsNews");
  try {
    Uri uri = Uri.parse("$baseUrl/rest/whats-new/last?max=${info.buildNumber}");
    var res = await http.get(uri, headers: DiaperProvider.instance.cloudHeaders(headers: {}, span: span));

    if(res.statusCode == 200) {
      parseWhatsNewsLog(res, logs);
    }
  } catch (err, s) {
    Sentry.captureException(err, stackTrace: s, withScope: (scope) {
      scope.span = span;
    });
    log("Fail load whats news", level: Level.SEVERE.value, error: err);
  } finally {
    span.finish();
  }

  return logs;
}


void parseWhatsNewsLog(http.Response res, List<WhatsNewLog> logs) {
  var json = jsonDecode(res.body);
  List<Map<String, dynamic>> listOfMap = List.from(json as List);
  for(var elem in listOfMap){

    Map<String, Map<String, String>> messages = Map<String, dynamic>.from(elem["messages"]).map((key, value) => MapEntry(key, Map<String, String>.from(value)));

    var log = WhatsNewLog(
        buildCode: elem["buildCode"],
        releaseName: elem["releaseName"],
        title: elem["title"],
        description: elem["description"],
        image: elem["image"],
        messages: messages
    );
    logs.add(log);
  }
}
