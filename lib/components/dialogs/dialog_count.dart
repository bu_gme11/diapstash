import 'package:diap_stash/common.dart';
import 'package:diap_stash/object/interfaces.dart';
import 'package:diap_stash/providers/settings_provider.dart';
import 'package:diap_stash/utils/comma_formatter.dart';
import 'package:diap_stash/utils/numerical_range_formatter.dart';
import 'package:flutter/services.dart';
import 'package:universal_io/io.dart';

class DialogCount extends StatefulWidget {
  final String title;
  final int? initalValue;
  final String okLabel;
  final List<int>? predefinedValues;
  final TextEditingController? textFieldController;
  final bool askPrice;
  final double? price;
  final bool canTotalDiaper;
  final void Function(int, double?) onUpdate;

  const DialogCount(
      {super.key,
      required this.title,
      required this.onUpdate,
      this.initalValue,
      required this.okLabel,
      this.predefinedValues,
      this.textFieldController,
      this.askPrice = false,
      this.price,
      this.canTotalDiaper = true,
      });

  @override
  State<DialogCount> createState() => _DialogCountState();
}

class _DialogCountState extends State<DialogCount> {
  late TextEditingController textFieldController;
  TextEditingController priceController = TextEditingController();

  bool inifiniteStock = false;
  bool totalPrice = true;


  @override
  void initState() {
    super.initState();
    if (widget.textFieldController != null) {
      textFieldController = widget.textFieldController!;
    } else {
      textFieldController = TextEditingController();
    }

    if (textFieldController.value.text.isEmpty) {
      textFieldController.text = (widget.initalValue ?? 0).toString();
    }

    if(widget.price != null){
      priceController.text = widget.price!.toStringAsFixed(2);
      totalPrice = false;
    }
    if(!widget.canTotalDiaper){
      totalPrice = false;
    }

    if (widget.initalValue == DiaperStock.infinity) {
      inifiniteStock = true;
      textFieldController.text = "0";
    }
  }

  String? get _errorText {
    var val = textFieldController.value.text;
    if (val.isNotEmpty) {
      var intVal = int.tryParse(val);
      if (intVal == null) {
        return I18N.current.invalid_number;
      }
    }
    return null;
  }

  @override
  Widget build(BuildContext context) {
    String? pricePerDiaper;
    double? ppd;
    String currencySymbol;
    NumberFormat format = NumberFormat.simpleCurrency();
    try {
      format = NumberFormat.simpleCurrency(locale: Platform.localeName);
    } catch (_) {}
    currencySymbol = format.currencySymbol;
    if (priceController.value.text.isNotEmpty) {
      var priceParsed = double.tryParse(priceController.value.text.replaceAll(r",", "."));
      var count = int.tryParse(textFieldController.value.text);
      if (priceParsed != null && count != null && count > 0) {
        ppd = priceParsed;
        if (totalPrice) {
          ppd = priceParsed / count;
        }
        String ppdStr = format.format(ppd);
        pricePerDiaper = "$ppdStr / ${I18N.current.diaper}";
      }
    }

    SettingsProvider sp = Provider.of(context);
    return AlertDialog(
      title: Text(widget.title),
      actions: [
        TextButton(
            onPressed: () => Navigator.pop(context),
            child: Text(I18N.current.cancel)),
        TextButton(
            onPressed: () {
              if (inifiniteStock) {
                widget.onUpdate(DiaperStock.infinity, ppd);
              } else {
                var rawVal = textFieldController.value.text;
                var count = int.tryParse(rawVal);
                if (count == null) {
                  return;
                }
                widget.onUpdate(count, ppd);
              }
            },
            child: Text(widget.okLabel))
      ],
      content: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          TextField(
            onChanged: (value) {
              setState(() {});
            },
            controller: textFieldController,
            enabled: !inifiniteStock,
            keyboardType: const TextInputType.numberWithOptions(signed: false, decimal: false),
            inputFormatters: [
              FilteringTextInputFormatter.digitsOnly,
              NumericalRangeFormatter(min: 0)
            ],
            decoration: InputDecoration(
                hintText: I18N.current.number_of_diaper, errorText: _errorText),
          ),
          if (!inifiniteStock &&
              widget.predefinedValues != null &&
              widget.predefinedValues!.isNotEmpty)
            Wrap(
              direction: Axis.horizontal,
              children: [
                for (var size in widget.predefinedValues!)
                  ElevatedButton(
                      onPressed: () {
                        //widget.onUpdate(size);
                        int? val = int.tryParse(textFieldController.value.text);
                        if (val != null) {
                          textFieldController.text = (val + size).toString();
                          setState(() {});
                        }
                      },
                      child: Text.rich(
                        TextSpan(children: [
                          const WidgetSpan(
                              child: Icon(
                            Icons.add,
                            size: 16,
                          )),
                          TextSpan(text: "$size")
                        ]),
                      ))
              ],
            ),
          if (sp.allowInfiniteStock)
            SwitchListTile(
                value: inifiniteStock,
                onChanged: (val) {
                  setState(() {
                    inifiniteStock = val;
                  });
                },
                title: Text(I18N.current.stock_infinite_stock)),
          if (widget.askPrice)
            Column(
              children: [
                TextFormField(
                  decoration: InputDecoration(
                      label: Text(I18N.current.price),
                      prefixText: currencySymbol,
                      helperText: pricePerDiaper),
                  controller: priceController,
                  keyboardType: const TextInputType.numberWithOptions(signed: false, decimal: true),
                  inputFormatters: [
                    CommaFormatter(),
                    FilteringTextInputFormatter.allow(RegExp(r"^[0-9]+([.,][0-9]*)?")),
                    NumericalRangeFormatter(min: 0)
                  ],
                  onChanged: (value) {
                    setState(() {});
                  },
                ),
                if ((ppd ?? -1) > 0 && widget.canTotalDiaper)
                  ToggleButtons(
                    isSelected: [totalPrice, !totalPrice],
                    onPressed: (index) {
                      totalPrice = index == 0;
                      setState(() {});
                    },
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(I18N.current.price_total_price),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(I18N.current.price_per_diaper),
                      ),
                    ],
                  )
              ],
            ),
        ],
      ),
    );
  }
}
