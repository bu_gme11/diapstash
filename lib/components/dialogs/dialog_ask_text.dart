import 'package:diap_stash/common.dart';
import 'package:sentry_flutter/sentry_flutter.dart';

class DialogAskText {
  final Text title;
  final Text cancelButton;
  final Text okButton;
  Text? label;
  Widget? bottom;
  void Function(BuildContext, TextEditingController)? cancelCallback;
  void Function(BuildContext, TextEditingController)? okCallback;

  DialogAskText(
      {required this.title,
      required this.cancelButton,
      required this.okButton,
      this.label,
      this.bottom,
      this.cancelCallback,
      this.okCallback});

  showAskTextDialog(BuildContext context) {
    Sentry.addBreadcrumb(Breadcrumb(
        category: "navigation",
        data: const {
          "openDialog": "askText",
        }
    ));
    showDialog(
        context: context,
        builder: (context) {
          TextEditingController nameCtrl = TextEditingController();
          return AlertDialog(
            title: title,
            content: Column(mainAxisSize: MainAxisSize.min, children: [
              TextField(
                  controller: nameCtrl,
                  decoration: InputDecoration(label: label)),
              if (bottom != null) bottom!
            ]),
            actions: [
              TextButton(
                  onPressed: cancelCallback == null
                      ? null
                      : () => cancelCallback!(context, nameCtrl),
                  child: cancelButton),
              TextButton(
                  onPressed:
                      okCallback == null ? null : () => okCallback!(context, nameCtrl),
                  child: okButton)
            ],
          );
        });
  }
}
