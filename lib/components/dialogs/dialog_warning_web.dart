import 'package:diap_stash/common.dart';
import 'package:diap_stash/providers/settings_provider.dart';
import 'package:flutter/foundation.dart';

void showDialogWarningWeb(BuildContext context) {
  if (!kIsWeb) return;
  SettingsProvider sp = Provider.of<SettingsProvider>(context, listen: false);

  if (sp.spInstance.getBool("dismiss_dialog_web_warning") ?? false) return;

  showDialog(
    context: context,
    builder: (context) => AlertDialog(
      title: Text(I18N.current.dialog_warning_web_title),
      content: Column(
        mainAxisSize: MainAxisSize.min,
        children: [Center(child: Text(I18N.current.dialog_warning_web_text))],
      ),
      actions: [
        TextButton(
            onPressed: () {
              sp.spInstance.setBool("dismiss_dialog_web_warning", true);
              Navigator.of(context).pop();
            },
            child: Text(I18N.current.dialog_warning_web_dont_show_me)),
        TextButton(
            onPressed: () {
              Navigator.of(context).pop();
            },
            child: Text(I18N.current.ok))
      ],
    ),
  );
}
