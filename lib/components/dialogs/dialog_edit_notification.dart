import 'package:date_time_picker/date_time_picker.dart';
import 'package:diap_stash/common.dart';
import 'package:diap_stash/components/catalog/select_type_widget.dart';
import 'package:diap_stash/object/interfaces.dart';
import 'package:diap_stash/object/notifications.dart';
import 'package:diap_stash/providers/diaper_provider.dart';
import 'package:diap_stash/providers/notification_provider.dart';

class DialogEditNotification extends StatefulWidget {
  final String title;
  final DSNotification? notification;
  final Future<void> Function(DSNotification) onSave;
  final bool askType;

  const DialogEditNotification(
      {super.key,
      this.notification,
      required this.title,
      required this.onSave,
      required this.askType});

  @override
  State<DialogEditNotification> createState() => _DialogEditNotificationState();
}

class _DialogEditNotificationState extends State<DialogEditNotification> {
  NotificationType type = NotificationType.REMINDER;
  DateTime durationPicker = DateTime(0, 1, 1, 6, 0);
  TextEditingController titleCtrl = TextEditingController();
  TextEditingController descCtrl = TextEditingController();
  TextEditingController durationCtrl = TextEditingController();
  DiaperType? diaperType;

  @override
  void initState() {
    if (widget.notification != null) {
      type = widget.notification!.type;
      durationPicker = DateTime(0, 1, 1, widget.notification!.duration.inHours,
          widget.notification!.duration.inMinutes.remainder(60));

      titleCtrl.text = widget.notification!.title;
      descCtrl.text = widget.notification!.description;

      if (widget.notification!.typeId != null) {
        diaperType =
            DiaperProvider.instance.getTypeById(widget.notification!.typeId!);
      }
    }
    durationCtrl.text = DateFormat.Hm().format(durationPicker);
    super.initState();
  }

  bool get canSave {
    if (durationPicker.minute == 0 && durationPicker.hour == 0) {
      return false;
    }

    if (widget.askType && diaperType == null) {
      return false;
    }

    if (type == NotificationType.CUSTOM) {
      return titleCtrl.value.text.isNotEmpty && descCtrl.value.text.isNotEmpty;
    }
    return true;
  }

  String? get errorPicker {
    if (durationPicker.minute == 0 && durationPicker.hour == 0) {
      return "Delay must be more than 0 minutes";
    }
    return null;
  }

  Duration getAverageDiaperTime(BuildContext context){
    DiaperProvider dp = Provider.of(context, listen: false);
    var changesDurations = dp.changes
        .where((c) => c.endTime != null && c.diapers.where((dc) => dc.type == diaperType!).isNotEmpty)
        .map((c) => c.endTime!.difference(c.startTime)).toList(growable: false);
    if(changesDurations.isEmpty){
      return Duration.zero;
    }
    var seconds = 0;
    for(var d in changesDurations){
      seconds += d.inSeconds;
    }
    return Duration(seconds: (seconds / changesDurations.length).round());
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text(widget.title),
      content: SingleChildScrollView(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            if (widget.askType)
              SelectTypeWidget(
                type: diaperType,
                onChange: (type) {
                  setState(() {
                    diaperType = type;
                  });
                },
              ),
            DropdownButtonFormField<NotificationType>(
              decoration:
                  InputDecoration(label: Text(I18N.current.notification_type)),
              value: type,
              isExpanded: true,
              onChanged: (val) {
                setState(() {
                  type = val!;
                });
              },
              items: [
                for (var t in NotificationType.values)
                  DropdownMenuItem(
                    value: t,
                    child: Text(t.localizedName(),
                        softWrap: true,
                        maxLines: 3,
                        overflow: TextOverflow.ellipsis),
                  )
              ],
            ),
            DateTimePicker(
              type: DateTimePickerType.time,
              timeLabelText: I18N.current.notification_delay,
              dateMask: DateFormat.Hm().pattern,
              decoration: InputDecoration(errorText: errorPicker),
              controller: durationCtrl,
              use24HourFormat: true,
              onChanged: (value) {
                DateTime dateTime = DateFormat.Hm().parse(value);
                setState(() {
                  durationPicker = dateTime;
                });
              },
            ),
            if (widget.askType)
              Align(
                alignment: Alignment.topRight,
                child: TextButton(
                  onPressed: diaperType != null && getAverageDiaperTime(context).inMinutes > 0 ? () {
                    var duration = getAverageDiaperTime(context);
                    setState(() {
                      durationPicker = DateTime(0, 1, 1, 0, 0).add(duration);
                      durationCtrl.text = DateFormat.Hm().format(durationPicker);
                    });
                  } : null,
                  style: ButtonStyle(
                      padding: MaterialStateProperty.all(
                          const EdgeInsets.only(top: 10, bottom: 10)),
                      alignment: Alignment.topRight),
                  child: Text(I18N.current.notification_use_average_time,
                      style: const TextStyle(fontSize: 12)),
                ),
              ),
            if (type == NotificationType.CUSTOM)
              TextFormField(
                controller: titleCtrl,
                decoration: InputDecoration(
                    label: Text(I18N.current.notification_title),
                    helperText: I18N.current.notification_delay_help),
                onChanged: (_) {
                  setState(() {});
                },
              ),
            if (type == NotificationType.CUSTOM)
              TextFormField(
                controller: descCtrl,
                decoration: InputDecoration(
                    label: Text(I18N.current.notification_description),
                    helperText: I18N.current.notification_delay_help),
                onChanged: (_) {
                  setState(() {});
                },
              ),
          ],
        ),
      ),
      actions: [
        TextButton(
            onPressed: () {
              Navigator.of(context).pop();
            },
            child: Text(I18N.current.cancel)),
        TextButton(
            onPressed: canSave
                ? () async {
                    Duration duration = Duration(
                        hours: durationPicker.hour,
                        minutes: durationPicker.minute);

                    if (duration.inMinutes == 0) return;

                    DSNotification n;
                    if (widget.notification == null) {
                      NotificationProvider np =
                          Provider.of<NotificationProvider>(context,
                              listen: false);

                      var title = titleCtrl.value.text;
                      var description = descCtrl.value.text;

                      n = await np.createNotification(
                        type: type,
                        duration: duration,
                        title: title,
                        description: description,
                        typeId: diaperType?.id,
                      );
                    } else {
                      n = widget.notification!;
                      n.type = type;
                      n.duration = duration;
                      n.title = titleCtrl.value.text;
                      n.description = descCtrl.value.text;
                      n.typeId = diaperType?.id;
                    }

                    await widget.onSave(n);
                    if (mounted) {
                      Navigator.of(context).pop();
                    }
                  }
                : null,
            child: Text(widget.notification != null
                ? I18N.current.edit
                : I18N.current.add))
      ],
    );
  }
}
