import 'dart:async';

import 'package:diap_stash/common.dart';

enum ResultDialogYesNo {
  yes, no, cancel
}

class DialogYesNo extends StatefulWidget {
  final Widget title;
  final Widget? content;
  final bool cancelButton;
  final Widget? cancelButtonText;
  final Duration? autoNoDelay;

  const DialogYesNo(
      {super.key,
      required this.title,
      this.content,
      this.cancelButton = false,
      this.cancelButtonText,
      this.autoNoDelay});

  @override
  State<DialogYesNo> createState() => _DialogYesNoState();
}

class _DialogYesNoState extends State<DialogYesNo> {
  int? secondsLeft;
  Timer? timer;

  @override
  void initState() {
    super.initState();
    if (widget.autoNoDelay != null) {
      secondsLeft = widget.autoNoDelay!.inSeconds;
      timer = Timer.periodic(const Duration(seconds: 1), (timer) {
        secondsLeft = secondsLeft! - 1;
        if (mounted) {
          setState(() {
            if (secondsLeft == 0) {
              timer.cancel();
              no().then((value) {});
            }
          });
        }
      });
    }
  }

  get noLabel {
    var label = I18N.current.no;
    if (secondsLeft != null) {
      label = "$label ($secondsLeft)";
    }
    return label;
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: widget.title,
      content: widget.content,
      actions: [
        if (widget.cancelButton)
          TextButton(
              onPressed: () async {
                if (context.mounted) {
                  Navigator.of(context).pop(ResultDialogYesNo.cancel);
                }
              },
              child: widget.cancelButtonText ?? Text(I18N.current.cancel)),
        TextButton(
            onPressed: () async {
              await no();
            },
            child: Text(noLabel)),
        TextButton(
            onPressed: () async {
              await yes();
            },
            child: Text(I18N.current.yes))
      ],
    );
  }

  Future<void> yes() async {
    if (mounted) {
      Navigator.of(context).pop(ResultDialogYesNo.yes);
    }
    if (timer != null) {
      timer!.cancel();
    }
  }

  Future<void> no() async {
    if (mounted) {
      Navigator.of(context).pop(ResultDialogYesNo.no);
    }
    if (timer != null) {
      timer!.cancel();
    }
  }
}
