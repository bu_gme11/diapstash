import 'package:azlistview/azlistview.dart';
import 'package:diap_stash/common.dart';
import 'package:diap_stash/components/catalog/catalog_brand_widget.dart';
import 'package:diap_stash/object/interfaces.dart';
import 'package:diap_stash/providers/diaper_provider.dart';

Widget buildCatalog(DiaperProvider dp,
    {Function(DiaperType type)? selectDiaper,
    Function(DiaperType type, String size)? selectDiaperWithSize,
    bool all = true,
    bool official = true, double typeSize = 100,
    required bool openSizeDialog}) {
  var types = dp.diaperTypes.where((t) => !t.hidden).toList();
  var brands = dp.brands
      .where((brand) =>
          types.where((type) => type.brandCode == brand.code).isNotEmpty)
      .toList();

  brands.sort((a, b) => a.name.compareTo(b.name));

  if (dp.favoritesTypes.isNotEmpty) {
    brands.insert(0, DiaperBrand.favoriteBrand());
  }

  if (types.where((element) => element.brand == null).isNotEmpty) {
    brands.add(DiaperBrand.otherBrand());
  }

  var listViewCatalog = AzListView(
      data: brands,
      itemCount: brands.length,
      indexBarData: SuspensionUtil.getTagIndexList(brands),
      indexBarOptions: const IndexBarOptions(
        needRebuild: true,
        ignoreDragCancel: true,
        downTextStyle: TextStyle(fontSize: 12, color: Colors.white),
        downItemDecoration:
            BoxDecoration(shape: BoxShape.circle, color: Colors.orangeAccent),
        indexHintWidth: 120 / 2,
        indexHintHeight: 100 / 2,
        indexHintAlignment: Alignment.centerRight,
        indexHintChildAlignment: Alignment(-0.25, 0.0),
        indexHintOffset: Offset(-20, 0),
      ),
      itemBuilder: (context, index) {
        var brand = brands.elementAt(index);
        return CatalogBrandWidget(
            brand: brand,
            official: official,
            types: types,
            all: all,
            selectDiaperWithSize: selectDiaperWithSize,
            selectDiaper: selectDiaper,
            openSizeDialog: openSizeDialog,
            typeSize: typeSize,
        );
      });
  return listViewCatalog;
}
