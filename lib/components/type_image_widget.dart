import 'package:cached_network_image/cached_network_image.dart';
import 'package:diap_stash/object/interfaces.dart';
import 'package:flutter/material.dart';
import 'package:diap_stash/common.dart';

class TypeImageWidget extends StatelessWidget {
  final DiaperType type;
  final double height;
  final double width;
  final double radius;
  final bool useCache;
  final bool border;

  const TypeImageWidget(
      {super.key,
      required this.type,
      required this.width,
      required this.height,
      this.radius = 10,
      this.useCache = true,
      this.border = true});

  @override
  Widget build(BuildContext context) {
    var sizedBox = SizedBox(
        width: width, height: height, child: Image.asset("Logo_Diaper.png"));

    // CachedNetworkImage.logLevel = CacheManagerLogLevel.verbose;
    var cacheKey = "diaper-${type.id}";

    return Container(
      decoration: border ? BoxDecoration(
          color: Colors.white.withAlpha(50),
          borderRadius: BorderRadius.circular(radius),
          border: Border.all(color: Colors.grey.withAlpha(255), width: 1)) : null,
      child: ClipRRect(
        borderRadius: BorderRadius.circular(radius),
        child: Container(
            child: type.image != null
                ? (useCache
                    ? CachedNetworkImage(
                        imageUrl: type.image!,
                        cacheKey: cacheKey,
                        errorWidget: (_, url, error) {
                          /*if (kIsWeb) {
                            Sentry.captureMessage(
                                "Error on loading ${type.name} (${type.id}) image (${type.image} / $url) (${error.toString()})");
                            Sentry.captureException(error, stackTrace: StackTrace.current);
                          }*/
                          return sizedBox;
                        },
                        width: width,
                        height: height,
                        useOldImageOnUrlChange: true,
                        fit: BoxFit.cover)
                    : Image.network(type.image!,
                        width: width,
                        height: height,
                        fit: BoxFit.cover, errorBuilder: (_, error, s) {
                        /*if (kIsWeb) {
                          Sentry.captureMessage(
                              "Error on loading ${type.name} (${type.id}) image (${type.image}) (${error.toString()})",
                              level: SentryLevel.warning);
                          Sentry.captureException(error, stackTrace: s);
                        }*/
                        return sizedBox;
                      }))
                : sizedBox),
      ),
    );
  }
}
