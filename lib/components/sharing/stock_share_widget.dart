import 'dart:async';
import 'dart:developer';

import 'package:diap_stash/common.dart';
import 'package:diap_stash/components/type_image_widget.dart';
import 'package:diap_stash/components/types/button_size_type_widget.dart';
import 'package:diap_stash/object/interfaces.dart';
import 'package:diap_stash/providers/diaper_provider.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';

class StockShareWidget extends StatefulWidget {
  final List<DiaperStock> currentStock;

  const StockShareWidget({required this.currentStock, super.key});

  @override
  State<StockShareWidget> createState() => _StockShareWidgetState();
}

class _StockShareWidgetState extends State<StockShareWidget> {
  final List<Future<bool>> loadedImages = [];

  List<Widget> children = [];

  final Completer<bool> _completerLoading = Completer<bool>();

  Future<bool> get loaded => _completerLoading.future;

  void buildChildren() {
    DiaperProvider dp = Provider.of(context, listen: false);
    children.clear();
    var currentStock = widget
        .currentStock
        .where((element) => element.total() > 0)
        .toList();
    for (var stock in currentStock) {
      var typeImageWidget = TypeImageWidget(
          type: stock.type, width: 50, height: 50, useCache: true);
      var child = SizedBox(
        width: 130,
        child: Card(
          child: Padding(
            padding: const EdgeInsets.all(5.0),
            child: Column(
              children: [
                Text(stock.type.name),
                if (stock.type.brand != null)
                  Text(stock.type.brand!.name,
                      style: const TextStyle(color: Colors.grey)),
                if (dp.isFavoriteType(stock.type))
                  const Icon(
                    Icons.star,
                    color: Colors.orange,
                  ),
                typeImageWidget,
                Wrap(
                  children: [
                    for (var size in stock.count.keys)
                      if (stock.count[size]! > 0)
                        ButtonSizeTypeWidget(
                            size: size,
                            color: stock.count[size]! < 10
                                ? Colors.red
                                : Colors.blue,
                            count: stock.count[size],
                            small: true)
                  ],
                ),
              ],
            ),
          ),
        ),
      );
      children.add(child);
    }
  }

  @override
  Widget build(BuildContext context) {
    buildChildren();
    //Future.forEach(loadedImages, (f) => f).then((value) => _completerLoading.complete(true));
    log("wait ${loadedImages.length}");
    Future.wait(loadedImages).then((value) {
      if (!_completerLoading.isCompleted) _completerLoading.complete(true);
      //return true;
    });
    //_completerLoading.complete(true);
    return Column(
      children: [
        StaggeredGrid.count(
          axisDirection: AxisDirection.down,
          crossAxisCount: 4,
          children: children,
        ),
        Padding(
          padding: const EdgeInsets.all(5.0),
          child: Row(children: [
            Image.asset("Logo_Soft.png",
                height: 30,
                width: 30,
                errorBuilder: (_, __, ___) => const SizedBox(
                      width: 30,
                      height: 30,
                    )),
            const Padding(
              padding: EdgeInsets.all(5.0),
              child: Text("Generated with DiapStash"),
            )
          ]),
        )
      ],
    );
  }
}
