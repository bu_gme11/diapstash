import 'package:diap_stash/common.dart';
import 'package:diap_stash/object/interfaces.dart';
import 'package:diap_stash/providers/diaper_provider.dart';

class ManageHistorySharingWidget extends StatefulWidget {
  const ManageHistorySharingWidget({super.key});

  @override
  State<ManageHistorySharingWidget> createState() =>
      _ManageHistorySharingWidgetState();
}

class _ManageHistorySharingWidgetState
    extends State<ManageHistorySharingWidget> {



  @override
  Widget build(BuildContext context) {
    DiaperProvider dp = Provider.of<DiaperProvider>(context, listen: false);

    return SizedBox(
      width: double.infinity,
      child: Column(
        children: [
          Text(I18N.current.history_sharing_settings_your_share,
              style: Theme
                  .of(context)
                  .textTheme
                  .headlineSmall),
          FutureBuilder<List<SharedHistory>>(
              future: dp.loadSharedHistory(),
              builder: (ctx, snap) {
                if (!snap.hasData) {
                  return const CircularProgressIndicator();
                }
                if (snap.data!.isEmpty) {
                  return Text(
                      I18N.current.history_sharing_settings_no_shared);
                }
                return SizedBox(
                  width: double.infinity,
                  child: ListView.builder(
                      shrinkWrap: true,
                      itemCount: snap.data?.length ?? 0,
                      itemBuilder: (ctx, index) {
                        var data = snap.data![index];
                        return Card(
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.stretch,
                              children: [
                                Row(
                                  mainAxisAlignment:
                                  MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(
                                      data.toName,
                                      style: const TextStyle(
                                          fontSize: 20,
                                          fontWeight: FontWeight.bold),
                                      textAlign: TextAlign.left,
                                    ),
                                    if (data.state == "PENDING")
                                      Text(
                                        I18N.current
                                            .history_sharing_settings_PENDING,
                                        style: const TextStyle(color: Colors.orange),
                                        textAlign: TextAlign.left,
                                      )
                                    else
                                      Text(
                                        I18N.current
                                            .history_sharing_settings_ACTIVE,
                                        style: const TextStyle(color: Colors.green),
                                        textAlign: TextAlign.left,
                                      ),
                                  ],
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: [
                                    TextButton(
                                        onPressed: () {
                                          dp
                                              .revokeSharingHistoryAccess(
                                              data.id, data.securityToken)
                                              .then((value) {
                                            setState(() {});
                                          });
                                        },
                                        child: Text(I18N.current.revoke))
                                  ],
                                )
                              ],
                            ),
                          ),
                        );
                      }),
                );
              }),
          Text(I18N.current.history_sharing_settings_your_access,
              style: Theme
                  .of(context)
                  .textTheme
                  .headlineSmall),
          FutureBuilder<List<SharedHistoryAccess>>(
              future: dp.loadSharedHistoryAccess(doNotifyListeners: false),
              builder: (ctx, snap) {
                if (!snap.hasData) {
                  return const CircularProgressIndicator();
                }
                if (snap.data!.isEmpty) {
                  return Text(
                      I18N.current.history_sharing_settings_no_share_access);
                }
                return SizedBox(
                  width: double.infinity,
                  child: ListView.builder(
                      shrinkWrap: true,
                      itemCount: snap.data?.length ?? 0,
                      itemBuilder: (ctx, index) {
                        var data = snap.data![index];
                        return Card(
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.stretch,
                                  children: [
                                    Row(
                                        mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text(
                                            data.fromName,
                                            style: const TextStyle(
                                                fontSize: 20,
                                                fontWeight: FontWeight.bold),
                                            textAlign: TextAlign.left,
                                          ),
                                        ]),
                                    Row(
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      children: [
                                        TextButton(
                                            onPressed: () {
                                              dp
                                                  .revokeSharingHistoryAccess(
                                                  data.id, data.securityToken)
                                                  .then((value) {
                                                setState(() {});
                                              });
                                            },
                                            child: Text(I18N.current.remove))
                                      ],
                                    )
                                  ]),
                            ));
                      }),
                );
              })
        ],
      ),
    );
  }
}