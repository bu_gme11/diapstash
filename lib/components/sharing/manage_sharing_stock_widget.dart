import 'package:diap_stash/common.dart';
import 'package:diap_stash/object/interfaces.dart';
import 'package:diap_stash/providers/diaper_provider.dart';

class ManageStockSharingWidget extends StatefulWidget {
  const ManageStockSharingWidget({super.key});

  @override
  State<ManageStockSharingWidget> createState() =>
      _ManageStockSharingWidgetState();
}

class _ManageStockSharingWidgetState extends State<ManageStockSharingWidget> {
  @override
  Widget build(BuildContext context) {
    DiaperProvider dp = Provider.of<DiaperProvider>(context, listen: false);

    return SizedBox(
      width: double.infinity,
      child: Column(
        children: [
          Text(I18N.current.stock_sharing_settings_your_share,
              style: Theme.of(context).textTheme.headlineSmall),
          FutureBuilder<List<SharedStock>>(
              future: dp.loadSharedStock(),
              builder: (ctx, snap) {
                if (!snap.hasData) {
                  return const CircularProgressIndicator();
                }
                if (snap.data!.isEmpty) {
                  return Text(I18N.current.stock_sharing_settings_no_shared);
                }
                return SizedBox(
                  width: double.infinity,
                  child: ListView.builder(
                      shrinkWrap: true,
                      itemCount: snap.data?.length ?? 0,
                      itemBuilder: (ctx, index) {
                        var data = snap.data![index];
                        return Card(
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.stretch,
                              children: [
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(
                                      data.toName,
                                      style: const TextStyle(
                                          fontSize: 20,
                                          fontWeight: FontWeight.bold),
                                      textAlign: TextAlign.left,
                                    ),
                                    if (data.state == "PENDING")
                                      Text(
                                        I18N.current
                                            .stock_sharing_settings_PENDING,
                                        style: const TextStyle(color: Colors.orange),
                                        textAlign: TextAlign.left,
                                      )
                                    else
                                      Text(
                                        I18N.current
                                            .stock_sharing_settings_ACTIVE,
                                        style: const TextStyle(color: Colors.green),
                                        textAlign: TextAlign.left,
                                      ),
                                  ],
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceAround,
                                  children: [
                                    Text(I18N.current.stock_sharing_read_only),
                                    Switch(
                                        value: data.readOnly,
                                        onChanged: (val) {
                                          data.readOnly = val;
                                          dp.updateSharingStock(data).then((_) {
                                            setState(() {});
                                          });
                                        })
                                  ],
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: [
                                    TextButton(
                                        onPressed: () {
                                          dp
                                              .revokeSharingStockAccess(
                                                  data.id, data.securityToken)
                                              .then((value) {
                                            setState(() {});
                                          });
                                        },
                                        child: Text(I18N.current.revoke))
                                  ],
                                )
                              ],
                            ),
                          ),
                        );
                      }),
                );
              }),
          Text(I18N.current.stock_sharing_settings_your_access,
              style: Theme.of(context).textTheme.headlineSmall),
          FutureBuilder<List<SharedStockAccess>>(
              future: dp.loadSharedStockAccess(doNotifyListeners: false),
              builder: (ctx, snap) {
                if (!snap.hasData) {
                  return const CircularProgressIndicator();
                }
                if (snap.data!.isEmpty) {
                  return Text(
                      I18N.current.stock_sharing_settings_no_share_access);
                }
                return SizedBox(
                  width: double.infinity,
                  child: ListView.builder(
                      shrinkWrap: true,
                      itemCount: snap.data?.length ?? 0,
                      itemBuilder: (ctx, index) {
                        var data = snap.data![index];
                        return Card(
                            child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Column(
                              crossAxisAlignment: CrossAxisAlignment.stretch,
                              children: [
                                Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text(
                                        data.fromName,
                                        style: const TextStyle(
                                            fontSize: 20,
                                            fontWeight: FontWeight.bold),
                                        textAlign: TextAlign.left,
                                      ),
                                    ]),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: [
                                    TextButton(
                                        onPressed: () {
                                          dp
                                              .revokeSharingStockAccess(
                                                  data.id, data.securityToken)
                                              .then((value) {
                                            setState(() {});
                                          });
                                        },
                                        child: Text(I18N.current.remove))
                                  ],
                                )
                              ]),
                        ));
                      }),
                );
              })
        ],
      ),
    );
  }
}
