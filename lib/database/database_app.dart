import 'package:drift/drift.dart';
import 'package:drift_sqflite/drift_sqflite.dart';

QueryExecutor createConnection() {
  return SqfliteQueryExecutor.inDatabaseFolder(path: 'diapstash.db', logStatements: false);
}
