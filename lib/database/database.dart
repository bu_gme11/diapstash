// ignore_for_file: non_constant_identifier_names

import 'dart:convert';

import 'package:diap_stash/database/database_stub.dart'
    if (dart.library.io) 'package:diap_stash/database/database_app.dart'
    if (dart.library.html) 'package:diap_stash/database/database_web.dart';
import 'package:diap_stash/object/interfaces.dart';
import 'package:diap_stash/object/notifications.dart';
import 'package:drift/drift.dart';

import 'dart:developer' as developer;

import 'package:sentry_flutter/sentry_flutter.dart';

part 'database.g.dart';

class DiaperTypeTable extends Table {
  @override
  String get tableName => "DiaperType";

  IntColumn get id => integer()();

  TextColumn get brand_code => text().nullable()();

  TextColumn get name => text()();

  TextColumn get image => text().nullable()();

  TextColumn get availableSizes => text().named("availableSizes").nullable()();

  BoolColumn get official => boolean()();

  TextColumn get type =>
      textEnum<DTType>().withDefault(Constant(DTType.diaper.name))();

  TextColumn get style => textEnum<DiaperStyle>().nullable()();

  TextColumn get target =>
      text().nullable().withDefault(const Constant("[]"))();

  BoolColumn get hidden => boolean().withDefault(const Constant(false))();

  BoolColumn get discontinued => boolean().withDefault(const Constant(false))();

  @override
  Set<Column> get primaryKey => {id};
}

class DiaperBrandTable extends Table {
  @override
  String get tableName => "DiaperBrand";

  TextColumn get code => text()();

  TextColumn get name => text()();

  TextColumn get image => text().nullable()();

  @override
  Set<Column> get primaryKey => {code};
}

class DiaperStockTable extends Table {
  @override
  String get tableName => "DiaperStock";

  IntColumn get type_id => integer()();

  TextColumn get counts => text()();

  TextColumn get prices => text().withDefault(const Constant("{}"))();

  DateTimeColumn get updatedAt => dateTime().nullable()();

  @override
  Set<Column> get primaryKey => {type_id};
}

class SettingsTable extends Table {
  @override
  String get tableName => "SETTINGS";

  TextColumn get name => text()();

  TextColumn get value => text()();

  @override
  Set<Column> get primaryKey => {name};
}

class ChangeTable extends Table {
  @override
  String get tableName => "Change";

  IntColumn get id => integer().autoIncrement()();

  TextColumn get startTime => text().named("startTime").nullable()();

  TextColumn get endTime => text().named("endTime").nullable()();

  TextColumn get note => text().named("note").nullable()();

  TextColumn get state => textEnum<ChangeState>().nullable()();

  RealColumn get wetness => real().named("wetness").nullable()();

  TextColumn get tags =>
      text().named("tags").map(ListInColumnConverter()).nullable()();

  DateTimeColumn get updatedAt =>
      dateTime().named("updatedAt").nullable()();
}

class ListInColumnConverter extends TypeConverter<List<String>, String> {
  @override
  List<String> fromSql(String fromDb) {
    try {
      return List<String>.from(jsonDecode(fromDb));
    } catch (e) {
      Sentry.captureException(e);
      developer.log("Error when convert data to list ($fromDb)", error: e);
    }
    return [];
  }

  @override
  String toSql(List<String> value) {
    return jsonEncode(value);
  }
}

class DiaperChangeTable extends Table {
  @override
  String get tableName => "DiaperChange";

  IntColumn get change_id => integer()();

  IntColumn get type_id => integer()();

  TextColumn get size => text()();

  RealColumn get price => real().nullable()();

  @override
  Set<Column> get primaryKey => {change_id, type_id};
}

class DiaperTypeProposalTable extends Table {
  @override
  String get tableName => "DiaperTypeProposal";

  IntColumn get type_id => integer()();

  TextColumn get proposal_id => text()();

  @override
  Set<Column> get primaryKey => {type_id};
}

class SharedHistoryAccessTable extends Table {
  String get TableName => "SharedHistoryAccessTable";

  IntColumn get id => integer()();

  TextColumn get securityToken => text()();

  TextColumn get fromName => text()();

  @override
  Set<Column> get primaryKey => {id};
}

class SharedStockAccessTable extends Table {
  String get TableName => "SharedStockAccessTable";

  IntColumn get id => integer()();

  TextColumn get securityToken => text()();

  TextColumn get fromName => text()();

  BoolColumn get readonly => boolean().withDefault(const Constant(false))();

  @override
  Set<Column> get primaryKey => {id};
}

class FavoriteTypeTable extends Table {
  @override
  String get tableName => "FavoriteTypeTable";

  IntColumn get type_id => integer().references(DiaperTypeTable, #id)();

  @override
  Set<Column> get primaryKey => {type_id};
}

class NotificationTable extends Table {
  @override
  String get tableName => "notification";

  IntColumn get id => integer().autoIncrement()();

  TextColumn get type => textEnum<NotificationType>()();

  IntColumn get duration => integer()();

  TextColumn get title => text().withDefault(const Constant("value"))();

  TextColumn get description => text().withDefault(const Constant("value"))();

  IntColumn get typeId => integer().nullable()();
}

class TypeInfoTable extends Table {
  @override
  String get tableName => "typeinfo";

  IntColumn get typeId => integer()();

  IntColumn get thresholdLowStock => integer().nullable()();

  TextColumn get note => text().nullable()();

  @override
  Set<Column> get primaryKey => {typeId};
}

class UserRatingTable extends Table {
  @override
  String get tableName => "user_rating";

  TextColumn get id => text()();

  IntColumn get typeId => integer()();

  TextColumn get criteria => text()();

  RealColumn get note => real()();

  DateTimeColumn get updatedAt => dateTime().withDefault(currentDateAndTime)();

  @override
  Set<Column> get primaryKey => {id};

}

@DriftDatabase(tables: [
  DiaperTypeTable,
  DiaperBrandTable,
  DiaperStockTable,
  SettingsTable,
  ChangeTable,
  DiaperChangeTable,
  DiaperTypeProposalTable,
  SharedHistoryAccessTable,
  FavoriteTypeTable,
  NotificationTable,
  SharedStockAccessTable,
  TypeInfoTable,
  UserRatingTable
])
class MyDatabase extends _$MyDatabase {
  MyDatabase._(super.e);

  factory MyDatabase() => MyDatabase._(createConnection());

  @override
  int get schemaVersion => 36;

  @override
  MigrationStrategy get migration {
    return MigrationStrategy(
      beforeOpen: (details) async {
        developer.log(
            "Migration old : ${details.versionBefore} new ${details.versionNow}");
        if (details.hadUpgrade) {
          await Sentry.addBreadcrumb(Breadcrumb(
              category: "info",
              message:
                  "Had Upgrade from ${details.versionBefore} to ${details.versionNow}",
              data: {
                "from": details.versionBefore,
                "to": details.versionNow,
              }));
        }
        try {
          await customUpdate(
              "update ${changeTable.actualTableName} set ${changeTable.updatedAt
                  .name} = null where ${changeTable.updatedAt
                  .name}='updatedAt'");
        }catch(e){
          //nothing
        }
      },
      onCreate: (Migrator m) async {
        await m.createAll();
      },
      onUpgrade: (Migrator m, int from, int to) async {
        /*if (kIsWeb) {
          await migrate(from, to, m);
        } else {
          await transaction(() async {
            await migrate(from, to, m);
          });
        }*/
        await migrate(from, to, m);
      },
    );
  }

  Future<void> migrate(int from, int to, Migrator m) async {
    var span = Sentry.startTransaction("migrate", "Migrate");
    await Sentry.addBreadcrumb(Breadcrumb(
        category: "info",
        message: "Migrate from $from to $to",
        data: {
          "from": from,
          "to": to,
        }));
    try {
      if (from < 2) {
        await m.createTable(settingsTable);
      }
      if (from < 3) {
        //await db.execute("ALTER TABLE  ${DiaperBrand.tableName} ADD image TEXT");
        await m.addColumn(diaperBrandTable, diaperBrandTable.image);
      }
      if (from < 5) {}
      if (from < 6) {
        await m.createTable(changeTable);
        await m.createTable(diaperChangeTable);
      }
      if (from <= 7) {
        await m.createTable(diaperTypeProposalTable);
      }

      if (from <= 11) {
        await m.createTable(sharedHistoryAccessTable);
      }

      if (from <= 12) {
        await m.createTable(favoriteTypeTable);
      }

      if (from <= 13) {
        await safeAddColumn(m, changeTable, changeTable.note);
        await safeAddColumn(m, changeTable, changeTable.state);
      }

      if (from <= 14) {
        await safeAddColumn(m, diaperTypeTable, diaperTypeTable.type);
      }

      if (from <= 15) {
        await m.createTable(notificationTable);
      }

      if (from <= 16) {
        await safeAddColumn(m, diaperStockTable, diaperStockTable.updatedAt);
      }

      if (from <= 17) {
        await m.createTable(sharedStockAccessTable);
      }

      if (from <= 18) {
        await m.createTable(typeInfoTable);
      }

      if (from <= 19) {
        try {
          await safeAddColumn(m, changeTable, changeTable.tags);
        } catch (e) {} // ignore: empty_catches
      }

      if (from <= 20) {
        await safeAddColumn(m, diaperTypeTable, diaperTypeTable.style);
        await safeAddColumn(m, diaperTypeTable, diaperTypeTable.target);
      }

      if (from <= 23) {
        await safeAddColumn(m, diaperTypeTable, diaperTypeTable.hidden);
      }

      if (from <= 24) {
        await safeAddColumn(m, diaperTypeTable, diaperTypeTable.discontinued);
        try {
          await m.alterTable(TableMigration(changeTable));
        } catch (e) {
          /*Sentry.captureMessage("Error on migratation ${e.toString()}", level: SentryLevel.error);
        Sentry.captureException(e, withScope: (scope) {
          scope.setExtra("where", "migration");
          scope.setExtra("from", from);
          scope.setExtra("to", to);
          scope.setExtra("table", "change");
        });*/
        }
      }

      if (from <= 25) {
        await safeAddColumn(m, notificationTable, notificationTable.title);
        await safeAddColumn(
            m, notificationTable, notificationTable.description);
      }
      if (from <= 26) {
        await safeAddColumn(m, notificationTable, notificationTable.typeId);
      }

      if (from <= 27) {
        try {
          await safeAddColumn(m, changeTable, changeTable.wetness);
        } catch (_) {}
      }

      if (from <= 30) {
        try {
          await m.alterTable(TableMigration(changeTable));
        } catch (e) {
          developer.log("Error on alter table table", error: e);
          developer.log(e.toString());
        }
      }

      if (from <= 31) {
        await safeAddColumn(m, changeTable, changeTable.wetness);
        await fixWetnessValueError();
      }

      if(from <= 32){
        await safeAddColumn(m, diaperChangeTable, diaperChangeTable.price);
        await safeAddColumn(m, diaperStockTable, diaperStockTable.prices);
      }

      if(from <= 33){
        await safeAddColumn(m, changeTable, changeTable.updatedAt);
      }

      if(from <= 34){
        await m.createTable(userRatingTable);
      }

      if(from <= 35){
        await m.deleteTable(diaperTypeProposalTable.actualTableName);
        await m.createTable(diaperTypeProposalTable);
      }

      span.finish(status: const SpanStatus.ok());
    } catch (e) {
      developer.log("Error on migrate $from to $to", error: e);
      developer.log(e.toString());

      await Sentry.captureMessage("Error on migratation ${e.toString()}",
          level: SentryLevel.error);
      await Sentry.captureException(e, withScope: (scope) {
        scope.setExtra("where", "migration");
        scope.setExtra("from", from);
        scope.setExtra("to", to);
      });
      await Sentry.addBreadcrumb(Breadcrumb(
          category: "error",
          message: "Error on migratation ${e.toString()}",
          data: {
            "from": from,
            "to": to,
          }));
      span.finish(status: const SpanStatus.internalError());
      rethrow;
    }

    await Sentry.addBreadcrumb(Breadcrumb(
        category: "info",
        message: "End migrate from $from to $to",
        data: {
          "from": from,
          "to": to,
        }));

    developer.log("Migration onUpgrade old : $from new $to");
  }

  Future<bool> columnExist(
      {required String tableName, required String columnName}) async {
    var queryRow = customSelect("PRAGMA table_info($tableName);");
    var val = await queryRow.get();
    for (var col in val) {
      if (col.data["name"] == columnName) {
        return true;
      }
    }

    return false;
  }

  Future<void> safeAddColumn(
      Migrator m, TableInfo table, GeneratedColumn column) async {
    if (!await columnExist(
        tableName: table.actualTableName, columnName: column.name)) {
      try {
        await m.addColumn(table, column);
        developer.log("Column '${column.name}' added on ${table.actualTableName}");
      } catch (e, s) {
        developer.log("Error on add column ${e.toString()}", error: e);
        await Sentry.captureException(e, stackTrace: s, withScope: (scope) {
          scope.setExtra("tableName", table.actualTableName);
          scope.setExtra("columnName", column.name);
        });
        rethrow;
      }
    } else {
      developer.log(
          "Not add column ${column.name} in ${table.actualTableName} because already exists");
    }
  }

  Future<void> fixWetnessValueError() async {
    try {
      var res = customSelect("select * from ${changeTable.actualTableName}");
      var rows = await res.get();
      await transaction(() async {
        for (var row in rows) {
          if (row.data.containsKey("wetness") && row.data["wetness"] != null) {
            var rawVal = row.data["wetness"];
            if (rawVal is double?) {
              continue;
            }
            var val = double.tryParse(rawVal);
            if (val == null) {
              developer.log("Update ${row.data["id"]} to fix wetness");
              double? nullDouble;
              var ok = await (changeTable.update()
                    ..where((tbl) => tbl.id.equals(row.data["id"])))
                  .write(ChangeTableCompanion(wetness: Value(nullDouble)));
              developer
                  .log("Update ${row.data["id"]} to fix wetness : done $ok");
            }
          }
        }
      });
    } catch (e) {
      developer.log("Error on debug, ${e.toString()}", error: e);
    }
  }
}
