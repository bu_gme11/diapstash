// ignore_for_file: avoid_print

import 'package:drift/drift.dart';
import 'package:drift/wasm.dart';
// ignore: depend_on_referenced_packages
import 'package:sqlite3/wasm.dart';

QueryExecutor createConnection() {
  return DatabaseConnection.delayed(Future(() async {
    print("Init DB");
    final result = await WasmDatabase.open(
        databaseName: 'diapstash', // prefer to only use valid identifiers here
        sqlite3Uri: Uri.parse('/sqlite3.wasm'),
        driftWorkerUri: Uri.parse('/drift_worker.dart.js'),
        initializeDatabase: () async {
          print('init db');
          Uint8List? oldDatabase;
          try {
            final fs = await IndexedDbFileSystem.open(dbName: 'diapstash');
            const oldPath =
                '/drift/diapstash/app.db'; // The path passed to WasmDatabase before


            // Check if the old database exists
            if (fs.xAccess(oldPath, 0) != 0) {
              // It does, then copy the old file
              var xx = fs.xOpen(Sqlite3Filename(oldPath), 0);
              var file = xx.file;
              final blob = Uint8List(file.xFileSize());
              file.xRead(blob, 0);
              oldDatabase = blob;
            } else {
              print("No old db ?");
            }

            await fs.close();
          }catch(e){
            print("Error on init new db");
            print(e.toString());
          }

          return oldDatabase;
        });

    if (result.missingFeatures.isNotEmpty) {
      // Depending how central local persistence is to your app, you may want
      // to show a warning to the user if only unrealiable implemetentations
      // are available.
      print('Using ${result.chosenImplementation} due to missing browser '
          'features: ${result.missingFeatures}');
    }

    return result.resolvedExecutor;
  }));
}
