// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'database.dart';

// ignore_for_file: type=lint
class $DiaperTypeTableTable extends DiaperTypeTable
    with TableInfo<$DiaperTypeTableTable, DiaperTypeTableData> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  $DiaperTypeTableTable(this.attachedDatabase, [this._alias]);
  static const VerificationMeta _idMeta = const VerificationMeta('id');
  @override
  late final GeneratedColumn<int> id = GeneratedColumn<int>(
      'id', aliasedName, false,
      type: DriftSqlType.int, requiredDuringInsert: false);
  static const VerificationMeta _brand_codeMeta =
      const VerificationMeta('brand_code');
  @override
  late final GeneratedColumn<String> brand_code = GeneratedColumn<String>(
      'brand_code', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _nameMeta = const VerificationMeta('name');
  @override
  late final GeneratedColumn<String> name = GeneratedColumn<String>(
      'name', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  static const VerificationMeta _imageMeta = const VerificationMeta('image');
  @override
  late final GeneratedColumn<String> image = GeneratedColumn<String>(
      'image', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _availableSizesMeta =
      const VerificationMeta('availableSizes');
  @override
  late final GeneratedColumn<String> availableSizes = GeneratedColumn<String>(
      'availableSizes', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _officialMeta =
      const VerificationMeta('official');
  @override
  late final GeneratedColumn<bool> official = GeneratedColumn<bool>(
      'official', aliasedName, false,
      type: DriftSqlType.bool,
      requiredDuringInsert: true,
      defaultConstraints:
          GeneratedColumn.constraintIsAlways('CHECK ("official" IN (0, 1))'));
  static const VerificationMeta _typeMeta = const VerificationMeta('type');
  @override
  late final GeneratedColumnWithTypeConverter<DTType, String> type =
      GeneratedColumn<String>('type', aliasedName, false,
              type: DriftSqlType.string,
              requiredDuringInsert: false,
              defaultValue: Constant(DTType.diaper.name))
          .withConverter<DTType>($DiaperTypeTableTable.$convertertype);
  static const VerificationMeta _styleMeta = const VerificationMeta('style');
  @override
  late final GeneratedColumnWithTypeConverter<DiaperStyle?, String> style =
      GeneratedColumn<String>('style', aliasedName, true,
              type: DriftSqlType.string, requiredDuringInsert: false)
          .withConverter<DiaperStyle?>($DiaperTypeTableTable.$converterstylen);
  static const VerificationMeta _targetMeta = const VerificationMeta('target');
  @override
  late final GeneratedColumn<String> target = GeneratedColumn<String>(
      'target', aliasedName, true,
      type: DriftSqlType.string,
      requiredDuringInsert: false,
      defaultValue: const Constant("[]"));
  static const VerificationMeta _hiddenMeta = const VerificationMeta('hidden');
  @override
  late final GeneratedColumn<bool> hidden = GeneratedColumn<bool>(
      'hidden', aliasedName, false,
      type: DriftSqlType.bool,
      requiredDuringInsert: false,
      defaultConstraints:
          GeneratedColumn.constraintIsAlways('CHECK ("hidden" IN (0, 1))'),
      defaultValue: const Constant(false));
  static const VerificationMeta _discontinuedMeta =
      const VerificationMeta('discontinued');
  @override
  late final GeneratedColumn<bool> discontinued = GeneratedColumn<bool>(
      'discontinued', aliasedName, false,
      type: DriftSqlType.bool,
      requiredDuringInsert: false,
      defaultConstraints: GeneratedColumn.constraintIsAlways(
          'CHECK ("discontinued" IN (0, 1))'),
      defaultValue: const Constant(false));
  @override
  List<GeneratedColumn> get $columns => [
        id,
        brand_code,
        name,
        image,
        availableSizes,
        official,
        type,
        style,
        target,
        hidden,
        discontinued
      ];
  @override
  String get aliasedName => _alias ?? actualTableName;
  @override
  String get actualTableName => $name;
  static const String $name = 'DiaperType';
  @override
  VerificationContext validateIntegrity(
      Insertable<DiaperTypeTableData> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id']!, _idMeta));
    }
    if (data.containsKey('brand_code')) {
      context.handle(
          _brand_codeMeta,
          brand_code.isAcceptableOrUnknown(
              data['brand_code']!, _brand_codeMeta));
    }
    if (data.containsKey('name')) {
      context.handle(
          _nameMeta, name.isAcceptableOrUnknown(data['name']!, _nameMeta));
    } else if (isInserting) {
      context.missing(_nameMeta);
    }
    if (data.containsKey('image')) {
      context.handle(
          _imageMeta, image.isAcceptableOrUnknown(data['image']!, _imageMeta));
    }
    if (data.containsKey('availableSizes')) {
      context.handle(
          _availableSizesMeta,
          availableSizes.isAcceptableOrUnknown(
              data['availableSizes']!, _availableSizesMeta));
    }
    if (data.containsKey('official')) {
      context.handle(_officialMeta,
          official.isAcceptableOrUnknown(data['official']!, _officialMeta));
    } else if (isInserting) {
      context.missing(_officialMeta);
    }
    context.handle(_typeMeta, const VerificationResult.success());
    context.handle(_styleMeta, const VerificationResult.success());
    if (data.containsKey('target')) {
      context.handle(_targetMeta,
          target.isAcceptableOrUnknown(data['target']!, _targetMeta));
    }
    if (data.containsKey('hidden')) {
      context.handle(_hiddenMeta,
          hidden.isAcceptableOrUnknown(data['hidden']!, _hiddenMeta));
    }
    if (data.containsKey('discontinued')) {
      context.handle(
          _discontinuedMeta,
          discontinued.isAcceptableOrUnknown(
              data['discontinued']!, _discontinuedMeta));
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  DiaperTypeTableData map(Map<String, dynamic> data, {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return DiaperTypeTableData(
      id: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}id'])!,
      brand_code: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}brand_code']),
      name: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}name'])!,
      image: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}image']),
      availableSizes: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}availableSizes']),
      official: attachedDatabase.typeMapping
          .read(DriftSqlType.bool, data['${effectivePrefix}official'])!,
      type: $DiaperTypeTableTable.$convertertype.fromSql(attachedDatabase
          .typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}type'])!),
      style: $DiaperTypeTableTable.$converterstylen.fromSql(attachedDatabase
          .typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}style'])),
      target: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}target']),
      hidden: attachedDatabase.typeMapping
          .read(DriftSqlType.bool, data['${effectivePrefix}hidden'])!,
      discontinued: attachedDatabase.typeMapping
          .read(DriftSqlType.bool, data['${effectivePrefix}discontinued'])!,
    );
  }

  @override
  $DiaperTypeTableTable createAlias(String alias) {
    return $DiaperTypeTableTable(attachedDatabase, alias);
  }

  static JsonTypeConverter2<DTType, String, String> $convertertype =
      const EnumNameConverter<DTType>(DTType.values);
  static JsonTypeConverter2<DiaperStyle, String, String> $converterstyle =
      const EnumNameConverter<DiaperStyle>(DiaperStyle.values);
  static JsonTypeConverter2<DiaperStyle?, String?, String?> $converterstylen =
      JsonTypeConverter2.asNullable($converterstyle);
}

class DiaperTypeTableData extends DataClass
    implements Insertable<DiaperTypeTableData> {
  final int id;
  final String? brand_code;
  final String name;
  final String? image;
  final String? availableSizes;
  final bool official;
  final DTType type;
  final DiaperStyle? style;
  final String? target;
  final bool hidden;
  final bool discontinued;
  const DiaperTypeTableData(
      {required this.id,
      this.brand_code,
      required this.name,
      this.image,
      this.availableSizes,
      required this.official,
      required this.type,
      this.style,
      this.target,
      required this.hidden,
      required this.discontinued});
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['id'] = Variable<int>(id);
    if (!nullToAbsent || brand_code != null) {
      map['brand_code'] = Variable<String>(brand_code);
    }
    map['name'] = Variable<String>(name);
    if (!nullToAbsent || image != null) {
      map['image'] = Variable<String>(image);
    }
    if (!nullToAbsent || availableSizes != null) {
      map['availableSizes'] = Variable<String>(availableSizes);
    }
    map['official'] = Variable<bool>(official);
    {
      final converter = $DiaperTypeTableTable.$convertertype;
      map['type'] = Variable<String>(converter.toSql(type));
    }
    if (!nullToAbsent || style != null) {
      final converter = $DiaperTypeTableTable.$converterstylen;
      map['style'] = Variable<String>(converter.toSql(style));
    }
    if (!nullToAbsent || target != null) {
      map['target'] = Variable<String>(target);
    }
    map['hidden'] = Variable<bool>(hidden);
    map['discontinued'] = Variable<bool>(discontinued);
    return map;
  }

  DiaperTypeTableCompanion toCompanion(bool nullToAbsent) {
    return DiaperTypeTableCompanion(
      id: Value(id),
      brand_code: brand_code == null && nullToAbsent
          ? const Value.absent()
          : Value(brand_code),
      name: Value(name),
      image:
          image == null && nullToAbsent ? const Value.absent() : Value(image),
      availableSizes: availableSizes == null && nullToAbsent
          ? const Value.absent()
          : Value(availableSizes),
      official: Value(official),
      type: Value(type),
      style:
          style == null && nullToAbsent ? const Value.absent() : Value(style),
      target:
          target == null && nullToAbsent ? const Value.absent() : Value(target),
      hidden: Value(hidden),
      discontinued: Value(discontinued),
    );
  }

  factory DiaperTypeTableData.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return DiaperTypeTableData(
      id: serializer.fromJson<int>(json['id']),
      brand_code: serializer.fromJson<String?>(json['brand_code']),
      name: serializer.fromJson<String>(json['name']),
      image: serializer.fromJson<String?>(json['image']),
      availableSizes: serializer.fromJson<String?>(json['availableSizes']),
      official: serializer.fromJson<bool>(json['official']),
      type: $DiaperTypeTableTable.$convertertype
          .fromJson(serializer.fromJson<String>(json['type'])),
      style: $DiaperTypeTableTable.$converterstylen
          .fromJson(serializer.fromJson<String?>(json['style'])),
      target: serializer.fromJson<String?>(json['target']),
      hidden: serializer.fromJson<bool>(json['hidden']),
      discontinued: serializer.fromJson<bool>(json['discontinued']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'brand_code': serializer.toJson<String?>(brand_code),
      'name': serializer.toJson<String>(name),
      'image': serializer.toJson<String?>(image),
      'availableSizes': serializer.toJson<String?>(availableSizes),
      'official': serializer.toJson<bool>(official),
      'type': serializer
          .toJson<String>($DiaperTypeTableTable.$convertertype.toJson(type)),
      'style': serializer.toJson<String?>(
          $DiaperTypeTableTable.$converterstylen.toJson(style)),
      'target': serializer.toJson<String?>(target),
      'hidden': serializer.toJson<bool>(hidden),
      'discontinued': serializer.toJson<bool>(discontinued),
    };
  }

  DiaperTypeTableData copyWith(
          {int? id,
          Value<String?> brand_code = const Value.absent(),
          String? name,
          Value<String?> image = const Value.absent(),
          Value<String?> availableSizes = const Value.absent(),
          bool? official,
          DTType? type,
          Value<DiaperStyle?> style = const Value.absent(),
          Value<String?> target = const Value.absent(),
          bool? hidden,
          bool? discontinued}) =>
      DiaperTypeTableData(
        id: id ?? this.id,
        brand_code: brand_code.present ? brand_code.value : this.brand_code,
        name: name ?? this.name,
        image: image.present ? image.value : this.image,
        availableSizes:
            availableSizes.present ? availableSizes.value : this.availableSizes,
        official: official ?? this.official,
        type: type ?? this.type,
        style: style.present ? style.value : this.style,
        target: target.present ? target.value : this.target,
        hidden: hidden ?? this.hidden,
        discontinued: discontinued ?? this.discontinued,
      );
  @override
  String toString() {
    return (StringBuffer('DiaperTypeTableData(')
          ..write('id: $id, ')
          ..write('brand_code: $brand_code, ')
          ..write('name: $name, ')
          ..write('image: $image, ')
          ..write('availableSizes: $availableSizes, ')
          ..write('official: $official, ')
          ..write('type: $type, ')
          ..write('style: $style, ')
          ..write('target: $target, ')
          ..write('hidden: $hidden, ')
          ..write('discontinued: $discontinued')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(id, brand_code, name, image, availableSizes,
      official, type, style, target, hidden, discontinued);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is DiaperTypeTableData &&
          other.id == this.id &&
          other.brand_code == this.brand_code &&
          other.name == this.name &&
          other.image == this.image &&
          other.availableSizes == this.availableSizes &&
          other.official == this.official &&
          other.type == this.type &&
          other.style == this.style &&
          other.target == this.target &&
          other.hidden == this.hidden &&
          other.discontinued == this.discontinued);
}

class DiaperTypeTableCompanion extends UpdateCompanion<DiaperTypeTableData> {
  final Value<int> id;
  final Value<String?> brand_code;
  final Value<String> name;
  final Value<String?> image;
  final Value<String?> availableSizes;
  final Value<bool> official;
  final Value<DTType> type;
  final Value<DiaperStyle?> style;
  final Value<String?> target;
  final Value<bool> hidden;
  final Value<bool> discontinued;
  const DiaperTypeTableCompanion({
    this.id = const Value.absent(),
    this.brand_code = const Value.absent(),
    this.name = const Value.absent(),
    this.image = const Value.absent(),
    this.availableSizes = const Value.absent(),
    this.official = const Value.absent(),
    this.type = const Value.absent(),
    this.style = const Value.absent(),
    this.target = const Value.absent(),
    this.hidden = const Value.absent(),
    this.discontinued = const Value.absent(),
  });
  DiaperTypeTableCompanion.insert({
    this.id = const Value.absent(),
    this.brand_code = const Value.absent(),
    required String name,
    this.image = const Value.absent(),
    this.availableSizes = const Value.absent(),
    required bool official,
    this.type = const Value.absent(),
    this.style = const Value.absent(),
    this.target = const Value.absent(),
    this.hidden = const Value.absent(),
    this.discontinued = const Value.absent(),
  })  : name = Value(name),
        official = Value(official);
  static Insertable<DiaperTypeTableData> custom({
    Expression<int>? id,
    Expression<String>? brand_code,
    Expression<String>? name,
    Expression<String>? image,
    Expression<String>? availableSizes,
    Expression<bool>? official,
    Expression<String>? type,
    Expression<String>? style,
    Expression<String>? target,
    Expression<bool>? hidden,
    Expression<bool>? discontinued,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (brand_code != null) 'brand_code': brand_code,
      if (name != null) 'name': name,
      if (image != null) 'image': image,
      if (availableSizes != null) 'availableSizes': availableSizes,
      if (official != null) 'official': official,
      if (type != null) 'type': type,
      if (style != null) 'style': style,
      if (target != null) 'target': target,
      if (hidden != null) 'hidden': hidden,
      if (discontinued != null) 'discontinued': discontinued,
    });
  }

  DiaperTypeTableCompanion copyWith(
      {Value<int>? id,
      Value<String?>? brand_code,
      Value<String>? name,
      Value<String?>? image,
      Value<String?>? availableSizes,
      Value<bool>? official,
      Value<DTType>? type,
      Value<DiaperStyle?>? style,
      Value<String?>? target,
      Value<bool>? hidden,
      Value<bool>? discontinued}) {
    return DiaperTypeTableCompanion(
      id: id ?? this.id,
      brand_code: brand_code ?? this.brand_code,
      name: name ?? this.name,
      image: image ?? this.image,
      availableSizes: availableSizes ?? this.availableSizes,
      official: official ?? this.official,
      type: type ?? this.type,
      style: style ?? this.style,
      target: target ?? this.target,
      hidden: hidden ?? this.hidden,
      discontinued: discontinued ?? this.discontinued,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (brand_code.present) {
      map['brand_code'] = Variable<String>(brand_code.value);
    }
    if (name.present) {
      map['name'] = Variable<String>(name.value);
    }
    if (image.present) {
      map['image'] = Variable<String>(image.value);
    }
    if (availableSizes.present) {
      map['availableSizes'] = Variable<String>(availableSizes.value);
    }
    if (official.present) {
      map['official'] = Variable<bool>(official.value);
    }
    if (type.present) {
      final converter = $DiaperTypeTableTable.$convertertype;

      map['type'] = Variable<String>(converter.toSql(type.value));
    }
    if (style.present) {
      final converter = $DiaperTypeTableTable.$converterstylen;

      map['style'] = Variable<String>(converter.toSql(style.value));
    }
    if (target.present) {
      map['target'] = Variable<String>(target.value);
    }
    if (hidden.present) {
      map['hidden'] = Variable<bool>(hidden.value);
    }
    if (discontinued.present) {
      map['discontinued'] = Variable<bool>(discontinued.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('DiaperTypeTableCompanion(')
          ..write('id: $id, ')
          ..write('brand_code: $brand_code, ')
          ..write('name: $name, ')
          ..write('image: $image, ')
          ..write('availableSizes: $availableSizes, ')
          ..write('official: $official, ')
          ..write('type: $type, ')
          ..write('style: $style, ')
          ..write('target: $target, ')
          ..write('hidden: $hidden, ')
          ..write('discontinued: $discontinued')
          ..write(')'))
        .toString();
  }
}

class $DiaperBrandTableTable extends DiaperBrandTable
    with TableInfo<$DiaperBrandTableTable, DiaperBrandTableData> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  $DiaperBrandTableTable(this.attachedDatabase, [this._alias]);
  static const VerificationMeta _codeMeta = const VerificationMeta('code');
  @override
  late final GeneratedColumn<String> code = GeneratedColumn<String>(
      'code', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  static const VerificationMeta _nameMeta = const VerificationMeta('name');
  @override
  late final GeneratedColumn<String> name = GeneratedColumn<String>(
      'name', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  static const VerificationMeta _imageMeta = const VerificationMeta('image');
  @override
  late final GeneratedColumn<String> image = GeneratedColumn<String>(
      'image', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  @override
  List<GeneratedColumn> get $columns => [code, name, image];
  @override
  String get aliasedName => _alias ?? actualTableName;
  @override
  String get actualTableName => $name;
  static const String $name = 'DiaperBrand';
  @override
  VerificationContext validateIntegrity(
      Insertable<DiaperBrandTableData> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('code')) {
      context.handle(
          _codeMeta, code.isAcceptableOrUnknown(data['code']!, _codeMeta));
    } else if (isInserting) {
      context.missing(_codeMeta);
    }
    if (data.containsKey('name')) {
      context.handle(
          _nameMeta, name.isAcceptableOrUnknown(data['name']!, _nameMeta));
    } else if (isInserting) {
      context.missing(_nameMeta);
    }
    if (data.containsKey('image')) {
      context.handle(
          _imageMeta, image.isAcceptableOrUnknown(data['image']!, _imageMeta));
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {code};
  @override
  DiaperBrandTableData map(Map<String, dynamic> data, {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return DiaperBrandTableData(
      code: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}code'])!,
      name: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}name'])!,
      image: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}image']),
    );
  }

  @override
  $DiaperBrandTableTable createAlias(String alias) {
    return $DiaperBrandTableTable(attachedDatabase, alias);
  }
}

class DiaperBrandTableData extends DataClass
    implements Insertable<DiaperBrandTableData> {
  final String code;
  final String name;
  final String? image;
  const DiaperBrandTableData(
      {required this.code, required this.name, this.image});
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['code'] = Variable<String>(code);
    map['name'] = Variable<String>(name);
    if (!nullToAbsent || image != null) {
      map['image'] = Variable<String>(image);
    }
    return map;
  }

  DiaperBrandTableCompanion toCompanion(bool nullToAbsent) {
    return DiaperBrandTableCompanion(
      code: Value(code),
      name: Value(name),
      image:
          image == null && nullToAbsent ? const Value.absent() : Value(image),
    );
  }

  factory DiaperBrandTableData.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return DiaperBrandTableData(
      code: serializer.fromJson<String>(json['code']),
      name: serializer.fromJson<String>(json['name']),
      image: serializer.fromJson<String?>(json['image']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'code': serializer.toJson<String>(code),
      'name': serializer.toJson<String>(name),
      'image': serializer.toJson<String?>(image),
    };
  }

  DiaperBrandTableData copyWith(
          {String? code,
          String? name,
          Value<String?> image = const Value.absent()}) =>
      DiaperBrandTableData(
        code: code ?? this.code,
        name: name ?? this.name,
        image: image.present ? image.value : this.image,
      );
  @override
  String toString() {
    return (StringBuffer('DiaperBrandTableData(')
          ..write('code: $code, ')
          ..write('name: $name, ')
          ..write('image: $image')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(code, name, image);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is DiaperBrandTableData &&
          other.code == this.code &&
          other.name == this.name &&
          other.image == this.image);
}

class DiaperBrandTableCompanion extends UpdateCompanion<DiaperBrandTableData> {
  final Value<String> code;
  final Value<String> name;
  final Value<String?> image;
  final Value<int> rowid;
  const DiaperBrandTableCompanion({
    this.code = const Value.absent(),
    this.name = const Value.absent(),
    this.image = const Value.absent(),
    this.rowid = const Value.absent(),
  });
  DiaperBrandTableCompanion.insert({
    required String code,
    required String name,
    this.image = const Value.absent(),
    this.rowid = const Value.absent(),
  })  : code = Value(code),
        name = Value(name);
  static Insertable<DiaperBrandTableData> custom({
    Expression<String>? code,
    Expression<String>? name,
    Expression<String>? image,
    Expression<int>? rowid,
  }) {
    return RawValuesInsertable({
      if (code != null) 'code': code,
      if (name != null) 'name': name,
      if (image != null) 'image': image,
      if (rowid != null) 'rowid': rowid,
    });
  }

  DiaperBrandTableCompanion copyWith(
      {Value<String>? code,
      Value<String>? name,
      Value<String?>? image,
      Value<int>? rowid}) {
    return DiaperBrandTableCompanion(
      code: code ?? this.code,
      name: name ?? this.name,
      image: image ?? this.image,
      rowid: rowid ?? this.rowid,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (code.present) {
      map['code'] = Variable<String>(code.value);
    }
    if (name.present) {
      map['name'] = Variable<String>(name.value);
    }
    if (image.present) {
      map['image'] = Variable<String>(image.value);
    }
    if (rowid.present) {
      map['rowid'] = Variable<int>(rowid.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('DiaperBrandTableCompanion(')
          ..write('code: $code, ')
          ..write('name: $name, ')
          ..write('image: $image, ')
          ..write('rowid: $rowid')
          ..write(')'))
        .toString();
  }
}

class $DiaperStockTableTable extends DiaperStockTable
    with TableInfo<$DiaperStockTableTable, DiaperStockTableData> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  $DiaperStockTableTable(this.attachedDatabase, [this._alias]);
  static const VerificationMeta _type_idMeta =
      const VerificationMeta('type_id');
  @override
  late final GeneratedColumn<int> type_id = GeneratedColumn<int>(
      'type_id', aliasedName, false,
      type: DriftSqlType.int, requiredDuringInsert: false);
  static const VerificationMeta _countsMeta = const VerificationMeta('counts');
  @override
  late final GeneratedColumn<String> counts = GeneratedColumn<String>(
      'counts', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  static const VerificationMeta _pricesMeta = const VerificationMeta('prices');
  @override
  late final GeneratedColumn<String> prices = GeneratedColumn<String>(
      'prices', aliasedName, false,
      type: DriftSqlType.string,
      requiredDuringInsert: false,
      defaultValue: const Constant("{}"));
  static const VerificationMeta _updatedAtMeta =
      const VerificationMeta('updatedAt');
  @override
  late final GeneratedColumn<DateTime> updatedAt = GeneratedColumn<DateTime>(
      'updated_at', aliasedName, true,
      type: DriftSqlType.dateTime, requiredDuringInsert: false);
  @override
  List<GeneratedColumn> get $columns => [type_id, counts, prices, updatedAt];
  @override
  String get aliasedName => _alias ?? actualTableName;
  @override
  String get actualTableName => $name;
  static const String $name = 'DiaperStock';
  @override
  VerificationContext validateIntegrity(
      Insertable<DiaperStockTableData> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('type_id')) {
      context.handle(_type_idMeta,
          type_id.isAcceptableOrUnknown(data['type_id']!, _type_idMeta));
    }
    if (data.containsKey('counts')) {
      context.handle(_countsMeta,
          counts.isAcceptableOrUnknown(data['counts']!, _countsMeta));
    } else if (isInserting) {
      context.missing(_countsMeta);
    }
    if (data.containsKey('prices')) {
      context.handle(_pricesMeta,
          prices.isAcceptableOrUnknown(data['prices']!, _pricesMeta));
    }
    if (data.containsKey('updated_at')) {
      context.handle(_updatedAtMeta,
          updatedAt.isAcceptableOrUnknown(data['updated_at']!, _updatedAtMeta));
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {type_id};
  @override
  DiaperStockTableData map(Map<String, dynamic> data, {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return DiaperStockTableData(
      type_id: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}type_id'])!,
      counts: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}counts'])!,
      prices: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}prices'])!,
      updatedAt: attachedDatabase.typeMapping
          .read(DriftSqlType.dateTime, data['${effectivePrefix}updated_at']),
    );
  }

  @override
  $DiaperStockTableTable createAlias(String alias) {
    return $DiaperStockTableTable(attachedDatabase, alias);
  }
}

class DiaperStockTableData extends DataClass
    implements Insertable<DiaperStockTableData> {
  final int type_id;
  final String counts;
  final String prices;
  final DateTime? updatedAt;
  const DiaperStockTableData(
      {required this.type_id,
      required this.counts,
      required this.prices,
      this.updatedAt});
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['type_id'] = Variable<int>(type_id);
    map['counts'] = Variable<String>(counts);
    map['prices'] = Variable<String>(prices);
    if (!nullToAbsent || updatedAt != null) {
      map['updated_at'] = Variable<DateTime>(updatedAt);
    }
    return map;
  }

  DiaperStockTableCompanion toCompanion(bool nullToAbsent) {
    return DiaperStockTableCompanion(
      type_id: Value(type_id),
      counts: Value(counts),
      prices: Value(prices),
      updatedAt: updatedAt == null && nullToAbsent
          ? const Value.absent()
          : Value(updatedAt),
    );
  }

  factory DiaperStockTableData.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return DiaperStockTableData(
      type_id: serializer.fromJson<int>(json['type_id']),
      counts: serializer.fromJson<String>(json['counts']),
      prices: serializer.fromJson<String>(json['prices']),
      updatedAt: serializer.fromJson<DateTime?>(json['updatedAt']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'type_id': serializer.toJson<int>(type_id),
      'counts': serializer.toJson<String>(counts),
      'prices': serializer.toJson<String>(prices),
      'updatedAt': serializer.toJson<DateTime?>(updatedAt),
    };
  }

  DiaperStockTableData copyWith(
          {int? type_id,
          String? counts,
          String? prices,
          Value<DateTime?> updatedAt = const Value.absent()}) =>
      DiaperStockTableData(
        type_id: type_id ?? this.type_id,
        counts: counts ?? this.counts,
        prices: prices ?? this.prices,
        updatedAt: updatedAt.present ? updatedAt.value : this.updatedAt,
      );
  @override
  String toString() {
    return (StringBuffer('DiaperStockTableData(')
          ..write('type_id: $type_id, ')
          ..write('counts: $counts, ')
          ..write('prices: $prices, ')
          ..write('updatedAt: $updatedAt')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(type_id, counts, prices, updatedAt);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is DiaperStockTableData &&
          other.type_id == this.type_id &&
          other.counts == this.counts &&
          other.prices == this.prices &&
          other.updatedAt == this.updatedAt);
}

class DiaperStockTableCompanion extends UpdateCompanion<DiaperStockTableData> {
  final Value<int> type_id;
  final Value<String> counts;
  final Value<String> prices;
  final Value<DateTime?> updatedAt;
  const DiaperStockTableCompanion({
    this.type_id = const Value.absent(),
    this.counts = const Value.absent(),
    this.prices = const Value.absent(),
    this.updatedAt = const Value.absent(),
  });
  DiaperStockTableCompanion.insert({
    this.type_id = const Value.absent(),
    required String counts,
    this.prices = const Value.absent(),
    this.updatedAt = const Value.absent(),
  }) : counts = Value(counts);
  static Insertable<DiaperStockTableData> custom({
    Expression<int>? type_id,
    Expression<String>? counts,
    Expression<String>? prices,
    Expression<DateTime>? updatedAt,
  }) {
    return RawValuesInsertable({
      if (type_id != null) 'type_id': type_id,
      if (counts != null) 'counts': counts,
      if (prices != null) 'prices': prices,
      if (updatedAt != null) 'updated_at': updatedAt,
    });
  }

  DiaperStockTableCompanion copyWith(
      {Value<int>? type_id,
      Value<String>? counts,
      Value<String>? prices,
      Value<DateTime?>? updatedAt}) {
    return DiaperStockTableCompanion(
      type_id: type_id ?? this.type_id,
      counts: counts ?? this.counts,
      prices: prices ?? this.prices,
      updatedAt: updatedAt ?? this.updatedAt,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (type_id.present) {
      map['type_id'] = Variable<int>(type_id.value);
    }
    if (counts.present) {
      map['counts'] = Variable<String>(counts.value);
    }
    if (prices.present) {
      map['prices'] = Variable<String>(prices.value);
    }
    if (updatedAt.present) {
      map['updated_at'] = Variable<DateTime>(updatedAt.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('DiaperStockTableCompanion(')
          ..write('type_id: $type_id, ')
          ..write('counts: $counts, ')
          ..write('prices: $prices, ')
          ..write('updatedAt: $updatedAt')
          ..write(')'))
        .toString();
  }
}

class $SettingsTableTable extends SettingsTable
    with TableInfo<$SettingsTableTable, SettingsTableData> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  $SettingsTableTable(this.attachedDatabase, [this._alias]);
  static const VerificationMeta _nameMeta = const VerificationMeta('name');
  @override
  late final GeneratedColumn<String> name = GeneratedColumn<String>(
      'name', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  static const VerificationMeta _valueMeta = const VerificationMeta('value');
  @override
  late final GeneratedColumn<String> value = GeneratedColumn<String>(
      'value', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  @override
  List<GeneratedColumn> get $columns => [name, value];
  @override
  String get aliasedName => _alias ?? actualTableName;
  @override
  String get actualTableName => $name;
  static const String $name = 'SETTINGS';
  @override
  VerificationContext validateIntegrity(Insertable<SettingsTableData> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('name')) {
      context.handle(
          _nameMeta, name.isAcceptableOrUnknown(data['name']!, _nameMeta));
    } else if (isInserting) {
      context.missing(_nameMeta);
    }
    if (data.containsKey('value')) {
      context.handle(
          _valueMeta, value.isAcceptableOrUnknown(data['value']!, _valueMeta));
    } else if (isInserting) {
      context.missing(_valueMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {name};
  @override
  SettingsTableData map(Map<String, dynamic> data, {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return SettingsTableData(
      name: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}name'])!,
      value: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}value'])!,
    );
  }

  @override
  $SettingsTableTable createAlias(String alias) {
    return $SettingsTableTable(attachedDatabase, alias);
  }
}

class SettingsTableData extends DataClass
    implements Insertable<SettingsTableData> {
  final String name;
  final String value;
  const SettingsTableData({required this.name, required this.value});
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['name'] = Variable<String>(name);
    map['value'] = Variable<String>(value);
    return map;
  }

  SettingsTableCompanion toCompanion(bool nullToAbsent) {
    return SettingsTableCompanion(
      name: Value(name),
      value: Value(value),
    );
  }

  factory SettingsTableData.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return SettingsTableData(
      name: serializer.fromJson<String>(json['name']),
      value: serializer.fromJson<String>(json['value']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'name': serializer.toJson<String>(name),
      'value': serializer.toJson<String>(value),
    };
  }

  SettingsTableData copyWith({String? name, String? value}) =>
      SettingsTableData(
        name: name ?? this.name,
        value: value ?? this.value,
      );
  @override
  String toString() {
    return (StringBuffer('SettingsTableData(')
          ..write('name: $name, ')
          ..write('value: $value')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(name, value);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is SettingsTableData &&
          other.name == this.name &&
          other.value == this.value);
}

class SettingsTableCompanion extends UpdateCompanion<SettingsTableData> {
  final Value<String> name;
  final Value<String> value;
  final Value<int> rowid;
  const SettingsTableCompanion({
    this.name = const Value.absent(),
    this.value = const Value.absent(),
    this.rowid = const Value.absent(),
  });
  SettingsTableCompanion.insert({
    required String name,
    required String value,
    this.rowid = const Value.absent(),
  })  : name = Value(name),
        value = Value(value);
  static Insertable<SettingsTableData> custom({
    Expression<String>? name,
    Expression<String>? value,
    Expression<int>? rowid,
  }) {
    return RawValuesInsertable({
      if (name != null) 'name': name,
      if (value != null) 'value': value,
      if (rowid != null) 'rowid': rowid,
    });
  }

  SettingsTableCompanion copyWith(
      {Value<String>? name, Value<String>? value, Value<int>? rowid}) {
    return SettingsTableCompanion(
      name: name ?? this.name,
      value: value ?? this.value,
      rowid: rowid ?? this.rowid,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (name.present) {
      map['name'] = Variable<String>(name.value);
    }
    if (value.present) {
      map['value'] = Variable<String>(value.value);
    }
    if (rowid.present) {
      map['rowid'] = Variable<int>(rowid.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('SettingsTableCompanion(')
          ..write('name: $name, ')
          ..write('value: $value, ')
          ..write('rowid: $rowid')
          ..write(')'))
        .toString();
  }
}

class $ChangeTableTable extends ChangeTable
    with TableInfo<$ChangeTableTable, ChangeTableData> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  $ChangeTableTable(this.attachedDatabase, [this._alias]);
  static const VerificationMeta _idMeta = const VerificationMeta('id');
  @override
  late final GeneratedColumn<int> id = GeneratedColumn<int>(
      'id', aliasedName, false,
      hasAutoIncrement: true,
      type: DriftSqlType.int,
      requiredDuringInsert: false,
      defaultConstraints:
          GeneratedColumn.constraintIsAlways('PRIMARY KEY AUTOINCREMENT'));
  static const VerificationMeta _startTimeMeta =
      const VerificationMeta('startTime');
  @override
  late final GeneratedColumn<String> startTime = GeneratedColumn<String>(
      'startTime', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _endTimeMeta =
      const VerificationMeta('endTime');
  @override
  late final GeneratedColumn<String> endTime = GeneratedColumn<String>(
      'endTime', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _noteMeta = const VerificationMeta('note');
  @override
  late final GeneratedColumn<String> note = GeneratedColumn<String>(
      'note', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _stateMeta = const VerificationMeta('state');
  @override
  late final GeneratedColumnWithTypeConverter<ChangeState?, String> state =
      GeneratedColumn<String>('state', aliasedName, true,
              type: DriftSqlType.string, requiredDuringInsert: false)
          .withConverter<ChangeState?>($ChangeTableTable.$converterstaten);
  static const VerificationMeta _wetnessMeta =
      const VerificationMeta('wetness');
  @override
  late final GeneratedColumn<double> wetness = GeneratedColumn<double>(
      'wetness', aliasedName, true,
      type: DriftSqlType.double, requiredDuringInsert: false);
  static const VerificationMeta _tagsMeta = const VerificationMeta('tags');
  @override
  late final GeneratedColumnWithTypeConverter<List<String>?, String> tags =
      GeneratedColumn<String>('tags', aliasedName, true,
              type: DriftSqlType.string, requiredDuringInsert: false)
          .withConverter<List<String>?>($ChangeTableTable.$convertertagsn);
  static const VerificationMeta _updatedAtMeta =
      const VerificationMeta('updatedAt');
  @override
  late final GeneratedColumn<DateTime> updatedAt = GeneratedColumn<DateTime>(
      'updatedAt', aliasedName, true,
      type: DriftSqlType.dateTime, requiredDuringInsert: false);
  @override
  List<GeneratedColumn> get $columns =>
      [id, startTime, endTime, note, state, wetness, tags, updatedAt];
  @override
  String get aliasedName => _alias ?? actualTableName;
  @override
  String get actualTableName => $name;
  static const String $name = 'Change';
  @override
  VerificationContext validateIntegrity(Insertable<ChangeTableData> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id']!, _idMeta));
    }
    if (data.containsKey('startTime')) {
      context.handle(_startTimeMeta,
          startTime.isAcceptableOrUnknown(data['startTime']!, _startTimeMeta));
    }
    if (data.containsKey('endTime')) {
      context.handle(_endTimeMeta,
          endTime.isAcceptableOrUnknown(data['endTime']!, _endTimeMeta));
    }
    if (data.containsKey('note')) {
      context.handle(
          _noteMeta, note.isAcceptableOrUnknown(data['note']!, _noteMeta));
    }
    context.handle(_stateMeta, const VerificationResult.success());
    if (data.containsKey('wetness')) {
      context.handle(_wetnessMeta,
          wetness.isAcceptableOrUnknown(data['wetness']!, _wetnessMeta));
    }
    context.handle(_tagsMeta, const VerificationResult.success());
    if (data.containsKey('updatedAt')) {
      context.handle(_updatedAtMeta,
          updatedAt.isAcceptableOrUnknown(data['updatedAt']!, _updatedAtMeta));
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  ChangeTableData map(Map<String, dynamic> data, {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return ChangeTableData(
      id: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}id'])!,
      startTime: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}startTime']),
      endTime: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}endTime']),
      note: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}note']),
      state: $ChangeTableTable.$converterstaten.fromSql(attachedDatabase
          .typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}state'])),
      wetness: attachedDatabase.typeMapping
          .read(DriftSqlType.double, data['${effectivePrefix}wetness']),
      tags: $ChangeTableTable.$convertertagsn.fromSql(attachedDatabase
          .typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}tags'])),
      updatedAt: attachedDatabase.typeMapping
          .read(DriftSqlType.dateTime, data['${effectivePrefix}updatedAt']),
    );
  }

  @override
  $ChangeTableTable createAlias(String alias) {
    return $ChangeTableTable(attachedDatabase, alias);
  }

  static JsonTypeConverter2<ChangeState, String, String> $converterstate =
      const EnumNameConverter<ChangeState>(ChangeState.values);
  static JsonTypeConverter2<ChangeState?, String?, String?> $converterstaten =
      JsonTypeConverter2.asNullable($converterstate);
  static TypeConverter<List<String>, String> $convertertags =
      ListInColumnConverter();
  static TypeConverter<List<String>?, String?> $convertertagsn =
      NullAwareTypeConverter.wrap($convertertags);
}

class ChangeTableData extends DataClass implements Insertable<ChangeTableData> {
  final int id;
  final String? startTime;
  final String? endTime;
  final String? note;
  final ChangeState? state;
  final double? wetness;
  final List<String>? tags;
  final DateTime? updatedAt;
  const ChangeTableData(
      {required this.id,
      this.startTime,
      this.endTime,
      this.note,
      this.state,
      this.wetness,
      this.tags,
      this.updatedAt});
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['id'] = Variable<int>(id);
    if (!nullToAbsent || startTime != null) {
      map['startTime'] = Variable<String>(startTime);
    }
    if (!nullToAbsent || endTime != null) {
      map['endTime'] = Variable<String>(endTime);
    }
    if (!nullToAbsent || note != null) {
      map['note'] = Variable<String>(note);
    }
    if (!nullToAbsent || state != null) {
      final converter = $ChangeTableTable.$converterstaten;
      map['state'] = Variable<String>(converter.toSql(state));
    }
    if (!nullToAbsent || wetness != null) {
      map['wetness'] = Variable<double>(wetness);
    }
    if (!nullToAbsent || tags != null) {
      final converter = $ChangeTableTable.$convertertagsn;
      map['tags'] = Variable<String>(converter.toSql(tags));
    }
    if (!nullToAbsent || updatedAt != null) {
      map['updatedAt'] = Variable<DateTime>(updatedAt);
    }
    return map;
  }

  ChangeTableCompanion toCompanion(bool nullToAbsent) {
    return ChangeTableCompanion(
      id: Value(id),
      startTime: startTime == null && nullToAbsent
          ? const Value.absent()
          : Value(startTime),
      endTime: endTime == null && nullToAbsent
          ? const Value.absent()
          : Value(endTime),
      note: note == null && nullToAbsent ? const Value.absent() : Value(note),
      state:
          state == null && nullToAbsent ? const Value.absent() : Value(state),
      wetness: wetness == null && nullToAbsent
          ? const Value.absent()
          : Value(wetness),
      tags: tags == null && nullToAbsent ? const Value.absent() : Value(tags),
      updatedAt: updatedAt == null && nullToAbsent
          ? const Value.absent()
          : Value(updatedAt),
    );
  }

  factory ChangeTableData.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return ChangeTableData(
      id: serializer.fromJson<int>(json['id']),
      startTime: serializer.fromJson<String?>(json['startTime']),
      endTime: serializer.fromJson<String?>(json['endTime']),
      note: serializer.fromJson<String?>(json['note']),
      state: $ChangeTableTable.$converterstaten
          .fromJson(serializer.fromJson<String?>(json['state'])),
      wetness: serializer.fromJson<double?>(json['wetness']),
      tags: serializer.fromJson<List<String>?>(json['tags']),
      updatedAt: serializer.fromJson<DateTime?>(json['updatedAt']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'startTime': serializer.toJson<String?>(startTime),
      'endTime': serializer.toJson<String?>(endTime),
      'note': serializer.toJson<String?>(note),
      'state': serializer
          .toJson<String?>($ChangeTableTable.$converterstaten.toJson(state)),
      'wetness': serializer.toJson<double?>(wetness),
      'tags': serializer.toJson<List<String>?>(tags),
      'updatedAt': serializer.toJson<DateTime?>(updatedAt),
    };
  }

  ChangeTableData copyWith(
          {int? id,
          Value<String?> startTime = const Value.absent(),
          Value<String?> endTime = const Value.absent(),
          Value<String?> note = const Value.absent(),
          Value<ChangeState?> state = const Value.absent(),
          Value<double?> wetness = const Value.absent(),
          Value<List<String>?> tags = const Value.absent(),
          Value<DateTime?> updatedAt = const Value.absent()}) =>
      ChangeTableData(
        id: id ?? this.id,
        startTime: startTime.present ? startTime.value : this.startTime,
        endTime: endTime.present ? endTime.value : this.endTime,
        note: note.present ? note.value : this.note,
        state: state.present ? state.value : this.state,
        wetness: wetness.present ? wetness.value : this.wetness,
        tags: tags.present ? tags.value : this.tags,
        updatedAt: updatedAt.present ? updatedAt.value : this.updatedAt,
      );
  @override
  String toString() {
    return (StringBuffer('ChangeTableData(')
          ..write('id: $id, ')
          ..write('startTime: $startTime, ')
          ..write('endTime: $endTime, ')
          ..write('note: $note, ')
          ..write('state: $state, ')
          ..write('wetness: $wetness, ')
          ..write('tags: $tags, ')
          ..write('updatedAt: $updatedAt')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(
      id, startTime, endTime, note, state, wetness, tags, updatedAt);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is ChangeTableData &&
          other.id == this.id &&
          other.startTime == this.startTime &&
          other.endTime == this.endTime &&
          other.note == this.note &&
          other.state == this.state &&
          other.wetness == this.wetness &&
          other.tags == this.tags &&
          other.updatedAt == this.updatedAt);
}

class ChangeTableCompanion extends UpdateCompanion<ChangeTableData> {
  final Value<int> id;
  final Value<String?> startTime;
  final Value<String?> endTime;
  final Value<String?> note;
  final Value<ChangeState?> state;
  final Value<double?> wetness;
  final Value<List<String>?> tags;
  final Value<DateTime?> updatedAt;
  const ChangeTableCompanion({
    this.id = const Value.absent(),
    this.startTime = const Value.absent(),
    this.endTime = const Value.absent(),
    this.note = const Value.absent(),
    this.state = const Value.absent(),
    this.wetness = const Value.absent(),
    this.tags = const Value.absent(),
    this.updatedAt = const Value.absent(),
  });
  ChangeTableCompanion.insert({
    this.id = const Value.absent(),
    this.startTime = const Value.absent(),
    this.endTime = const Value.absent(),
    this.note = const Value.absent(),
    this.state = const Value.absent(),
    this.wetness = const Value.absent(),
    this.tags = const Value.absent(),
    this.updatedAt = const Value.absent(),
  });
  static Insertable<ChangeTableData> custom({
    Expression<int>? id,
    Expression<String>? startTime,
    Expression<String>? endTime,
    Expression<String>? note,
    Expression<String>? state,
    Expression<double>? wetness,
    Expression<String>? tags,
    Expression<DateTime>? updatedAt,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (startTime != null) 'startTime': startTime,
      if (endTime != null) 'endTime': endTime,
      if (note != null) 'note': note,
      if (state != null) 'state': state,
      if (wetness != null) 'wetness': wetness,
      if (tags != null) 'tags': tags,
      if (updatedAt != null) 'updatedAt': updatedAt,
    });
  }

  ChangeTableCompanion copyWith(
      {Value<int>? id,
      Value<String?>? startTime,
      Value<String?>? endTime,
      Value<String?>? note,
      Value<ChangeState?>? state,
      Value<double?>? wetness,
      Value<List<String>?>? tags,
      Value<DateTime?>? updatedAt}) {
    return ChangeTableCompanion(
      id: id ?? this.id,
      startTime: startTime ?? this.startTime,
      endTime: endTime ?? this.endTime,
      note: note ?? this.note,
      state: state ?? this.state,
      wetness: wetness ?? this.wetness,
      tags: tags ?? this.tags,
      updatedAt: updatedAt ?? this.updatedAt,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (startTime.present) {
      map['startTime'] = Variable<String>(startTime.value);
    }
    if (endTime.present) {
      map['endTime'] = Variable<String>(endTime.value);
    }
    if (note.present) {
      map['note'] = Variable<String>(note.value);
    }
    if (state.present) {
      final converter = $ChangeTableTable.$converterstaten;

      map['state'] = Variable<String>(converter.toSql(state.value));
    }
    if (wetness.present) {
      map['wetness'] = Variable<double>(wetness.value);
    }
    if (tags.present) {
      final converter = $ChangeTableTable.$convertertagsn;

      map['tags'] = Variable<String>(converter.toSql(tags.value));
    }
    if (updatedAt.present) {
      map['updatedAt'] = Variable<DateTime>(updatedAt.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('ChangeTableCompanion(')
          ..write('id: $id, ')
          ..write('startTime: $startTime, ')
          ..write('endTime: $endTime, ')
          ..write('note: $note, ')
          ..write('state: $state, ')
          ..write('wetness: $wetness, ')
          ..write('tags: $tags, ')
          ..write('updatedAt: $updatedAt')
          ..write(')'))
        .toString();
  }
}

class $DiaperChangeTableTable extends DiaperChangeTable
    with TableInfo<$DiaperChangeTableTable, DiaperChangeTableData> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  $DiaperChangeTableTable(this.attachedDatabase, [this._alias]);
  static const VerificationMeta _change_idMeta =
      const VerificationMeta('change_id');
  @override
  late final GeneratedColumn<int> change_id = GeneratedColumn<int>(
      'change_id', aliasedName, false,
      type: DriftSqlType.int, requiredDuringInsert: true);
  static const VerificationMeta _type_idMeta =
      const VerificationMeta('type_id');
  @override
  late final GeneratedColumn<int> type_id = GeneratedColumn<int>(
      'type_id', aliasedName, false,
      type: DriftSqlType.int, requiredDuringInsert: true);
  static const VerificationMeta _sizeMeta = const VerificationMeta('size');
  @override
  late final GeneratedColumn<String> size = GeneratedColumn<String>(
      'size', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  static const VerificationMeta _priceMeta = const VerificationMeta('price');
  @override
  late final GeneratedColumn<double> price = GeneratedColumn<double>(
      'price', aliasedName, true,
      type: DriftSqlType.double, requiredDuringInsert: false);
  @override
  List<GeneratedColumn> get $columns => [change_id, type_id, size, price];
  @override
  String get aliasedName => _alias ?? actualTableName;
  @override
  String get actualTableName => $name;
  static const String $name = 'DiaperChange';
  @override
  VerificationContext validateIntegrity(
      Insertable<DiaperChangeTableData> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('change_id')) {
      context.handle(_change_idMeta,
          change_id.isAcceptableOrUnknown(data['change_id']!, _change_idMeta));
    } else if (isInserting) {
      context.missing(_change_idMeta);
    }
    if (data.containsKey('type_id')) {
      context.handle(_type_idMeta,
          type_id.isAcceptableOrUnknown(data['type_id']!, _type_idMeta));
    } else if (isInserting) {
      context.missing(_type_idMeta);
    }
    if (data.containsKey('size')) {
      context.handle(
          _sizeMeta, size.isAcceptableOrUnknown(data['size']!, _sizeMeta));
    } else if (isInserting) {
      context.missing(_sizeMeta);
    }
    if (data.containsKey('price')) {
      context.handle(
          _priceMeta, price.isAcceptableOrUnknown(data['price']!, _priceMeta));
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {change_id, type_id};
  @override
  DiaperChangeTableData map(Map<String, dynamic> data, {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return DiaperChangeTableData(
      change_id: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}change_id'])!,
      type_id: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}type_id'])!,
      size: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}size'])!,
      price: attachedDatabase.typeMapping
          .read(DriftSqlType.double, data['${effectivePrefix}price']),
    );
  }

  @override
  $DiaperChangeTableTable createAlias(String alias) {
    return $DiaperChangeTableTable(attachedDatabase, alias);
  }
}

class DiaperChangeTableData extends DataClass
    implements Insertable<DiaperChangeTableData> {
  final int change_id;
  final int type_id;
  final String size;
  final double? price;
  const DiaperChangeTableData(
      {required this.change_id,
      required this.type_id,
      required this.size,
      this.price});
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['change_id'] = Variable<int>(change_id);
    map['type_id'] = Variable<int>(type_id);
    map['size'] = Variable<String>(size);
    if (!nullToAbsent || price != null) {
      map['price'] = Variable<double>(price);
    }
    return map;
  }

  DiaperChangeTableCompanion toCompanion(bool nullToAbsent) {
    return DiaperChangeTableCompanion(
      change_id: Value(change_id),
      type_id: Value(type_id),
      size: Value(size),
      price:
          price == null && nullToAbsent ? const Value.absent() : Value(price),
    );
  }

  factory DiaperChangeTableData.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return DiaperChangeTableData(
      change_id: serializer.fromJson<int>(json['change_id']),
      type_id: serializer.fromJson<int>(json['type_id']),
      size: serializer.fromJson<String>(json['size']),
      price: serializer.fromJson<double?>(json['price']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'change_id': serializer.toJson<int>(change_id),
      'type_id': serializer.toJson<int>(type_id),
      'size': serializer.toJson<String>(size),
      'price': serializer.toJson<double?>(price),
    };
  }

  DiaperChangeTableData copyWith(
          {int? change_id,
          int? type_id,
          String? size,
          Value<double?> price = const Value.absent()}) =>
      DiaperChangeTableData(
        change_id: change_id ?? this.change_id,
        type_id: type_id ?? this.type_id,
        size: size ?? this.size,
        price: price.present ? price.value : this.price,
      );
  @override
  String toString() {
    return (StringBuffer('DiaperChangeTableData(')
          ..write('change_id: $change_id, ')
          ..write('type_id: $type_id, ')
          ..write('size: $size, ')
          ..write('price: $price')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(change_id, type_id, size, price);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is DiaperChangeTableData &&
          other.change_id == this.change_id &&
          other.type_id == this.type_id &&
          other.size == this.size &&
          other.price == this.price);
}

class DiaperChangeTableCompanion
    extends UpdateCompanion<DiaperChangeTableData> {
  final Value<int> change_id;
  final Value<int> type_id;
  final Value<String> size;
  final Value<double?> price;
  final Value<int> rowid;
  const DiaperChangeTableCompanion({
    this.change_id = const Value.absent(),
    this.type_id = const Value.absent(),
    this.size = const Value.absent(),
    this.price = const Value.absent(),
    this.rowid = const Value.absent(),
  });
  DiaperChangeTableCompanion.insert({
    required int change_id,
    required int type_id,
    required String size,
    this.price = const Value.absent(),
    this.rowid = const Value.absent(),
  })  : change_id = Value(change_id),
        type_id = Value(type_id),
        size = Value(size);
  static Insertable<DiaperChangeTableData> custom({
    Expression<int>? change_id,
    Expression<int>? type_id,
    Expression<String>? size,
    Expression<double>? price,
    Expression<int>? rowid,
  }) {
    return RawValuesInsertable({
      if (change_id != null) 'change_id': change_id,
      if (type_id != null) 'type_id': type_id,
      if (size != null) 'size': size,
      if (price != null) 'price': price,
      if (rowid != null) 'rowid': rowid,
    });
  }

  DiaperChangeTableCompanion copyWith(
      {Value<int>? change_id,
      Value<int>? type_id,
      Value<String>? size,
      Value<double?>? price,
      Value<int>? rowid}) {
    return DiaperChangeTableCompanion(
      change_id: change_id ?? this.change_id,
      type_id: type_id ?? this.type_id,
      size: size ?? this.size,
      price: price ?? this.price,
      rowid: rowid ?? this.rowid,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (change_id.present) {
      map['change_id'] = Variable<int>(change_id.value);
    }
    if (type_id.present) {
      map['type_id'] = Variable<int>(type_id.value);
    }
    if (size.present) {
      map['size'] = Variable<String>(size.value);
    }
    if (price.present) {
      map['price'] = Variable<double>(price.value);
    }
    if (rowid.present) {
      map['rowid'] = Variable<int>(rowid.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('DiaperChangeTableCompanion(')
          ..write('change_id: $change_id, ')
          ..write('type_id: $type_id, ')
          ..write('size: $size, ')
          ..write('price: $price, ')
          ..write('rowid: $rowid')
          ..write(')'))
        .toString();
  }
}

class $DiaperTypeProposalTableTable extends DiaperTypeProposalTable
    with TableInfo<$DiaperTypeProposalTableTable, DiaperTypeProposalTableData> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  $DiaperTypeProposalTableTable(this.attachedDatabase, [this._alias]);
  static const VerificationMeta _type_idMeta =
      const VerificationMeta('type_id');
  @override
  late final GeneratedColumn<int> type_id = GeneratedColumn<int>(
      'type_id', aliasedName, false,
      type: DriftSqlType.int, requiredDuringInsert: false);
  static const VerificationMeta _proposal_idMeta =
      const VerificationMeta('proposal_id');
  @override
  late final GeneratedColumn<String> proposal_id = GeneratedColumn<String>(
      'proposal_id', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  @override
  List<GeneratedColumn> get $columns => [type_id, proposal_id];
  @override
  String get aliasedName => _alias ?? actualTableName;
  @override
  String get actualTableName => $name;
  static const String $name = 'DiaperTypeProposal';
  @override
  VerificationContext validateIntegrity(
      Insertable<DiaperTypeProposalTableData> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('type_id')) {
      context.handle(_type_idMeta,
          type_id.isAcceptableOrUnknown(data['type_id']!, _type_idMeta));
    }
    if (data.containsKey('proposal_id')) {
      context.handle(
          _proposal_idMeta,
          proposal_id.isAcceptableOrUnknown(
              data['proposal_id']!, _proposal_idMeta));
    } else if (isInserting) {
      context.missing(_proposal_idMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {type_id};
  @override
  DiaperTypeProposalTableData map(Map<String, dynamic> data,
      {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return DiaperTypeProposalTableData(
      type_id: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}type_id'])!,
      proposal_id: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}proposal_id'])!,
    );
  }

  @override
  $DiaperTypeProposalTableTable createAlias(String alias) {
    return $DiaperTypeProposalTableTable(attachedDatabase, alias);
  }
}

class DiaperTypeProposalTableData extends DataClass
    implements Insertable<DiaperTypeProposalTableData> {
  final int type_id;
  final String proposal_id;
  const DiaperTypeProposalTableData(
      {required this.type_id, required this.proposal_id});
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['type_id'] = Variable<int>(type_id);
    map['proposal_id'] = Variable<String>(proposal_id);
    return map;
  }

  DiaperTypeProposalTableCompanion toCompanion(bool nullToAbsent) {
    return DiaperTypeProposalTableCompanion(
      type_id: Value(type_id),
      proposal_id: Value(proposal_id),
    );
  }

  factory DiaperTypeProposalTableData.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return DiaperTypeProposalTableData(
      type_id: serializer.fromJson<int>(json['type_id']),
      proposal_id: serializer.fromJson<String>(json['proposal_id']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'type_id': serializer.toJson<int>(type_id),
      'proposal_id': serializer.toJson<String>(proposal_id),
    };
  }

  DiaperTypeProposalTableData copyWith({int? type_id, String? proposal_id}) =>
      DiaperTypeProposalTableData(
        type_id: type_id ?? this.type_id,
        proposal_id: proposal_id ?? this.proposal_id,
      );
  @override
  String toString() {
    return (StringBuffer('DiaperTypeProposalTableData(')
          ..write('type_id: $type_id, ')
          ..write('proposal_id: $proposal_id')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(type_id, proposal_id);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is DiaperTypeProposalTableData &&
          other.type_id == this.type_id &&
          other.proposal_id == this.proposal_id);
}

class DiaperTypeProposalTableCompanion
    extends UpdateCompanion<DiaperTypeProposalTableData> {
  final Value<int> type_id;
  final Value<String> proposal_id;
  const DiaperTypeProposalTableCompanion({
    this.type_id = const Value.absent(),
    this.proposal_id = const Value.absent(),
  });
  DiaperTypeProposalTableCompanion.insert({
    this.type_id = const Value.absent(),
    required String proposal_id,
  }) : proposal_id = Value(proposal_id);
  static Insertable<DiaperTypeProposalTableData> custom({
    Expression<int>? type_id,
    Expression<String>? proposal_id,
  }) {
    return RawValuesInsertable({
      if (type_id != null) 'type_id': type_id,
      if (proposal_id != null) 'proposal_id': proposal_id,
    });
  }

  DiaperTypeProposalTableCompanion copyWith(
      {Value<int>? type_id, Value<String>? proposal_id}) {
    return DiaperTypeProposalTableCompanion(
      type_id: type_id ?? this.type_id,
      proposal_id: proposal_id ?? this.proposal_id,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (type_id.present) {
      map['type_id'] = Variable<int>(type_id.value);
    }
    if (proposal_id.present) {
      map['proposal_id'] = Variable<String>(proposal_id.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('DiaperTypeProposalTableCompanion(')
          ..write('type_id: $type_id, ')
          ..write('proposal_id: $proposal_id')
          ..write(')'))
        .toString();
  }
}

class $SharedHistoryAccessTableTable extends SharedHistoryAccessTable
    with
        TableInfo<$SharedHistoryAccessTableTable,
            SharedHistoryAccessTableData> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  $SharedHistoryAccessTableTable(this.attachedDatabase, [this._alias]);
  static const VerificationMeta _idMeta = const VerificationMeta('id');
  @override
  late final GeneratedColumn<int> id = GeneratedColumn<int>(
      'id', aliasedName, false,
      type: DriftSqlType.int, requiredDuringInsert: false);
  static const VerificationMeta _securityTokenMeta =
      const VerificationMeta('securityToken');
  @override
  late final GeneratedColumn<String> securityToken = GeneratedColumn<String>(
      'security_token', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  static const VerificationMeta _fromNameMeta =
      const VerificationMeta('fromName');
  @override
  late final GeneratedColumn<String> fromName = GeneratedColumn<String>(
      'from_name', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  @override
  List<GeneratedColumn> get $columns => [id, securityToken, fromName];
  @override
  String get aliasedName => _alias ?? actualTableName;
  @override
  String get actualTableName => $name;
  static const String $name = 'shared_history_access_table';
  @override
  VerificationContext validateIntegrity(
      Insertable<SharedHistoryAccessTableData> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id']!, _idMeta));
    }
    if (data.containsKey('security_token')) {
      context.handle(
          _securityTokenMeta,
          securityToken.isAcceptableOrUnknown(
              data['security_token']!, _securityTokenMeta));
    } else if (isInserting) {
      context.missing(_securityTokenMeta);
    }
    if (data.containsKey('from_name')) {
      context.handle(_fromNameMeta,
          fromName.isAcceptableOrUnknown(data['from_name']!, _fromNameMeta));
    } else if (isInserting) {
      context.missing(_fromNameMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  SharedHistoryAccessTableData map(Map<String, dynamic> data,
      {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return SharedHistoryAccessTableData(
      id: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}id'])!,
      securityToken: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}security_token'])!,
      fromName: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}from_name'])!,
    );
  }

  @override
  $SharedHistoryAccessTableTable createAlias(String alias) {
    return $SharedHistoryAccessTableTable(attachedDatabase, alias);
  }
}

class SharedHistoryAccessTableData extends DataClass
    implements Insertable<SharedHistoryAccessTableData> {
  final int id;
  final String securityToken;
  final String fromName;
  const SharedHistoryAccessTableData(
      {required this.id, required this.securityToken, required this.fromName});
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['id'] = Variable<int>(id);
    map['security_token'] = Variable<String>(securityToken);
    map['from_name'] = Variable<String>(fromName);
    return map;
  }

  SharedHistoryAccessTableCompanion toCompanion(bool nullToAbsent) {
    return SharedHistoryAccessTableCompanion(
      id: Value(id),
      securityToken: Value(securityToken),
      fromName: Value(fromName),
    );
  }

  factory SharedHistoryAccessTableData.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return SharedHistoryAccessTableData(
      id: serializer.fromJson<int>(json['id']),
      securityToken: serializer.fromJson<String>(json['securityToken']),
      fromName: serializer.fromJson<String>(json['fromName']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'securityToken': serializer.toJson<String>(securityToken),
      'fromName': serializer.toJson<String>(fromName),
    };
  }

  SharedHistoryAccessTableData copyWith(
          {int? id, String? securityToken, String? fromName}) =>
      SharedHistoryAccessTableData(
        id: id ?? this.id,
        securityToken: securityToken ?? this.securityToken,
        fromName: fromName ?? this.fromName,
      );
  @override
  String toString() {
    return (StringBuffer('SharedHistoryAccessTableData(')
          ..write('id: $id, ')
          ..write('securityToken: $securityToken, ')
          ..write('fromName: $fromName')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(id, securityToken, fromName);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is SharedHistoryAccessTableData &&
          other.id == this.id &&
          other.securityToken == this.securityToken &&
          other.fromName == this.fromName);
}

class SharedHistoryAccessTableCompanion
    extends UpdateCompanion<SharedHistoryAccessTableData> {
  final Value<int> id;
  final Value<String> securityToken;
  final Value<String> fromName;
  const SharedHistoryAccessTableCompanion({
    this.id = const Value.absent(),
    this.securityToken = const Value.absent(),
    this.fromName = const Value.absent(),
  });
  SharedHistoryAccessTableCompanion.insert({
    this.id = const Value.absent(),
    required String securityToken,
    required String fromName,
  })  : securityToken = Value(securityToken),
        fromName = Value(fromName);
  static Insertable<SharedHistoryAccessTableData> custom({
    Expression<int>? id,
    Expression<String>? securityToken,
    Expression<String>? fromName,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (securityToken != null) 'security_token': securityToken,
      if (fromName != null) 'from_name': fromName,
    });
  }

  SharedHistoryAccessTableCompanion copyWith(
      {Value<int>? id, Value<String>? securityToken, Value<String>? fromName}) {
    return SharedHistoryAccessTableCompanion(
      id: id ?? this.id,
      securityToken: securityToken ?? this.securityToken,
      fromName: fromName ?? this.fromName,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (securityToken.present) {
      map['security_token'] = Variable<String>(securityToken.value);
    }
    if (fromName.present) {
      map['from_name'] = Variable<String>(fromName.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('SharedHistoryAccessTableCompanion(')
          ..write('id: $id, ')
          ..write('securityToken: $securityToken, ')
          ..write('fromName: $fromName')
          ..write(')'))
        .toString();
  }
}

class $FavoriteTypeTableTable extends FavoriteTypeTable
    with TableInfo<$FavoriteTypeTableTable, FavoriteTypeTableData> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  $FavoriteTypeTableTable(this.attachedDatabase, [this._alias]);
  static const VerificationMeta _type_idMeta =
      const VerificationMeta('type_id');
  @override
  late final GeneratedColumn<int> type_id = GeneratedColumn<int>(
      'type_id', aliasedName, false,
      type: DriftSqlType.int,
      requiredDuringInsert: false,
      defaultConstraints:
          GeneratedColumn.constraintIsAlways('REFERENCES DiaperType (id)'));
  @override
  List<GeneratedColumn> get $columns => [type_id];
  @override
  String get aliasedName => _alias ?? actualTableName;
  @override
  String get actualTableName => $name;
  static const String $name = 'FavoriteTypeTable';
  @override
  VerificationContext validateIntegrity(
      Insertable<FavoriteTypeTableData> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('type_id')) {
      context.handle(_type_idMeta,
          type_id.isAcceptableOrUnknown(data['type_id']!, _type_idMeta));
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {type_id};
  @override
  FavoriteTypeTableData map(Map<String, dynamic> data, {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return FavoriteTypeTableData(
      type_id: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}type_id'])!,
    );
  }

  @override
  $FavoriteTypeTableTable createAlias(String alias) {
    return $FavoriteTypeTableTable(attachedDatabase, alias);
  }
}

class FavoriteTypeTableData extends DataClass
    implements Insertable<FavoriteTypeTableData> {
  final int type_id;
  const FavoriteTypeTableData({required this.type_id});
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['type_id'] = Variable<int>(type_id);
    return map;
  }

  FavoriteTypeTableCompanion toCompanion(bool nullToAbsent) {
    return FavoriteTypeTableCompanion(
      type_id: Value(type_id),
    );
  }

  factory FavoriteTypeTableData.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return FavoriteTypeTableData(
      type_id: serializer.fromJson<int>(json['type_id']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'type_id': serializer.toJson<int>(type_id),
    };
  }

  FavoriteTypeTableData copyWith({int? type_id}) => FavoriteTypeTableData(
        type_id: type_id ?? this.type_id,
      );
  @override
  String toString() {
    return (StringBuffer('FavoriteTypeTableData(')
          ..write('type_id: $type_id')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => type_id.hashCode;
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is FavoriteTypeTableData && other.type_id == this.type_id);
}

class FavoriteTypeTableCompanion
    extends UpdateCompanion<FavoriteTypeTableData> {
  final Value<int> type_id;
  const FavoriteTypeTableCompanion({
    this.type_id = const Value.absent(),
  });
  FavoriteTypeTableCompanion.insert({
    this.type_id = const Value.absent(),
  });
  static Insertable<FavoriteTypeTableData> custom({
    Expression<int>? type_id,
  }) {
    return RawValuesInsertable({
      if (type_id != null) 'type_id': type_id,
    });
  }

  FavoriteTypeTableCompanion copyWith({Value<int>? type_id}) {
    return FavoriteTypeTableCompanion(
      type_id: type_id ?? this.type_id,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (type_id.present) {
      map['type_id'] = Variable<int>(type_id.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('FavoriteTypeTableCompanion(')
          ..write('type_id: $type_id')
          ..write(')'))
        .toString();
  }
}

class $NotificationTableTable extends NotificationTable
    with TableInfo<$NotificationTableTable, NotificationTableData> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  $NotificationTableTable(this.attachedDatabase, [this._alias]);
  static const VerificationMeta _idMeta = const VerificationMeta('id');
  @override
  late final GeneratedColumn<int> id = GeneratedColumn<int>(
      'id', aliasedName, false,
      hasAutoIncrement: true,
      type: DriftSqlType.int,
      requiredDuringInsert: false,
      defaultConstraints:
          GeneratedColumn.constraintIsAlways('PRIMARY KEY AUTOINCREMENT'));
  static const VerificationMeta _typeMeta = const VerificationMeta('type');
  @override
  late final GeneratedColumnWithTypeConverter<NotificationType, String> type =
      GeneratedColumn<String>('type', aliasedName, false,
              type: DriftSqlType.string, requiredDuringInsert: true)
          .withConverter<NotificationType>(
              $NotificationTableTable.$convertertype);
  static const VerificationMeta _durationMeta =
      const VerificationMeta('duration');
  @override
  late final GeneratedColumn<int> duration = GeneratedColumn<int>(
      'duration', aliasedName, false,
      type: DriftSqlType.int, requiredDuringInsert: true);
  static const VerificationMeta _titleMeta = const VerificationMeta('title');
  @override
  late final GeneratedColumn<String> title = GeneratedColumn<String>(
      'title', aliasedName, false,
      type: DriftSqlType.string,
      requiredDuringInsert: false,
      defaultValue: const Constant("value"));
  static const VerificationMeta _descriptionMeta =
      const VerificationMeta('description');
  @override
  late final GeneratedColumn<String> description = GeneratedColumn<String>(
      'description', aliasedName, false,
      type: DriftSqlType.string,
      requiredDuringInsert: false,
      defaultValue: const Constant("value"));
  static const VerificationMeta _typeIdMeta = const VerificationMeta('typeId');
  @override
  late final GeneratedColumn<int> typeId = GeneratedColumn<int>(
      'type_id', aliasedName, true,
      type: DriftSqlType.int, requiredDuringInsert: false);
  @override
  List<GeneratedColumn> get $columns =>
      [id, type, duration, title, description, typeId];
  @override
  String get aliasedName => _alias ?? actualTableName;
  @override
  String get actualTableName => $name;
  static const String $name = 'notification';
  @override
  VerificationContext validateIntegrity(
      Insertable<NotificationTableData> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id']!, _idMeta));
    }
    context.handle(_typeMeta, const VerificationResult.success());
    if (data.containsKey('duration')) {
      context.handle(_durationMeta,
          duration.isAcceptableOrUnknown(data['duration']!, _durationMeta));
    } else if (isInserting) {
      context.missing(_durationMeta);
    }
    if (data.containsKey('title')) {
      context.handle(
          _titleMeta, title.isAcceptableOrUnknown(data['title']!, _titleMeta));
    }
    if (data.containsKey('description')) {
      context.handle(
          _descriptionMeta,
          description.isAcceptableOrUnknown(
              data['description']!, _descriptionMeta));
    }
    if (data.containsKey('type_id')) {
      context.handle(_typeIdMeta,
          typeId.isAcceptableOrUnknown(data['type_id']!, _typeIdMeta));
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  NotificationTableData map(Map<String, dynamic> data, {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return NotificationTableData(
      id: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}id'])!,
      type: $NotificationTableTable.$convertertype.fromSql(attachedDatabase
          .typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}type'])!),
      duration: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}duration'])!,
      title: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}title'])!,
      description: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}description'])!,
      typeId: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}type_id']),
    );
  }

  @override
  $NotificationTableTable createAlias(String alias) {
    return $NotificationTableTable(attachedDatabase, alias);
  }

  static JsonTypeConverter2<NotificationType, String, String> $convertertype =
      const EnumNameConverter<NotificationType>(NotificationType.values);
}

class NotificationTableData extends DataClass
    implements Insertable<NotificationTableData> {
  final int id;
  final NotificationType type;
  final int duration;
  final String title;
  final String description;
  final int? typeId;
  const NotificationTableData(
      {required this.id,
      required this.type,
      required this.duration,
      required this.title,
      required this.description,
      this.typeId});
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['id'] = Variable<int>(id);
    {
      final converter = $NotificationTableTable.$convertertype;
      map['type'] = Variable<String>(converter.toSql(type));
    }
    map['duration'] = Variable<int>(duration);
    map['title'] = Variable<String>(title);
    map['description'] = Variable<String>(description);
    if (!nullToAbsent || typeId != null) {
      map['type_id'] = Variable<int>(typeId);
    }
    return map;
  }

  NotificationTableCompanion toCompanion(bool nullToAbsent) {
    return NotificationTableCompanion(
      id: Value(id),
      type: Value(type),
      duration: Value(duration),
      title: Value(title),
      description: Value(description),
      typeId:
          typeId == null && nullToAbsent ? const Value.absent() : Value(typeId),
    );
  }

  factory NotificationTableData.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return NotificationTableData(
      id: serializer.fromJson<int>(json['id']),
      type: $NotificationTableTable.$convertertype
          .fromJson(serializer.fromJson<String>(json['type'])),
      duration: serializer.fromJson<int>(json['duration']),
      title: serializer.fromJson<String>(json['title']),
      description: serializer.fromJson<String>(json['description']),
      typeId: serializer.fromJson<int?>(json['typeId']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'type': serializer
          .toJson<String>($NotificationTableTable.$convertertype.toJson(type)),
      'duration': serializer.toJson<int>(duration),
      'title': serializer.toJson<String>(title),
      'description': serializer.toJson<String>(description),
      'typeId': serializer.toJson<int?>(typeId),
    };
  }

  NotificationTableData copyWith(
          {int? id,
          NotificationType? type,
          int? duration,
          String? title,
          String? description,
          Value<int?> typeId = const Value.absent()}) =>
      NotificationTableData(
        id: id ?? this.id,
        type: type ?? this.type,
        duration: duration ?? this.duration,
        title: title ?? this.title,
        description: description ?? this.description,
        typeId: typeId.present ? typeId.value : this.typeId,
      );
  @override
  String toString() {
    return (StringBuffer('NotificationTableData(')
          ..write('id: $id, ')
          ..write('type: $type, ')
          ..write('duration: $duration, ')
          ..write('title: $title, ')
          ..write('description: $description, ')
          ..write('typeId: $typeId')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode =>
      Object.hash(id, type, duration, title, description, typeId);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is NotificationTableData &&
          other.id == this.id &&
          other.type == this.type &&
          other.duration == this.duration &&
          other.title == this.title &&
          other.description == this.description &&
          other.typeId == this.typeId);
}

class NotificationTableCompanion
    extends UpdateCompanion<NotificationTableData> {
  final Value<int> id;
  final Value<NotificationType> type;
  final Value<int> duration;
  final Value<String> title;
  final Value<String> description;
  final Value<int?> typeId;
  const NotificationTableCompanion({
    this.id = const Value.absent(),
    this.type = const Value.absent(),
    this.duration = const Value.absent(),
    this.title = const Value.absent(),
    this.description = const Value.absent(),
    this.typeId = const Value.absent(),
  });
  NotificationTableCompanion.insert({
    this.id = const Value.absent(),
    required NotificationType type,
    required int duration,
    this.title = const Value.absent(),
    this.description = const Value.absent(),
    this.typeId = const Value.absent(),
  })  : type = Value(type),
        duration = Value(duration);
  static Insertable<NotificationTableData> custom({
    Expression<int>? id,
    Expression<String>? type,
    Expression<int>? duration,
    Expression<String>? title,
    Expression<String>? description,
    Expression<int>? typeId,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (type != null) 'type': type,
      if (duration != null) 'duration': duration,
      if (title != null) 'title': title,
      if (description != null) 'description': description,
      if (typeId != null) 'type_id': typeId,
    });
  }

  NotificationTableCompanion copyWith(
      {Value<int>? id,
      Value<NotificationType>? type,
      Value<int>? duration,
      Value<String>? title,
      Value<String>? description,
      Value<int?>? typeId}) {
    return NotificationTableCompanion(
      id: id ?? this.id,
      type: type ?? this.type,
      duration: duration ?? this.duration,
      title: title ?? this.title,
      description: description ?? this.description,
      typeId: typeId ?? this.typeId,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (type.present) {
      final converter = $NotificationTableTable.$convertertype;

      map['type'] = Variable<String>(converter.toSql(type.value));
    }
    if (duration.present) {
      map['duration'] = Variable<int>(duration.value);
    }
    if (title.present) {
      map['title'] = Variable<String>(title.value);
    }
    if (description.present) {
      map['description'] = Variable<String>(description.value);
    }
    if (typeId.present) {
      map['type_id'] = Variable<int>(typeId.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('NotificationTableCompanion(')
          ..write('id: $id, ')
          ..write('type: $type, ')
          ..write('duration: $duration, ')
          ..write('title: $title, ')
          ..write('description: $description, ')
          ..write('typeId: $typeId')
          ..write(')'))
        .toString();
  }
}

class $SharedStockAccessTableTable extends SharedStockAccessTable
    with TableInfo<$SharedStockAccessTableTable, SharedStockAccessTableData> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  $SharedStockAccessTableTable(this.attachedDatabase, [this._alias]);
  static const VerificationMeta _idMeta = const VerificationMeta('id');
  @override
  late final GeneratedColumn<int> id = GeneratedColumn<int>(
      'id', aliasedName, false,
      type: DriftSqlType.int, requiredDuringInsert: false);
  static const VerificationMeta _securityTokenMeta =
      const VerificationMeta('securityToken');
  @override
  late final GeneratedColumn<String> securityToken = GeneratedColumn<String>(
      'security_token', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  static const VerificationMeta _fromNameMeta =
      const VerificationMeta('fromName');
  @override
  late final GeneratedColumn<String> fromName = GeneratedColumn<String>(
      'from_name', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  static const VerificationMeta _readonlyMeta =
      const VerificationMeta('readonly');
  @override
  late final GeneratedColumn<bool> readonly = GeneratedColumn<bool>(
      'readonly', aliasedName, false,
      type: DriftSqlType.bool,
      requiredDuringInsert: false,
      defaultConstraints:
          GeneratedColumn.constraintIsAlways('CHECK ("readonly" IN (0, 1))'),
      defaultValue: const Constant(false));
  @override
  List<GeneratedColumn> get $columns => [id, securityToken, fromName, readonly];
  @override
  String get aliasedName => _alias ?? actualTableName;
  @override
  String get actualTableName => $name;
  static const String $name = 'shared_stock_access_table';
  @override
  VerificationContext validateIntegrity(
      Insertable<SharedStockAccessTableData> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id']!, _idMeta));
    }
    if (data.containsKey('security_token')) {
      context.handle(
          _securityTokenMeta,
          securityToken.isAcceptableOrUnknown(
              data['security_token']!, _securityTokenMeta));
    } else if (isInserting) {
      context.missing(_securityTokenMeta);
    }
    if (data.containsKey('from_name')) {
      context.handle(_fromNameMeta,
          fromName.isAcceptableOrUnknown(data['from_name']!, _fromNameMeta));
    } else if (isInserting) {
      context.missing(_fromNameMeta);
    }
    if (data.containsKey('readonly')) {
      context.handle(_readonlyMeta,
          readonly.isAcceptableOrUnknown(data['readonly']!, _readonlyMeta));
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  SharedStockAccessTableData map(Map<String, dynamic> data,
      {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return SharedStockAccessTableData(
      id: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}id'])!,
      securityToken: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}security_token'])!,
      fromName: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}from_name'])!,
      readonly: attachedDatabase.typeMapping
          .read(DriftSqlType.bool, data['${effectivePrefix}readonly'])!,
    );
  }

  @override
  $SharedStockAccessTableTable createAlias(String alias) {
    return $SharedStockAccessTableTable(attachedDatabase, alias);
  }
}

class SharedStockAccessTableData extends DataClass
    implements Insertable<SharedStockAccessTableData> {
  final int id;
  final String securityToken;
  final String fromName;
  final bool readonly;
  const SharedStockAccessTableData(
      {required this.id,
      required this.securityToken,
      required this.fromName,
      required this.readonly});
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['id'] = Variable<int>(id);
    map['security_token'] = Variable<String>(securityToken);
    map['from_name'] = Variable<String>(fromName);
    map['readonly'] = Variable<bool>(readonly);
    return map;
  }

  SharedStockAccessTableCompanion toCompanion(bool nullToAbsent) {
    return SharedStockAccessTableCompanion(
      id: Value(id),
      securityToken: Value(securityToken),
      fromName: Value(fromName),
      readonly: Value(readonly),
    );
  }

  factory SharedStockAccessTableData.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return SharedStockAccessTableData(
      id: serializer.fromJson<int>(json['id']),
      securityToken: serializer.fromJson<String>(json['securityToken']),
      fromName: serializer.fromJson<String>(json['fromName']),
      readonly: serializer.fromJson<bool>(json['readonly']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'securityToken': serializer.toJson<String>(securityToken),
      'fromName': serializer.toJson<String>(fromName),
      'readonly': serializer.toJson<bool>(readonly),
    };
  }

  SharedStockAccessTableData copyWith(
          {int? id, String? securityToken, String? fromName, bool? readonly}) =>
      SharedStockAccessTableData(
        id: id ?? this.id,
        securityToken: securityToken ?? this.securityToken,
        fromName: fromName ?? this.fromName,
        readonly: readonly ?? this.readonly,
      );
  @override
  String toString() {
    return (StringBuffer('SharedStockAccessTableData(')
          ..write('id: $id, ')
          ..write('securityToken: $securityToken, ')
          ..write('fromName: $fromName, ')
          ..write('readonly: $readonly')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(id, securityToken, fromName, readonly);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is SharedStockAccessTableData &&
          other.id == this.id &&
          other.securityToken == this.securityToken &&
          other.fromName == this.fromName &&
          other.readonly == this.readonly);
}

class SharedStockAccessTableCompanion
    extends UpdateCompanion<SharedStockAccessTableData> {
  final Value<int> id;
  final Value<String> securityToken;
  final Value<String> fromName;
  final Value<bool> readonly;
  const SharedStockAccessTableCompanion({
    this.id = const Value.absent(),
    this.securityToken = const Value.absent(),
    this.fromName = const Value.absent(),
    this.readonly = const Value.absent(),
  });
  SharedStockAccessTableCompanion.insert({
    this.id = const Value.absent(),
    required String securityToken,
    required String fromName,
    this.readonly = const Value.absent(),
  })  : securityToken = Value(securityToken),
        fromName = Value(fromName);
  static Insertable<SharedStockAccessTableData> custom({
    Expression<int>? id,
    Expression<String>? securityToken,
    Expression<String>? fromName,
    Expression<bool>? readonly,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (securityToken != null) 'security_token': securityToken,
      if (fromName != null) 'from_name': fromName,
      if (readonly != null) 'readonly': readonly,
    });
  }

  SharedStockAccessTableCompanion copyWith(
      {Value<int>? id,
      Value<String>? securityToken,
      Value<String>? fromName,
      Value<bool>? readonly}) {
    return SharedStockAccessTableCompanion(
      id: id ?? this.id,
      securityToken: securityToken ?? this.securityToken,
      fromName: fromName ?? this.fromName,
      readonly: readonly ?? this.readonly,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (securityToken.present) {
      map['security_token'] = Variable<String>(securityToken.value);
    }
    if (fromName.present) {
      map['from_name'] = Variable<String>(fromName.value);
    }
    if (readonly.present) {
      map['readonly'] = Variable<bool>(readonly.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('SharedStockAccessTableCompanion(')
          ..write('id: $id, ')
          ..write('securityToken: $securityToken, ')
          ..write('fromName: $fromName, ')
          ..write('readonly: $readonly')
          ..write(')'))
        .toString();
  }
}

class $TypeInfoTableTable extends TypeInfoTable
    with TableInfo<$TypeInfoTableTable, TypeInfoTableData> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  $TypeInfoTableTable(this.attachedDatabase, [this._alias]);
  static const VerificationMeta _typeIdMeta = const VerificationMeta('typeId');
  @override
  late final GeneratedColumn<int> typeId = GeneratedColumn<int>(
      'type_id', aliasedName, false,
      type: DriftSqlType.int, requiredDuringInsert: false);
  static const VerificationMeta _thresholdLowStockMeta =
      const VerificationMeta('thresholdLowStock');
  @override
  late final GeneratedColumn<int> thresholdLowStock = GeneratedColumn<int>(
      'threshold_low_stock', aliasedName, true,
      type: DriftSqlType.int, requiredDuringInsert: false);
  static const VerificationMeta _noteMeta = const VerificationMeta('note');
  @override
  late final GeneratedColumn<String> note = GeneratedColumn<String>(
      'note', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  @override
  List<GeneratedColumn> get $columns => [typeId, thresholdLowStock, note];
  @override
  String get aliasedName => _alias ?? actualTableName;
  @override
  String get actualTableName => $name;
  static const String $name = 'typeinfo';
  @override
  VerificationContext validateIntegrity(Insertable<TypeInfoTableData> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('type_id')) {
      context.handle(_typeIdMeta,
          typeId.isAcceptableOrUnknown(data['type_id']!, _typeIdMeta));
    }
    if (data.containsKey('threshold_low_stock')) {
      context.handle(
          _thresholdLowStockMeta,
          thresholdLowStock.isAcceptableOrUnknown(
              data['threshold_low_stock']!, _thresholdLowStockMeta));
    }
    if (data.containsKey('note')) {
      context.handle(
          _noteMeta, note.isAcceptableOrUnknown(data['note']!, _noteMeta));
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {typeId};
  @override
  TypeInfoTableData map(Map<String, dynamic> data, {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return TypeInfoTableData(
      typeId: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}type_id'])!,
      thresholdLowStock: attachedDatabase.typeMapping.read(
          DriftSqlType.int, data['${effectivePrefix}threshold_low_stock']),
      note: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}note']),
    );
  }

  @override
  $TypeInfoTableTable createAlias(String alias) {
    return $TypeInfoTableTable(attachedDatabase, alias);
  }
}

class TypeInfoTableData extends DataClass
    implements Insertable<TypeInfoTableData> {
  final int typeId;
  final int? thresholdLowStock;
  final String? note;
  const TypeInfoTableData(
      {required this.typeId, this.thresholdLowStock, this.note});
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['type_id'] = Variable<int>(typeId);
    if (!nullToAbsent || thresholdLowStock != null) {
      map['threshold_low_stock'] = Variable<int>(thresholdLowStock);
    }
    if (!nullToAbsent || note != null) {
      map['note'] = Variable<String>(note);
    }
    return map;
  }

  TypeInfoTableCompanion toCompanion(bool nullToAbsent) {
    return TypeInfoTableCompanion(
      typeId: Value(typeId),
      thresholdLowStock: thresholdLowStock == null && nullToAbsent
          ? const Value.absent()
          : Value(thresholdLowStock),
      note: note == null && nullToAbsent ? const Value.absent() : Value(note),
    );
  }

  factory TypeInfoTableData.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return TypeInfoTableData(
      typeId: serializer.fromJson<int>(json['typeId']),
      thresholdLowStock: serializer.fromJson<int?>(json['thresholdLowStock']),
      note: serializer.fromJson<String?>(json['note']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'typeId': serializer.toJson<int>(typeId),
      'thresholdLowStock': serializer.toJson<int?>(thresholdLowStock),
      'note': serializer.toJson<String?>(note),
    };
  }

  TypeInfoTableData copyWith(
          {int? typeId,
          Value<int?> thresholdLowStock = const Value.absent(),
          Value<String?> note = const Value.absent()}) =>
      TypeInfoTableData(
        typeId: typeId ?? this.typeId,
        thresholdLowStock: thresholdLowStock.present
            ? thresholdLowStock.value
            : this.thresholdLowStock,
        note: note.present ? note.value : this.note,
      );
  @override
  String toString() {
    return (StringBuffer('TypeInfoTableData(')
          ..write('typeId: $typeId, ')
          ..write('thresholdLowStock: $thresholdLowStock, ')
          ..write('note: $note')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(typeId, thresholdLowStock, note);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is TypeInfoTableData &&
          other.typeId == this.typeId &&
          other.thresholdLowStock == this.thresholdLowStock &&
          other.note == this.note);
}

class TypeInfoTableCompanion extends UpdateCompanion<TypeInfoTableData> {
  final Value<int> typeId;
  final Value<int?> thresholdLowStock;
  final Value<String?> note;
  const TypeInfoTableCompanion({
    this.typeId = const Value.absent(),
    this.thresholdLowStock = const Value.absent(),
    this.note = const Value.absent(),
  });
  TypeInfoTableCompanion.insert({
    this.typeId = const Value.absent(),
    this.thresholdLowStock = const Value.absent(),
    this.note = const Value.absent(),
  });
  static Insertable<TypeInfoTableData> custom({
    Expression<int>? typeId,
    Expression<int>? thresholdLowStock,
    Expression<String>? note,
  }) {
    return RawValuesInsertable({
      if (typeId != null) 'type_id': typeId,
      if (thresholdLowStock != null) 'threshold_low_stock': thresholdLowStock,
      if (note != null) 'note': note,
    });
  }

  TypeInfoTableCompanion copyWith(
      {Value<int>? typeId,
      Value<int?>? thresholdLowStock,
      Value<String?>? note}) {
    return TypeInfoTableCompanion(
      typeId: typeId ?? this.typeId,
      thresholdLowStock: thresholdLowStock ?? this.thresholdLowStock,
      note: note ?? this.note,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (typeId.present) {
      map['type_id'] = Variable<int>(typeId.value);
    }
    if (thresholdLowStock.present) {
      map['threshold_low_stock'] = Variable<int>(thresholdLowStock.value);
    }
    if (note.present) {
      map['note'] = Variable<String>(note.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('TypeInfoTableCompanion(')
          ..write('typeId: $typeId, ')
          ..write('thresholdLowStock: $thresholdLowStock, ')
          ..write('note: $note')
          ..write(')'))
        .toString();
  }
}

class $UserRatingTableTable extends UserRatingTable
    with TableInfo<$UserRatingTableTable, UserRatingTableData> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  $UserRatingTableTable(this.attachedDatabase, [this._alias]);
  static const VerificationMeta _idMeta = const VerificationMeta('id');
  @override
  late final GeneratedColumn<String> id = GeneratedColumn<String>(
      'id', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  static const VerificationMeta _typeIdMeta = const VerificationMeta('typeId');
  @override
  late final GeneratedColumn<int> typeId = GeneratedColumn<int>(
      'type_id', aliasedName, false,
      type: DriftSqlType.int, requiredDuringInsert: true);
  static const VerificationMeta _criteriaMeta =
      const VerificationMeta('criteria');
  @override
  late final GeneratedColumn<String> criteria = GeneratedColumn<String>(
      'criteria', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  static const VerificationMeta _noteMeta = const VerificationMeta('note');
  @override
  late final GeneratedColumn<double> note = GeneratedColumn<double>(
      'note', aliasedName, false,
      type: DriftSqlType.double, requiredDuringInsert: true);
  static const VerificationMeta _updatedAtMeta =
      const VerificationMeta('updatedAt');
  @override
  late final GeneratedColumn<DateTime> updatedAt = GeneratedColumn<DateTime>(
      'updated_at', aliasedName, false,
      type: DriftSqlType.dateTime,
      requiredDuringInsert: false,
      defaultValue: currentDateAndTime);
  @override
  List<GeneratedColumn> get $columns => [id, typeId, criteria, note, updatedAt];
  @override
  String get aliasedName => _alias ?? actualTableName;
  @override
  String get actualTableName => $name;
  static const String $name = 'user_rating';
  @override
  VerificationContext validateIntegrity(
      Insertable<UserRatingTableData> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id']!, _idMeta));
    } else if (isInserting) {
      context.missing(_idMeta);
    }
    if (data.containsKey('type_id')) {
      context.handle(_typeIdMeta,
          typeId.isAcceptableOrUnknown(data['type_id']!, _typeIdMeta));
    } else if (isInserting) {
      context.missing(_typeIdMeta);
    }
    if (data.containsKey('criteria')) {
      context.handle(_criteriaMeta,
          criteria.isAcceptableOrUnknown(data['criteria']!, _criteriaMeta));
    } else if (isInserting) {
      context.missing(_criteriaMeta);
    }
    if (data.containsKey('note')) {
      context.handle(
          _noteMeta, note.isAcceptableOrUnknown(data['note']!, _noteMeta));
    } else if (isInserting) {
      context.missing(_noteMeta);
    }
    if (data.containsKey('updated_at')) {
      context.handle(_updatedAtMeta,
          updatedAt.isAcceptableOrUnknown(data['updated_at']!, _updatedAtMeta));
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  UserRatingTableData map(Map<String, dynamic> data, {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return UserRatingTableData(
      id: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}id'])!,
      typeId: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}type_id'])!,
      criteria: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}criteria'])!,
      note: attachedDatabase.typeMapping
          .read(DriftSqlType.double, data['${effectivePrefix}note'])!,
      updatedAt: attachedDatabase.typeMapping
          .read(DriftSqlType.dateTime, data['${effectivePrefix}updated_at'])!,
    );
  }

  @override
  $UserRatingTableTable createAlias(String alias) {
    return $UserRatingTableTable(attachedDatabase, alias);
  }
}

class UserRatingTableData extends DataClass
    implements Insertable<UserRatingTableData> {
  final String id;
  final int typeId;
  final String criteria;
  final double note;
  final DateTime updatedAt;
  const UserRatingTableData(
      {required this.id,
      required this.typeId,
      required this.criteria,
      required this.note,
      required this.updatedAt});
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['id'] = Variable<String>(id);
    map['type_id'] = Variable<int>(typeId);
    map['criteria'] = Variable<String>(criteria);
    map['note'] = Variable<double>(note);
    map['updated_at'] = Variable<DateTime>(updatedAt);
    return map;
  }

  UserRatingTableCompanion toCompanion(bool nullToAbsent) {
    return UserRatingTableCompanion(
      id: Value(id),
      typeId: Value(typeId),
      criteria: Value(criteria),
      note: Value(note),
      updatedAt: Value(updatedAt),
    );
  }

  factory UserRatingTableData.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return UserRatingTableData(
      id: serializer.fromJson<String>(json['id']),
      typeId: serializer.fromJson<int>(json['typeId']),
      criteria: serializer.fromJson<String>(json['criteria']),
      note: serializer.fromJson<double>(json['note']),
      updatedAt: serializer.fromJson<DateTime>(json['updatedAt']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<String>(id),
      'typeId': serializer.toJson<int>(typeId),
      'criteria': serializer.toJson<String>(criteria),
      'note': serializer.toJson<double>(note),
      'updatedAt': serializer.toJson<DateTime>(updatedAt),
    };
  }

  UserRatingTableData copyWith(
          {String? id,
          int? typeId,
          String? criteria,
          double? note,
          DateTime? updatedAt}) =>
      UserRatingTableData(
        id: id ?? this.id,
        typeId: typeId ?? this.typeId,
        criteria: criteria ?? this.criteria,
        note: note ?? this.note,
        updatedAt: updatedAt ?? this.updatedAt,
      );
  @override
  String toString() {
    return (StringBuffer('UserRatingTableData(')
          ..write('id: $id, ')
          ..write('typeId: $typeId, ')
          ..write('criteria: $criteria, ')
          ..write('note: $note, ')
          ..write('updatedAt: $updatedAt')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(id, typeId, criteria, note, updatedAt);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is UserRatingTableData &&
          other.id == this.id &&
          other.typeId == this.typeId &&
          other.criteria == this.criteria &&
          other.note == this.note &&
          other.updatedAt == this.updatedAt);
}

class UserRatingTableCompanion extends UpdateCompanion<UserRatingTableData> {
  final Value<String> id;
  final Value<int> typeId;
  final Value<String> criteria;
  final Value<double> note;
  final Value<DateTime> updatedAt;
  final Value<int> rowid;
  const UserRatingTableCompanion({
    this.id = const Value.absent(),
    this.typeId = const Value.absent(),
    this.criteria = const Value.absent(),
    this.note = const Value.absent(),
    this.updatedAt = const Value.absent(),
    this.rowid = const Value.absent(),
  });
  UserRatingTableCompanion.insert({
    required String id,
    required int typeId,
    required String criteria,
    required double note,
    this.updatedAt = const Value.absent(),
    this.rowid = const Value.absent(),
  })  : id = Value(id),
        typeId = Value(typeId),
        criteria = Value(criteria),
        note = Value(note);
  static Insertable<UserRatingTableData> custom({
    Expression<String>? id,
    Expression<int>? typeId,
    Expression<String>? criteria,
    Expression<double>? note,
    Expression<DateTime>? updatedAt,
    Expression<int>? rowid,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (typeId != null) 'type_id': typeId,
      if (criteria != null) 'criteria': criteria,
      if (note != null) 'note': note,
      if (updatedAt != null) 'updated_at': updatedAt,
      if (rowid != null) 'rowid': rowid,
    });
  }

  UserRatingTableCompanion copyWith(
      {Value<String>? id,
      Value<int>? typeId,
      Value<String>? criteria,
      Value<double>? note,
      Value<DateTime>? updatedAt,
      Value<int>? rowid}) {
    return UserRatingTableCompanion(
      id: id ?? this.id,
      typeId: typeId ?? this.typeId,
      criteria: criteria ?? this.criteria,
      note: note ?? this.note,
      updatedAt: updatedAt ?? this.updatedAt,
      rowid: rowid ?? this.rowid,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<String>(id.value);
    }
    if (typeId.present) {
      map['type_id'] = Variable<int>(typeId.value);
    }
    if (criteria.present) {
      map['criteria'] = Variable<String>(criteria.value);
    }
    if (note.present) {
      map['note'] = Variable<double>(note.value);
    }
    if (updatedAt.present) {
      map['updated_at'] = Variable<DateTime>(updatedAt.value);
    }
    if (rowid.present) {
      map['rowid'] = Variable<int>(rowid.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('UserRatingTableCompanion(')
          ..write('id: $id, ')
          ..write('typeId: $typeId, ')
          ..write('criteria: $criteria, ')
          ..write('note: $note, ')
          ..write('updatedAt: $updatedAt, ')
          ..write('rowid: $rowid')
          ..write(')'))
        .toString();
  }
}

abstract class _$MyDatabase extends GeneratedDatabase {
  _$MyDatabase(QueryExecutor e) : super(e);
  late final $DiaperTypeTableTable diaperTypeTable =
      $DiaperTypeTableTable(this);
  late final $DiaperBrandTableTable diaperBrandTable =
      $DiaperBrandTableTable(this);
  late final $DiaperStockTableTable diaperStockTable =
      $DiaperStockTableTable(this);
  late final $SettingsTableTable settingsTable = $SettingsTableTable(this);
  late final $ChangeTableTable changeTable = $ChangeTableTable(this);
  late final $DiaperChangeTableTable diaperChangeTable =
      $DiaperChangeTableTable(this);
  late final $DiaperTypeProposalTableTable diaperTypeProposalTable =
      $DiaperTypeProposalTableTable(this);
  late final $SharedHistoryAccessTableTable sharedHistoryAccessTable =
      $SharedHistoryAccessTableTable(this);
  late final $FavoriteTypeTableTable favoriteTypeTable =
      $FavoriteTypeTableTable(this);
  late final $NotificationTableTable notificationTable =
      $NotificationTableTable(this);
  late final $SharedStockAccessTableTable sharedStockAccessTable =
      $SharedStockAccessTableTable(this);
  late final $TypeInfoTableTable typeInfoTable = $TypeInfoTableTable(this);
  late final $UserRatingTableTable userRatingTable =
      $UserRatingTableTable(this);
  @override
  Iterable<TableInfo<Table, Object?>> get allTables =>
      allSchemaEntities.whereType<TableInfo<Table, Object?>>();
  @override
  List<DatabaseSchemaEntity> get allSchemaEntities => [
        diaperTypeTable,
        diaperBrandTable,
        diaperStockTable,
        settingsTable,
        changeTable,
        diaperChangeTable,
        diaperTypeProposalTable,
        sharedHistoryAccessTable,
        favoriteTypeTable,
        notificationTable,
        sharedStockAccessTable,
        typeInfoTable,
        userRatingTable
      ];
}
