import 'dart:io';
import 'dart:isolate';
import 'dart:ui';

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:diap_stash/constants.dart';
import 'package:diap_stash/providers/diaper_provider.dart';
import 'package:diap_stash/providers/notification_provider.dart';
import 'package:flutter/foundation.dart';
import 'package:sentry_flutter/sentry_flutter.dart';
import 'dart:developer' as developer;

import 'package:workmanager/workmanager.dart';

// ignore: unused_element
const _taskChangeNotificationIOS =
    "fr.diapstash.diap_stash.task.ios.change-notification";

const debugMode = false;

Future<void> setupBackgroundTask() async {
  if (kIsWeb) {
    developer.log("Web, no bg task");
    return;
  }

  var port = ReceivePort('bg port');
  IsolateNameServer.registerPortWithName(port.sendPort, bgTaskPort);
  port.listen((dynamic data) async {
    developer.log("port receive $data");
    Sentry.addBreadcrumb(Breadcrumb(
        message: "bg task port receive $data", level: SentryLevel.debug));
    switch (data as String) {
      case taskChangeNotification:
        try {
          await runCheckChangeNotification();
          developer.log("ok runCheckChangeNotification");
        } catch (e) {
          developer.log("Error when runCheckChangeNotification", error: e);
          developer.log("Error when runCheckChangeNotification");
          developer.log(e.toString());
          Sentry.captureException(e);
        } finally {
          developer.log("done runCheckChangeNotification");
        }
        break;
    }
  }, onError: (err){
    developer.log("Error on background port", error: err);
  }, onDone: (){
    developer.log("onDone on background port");
  }, cancelOnError: false);

  developer.log("Register task");
  if (Platform.isAndroid) {
    await Workmanager()
        .initialize(callbackDispatcher, isInDebugMode: kDebugMode && debugMode);
    Workmanager().cancelByUniqueName(taskChangeNotification);
    Workmanager().registerPeriodicTask(
        taskChangeNotification, taskChangeNotification,
        frequency: const Duration(minutes: 30),
        initialDelay: const Duration(minutes: 1),
        constraints: Constraints(
            networkType: NetworkType.connected,
            requiresBatteryNotLow: true,
            requiresStorageNotLow: true,
            requiresDeviceIdle: false),
        inputData: {});
  } else if (Platform.isIOS) {
    /*try {
      await Workmanager().initialize(callbackDispatcher, isInDebugMode: kDebugMode && debugMode);
      Workmanager().cancelByUniqueName(_task_change_notification_ios);
      Workmanager().registerOneOffTask(
          _task_change_notification_ios, _task_change_notification_ios,
          initialDelay: const Duration(seconds: 30));
    } catch (e) {
      developer.log("error when setup Workmanager on ios");
      developer.log(e);
    }*/
  }
}

@pragma('vm:entry-point')
void callbackDispatcher() {
  Workmanager().executeTask((task, inputData) async {
    try {
      developer.log(
          "[callbackDispatcher] ${DateTime.now()
              .toIso8601String()} : Native called background task: $task");
      developer.log("[callbackDispatcher] call via isolate");
      final sendPort = IsolateNameServer.lookupPortByName(bgTaskPort);
      if (sendPort != null) {
        sendPort.send(task);
        developer.log("[callbackDispatcher] Ok to send port");
        return true;
      } else {
        developer.log("[callbackDispatcher] No send port...");
      }
    }catch(error){
      developer.log("[callbackDispatcher] error on callbackDispatcher", error: error);
    }
    return false;
  });
}

Future<void> runCheckChangeNotification() async {
  Sentry.addBreadcrumb(Breadcrumb(
      message: "runCheckChangeNotification", level: SentryLevel.debug));
  developer.log("runCheckChangeNotification");
  try {
    var diaperProvider = DiaperProvider.instance;
    final connectivityResult = await (Connectivity().checkConnectivity());

    if (connectivityResult == ConnectivityResult.none) {
      developer.log("runCheckChangeNotification end - connectivityResult $connectivityResult");
      Sentry.addBreadcrumb(Breadcrumb(
          message: "runCheckChangeNotification end - connectivityResult $connectivityResult", level: SentryLevel.debug));
      return;
    }

    await diaperProvider.loadChange(setupNotification: false, doNotifyListeners: true);
    var np = NotificationProvider.instance;
    if (np != null) {
      if (diaperProvider.currentChange != null) {
        await np.setupNotifications(
            diaperProvider.currentChange!,
            requestPermission: false);
      } else {
        if (await np.hasChangeNotification(requestPermission: false)) {
          await np
              .cancelChangeNotification(requestPermission: false);
        }
      }
      await np.setupNotificationNightMode();
    }
  } catch (e, s) {
    Sentry.captureMessage("Error on runCheckChangeNotification",
        level: SentryLevel.error);
    Sentry.captureException(e, stackTrace: s);
  }
  Sentry.addBreadcrumb(Breadcrumb(
      message: "runCheckChangeNotification end", level: SentryLevel.debug));
}
