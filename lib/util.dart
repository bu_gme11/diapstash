import 'package:diap_stash/common.dart';
import 'package:diap_stash/providers/settings_provider.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'dart:math' as math;

// ignore: depend_on_referenced_packages
import 'package:universal_io/io.dart';

//final BaseCacheManager baseCacheManager = BaseCacheManager();

class CustomCacheManager {
  static const key = 'customCacheKey';
  static CacheManager instance = CacheManager(
    Config(
      key,
      stalePeriod: const Duration(days: 7),
      maxNrOfCacheObjects: 500,
      repo: JsonCacheInfoRepository(databaseName: key),
      //fileSystem: IOFileSystem(key),
      fileService: HttpFileService(),
    ),
  );
}

String durationToString(Duration duration) {
  var pattern = I18N.current.duration_hour_minutes_format;
  if (duration.inDays >= 1) {
    pattern = I18N.current.duration_date_hour_minutes_format;
  }

  var format = DateFormat(pattern, Platform.localeName);

  // ignore: invalid_use_of_visible_for_testing_member, deprecated_member_use
  var parsedPattern = format.parsePattern(pattern);

  DateTime d = DateTime(0).add(duration);
  var result = StringBuffer();

  for (var field in parsedPattern) {
    if (field.fullPattern() == 'd') {
      result.write(duration.inDays.toString());
    } else if (field.fullPattern() == 'dd') {
      result.write(duration.inDays.toString().padLeft(2, '0'));
    } else {
      result.write(field.format(d));
    }
  }
  return result.toString();
}

String dateTimeToString(DateTime date) {
  var timeFormat = DateFormat().add_Hm_12h();
  String res = timeFormat.format(date);

  if (DateTime
      .now()
      .difference(date)
      .inDays >= 1) {
    var format = DateFormat.yMd(Platform.localeName);
    res = "${format.format(date)} $res";
  }
  return res;
}

const _sizeVal = {
  'S': 0,
  'M': 1,
  'L': 2,
};

const _sizeXMult = {'S': -1, 'L': 1};

int compareSize(String sizeA, String sizeB) {
  if (sizeA.contains("/") && sizeB.contains("/")) {
    return sizeA.compareTo(sizeB);
  } else if (sizeA.contains("/")) {
    return 1;
  } else if (sizeB.contains("/")) {
    return -1;
  }

  if (sizeA.isEmpty && sizeB.isEmpty) {
    return 0;
  } else if (sizeA.isEmpty) {
    return 1;
  } else if (sizeB.isEmpty) {
    return -1;
  }

  var codeSizeA = sizeA.characters.last;
  var codeSizeB = sizeB.characters.last;

  if (_sizeVal[codeSizeA] == null) {
    return 1;
  }
  if (_sizeVal[codeSizeB] == null) {
    return -1;
  }

  if (_sizeVal[codeSizeA] != _sizeVal[codeSizeB]) {
    return _sizeVal[codeSizeA]!.compareTo(_sizeVal[codeSizeB]!);
  }

  var xxxA = sizeA.length > 1 ? sizeA.substring(0, sizeA.length - 1) : "";
  var xxxB = sizeB.length > 1 ? sizeB.substring(0, sizeB.length - 1) : "";

  var regexNumberX = RegExp(r"(?<count>\d+)[X][A-Z]");
  var matchA = regexNumberX.firstMatch(sizeA);
  if (matchA != null) {
    var numberX = int.parse(matchA.namedGroup("count")!);
    xxxA = "".padLeft(numberX, "X");
  }
  var matchB = regexNumberX.firstMatch(sizeB);
  if (matchB != null) {
    var numberX = int.parse(matchB.namedGroup("count")!);
    xxxB = "".padLeft(numberX, "X");
  }

  var lengthA = xxxA.length * (_sizeXMult[codeSizeA] ?? 1);
  var lengthB = xxxB.length * (_sizeXMult[codeSizeB] ?? 1);
  return lengthA.compareTo(lengthB);
}

extension HexColor on Color {
  static Color fromHex(String hexString) {
    final buffer = StringBuffer();
    if (hexString.length == 6 || hexString.length == 7) buffer.write('ff');
    buffer.write(hexString.replaceFirst('#', ''));
    return Color(int.parse(buffer.toString(), radix: 16));
  }

  static Color fromText(String text) {
    var hash = 3001;
    for (var i = 0; i < text.length; i++) {
      hash = ((hash << 4) + hash) + text.codeUnitAt(i);
    }
    if (hash < 0) {
      hash = hash * -1;
    }

    var hexVal = hash.toRadixString(16);
    while (hexVal.length < 6) {
      hexVal = "f$hexVal";
    }
    return fromHex(hexVal.substring(0, 6));
  }
}

extension AutoHour12 on DateFormat {
  // ignore: non_constant_identifier_names
  DateFormat add_Hm_12h([BuildContext? context]) {
    try {
      if (!SettingsProvider.instance.use24HourFormat(context)) {
        return add_jm();
      }
    }catch(_){}
    return add_Hm();
  }
}


Duration meanDuration(List<Duration> durations) {
  var milliseconds = durations.map((e) => e.inMilliseconds).toList();
  if (milliseconds.isEmpty) {
    return Duration.zero;
  }

  var meanMilliseconds =
      milliseconds.reduce((a, b) => a + b) / milliseconds.length;
  return Duration(milliseconds: meanMilliseconds.floor());
}

Duration sumDuration(List<Duration> durations) {
  var milliseconds = durations.map((e) => e.inMilliseconds).toList();
  if (milliseconds.isEmpty) {
    return Duration.zero;
  }
  var sumMilliseconds = milliseconds.reduce((a, b) => a + b);
  return Duration(milliseconds: sumMilliseconds.floor());
}

Duration maxDuration(List<Duration> durations) {
  var milliseconds = durations.map((e) => e.inMilliseconds).toList();
  if (milliseconds.isEmpty) {
    return Duration.zero;
  }
  var meanMilliseconds = milliseconds.reduce(math.max);
  return Duration(milliseconds: meanMilliseconds.floor());
}

Duration minDuration(List<Duration> durations) {
  var milliseconds = durations.map((e) => e.inMilliseconds).toList();
  if (milliseconds.isEmpty) {
    return Duration.zero;
  }
  var meanMilliseconds = milliseconds.reduce(math.min);
  return Duration(milliseconds: meanMilliseconds.floor());
}

bool useMobileLayout(BuildContext context){
  var sizeOf = MediaQuery.sizeOf(context);
  var side = sizeOf.width;
  return side < 800;
}