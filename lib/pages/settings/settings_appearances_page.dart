import 'package:diap_stash/common.dart';
import 'package:diap_stash/components/settings/config_widgets/radio_settings_widget.dart';
import 'package:diap_stash/pages/settings_page.dart';
import 'package:diap_stash/providers/settings_provider.dart';
import 'package:flutter/foundation.dart';
import 'package:settings_ui/settings_ui.dart';
import 'package:variable_app_icon/variable_app_icon.dart';

class SettingsAppearancesPage extends StatefulWidget {
  const SettingsAppearancesPage({super.key});

  @override
  State<SettingsAppearancesPage> createState() =>
      _SettingsAppearancesPageState();
}

class _SettingsAppearancesPageState extends State<SettingsAppearancesPage> {
  @override
  Widget build(BuildContext context) {
    SettingsProvider settingsProvider = Provider.of<SettingsProvider>(context);
    var icon = settingsProvider.spInstance.getString("icon") ?? "discret";
    var theme = settingsProvider.spInstance.getString("theme") ?? "system";
    var use24HFormat = settingsProvider.spInstance
            .getString(SettingsProvider.keyUse24HourFormat) ??
        "system";

    return Scaffold(
        appBar: AppBar(
          title: Text(I18N.current.settings_appearances),
        ),
        body: SettingsList(
          //platform: DevicePlatform.iOS,
          sections: [
            SettingsSection(
                //title: Text(""),
                tiles: [
                  if (!kIsWeb)
                    SettingsTile(
                        title: Text(I18N.current.settings_icon),
                        leading: const Icon(Icons.home),
                        value: RadioSettingsWdiget(
                            group: "icon",
                            value: icon,
                            //text: Text(I18N.current.settings_icon),
                            options: {
                              "discret":
                                  Text(I18N.current.settings_icon_discret),
                              "diaper": Text(I18N.current.settings_icon_diaper),
                            },
                            onChanged: (value) {
                              switch (value) {
                                case "diaper":
                                  settingsProvider.spInstance
                                      .setString("icon", value!);
                                  VariableAppIcon.changeAppIcon(
                                      iosIcon: "LogoDiaper",
                                      androidIconId: "appicon.DIAPER");
                                  setState(() {});
                                  break;
                                case "discret":
                                  settingsProvider.spInstance
                                      .setString("icon", value!);
                                  VariableAppIcon.changeAppIcon(
                                      iosIcon: "AppIcon",
                                      androidIconId: "appicon.DEFAULT");
                                  setState(() {});
                                  break;
                              }
                            })),
                  SettingsTile(
                      title: Text(I18N.current.settings_theme),
                      leading: const Icon(Icons.light_mode),
                      value: RadioSettingsWdiget(
                          group: "theme",
                          value: theme,
                          //text: Text(I18N.current.settings_theme),
                          options: {
                            "system": Text(I18N.current.settings_theme_system,
                                softWrap: true),
                            "light": Text(I18N.current.settings_theme_light,
                                softWrap: true),
                            "dark": Text(I18N.current.settings_theme_dark,
                                softWrap: true),
                          },
                          onChanged: (value) {
                            settingsProvider.spInstance
                                .setString("theme", value ?? "system");
                            setState(() {
                              // ignore: invalid_use_of_visible_for_testing_member, invalid_use_of_protected_member
                              settingsProvider.notifyListeners();
                            });
                          })),
                  SettingsTile(
                    title: Text(I18N.current.settings_use_24_hour_format),
                    leading: const Icon(Icons.access_time_outlined),
                    value: RadioSettingsWdiget(
                        group: "use24HourFormat",
                        value: use24HFormat,
                        //text: Text(I18N.current.settings_theme),
                        options: {
                          "system": Text(
                              I18N.current.settings_use_24_hour_format_system,
                              softWrap: true),
                          "true": Text(
                              I18N.current.settings_use_24_hour_format_24h,
                              softWrap: true),
                          "false": Text(
                              I18N.current.settings_use_24_hour_format_12h,
                              softWrap: true),
                        },
                        onChanged: (val) {
                          settingsProvider.spInstance.setString(
                              SettingsProvider.keyUse24HourFormat,
                              val ?? "system");
                          setState(() {
                            // ignore: invalid_use_of_visible_for_testing_member, invalid_use_of_protected_member
                            settingsProvider.notifyListeners();
                          });
                        }),
                  )
                ]),
          ],
          lightTheme: SettingsPage.settingsThemeData(context),
        ));
  }
}
