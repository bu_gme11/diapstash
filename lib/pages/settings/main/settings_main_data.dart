import 'dart:convert';
import 'dart:developer';
import 'dart:math' as math;
import 'package:csv/csv.dart';
import 'package:diap_stash/common.dart';
import 'package:diap_stash/components/dialogs/dialog_yes_no.dart';
import 'package:diap_stash/pages/settings/settings_manage_sharing_page.dart';
import 'package:diap_stash/pages/settings_page.dart';
import 'package:diap_stash/providers/diaper_provider.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:sentry_flutter/sentry_flutter.dart';
import 'package:settings_ui/settings_ui.dart';
import 'package:share_plus/share_plus.dart';
import 'package:universal_html/html.dart' as html;

extension SettingsPageStateData on SettingsPageState {
  SettingsSection buildData() {
    return SettingsSection(
      tiles: [
        SettingsTile(
            title: Text(I18N.current.settings_data_force_reload_cloud),
            leading: const Icon(Icons.refresh),
            onPressed: (context) {
              DefaultCacheManager().emptyCache();
              Provider.of<DiaperProvider>(context, listen: false).loadData();
            }),
        SettingsTile.navigation(
          title: Text(I18N.current.settings_data_manage_sharing),
          leading: const Icon(Icons.share),
          onPressed: (context) {
            Navigator.of(context).push(MaterialPageRoute(
                builder: (context) => const SettingsManageSharingPage()));
          },
        ),
        //if (!kIsWeb)
        SettingsTile(
          title: Text(I18N.current.settings_data_export_history),
          leading: const Icon(Icons.history),
          onPressed: (context) {
            if (kIsWeb) {
              downloadWebHistoryCSV();
            } else {
              exportHistoryCSV();
            }
          },
        ),
        SettingsTile(
          title: Text(I18N.current.settings_data_export_stock),
          leading: const Icon(Icons.shelves),
          onPressed: (context) {
            if (kIsWeb) {
              downloadWebStockCSV();
            } else {
              exportStockCSV();
            }
          },
        ),
        SettingsTile(
          title: Text(I18N.current.settings_data_clear_history),
          leading: const Icon(Icons.clear),
          onPressed: (context) async {
            var diaperProvider =
                Provider.of<DiaperProvider>(context, listen: false);
            ResultDialogYesNo? result = await showDialog(
                context: context,
                builder: (ctx) => DialogYesNo(
                    title: Text(I18N.current.settings_data_clear_history),
                    content: Text(
                        I18N.current.settings_data_clear_history_confirm)));
            if (result == ResultDialogYesNo.yes) {
              await diaperProvider.resetChangeHistory(doNotifyListeners: true);
            }
          },
        )
      ],
      title: Text(I18N.current.settings_data),
    );
  }

  Future<String> generateStockCSV() async {
    DiaperProvider dp = Provider.of<DiaperProvider>(context, listen: false);

    var stock = dp.currentStock.toList(growable: false);
    var listCSV = stock.map((e) {
      return [
        e.type.brandCode,
        e.type.brand?.name,
        e.type.name,
        e.type.availableSizes,
        e.type.official,
        e.type.type.toString(),
        e.type.target,
        e.type.style,
        e.type.discontinued,
        e.total(),
        e.count,
        e.prices
      ];
    });

    const headers = [
      "brandCode",
      "brandName",
      "typeName",
      "availableSizes",
      "official",
      "type",
      "target",
      "style",
      "discontinued",
      "stockTotal",
      "count",
      "prices"
    ];

    String csv = const ListToCsvConverter().convert([headers, ...listCSV]);
    return csv;
  }

  Future<String> generateHistoryCSV() async {
    DiaperProvider dp = Provider.of<DiaperProvider>(context, listen: false);
    var list = dp.changes.toList();
    list.sort((a, b) => a.startTime.compareTo(b.startTime));

    var listCSV = list.map((e) => e.toCSV()).toList();

    double maxDiapers = 1.0;

    if (listCSV.isNotEmpty) {
      maxDiapers = listCSV.map((e) => (e.length - 2) / 4).reduce(math.max);
    }

    var headers = ["startTime", "endTime", "tags", "state", "price"];
    for (var i = 1; i <= maxDiapers; i++) {
      headers
          .addAll([
        "brandCode_$i",
        "brandName_$i",
        "type_name_$i",
        "size_$i",
        "price_$i"
      ]);
    }

    String csv = const ListToCsvConverter().convert([headers, ...listCSV]);
    return csv;
  }

  void exportHistoryCSV() async {
    try {
      var csv = await generateHistoryCSV();
      var bytes = Uint8List.fromList(utf8.encode(csv));

      await Share.shareXFiles([
        XFile.fromData(bytes,
            mimeType: "text/csv", name: "diapstash_history.csv")
      ], subject: "DiapStash history");
    } catch (e, s) {
      log("Error on export csv $e");
      Sentry.captureException(e, stackTrace: s);
    }
  }

  void exportStockCSV() async {
    try {
      var csv = await generateStockCSV();
      var bytes = Uint8List.fromList(utf8.encode(csv));
      await Share.shareXFiles([
        XFile.fromData(bytes,
            mimeType: "text/csv", name: "diapstash_stock.csv")
      ], subject: "DiapStash stock");
    } catch (e, s) {
      log("Error on export stock csv $e");
      Sentry.captureException(e, stackTrace: s);
    }
  }

  void downloadWebHistoryCSV() async {
    try {
      var csv = await generateHistoryCSV();
      var bytes = Uint8List.fromList(utf8.encode(csv));
      final content = base64Encode(bytes);
      html.AnchorElement(href: "data:text/csv;charset=utf-8;base64,$content")
        ..setAttribute("download", "diapstash_history.csv")
        ..click();
    } catch (e, s) {
      log("Error on export csv $e");
      Sentry.captureException(e, stackTrace: s);
    }
  }

  void downloadWebStockCSV() async {
    try {
      var csv = await generateStockCSV();
      var bytes = Uint8List.fromList(utf8.encode(csv));
      final content = base64Encode(bytes);
      html.AnchorElement(href: "data:text/csv;charset=utf-8;base64,$content")
        ..setAttribute("download", "diapstash_stock.csv")
        ..click();
    } catch (e, s) {
      log("Error on export stock csv $e");
      Sentry.captureException(e, stackTrace: s);
    }
  }
}
