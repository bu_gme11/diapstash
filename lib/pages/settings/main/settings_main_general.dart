import 'package:diap_stash/common.dart';
import 'package:diap_stash/components/catalog/catalog_filter.dart';
import 'package:diap_stash/components/settings/config_widgets/radio_settings_widget.dart';
import 'package:diap_stash/pages/settings/settings_appearances_page.dart';
import 'package:diap_stash/pages/settings/settings_notifications_page.dart';
import 'package:diap_stash/pages/settings_page.dart';
import 'package:diap_stash/providers/settings_provider.dart';
import 'package:diap_stash/utils/numerical_range_formatter.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localized_locales/flutter_localized_locales.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:settings_ui/settings_ui.dart';

extension SettingsPageStateGeneral on SettingsPageState {
  SettingsSection buildGeneral() {
    SettingsProvider sp = Provider.of<SettingsProvider>(context, listen: true);

    String? language = I18N.current.settings_general_language_system;
    if (sp.language != "system") {
      language = LocaleNames.of(context)!.nameOf(sp.language);
    }

    return SettingsSection(
      tiles: [
        SettingsTile.navigation(
          leading: const Icon(Icons.color_lens_outlined),
          title: Text(I18N.current.settings_appearances),
          onPressed: (ctx) {
            Navigator.of(context).push(MaterialPageRoute(
              builder: (context) => const SettingsAppearancesPage(),
            ));
          },
        ),
        if (!kIsWeb)
          SettingsTile.navigation(
            leading: const Icon(Icons.notifications),
            title: Text(I18N.current.settings_notification),
            onPressed: (ctx) {
              Navigator.of(context).push(MaterialPageRoute(
                builder: (context) => const SettingsNotificationsPage(),
              ));
            },
          ),
        SettingsTile(
          leading: const Icon(Icons.translate),
          title: Text(I18N.current.settings_general_language),
          onPressed: (ctx) {
            showDialogLanguage(ctx);
          },
          trailing: language != null ? Text(language) : null,
        ),
        SettingsTile(
            title: Text(I18N.current.threshold_low_stock),
            leading: const Icon(Icons.trending_down),
            trailing: !kIsWeb ? buildThresholdEdit(context, sp) : null,
            description: kIsWeb ? buildThresholdEdit(context, sp) : null),
        SettingsTile.navigation(
          title: Text(I18N.current.settings_general_predefined_bag_size),
          leading: const FaIcon(FontAwesomeIcons.boxOpen),
          onPressed: (context) {
            dialogPrefredSizeBag(context);
          },
        ),
        SettingsTile.navigation(
          title: Text(I18N.current.settings_general_keep_zero_stock),
          leading: const FaIcon(FontAwesomeIcons.eyeSlash),
          trailing: Text(sp.keepZeroStockDiaperI18N),
          onPressed: (context) {
            dialogKeepStockDiaper(context);
          },
        ),
        SettingsTile.switchTile(
          initialValue: sp.allowInfiniteStock,
          onToggle: (val) async {
            sp.spInstance
                .setBool(SettingsProvider.keyAllowInfiniteStock, val)
                .then((value) {
              if (mounted) {
                // ignore: invalid_use_of_protected_member
                setState(() {});
              }
            });
          },
          title: Text(I18N.current.settings_general_allow_infinite_stock),
          leading: const FaIcon(FontAwesomeIcons.infinity),
        ),
        SettingsTile.switchTile(
          initialValue: sp.allowRandomStock,
          onToggle: (val) async {
            await sp.setAllowRandomStock(val);
          },
          title: Text(I18N.current.settings_general_allow_random_stock),
          leading: const FaIcon(FontAwesomeIcons.dice),
        ),
        SettingsTile.navigation(
          title: Text(I18N.current.settings_general_filter_catalog),
          leading: const Icon(Icons.filter_alt_outlined),
          onPressed: (context) {
            dialogFilterCatalog(context);
          },
        ),
        SettingsTile.switchTile(
          title: Text(I18N.current.settings_general_send_rating),
          description: Text(sp.hasCloud()
              ? I18N.current.settings_general_send_rating_description_cloud_sync
              : I18N.current.settings_general_send_rating_description),
          enabled: !sp.hasCloud(),
          leading: const Icon(Icons.star),
          initialValue: sp.hasCloud() || sp.sendRating,
          onToggle: !sp.hasCloud()
              ? (value) {
                  sp.setSendRating(value);
                }
              : null,
        ),
      ],
      title: Text(I18N.current.settings_general),
    );
  }

  void showDialogLanguage(BuildContext ctx) {
    showDialog(
        context: ctx,
        builder: (context) {
          SettingsProvider sp = Provider.of(context, listen: true);
          var currentLanguage = sp.language;

          Map<String, Widget> locales = {
            "system": Text(I18N.current.settings_general_language_system)
          };
          for (var l in I18N.translatedLocales) {
            var localName = LocaleNames.of(context)!.nameOf(l.toLanguageTag());
            if (localName != null) {
              locales[l.toLanguageTag()] = Text(localName);
            }
          }

          return AlertDialog(
            title: Text(I18N.current.settings_general_language),
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                RadioSettingsWdiget(
                  group: "locale",
                  value: currentLanguage,
                  options: locales,
                  onChanged: (value) {
                    if (value != null) {
                      sp.setLanguage(value);
                    }
                  },
                )
              ],
            ),
            actions: [
              TextButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: Text(I18N.current.close))
            ],
          );
        });
  }

  void dialogKeepStockDiaper(BuildContext context) {
    SettingsProvider sp = Provider.of(context, listen: false);
    showDialog(
        context: context,
        builder: (context) => AlertDialog(
            title: Text(I18N.current.settings_general_keep_zero_stock),
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                RadioSettingsWdiget(
                  group: "keepZeroStock",
                  value: sp.keepZeroStockDiaper,
                  options: {
                    "never": Text(
                        I18N.current.settings_general_keep_zero_stock_never),
                    "favorite": Text(I18N.current
                        .settings_general_keep_zero_stock_only_favorite),
                    "always": Text(
                        I18N.current.settings_general_keep_zero_stock_always)
                  },
                  onChanged: (value) {
                    sp.setKeepZeroStockDiaper(value!).then((value) {
                      Navigator.of(context).pop();
                    });
                  },
                ),
              ],
            )));
  }

  void dialogFilterCatalog(BuildContext context) {
    //SettingsProvider sp = Provider.of(context, listen: false);

    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text(I18N.current.settings_general_filter_catalog),
            content: getCatalogFilter(),
            actions: [
              TextButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: Text(I18N.current.close))
            ],
          );
        });
  }

  void dialogPrefredSizeBag(BuildContext context) {
    showDialog(
      context: context,
      builder: (context) {
        TextEditingController addCtrl = TextEditingController();

        return AlertDialog(
          title: Text(I18N.current.settings_general_predefined_bag_size),
          content: StatefulBuilder(
            builder: (context, setState) {
              var sizes = SettingsProvider.instance.predefinedBagSize.toList();
              var ctrls =
                  sizes.map((e) => TextEditingController(text: "$e")).toList();
              var addFocusNode = FocusNode();

              addSize() {
                var val = int.tryParse(addCtrl.text);
                if (val != null && !sizes.contains(val)) {
                  var clonedSizes = sizes.toList();
                  clonedSizes.add(val);
                  clonedSizes.sort((a, b) => a - b);
                  SettingsProvider.instance
                      .setPredefinedBagSize(clonedSizes)
                      .then((b) {
                    addCtrl.text = "";
                    setState(() {});
                  });
                }
              }

              return Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  for (var i = 0; i < sizes.length; i++)
                    TextFormField(
                      controller: ctrls[i],
                      onChanged: (val) {
                        var intVal = int.tryParse(val);
                        if (intVal != null) {
                          var clonedSizes = sizes.toList();
                          clonedSizes[i] = intVal;
                          clonedSizes.sort((a, b) => a - b);
                          SettingsProvider.instance
                              .setPredefinedBagSize(clonedSizes)
                              .then((b) {
                            setState(() {
                              addFocusNode.requestFocus();
                            });
                          });
                        }
                      },
                      inputFormatters: [
                        FilteringTextInputFormatter.digitsOnly,
                        NumericalRangeFormatter(min: 1)
                      ],
                      decoration: InputDecoration(
                          suffixIcon: TextButton(
                              onPressed: () {
                                var clonedSizes = sizes.toList();
                                clonedSizes.removeAt(i);
                                SettingsProvider.instance
                                    .setPredefinedBagSize(clonedSizes)
                                    .then((b) {
                                  setState(() {});
                                  addCtrl.text = "";
                                });
                              },
                              child: const Icon(Icons.delete))),
                    ),
                  if (sizes.isEmpty)
                    TextFormField(
                      initialValue:
                          I18N.current.settings_general_no_predefined_bag_size,
                      enabled: false,
                    ),
                  TextFormField(
                    focusNode: addFocusNode,
                    autofocus: true,
                    controller: addCtrl,
                    inputFormatters: [
                      FilteringTextInputFormatter.digitsOnly,
                      NumericalRangeFormatter(min: 1)
                    ],
                    decoration: InputDecoration(
                        suffixIcon: TextButton(
                            onPressed: addSize, child: const Icon(Icons.add))),
                    onFieldSubmitted: (value) {
                      addSize();
                    },
                  ),
                ],
              );
            },
          ),
          actions: [
            TextButton(
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: Text(I18N.current.close))
          ],
        );
      },
    );
  }

  Align buildThresholdEdit(BuildContext context, SettingsProvider sp) {
    return Align(
      alignment: Alignment.centerRight,
      child: SizedBox(
        width: !kIsWeb ? 50 : null,
        child: TextFormField(
            controller: thresholdLowStockCtrl,
            keyboardType: const TextInputType.numberWithOptions(
                decimal: false, signed: false),
            inputFormatters: [
              FilteringTextInputFormatter.digitsOnly,
              NumericalRangeFormatter(min: 0)
            ],
            textAlign: TextAlign.right,
            onFieldSubmitted: (val) {
              var valInt = int.tryParse(val);
              if (valInt != null) {
                sp.spInstance.setInt(
                    SettingsProvider.keyThresholdLowStock, valInt);
              }
            }),
      ),
    );
  }
}

