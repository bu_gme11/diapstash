import 'package:diap_stash/common.dart';
import 'package:diap_stash/components/dialogs/whats_new_dialog.dart';
import 'package:diap_stash/pages/settings_page.dart';
import 'package:diap_stash/providers/settings_provider.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:settings_ui/settings_ui.dart';
import 'package:universal_io/io.dart';
import 'package:url_launcher/url_launcher.dart';

extension SettingsPageStateAbout on SettingsPageState {
  SettingsSection buildAbout() {
    SettingsProvider sp = Provider.of(context, listen: true);
    return SettingsSection(
      tiles: [
        buildPackageInfoSettingsTile(
            title: I18N.current.settings_about_version,
            builder: (info) => Text(info.version)),
        SettingsTile.navigation(
            title: Text(I18N.current.settings_about_show_whats_news),
            onPressed: (context) async {
              await generateReleaseDialog(context, showLast: true, force: true);
            }),
        SettingsTile.switchTile(
            initialValue: sp.whatNewHide,
            onToggle: (value) async {
              await sp.setWhatsNewHide(value);
            },
            title: Text(I18N.current.settings_about_whats_news_hide)),
        buildPackageInfoSettingsTile(
            title: I18N.current.settings_about_build_number,
            builder: (info) => Text(info.buildNumber)),
        buildLinkSettingsTile(
            title: I18N.current.settings_about_website, url: websiteURL,
            faIcon: const FaIcon(FontAwesomeIcons.globe)),
        buildLinkSettingsTile(
            title: I18N.current.settings_about_catalog,
            url: catalogURL,
            faIcon: const FaIcon(FontAwesomeIcons.list)),
        buildLinkSettingsTile(
            title: I18N.current.settings_about_privacy,
            url: privacyURL,
            faIcon: const FaIcon(FontAwesomeIcons.lock)),
        buildLinkSettingsTile(
            title: I18N.current.settings_about_reddit_community,
            url: redditURL,
            faIcon: const FaIcon(FontAwesomeIcons.reddit)),
        buildLinkSettingsTile(
            title: I18N.current.settings_about_source_code,
            url: gitlabURL,
            faIcon: const FaIcon(FontAwesomeIcons.gitlab)),
      ],
      title: Text(I18N.current.settings_about),
    );
  }

  SettingsSection buildCredits() {
    return SettingsSection(
      tiles: [
        SettingsTile(
          title: Text(I18N.current.settings_credits_translations),
          description: Table(columnWidths: const {
            1: FlexColumnWidth(2)
          }, children: [
            buildCreditRow(I18N.current.settings_credits_translations_spanish,
                I18N.current.settings_credits_translations_spanish_authors),
            buildCreditRow(I18N.current.settings_credits_translations_dutch,
                I18N.current.settings_credits_translations_dutch_authors),
            buildCreditRow(I18N.current.settings_credits_translations_german,
                I18N.current.settings_credits_translations_german_authors),
            buildCreditRow(I18N.current.settings_credits_translations_italian,
                I18N.current.settings_credits_translations_italian_authors),
            buildCreditRow(I18N.current.settings_credits_translations_korean,
                I18N.current.settings_credits_translations_korean_authors),
          ]),
        ),
        buildLinkSettingsTile(
            title: I18N.current.settings_credits_translations_contribute,
            url: crowdinURL,
            icon: const Icon(Icons.translate)
        ),
      ],
      title: Text(I18N.current.settings_credits),
    );
  }

  AbstractSettingsTile buildPackageInfoSettingsTile(
      {required String title,
      required Widget Function(PackageInfo) builder,
      Widget? leading}) {
    return SettingsTile(
        title: Text(title),
        value: FutureBuilder(
          future: PackageInfo.fromPlatform(),
          builder: (context, snapshot) {
            if (!snapshot.hasData) return Container();
            return builder(snapshot.data!);
          },
        ),
        leading: leading);
  }

  AbstractSettingsTile buildLinkSettingsTile(
      {required String title,
      required String url,
      String? description,
      FaIcon? faIcon,
      Icon? icon}) {
    return SettingsTile.navigation(
      title: Text(title),
      value: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Container(
          constraints: Platform.isIOS ? const BoxConstraints(maxWidth: 175) : null,
          child: Text(url,
              softWrap: true,
              style: const TextStyle(
                  color: Colors.blue, decoration: TextDecoration.underline)),
        ),
      ),
      leading: faIcon ?? icon,
      description: description != null ? Text(description) : null,
      onPressed: (context) {
        var uri = Uri.parse(url);
        launchUrl(uri, mode: LaunchMode.platformDefault);
      },
    );
  }
}

TableRow buildCreditRow(String lang, String authors) {
  if (authors.trim().isEmpty || authors == "_") {
    return TableRow(children: [Container(), Container()]);
  }
  return TableRow(children: [
    Text(
      lang,
      textAlign: TextAlign.center,
      style: const TextStyle(fontWeight: FontWeight.bold),
    ),
    Text(
      authors,
      textAlign: TextAlign.center,
    ),
  ]);
}
