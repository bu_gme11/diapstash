// ignore_for_file: invalid_use_of_protected_member

import 'package:diap_stash/common.dart';
import 'package:diap_stash/components/dialogs/dialog_yes_no.dart';
import 'package:diap_stash/pages/main_page.dart';
import 'package:diap_stash/pages/settings_page.dart';
import 'package:diap_stash/providers/diaper_provider.dart';
import 'package:diap_stash/providers/settings_provider.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';
import 'package:mobile_scanner/mobile_scanner.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:sentry_flutter/sentry_flutter.dart';
import 'package:settings_ui/settings_ui.dart';
import 'package:share_plus/share_plus.dart';

extension SettingsPageStateCloud on SettingsPageState {
  SettingsSection buildCloud() {
    var settingsProvider = Provider.of<SettingsProvider>(context, listen: true);
    DiaperProvider diaperProvider =
        Provider.of<DiaperProvider>(context, listen: false);

    var hasCloud = settingsProvider.hasCloud();
    return SettingsSection(
      tiles: [
        SettingsTile.switchTile(
          title: Text(I18N.current.settings_cloud_save_in_cloud),
          leading: const Icon(Icons.cloud),
          initialValue: hasCloud,
          onToggle: (val) {
            if (val == true) {
              enableCloudPrivacyDialog(context).then((ok) async {
                if (ok == true) {
                  await settingsProvider.setCloud(true);
                  await diaperProvider.sendDataToCloud();
                  if (mounted) {
                    setState(() {});
                  }
                }
              });
            } else {
              askDeleteUser();
            }
          },
          description: hasCloud
              ? Text(I18N.current.settings_cloud_sync_info_delete)
              : null,
        ),
        if (hasCloud)
          SettingsTile.navigation(
            title: Text(I18N.current.settings_cloud_sync_another_with_devices),
            leading: const Icon(Icons.qr_code),
            onPressed: (context) {
              showDialog(
                context: context,
                builder: (context) {
                  return AlertDialog(
                    title: Text(
                        I18N.current.settings_cloud_sync_another_with_devices),
                    content: SizedBox(
                        width: 250,
                        child: QrImageView(
                          backgroundColor: Colors.white,
                          data: settingsProvider.getQRCodeData(),
                        )),
                    actions: [
                      TextButton(
                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                          child: Text(I18N.current.close))
                    ],
                  );
                },
              );
            },
          )
        else if (!kIsWeb)
          SettingsTile.navigation(
            title: Text(I18N.current.settings_cloud_sync_another_from_devices),
            leading: const Icon(Icons.qr_code),
            onPressed: (context) async {
              if ((await Permission.camera.request()).isGranted) {
                // ignore: use_build_context_synchronously
                await showDialog(
                  context: context,
                  builder: (context) {
                    return AlertDialog(
                      title: Text(I18N
                          .current.settings_cloud_sync_another_from_devices),
                      content: SizedBox(
                          width: 400,
                          height: 400,
                          child: MobileScanner(
                              controller: MobileScannerController(
                                detectionSpeed: DetectionSpeed.noDuplicates,
                                autoStart: true,
                                facing: CameraFacing.back,
                                torchEnabled: false,
                              ),
                              onDetect: (barcode) async {
                                var navigatorState = Navigator.of(context,
                                    rootNavigator: true);
                                if(settingsProvider.hasCloud()){
                                  navigatorState.pushNamedAndRemoveUntil(
                                      MainPage.routeName, (route) => false);
                                  return;
                                }
                                if (barcode.barcodes.isEmpty) {
                                  debugPrint('Failed to scan Barcode');
                                } else {
                                  final String code =
                                      barcode.barcodes.first.rawValue!;

                                  var ok = await settingsProvider
                                      .cloudFromQRCode(code);
                                  if (ok) {
                                    navigatorState.pushNamedAndRemoveUntil(
                                        MainPage.routeName, (route) => false);
                                    await DiaperProvider.instance.sendDataToCloud();
                                    await DiaperProvider.instance.loadData();
                                    if (context.mounted) {
                                      setState(() {});

                                    }
                                  }
                                  debugPrint('Barcode found! $code');
                                }
                              })),
                      actions: [
                        TextButton(
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                            child: Text(I18N.current.cancel))
                      ],
                    );
                  },
                );
              } else {
                // ignore: use_build_context_synchronously
                var scaffoldMessengerState = ScaffoldMessenger.of(context);
                scaffoldMessengerState.showSnackBar(SnackBar(
                  content: Text(
                      I18N.current.settings_cloud_sync_camera_perms_denied),
                  backgroundColor: Colors.red,
                ));
              }
            },
          ),
        if (hasCloud)
          SettingsTile.navigation(
            title: Text(I18N.current.settings_cloud_sync_share_link),
            leading: const Icon(Icons.link),
            onPressed: (context) async {
              var url = buildCloudShareURL(settingsProvider);
              await Share.share(url, subject: "DiapStash Cloud Sync Link");
            },
          ),
        if (hasCloud)
          SettingsTile.navigation(
            title: Text(I18N.current.settings_cloud_sync_copy_link),
            leading: const Icon(Icons.copy),
            onPressed: (context) async {
              var url = buildCloudShareURL(settingsProvider);
              await Clipboard.setData(ClipboardData(text: url));
              // ignore: use_build_context_synchronously
              ScaffoldMessenger.of(context)
                  .showSnackBar(SnackBar(content: Text(I18N.current.copied)));
            },
          )
      ],
      title: Text(I18N.current.settings_cloud),
    );
  }

  Future<bool?> enableCloudPrivacyDialog(BuildContext context,
      {VoidCallback? onConfirmation}) {
    Sentry.addBreadcrumb(Breadcrumb(category: "navigation", data: const {
      "openDialog": "enableCloudPrivacyDialog",
    }));
    return showDialog<bool>(
        context: context,
        builder: (ctx) => AlertDialog(
              title: Text(I18N.current.settings_personnal_data),
              content: Text(I18N.current.settings_about_privacy_text),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.pop(ctx, false);
                    },
                    child: Text(I18N.current.cancel)),
                TextButton(
                    onPressed: () async {
                      if (onConfirmation != null) {
                        onConfirmation();
                      }
                      Navigator.pop(ctx, true);
                    },
                    child: Text(I18N.current.confirm))
              ],
            ));
  }

  askDeleteUser() async {
    SettingsProvider sp = Provider.of<SettingsProvider>(context, listen: false);
    DiaperProvider dp = Provider.of<DiaperProvider>(context, listen: false);
    ResultDialogYesNo? result = await showDialog(
        context: context,
        builder: (context) => DialogYesNo(
            title: Text(I18N.current.settings_cloud_ask_delete_user_title),
            content: Text(I18N.current.settings_cloud_ask_delete_user_content),
            cancelButton: true));

    if (result == ResultDialogYesNo.yes) {
      await dp.unlinkCloudSync(dropCloudSync: true, doNotifyListeners: true);
      await sp.setCloud(false);
      if (mounted) {
        setState(() {});
      }
    } else { // assume no or null
      await dp.unlinkCloudSync(dropCloudSync: false, doNotifyListeners: true);
      await sp.setCloud(false);
      if (mounted) {
        setState(() {});
      }
    }
  }

  String buildCloudShareURL(SettingsProvider settingsProvider) {
    var data = settingsProvider.getQRCodeData().replaceAll("\$", "___");

    var url = "https://diapstash.com/cloud-sync/${Uri.encodeFull(data)}";
    return url;
  }
}
