import 'package:diap_stash/common.dart';
import 'package:diap_stash/pages/main_page.dart';
import 'package:diap_stash/pages/settings_page.dart';
import 'package:settings_ui/settings_ui.dart';
import 'package:universal_html/html.dart' as html;

extension SettingsPageStateWeb on SettingsPageState {
  SettingsSection buildWeb() {
    return SettingsSection(tiles: [
      SettingsTile(
        title: Text(I18N.current.settings_web_refresh_app),
        leading: const Icon(Icons.replay_circle_filled),
        onPressed: (context) {
          html.window.location.reload();
        },
      ),
      SettingsTile(
        title: Text(I18N.current.settings_web_use_link),
        leading: const Icon(Icons.link),
        description: Text(I18N.current.settings_web_use_link_description),
        onPressed: (context) {
          showDialogLink(context);
        },
      ),
    ], title: Text(I18N.current.settings_web));
  }

  void showDialogLink(BuildContext context) {
    showDialog(
      context: context,
      builder: (context) {
        var ctrl = TextEditingController();


        return StatefulBuilder(builder: (context, setState) {

          isValid() {
            if (ctrl.value.text.isEmpty) return false;
            var uri = Uri.tryParse(ctrl.value.text);
            return uri != null && uri.hasAbsolutePath && uri.hasScheme && uri.host.isNotEmpty && uri.pathSegments.length > 1;
          }

          return AlertDialog(
            title: Text(I18N.current.settings_web_use_link),
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                TextFormField(
                  controller: ctrl,
                  onChanged: (val){
                    setState(() {

                    });
                  },
                  decoration: InputDecoration(label: Text(I18N.current.link)),
                ),
              ],
            ),
            actions: [
              TextButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: Text(I18N.current.cancel)),
              TextButton(
                  onPressed: isValid()
                      ? () {
                    var uri = Uri.tryParse(ctrl.value.text);
                    if (uri != null) {
                      Navigator.of(context).pop();
                      openAppLink(context, uri);
                    }
                  }
                      : null,
                  child: Text(I18N.current.use))
            ],
          );
        },);
      },
    );
  }
}
