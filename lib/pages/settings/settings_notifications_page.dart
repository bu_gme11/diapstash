import 'package:app_settings/app_settings.dart';
import 'package:diap_stash/common.dart';
import 'package:diap_stash/components/dialogs/dialog_edit_notification.dart';
import 'package:diap_stash/components/dialogs/dialog_yes_no.dart';
import 'package:diap_stash/object/interfaces.dart';
import 'package:diap_stash/object/notifications.dart';
import 'package:diap_stash/pages/settings_page.dart';
import 'package:diap_stash/providers/diaper_provider.dart';
import 'package:diap_stash/providers/notification_provider.dart';
import 'package:diap_stash/providers/settings_provider.dart';
import 'package:diap_stash/util.dart';
import 'package:diap_stash/utils/period.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:settings_ui/settings_ui.dart';

class SettingsNotificationsPage extends StatelessWidget {
  const SettingsNotificationsPage({super.key});

  @override
  Widget build(BuildContext context) {
    NotificationProvider np =
        Provider.of<NotificationProvider>(context, listen: true);
    DiaperProvider dp = Provider.of<DiaperProvider>(context);

    var defaultNotifications =
        np.notifications.where((element) => element.typeId == null).toList();
    var typedNotifications =
        np.notifications.where((element) => element.typeId != null).toList();

    Map<DiaperType, List<DSNotification>> mapTypeNotifications = {};

    for (var n in typedNotifications) {
      var type = dp.getTypeById(n.typeId!);
      if (type != null) {
        mapTypeNotifications.update(type, (value) {
          value.add(n);
          return value;
        }, ifAbsent: () {
          return [n];
        });
      }
    }

    var typedNotificationsEntries = mapTypeNotifications.entries.toList();
    typedNotificationsEntries.sort((a, b) => a.key.compareTo(b.key));

    return Scaffold(
        appBar: AppBar(
          title: Text(I18N.current.settings_notification),
        ),
        body: StatefulBuilder(
            builder: (ctx, setState) => FutureBuilder<bool>(
                future: np.askPermissions(requestPermission: false),
                builder: (context, snapshot) {
                  var allowed = snapshot.data ?? true;
                  return SettingsList(
                      //platform: DevicePlatform.iOS,
                      sections: [
                        SettingsSection(
                          title:
                              Text(I18N.current.settings_notification_default),
                          tiles: [
                            if (!allowed)
                              buildNotificationNotAllowed(np, ctx, setState),
                            for (var n in defaultNotifications)
                              buildTileNotification(context, np, n, allowed, false),
                            SettingsTile(
                                enabled: allowed,
                                title: Text(
                                    I18N.current.settings_notification_add),
                                leading: const Icon(Icons.notification_add),
                                onPressed: (context) async {
                                  showDialog(
                                      context: context,
                                      builder: (context) =>
                                          DialogEditNotification(
                                            title: I18N.current
                                                .notification_dialog_create,
                                            onSave: (var n) async {
                                              await np.saveNotification(n);
                                            },
                                            askType: false,
                                          ));
                                })
                          ],
                        ),
                        for (var nt in typedNotificationsEntries)
                          SettingsSection(
                            title: Text(nt.key.fullName),
                            tiles: [
                              for (var n in nt.value)
                                buildTileNotification(context, np, n, allowed, true)
                            ],
                          ),
                        SettingsSection(
                          tiles: [
                            SettingsTile(
                                enabled: allowed,
                                title: Text(I18N
                                    .current.settings_notification_add_type),
                                leading: const Icon(Icons.notification_add),
                                description: Text(I18N.current
                                    .settings_notification_add_type_desc),
                                onPressed: (context) async {
                                  showDialog(
                                      context: context,
                                      builder: (context) =>
                                          DialogEditNotification(
                                            title: I18N.current
                                                .notification_dialog_create,
                                            onSave: (var n) async {
                                              await np.saveNotification(n);
                                            },
                                            askType: true,
                                          ));
                                })
                          ],
                          //title: Text(I18N.current.settings_notification_diaper_type),
                        ),
                        buildSectionNightMode(context, allowed)
                      ],
                      lightTheme: SettingsPage.settingsThemeData(context));
                })));
  }

  SettingsTile buildTileNotification(BuildContext context, NotificationProvider np,
      DSNotification n, bool allowed, bool askType) {
    return SettingsTile(
        enabled: allowed,
        title: Text(n.type.localizedName()),
        value: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.max,
          children: [
            if (n.type == NotificationType.CUSTOM)
              Text(n.title, overflow: TextOverflow.ellipsis, maxLines: 2),
            Text(durationToString(n.duration))
          ],
        ),
        trailing: ButtonBar(
            alignment: MainAxisAlignment.end,
            buttonPadding: const EdgeInsets.all(0),
            children: [
              IconButton(
                  onPressed: () {
                    showDialog(
                        context: context,
                        builder: (context) => DialogEditNotification(
                              title: I18N.current.notification_dialog_edit,
                              notification: n,
                              onSave: (var n) async {
                                await np.saveNotification(n);
                              },
                              askType: askType,
                            ));
                  },
                  color: Theme.of(context).buttonTheme.colorScheme?.primary,
                  icon: const Icon(Icons.edit)),
              IconButton(
                  onPressed: () async {
                    ResultDialogYesNo? result = await showDialog(
                        context: context,
                        builder: (context) => DialogYesNo(
                            title:
                                Text(I18N.current.settings_notification_remove),
                            content: Text(
                                I18N.current.settings_notification_remove_text)));
                    if(result == ResultDialogYesNo.yes){
                      await np.deleteNotification(n);
                    }
                  },
                  color: Theme.of(context).buttonTheme.colorScheme?.primary,
                  icon: const Icon(Icons.delete))
            ]),
        leading: GestureDetector(
          child: const Icon(Icons.timer_outlined),
          onLongPress: () {
            np.notificationPreview(n);
          },
        ),
        onPressed: (context) {
          //np.notificationPreview(n);
        });
  }


  SettingsTile buildNotificationNotAllowed(
      NotificationProvider np, BuildContext ctx, StateSetter setState) {
    return SettingsTile(
      title: Text(
        I18N.current.settings_notification_not_allowed,
        style: const TextStyle(color: Colors.red),
      ),
      value: Text(I18N.current.settings_notification_not_allowed_click_enable),
      onPressed: (context) async {
        bool isGranted = false;
        try {
          var status = await Permission.notification.request();
          isGranted = status.isGranted;
        } catch (e) {
          //nothing
        }
        if (!isGranted) {
          AppSettings.openAppSettings(type: AppSettingsType.notification);
        }
      },
    );
  }

  SettingsSection buildSectionNightMode(BuildContext context, bool allowed) {
    SettingsProvider sp = Provider.of(context, listen: true);
    bool enable = sp.notificationNightMode;
    Period period = sp.notificationNightPeriod;
    bool valid = period.isValid;
    var nmNotifications = sp.notificationNightModeNotifications;
    var notDefined = Text(
      I18N.current.settings_notification_night_mode_notify_not_defined,
      style: TextStyle(color: Colors.grey.shade500),
    );

    var format = DateFormat().add_Hm_12h(context);

    return SettingsSection(tiles: [
      SettingsTile.switchTile(
          enabled: allowed,
          initialValue: enable,
          onToggle: (val) async {
            await sp.setNotificationNightMode(val);
            await updateNotifications();
          },
          title: Text(I18N.current.settings_notification_night_mode_switch)),
      SettingsTile(
          enabled: enable && allowed,
          title: Text(I18N.current.settings_notification_night_mode_period),
          onPressed: (context) async {
            await showDialogPeriod(context);
          },
          value: Padding(
            padding: const EdgeInsets.only(left: 15),
            child: valid
                ? Row(
                    children: [
                      Text(period.startFormated(format) ?? ""),
                      const Icon(Icons.arrow_forward),
                      Text(period.endFormated(format) ?? "")
                    ],
                  )
                : notDefined,
          )),
      SettingsTile(
          enabled: enable && valid && allowed,
          title: Text(I18N.current
              .settings_notification_night_mode_notify_start_not_wearing),
          onPressed: (context) async {
            await showNotificationNightModeDialog(
                context,
                NotificationProvider.nightModeStartNoWearing,
                I18N.current
                    .settings_notification_night_mode_notify_start_not_wearing);
          },
          value: Padding(
              padding: const EdgeInsets.only(left: 15),
              child: nmNotifications[
                          NotificationProvider.nightModeStartNoWearing] !=
                      null
                  ? Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(nmNotifications[NotificationProvider
                                .nightModeStartNoWearing]?["title"] ??
                            ""),
                        Text(nmNotifications[NotificationProvider
                                .nightModeStartNoWearing]?["description"] ??
                            ""),
                      ],
                    )
                  : notDefined)),
      SettingsTile(
          enabled: enable && valid && allowed,
          title: Text(I18N
              .current.settings_notification_night_mode_notify_start_wearing),
          onPressed: (context) async {
            await showNotificationNightModeDialog(
                context,
                NotificationProvider.nightModeStartWearing,
                I18N.current
                    .settings_notification_night_mode_notify_start_wearing);
          },
          value: Padding(
              padding: const EdgeInsets.only(left: 15),
              child:
                  nmNotifications[NotificationProvider.nightModeStartWearing] !=
                          null
                      ? Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(nmNotifications[NotificationProvider
                                    .nightModeStartWearing]?["title"] ??
                                ""),
                            Text(nmNotifications[NotificationProvider
                                    .nightModeStartWearing]?["description"] ??
                                ""),
                          ],
                        )
                      : notDefined)),
      SettingsTile(
          enabled: enable && valid && allowed,
          title: Text(
              I18N.current.settings_notification_night_mode_notify_end_wearing),
          onPressed: (context) async {
            await showNotificationNightModeDialog(
                context,
                NotificationProvider.nightModeEndWearing,
                I18N.current
                    .settings_notification_night_mode_notify_end_wearing);
          },
          value: Padding(
              padding: const EdgeInsets.only(left: 15),
              child:
                  nmNotifications[NotificationProvider.nightModeEndWearing] !=
                          null
                      ? Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(nmNotifications[NotificationProvider
                                    .nightModeEndWearing]?["title"] ??
                                ""),
                            Text(nmNotifications[NotificationProvider
                                    .nightModeEndWearing]?["description"] ??
                                ""),
                          ],
                        )
                      : notDefined)),
      SettingsTile.switchTile(
          enabled: enable && valid && allowed,
          initialValue: sp.notificationNightPropagate,
          onToggle: (val) async {
            sp.setNotificationNightModeNotificationPropagate(val);
            await updateNotifications();
          },
          title: Text(I18N.current
              .settings_notification_night_mode_notify_postpone_to_end)),
    ], title: Text(I18N.current.settings_notification_night_mode_title));
  }

  Future<void> updateNotifications() async {
    var currentChange = DiaperProvider.instance.currentChange;
    if (currentChange != null) {
      await NotificationProvider.instance
          ?.setupNotifications(currentChange, requestPermission: false);
    }
    await NotificationProvider.instance?.setupNotificationNightMode();
  }

  showDialogPeriod(BuildContext context) async {
    SettingsProvider sp = Provider.of(context, listen: false);
    Period period = sp.notificationNightPeriod;
    var timeFormat = DateFormat().add_Hm_12h(context);
    var use24HourFormat = MediaQuery.alwaysUse24HourFormatOf(context);
    await showDialog(
        context: context,
        builder: (context) => StatefulBuilder(builder: (context, setState) {
              var ctrlStart =
                  TextEditingController(text: period.startFormated(timeFormat));
              var ctrlEnd =
                  TextEditingController(text: period.endFormated(timeFormat));
              return AlertDialog(
                title:
                    Text(I18N.current.settings_notification_night_mode_period),
                content: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    TextFormField(
                      decoration: InputDecoration(
                          label: Text(I18N.current
                              .settings_notification_night_mode_period_start)),
                      readOnly: true,
                      controller: ctrlStart,
                      onTap: () async {
                        var timeOfDay = TimeOfDay.fromDateTime(
                            period.start ?? DateTime.now());
                        var res = await showTimePicker(
                            context: context, initialTime: timeOfDay);
                        if (res != null) {
                          period.start = DateTime.now().copyWith(
                              hour: res.hour,
                              minute: res.minute,
                              second: 0,
                              millisecond: 0);
                          setState(() {
                            ctrlStart.text =
                                period.startFormated(timeFormat) ?? "";
                          });
                        }
                      },
                    ),
                    TextFormField(
                      decoration: InputDecoration(
                          label: Text(I18N.current
                              .settings_notification_night_mode_period_end)),
                      readOnly: true,
                      controller: ctrlEnd,
                      onTap: () async {
                        var timeOfDay = TimeOfDay.fromDateTime(
                            period.end ?? DateTime.now());
                        var res = await showTimePicker(
                            builder: (BuildContext context, Widget? child) {
                              return MediaQuery(
                                data: MediaQuery.of(context).copyWith(
                                    alwaysUse24HourFormat: use24HourFormat),
                                child: child ?? const SizedBox(),
                              );
                            },
                            context: context,
                            initialTime: timeOfDay);
                        if (res != null) {
                          period.end = DateTime.now().copyWith(
                              hour: res.hour,
                              minute: res.minute,
                              second: 0,
                              millisecond: 0);
                          setState(() {
                            ctrlEnd.text = period.endFormated(timeFormat) ?? "";
                          });
                        }
                      },
                    ),
                  ],
                ),
                actions: [
                  TextButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: Text(I18N.current.cancel)),
                  TextButton(
                      onPressed: period.isValid
                          ? () async {
                              await sp.setNotificationNightModePeriod(period);
                              await updateNotifications();
                              // ignore: use_build_context_synchronously
                              Navigator.of(context).pop();
                            }
                          : null,
                      child: Text(I18N.current.save))
                ],
              );
            }));
  }

  showNotificationNightModeDialog(
      BuildContext context, String type, String text) async {
    SettingsProvider sp = Provider.of(context, listen: false);
    var nmNotifications = sp.notificationNightModeNotifications;
    var title = nmNotifications[type]?["title"] ?? "";
    var description = nmNotifications[type]?["description"] ?? "";
    await showDialog(
        context: context,
        builder: (context) => StatefulBuilder(builder: (context, setState) {
              var valid = title.isNotEmpty && description.isNotEmpty;
              return AlertDialog(
                title: Text(text),
                content: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    TextFormField(
                      initialValue: title,
                      decoration: InputDecoration(
                          label: Text(I18N.current.notification_title)),
                      onChanged: (value) {
                        setState(() {
                          title = value;
                        });
                      },
                    ),
                    TextFormField(
                      initialValue: description,
                      decoration: InputDecoration(
                          label: Text(I18N.current.notification_description)),
                      onChanged: (value) {
                        setState(() {
                          description = value;
                        });
                      },
                    ),
                  ],
                ),
                actions: [
                  TextButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: Text(I18N.current.cancel)),
                  if (nmNotifications.containsKey(type))
                    TextButton(
                        onPressed: () async {
                          nmNotifications.remove(type);
                          await sp.setNotificationNightModeNotifications(
                              nmNotifications);
                          await updateNotifications();
                          // ignore: use_build_context_synchronously
                          Navigator.of(context).pop();
                        },
                        child: Text(I18N.current.remove)),
                  TextButton(
                      onPressed: valid
                          ? () async {
                              nmNotifications[type] = {
                                "title": title,
                                "description": description
                              };
                              await sp.setNotificationNightModeNotifications(
                                  nmNotifications);
                              await updateNotifications();
                              // ignore: use_build_context_synchronously
                              Navigator.of(context).pop();
                            }
                          : null,
                      child: Text(I18N.current.save))
                ],
              );
            }));
  }
}
