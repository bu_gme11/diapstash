import 'dart:developer';

import 'package:diap_stash/common.dart';
import 'package:diap_stash/object/interfaces.dart';
import 'package:diap_stash/pages/settings_page.dart';
import 'package:diap_stash/providers/diaper_provider.dart';
import 'package:settings_ui/settings_ui.dart';

class SettingsManageSharingPage extends StatefulWidget {
  const SettingsManageSharingPage({super.key});

  @override
  State<SettingsManageSharingPage> createState() =>
      _SettingsManageSharingPageState();
}

class _SettingsManageSharingPageState extends State<SettingsManageSharingPage> {
  @override
  Widget build(BuildContext context) {
    DiaperProvider dp = Provider.of<DiaperProvider>(context, listen: false);
    log("build");
    return Scaffold(
        appBar: AppBar(
          title: Text(I18N.current.settings_data_manage_sharing),
        ),
        body: FutureBuilder(
          future: Future.wait([
            dp.loadSharedStock(),
            dp.loadSharedStockAccess(doNotifyListeners: false),
            dp.loadSharedHistory(),
            dp.loadSharedHistoryAccess(doNotifyListeners: false)
          ]),
          builder: (context, snap) {
            if (!snap.hasData) {
              return const Center(
                child: CircularProgressIndicator(),
              );
            }

            List<SharedStock> sharedStock = snap.data![0].cast();
            List<SharedStockAccess> sharedStockAccess = snap.data![1].cast();
            List<SharedHistory> sharedHistories = snap.data![2].cast();
            List<SharedHistoryAccess> sharedHistoriesAccess =
                snap.data![3].cast();

            return SettingsList(
                //platform: DevicePlatform.iOS,
                lightTheme: SettingsPage.settingsThemeData(context),
                sections: [
                  SettingsSection(
                    tiles: [
                      if (sharedStock.isEmpty)
                        SettingsTile(
                            title: Text(
                                I18N.current.stock_sharing_settings_no_shared)),
                      for (var ss in sharedStock)
                        SettingsTile(
                            title: Text(ss.toName),
                            description: SwitchListTile.adaptive(
                              title: Text(I18N.current.stock_sharing_read_only),
                              value: ss.readOnly,
                              onChanged: (val) {
                                ss.readOnly = val;
                                dp.updateSharingStock(ss).then((_) {
                                  setState(() {});
                                });
                              },
                            ),
                            trailing: Row(
                              children: [
                                if (ss.state == "PENDING")
                                  Text(
                                    I18N.current.stock_sharing_settings_PENDING,
                                    style:
                                        const TextStyle(color: Colors.orange),
                                    textAlign: TextAlign.left,
                                  )
                                else
                                  Text(
                                    I18N.current.stock_sharing_settings_ACTIVE,
                                    style: const TextStyle(color: Colors.green),
                                    textAlign: TextAlign.left,
                                  ),
                                TextButton(
                                  onPressed: () {
                                    dp
                                        .revokeSharingStockAccess(
                                        ss.id, ss.securityToken)
                                        .then((value) {
                                      setState(() {});
                                    });
                                  },
                                  child: Text(I18N.current.revoke),
                                ),
                              ],
                            ))
                    ],
                    title: Text(I18N.current.stock_sharing_settings_your_share),
                  ),
                  SettingsSection(
                    tiles: [
                      if (sharedStockAccess.isEmpty)
                        SettingsTile(
                            title: Text(I18N.current
                                .stock_sharing_settings_no_share_access)),
                      for (var ssa in sharedStockAccess)
                        SettingsTile(
                          title: Text(ssa.fromName),
                          trailing: TextButton(
                              onPressed: () {
                                dp
                                    .revokeSharingStockAccess(
                                        ssa.id, ssa.securityToken)
                                    .then((value) {
                                  setState(() {});
                                });
                              },
                              child: Text(I18N.current.remove)),
                        )
                    ],
                    title:
                        Text(I18N.current.stock_sharing_settings_your_access),
                  ),
                  SettingsSection(
                    tiles: [
                      if (sharedHistories.isEmpty)
                        SettingsTile(
                            title: Text(I18N
                                .current.history_sharing_settings_no_shared)),
                      for (var sh in sharedHistories)
                        SettingsTile(
                            title: Text(sh.toName),
                            trailing: Row(
                              children: [
                                if (sh.state == "PENDING")
                                  Text(
                                    I18N.current.stock_sharing_settings_PENDING,
                                    style:
                                        const TextStyle(color: Colors.orange),
                                    textAlign: TextAlign.left,
                                  )
                                else
                                  Text(
                                    I18N.current.stock_sharing_settings_ACTIVE,
                                    style: const TextStyle(color: Colors.green),
                                    textAlign: TextAlign.left,
                                  ),
                                TextButton(
                                  onPressed: () {
                                    dp
                                        .revokeSharingHistoryAccess(
                                        sh.id, sh.securityToken)
                                        .then((value) {
                                      setState(() {});
                                    });
                                  },
                                  child: Text(I18N.current.revoke),
                                ),
                              ],
                            ))
                    ],
                    title:
                        Text(I18N.current.history_sharing_settings_your_share),
                  ),
                  SettingsSection(
                    tiles: [
                      if (sharedHistoriesAccess.isEmpty)
                        SettingsTile(
                            title: Text(I18N.current
                                .history_sharing_settings_no_share_access)),
                      for (var sha in sharedHistoriesAccess)
                        SettingsTile(
                          title: Text(sha.fromName),
                          trailing: TextButton(
                              onPressed: () {
                                dp
                                    .revokeSharingHistoryAccess(
                                        sha.id, sha.securityToken)
                                    .then((value) {
                                  setState(() {});
                                });
                              },
                              child: Text(I18N.current.remove)),
                        )
                    ],
                    title:
                        Text(I18N.current.history_sharing_settings_your_access),
                  )
                ]);
          },
        ));
  }
}
