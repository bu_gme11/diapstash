import 'package:diap_stash/common.dart';
import 'package:diap_stash/components/sharing/stock_share_widget.dart';
import 'package:diap_stash/providers/diaper_provider.dart';
import 'package:flutter/foundation.dart';
import 'package:pasteboard/pasteboard.dart';
import 'package:screenshot/screenshot.dart';
import 'package:share_plus/share_plus.dart';

class StockSharingPage extends StatefulWidget {
  const StockSharingPage({super.key});

  @override
  State<StockSharingPage> createState() => _StockSharingPageState();
}

class _StockSharingPageState extends State<StockSharingPage> {
  final ScreenshotController screenshotController = ScreenshotController();

  OverlayEntry? overlayEntry;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      showOverlay(context);
    });
  }

  void showOverlay(BuildContext context) async {
    final overlay = Overlay.of(context);
    overlayEntry = OverlayEntry(builder: (ctx) {
      var headlineMedium2 = Theme.of(context).textTheme.headlineMedium;
      var bgColor = Colors.grey.withAlpha(230);

      return Container(
        color: bgColor,
        alignment: Alignment.center,
        child: Text(I18N.current.share_generate_image,
            textAlign: TextAlign.center, style: headlineMedium2),
      );
    });

    overlay.insert(overlayEntry!);

    setState(() {
      screenshotController
          .capture(delay: const Duration(seconds: 1))
          .then((bytes) async {
        overlayEntry?.remove();
        overlayEntry = null;
        if(kIsWeb){
          await Pasteboard.writeImage(bytes);
          if(mounted) {
            ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text(I18N.current.copied)));
            Navigator.of(context).pop();
          }
        }else {
          Navigator.of(context).pop();
          await Share.shareXFiles(
              [XFile.fromData(bytes!, mimeType: "image/png")]);
        }
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    DiaperProvider dp = Provider.of<DiaperProvider>(context);

    return Scaffold(
      appBar: AppBar(),
      primary: true,
      body: Wrap(
        children: [
          Screenshot(
              controller: screenshotController,
              child: StockShareWidget(currentStock: dp.currentStockSorted))
        ],
      ),
    );
  }
}
