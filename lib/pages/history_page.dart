import 'dart:developer';

import 'package:diap_stash/common.dart';
import 'package:diap_stash/components/history/edit_change.dart';
import 'package:diap_stash/components/history/history_change_widget.dart';
import 'package:diap_stash/object/interfaces.dart';

import 'package:diap_stash/components/history/history_no_change_widget.dart';
import 'package:diap_stash/providers/diaper_provider.dart';
import 'package:sentry_flutter/sentry_flutter.dart';

class HistoryPage extends StatefulWidget {
  const HistoryPage({super.key});

  @override
  State<StatefulWidget> createState() => _HistoryPageState();
}

class HistoryData {
  final Change? change;
  final DateTime startTime;
  final DateTime? endTime;
  final bool errorDate;

  const HistoryData(
      {required this.startTime,
      this.change,
      this.endTime,
      this.errorDate = false});

  factory HistoryData.fromChange(Change c, {bool errorDate = false}) {
    return HistoryData(change: c, startTime: c.startTime, endTime: c.endTime, errorDate: errorDate);
  }

  Duration duration() {
    return (endTime ?? DateTime.now()).difference(startTime);
  }
}

class _HistoryPageState extends State<HistoryPage>
    with TickerProviderStateMixin {
  late TabController _controller;
  late TabController _tabSharedController;

  SharedHistoryAccess? selectedSHA;

  @override
  void initState() {
    super.initState();
    _controller = TabController(length: 2, vsync: this);
    _tabSharedController = TabController(length: 0, vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    var diaperProvider = Provider.of<DiaperProvider>(context, listen: true);

    var hasSHA = diaperProvider.sharedHistoriesAccess.isNotEmpty;

    //buildYourTab(diaperProvider, context);

    var tablength = hasSHA ? 2 : 1;
    if (_controller.length != tablength) {
      log("Change tab controller");
      _controller = TabController(length: tablength, vsync: this);
    }

    return Column(crossAxisAlignment: CrossAxisAlignment.stretch, children: [
      if (hasSHA)
        TabBar(
          controller: _controller,
          tabs: [
            Tab(text: I18N.current.history_tab_your),
            if (hasSHA)
              Tab(text: I18N.current.history_tab_other)
          ],
        ),
      Expanded(
          child: Padding(
              padding: const EdgeInsets.all(5),
              child: TabBarView(controller: _controller, children: [
                buildHistoryTab(diaperProvider.changes, context),
                if (hasSHA) buildOtherTabs(diaperProvider, context)
              ])))
    ]);
  }

  Widget buildOtherTabs(DiaperProvider diaperProvider, BuildContext context) {

    var sharedHistoriesAccess = diaperProvider.sharedHistoriesAccess;
    if(_tabSharedController.length != sharedHistoriesAccess.length){
      _tabSharedController = TabController(length: sharedHistoriesAccess.length, vsync: this);
    }

    return Column(children: [
      SizedBox(
        height: 35,
        child: TabBar(
          controller: _tabSharedController,
          tabs: [for (var ssa in sharedHistoriesAccess) Text(ssa.fromName)],
        ),
      ),
      Expanded(
        child: TabBarView(controller: _tabSharedController, children: [
          for (var sha in sharedHistoriesAccess)
            FutureBuilder<List<Change>>(
              future: diaperProvider.getSharedHistory(sha),
              builder: (context, snapshot) {
                if (!snapshot.hasData) {
                  return const CircularProgressIndicator();
                }
                return buildHistoryTab(snapshot.data!, context, readOnly: true);
              },
            )
        ]),
      )
    ]);
  }

  StatelessWidget buildHistoryTab(List<Change> changesTmp, BuildContext context,
      {bool readOnly = false}) {
    var changes = changesTmp.toList();
    changes.sort((a, b) {
      if (a.endTime == null) {
        return -1;
      } else if (b.endTime == null) {
        return 1;
      }
      return b.startTime.compareTo(a.startTime);
    });

    final List<HistoryData> histories = [];
    if (changes.isNotEmpty) {
      var first = changes.first;
      if (first.endTime != null) {
        histories.add(HistoryData(startTime: first.endTime!));
      }
      histories.add(HistoryData.fromChange(first));

      for (var i = 1; i < changes.length; i++) {
        var prev = changes.elementAt(i - 1);
        var current = changes.elementAt(i);
        var currentEndTime = current.endTime ?? prev.startTime;
        bool error = currentEndTime.isAfter(prev.startTime);
        if (prev.startTime.difference(currentEndTime).inSeconds > 59) {
          histories.add(HistoryData(
              startTime: currentEndTime,
              endTime: prev.startTime));
        }

        if (current.diapers.isNotEmpty) {
          histories.add(HistoryData.fromChange(current, errorDate: error));
        }
      }
    }

    histories.sort((a, b) {
      if (a.change != null && a.change!.endTime == null) {
        return -1;
      } else if (b.change != null && b.change!.endTime == null) {
        return 1;
      }
      return b.startTime.compareTo(a.startTime);
    });

    return histories.isEmpty
        ? Container(
            alignment: Alignment.center,
            child: Text(I18N.current.history_never_worn),
          )
        : ListView.builder(
            itemCount: histories.length,
            itemBuilder: (context, index) {
              var history = histories.elementAt(index);
              return GestureDetector(
                onTap: readOnly
                    ? null
                    : () {
                        if (history.change != null) {
                          editChange(context, history);
                        }
                      },
                child: (history.change != null)
                    ? (history.errorDate)
                        ? Tooltip(
                            message:
                                "There seems to be a conflict in the dates",
                            child: HistoryChangeWidget(
                                change: history.change!, errorDate: true))
                        : HistoryChangeWidget(change: history.change!)
                    : HistoryNoChangeWidget(history: history),
              );
            });
  }

  void editChange(BuildContext context, HistoryData history) {
    //DiaperProvider dp = Provider.of<DiaperProvider>(context, listen: false);
    Sentry.addBreadcrumb(Breadcrumb(category: "navigation", data: const {
      "openDialog": "dialogEditChange",
    }));
    showEditChangePage(context: context, change: history.change!, onDone: (){
      if(mounted){
        setState(() { });
      }
    });
  }
}
