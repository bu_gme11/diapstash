import 'dart:async';
import 'dart:math';

import 'package:app_links/app_links.dart';
import 'package:async/async.dart';
import 'package:diap_stash/common.dart';
import 'package:diap_stash/components/catalog/catalog_filter.dart';
import 'package:diap_stash/components/dialogs/dialog_ask_text.dart';
import 'package:diap_stash/components/dialogs/dialog_warning_web.dart';
import 'package:diap_stash/components/dialogs/dialog_yes_no.dart';
import 'package:diap_stash/components/dialogs/whats_new_dialog.dart';
import 'package:diap_stash/components/current_wearing_widget.dart';
import 'package:diap_stash/components/searcheable_app_bar.dart';
import 'package:diap_stash/components/tutorials/create_customs.dart';
import 'package:diap_stash/components/tutorials/submit_official_type.dart';
import 'package:diap_stash/components/type_card_widget.dart';
import 'package:diap_stash/components/type_image_widget.dart';
import 'package:diap_stash/components/types/button_size_type_widget.dart';
import 'package:diap_stash/main.dart';
import 'package:diap_stash/object/interfaces.dart';
import 'package:diap_stash/object/proposals.dart';
import 'package:diap_stash/pages/catalog_page.dart';
import 'package:diap_stash/pages/catalog/customs/custom_type_page.dart';
import 'package:diap_stash/pages/history/history_edit_change_page.dart';
import 'package:diap_stash/pages/history_page.dart';
import 'package:diap_stash/pages/settings_page.dart';
import 'package:diap_stash/pages/stats_page.dart';
import 'package:diap_stash/pages/stock_page.dart';
import 'package:diap_stash/providers/diaper_provider.dart';
import 'package:diap_stash/providers/notification_provider.dart';

import 'dart:developer' as developer;

import 'package:diap_stash/providers/settings_provider.dart';
import 'package:diap_stash/utils/stocks.dart';
import 'package:flutter/foundation.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:sentry_flutter/sentry_flutter.dart';
import 'package:share_plus/share_plus.dart';

part "main/app_links_ds.dart";

part "main/stock_actions.dart";

part "main/catalog_actions.dart";

part "main/history_actions.dart";

part "main/tutorial.dart";

part "main/proposal.dart";

GlobalKey keyCatalogButton = GlobalKey();
GlobalKey keyShareBoundary = GlobalKey();

class _EndFloatFabLocation extends StandardFabLocation
    with FabEndOffsetX, FabFloatOffsetY {
  BuildContext context;

  _EndFloatFabLocation({required this.context});

  @override
  double getOffsetY(
      ScaffoldPrelayoutGeometry scaffoldGeometry, double adjustment) {
    var val = super.getOffsetY(scaffoldGeometry, adjustment);

    DiaperProvider dp = Provider.of<DiaperProvider>(context, listen: false);
    if (dp.currentChange != null && dp.currentChange!.diapers.isNotEmpty) {
      return val - 75;
    }

    return val;
  }

  @override
  String toString() => 'FloatingActionButtonLocation.endFloat';
}

class MainPage extends StatefulWidget {
  static const routeName = 'MainPage';
  static const routeArgsName = 'MainPageArgs';

  final int selectedIndex;

  const MainPage({super.key, this.selectedIndex = 0});

  @override
  State<MainPage> createState() => MainPageState();
}

final GlobalKey stockPageKey = GlobalKey();
final GlobalKey littleBoyKey = GlobalKey();
final TextEditingController searchCatalogControler = TextEditingController();

class MainPageState extends State<MainPage> with WidgetsBindingObserver {
  final StockPage _stockPage = StockPage(key: stockPageKey);
  final SettingsPage _settingsPage = const SettingsPage();
  final CatalogPage _catalogPage =
      CatalogPage(searchController: searchCatalogControler);
  final HistoryPage _historyPage = const HistoryPage();

  final StatsPage _statsPage = const StatsPage();

  //final WidgetsToImageController wtiCtrl = WidgetsToImageController();

  late _EndFloatFabLocation _endFloatFabLocation;
  late DiaperProvider diaperProvider;

  late AppLinks _appLinks;
  StreamSubscription<Uri>? _linkSubscription;

  Widget? shareWidget;

  bool showShare = false;

  int selectedIndex = 0;
  DateTime? lastFullLoad;

  @override
  void initState() {
    super.initState();
    selectedIndex = widget.selectedIndex;
    _endFloatFabLocation = _EndFloatFabLocation(context: context);
    diaperProvider = Provider.of<DiaperProvider>(context, listen: false);

    WidgetsBinding.instance.addObserver(this);
    WidgetsBinding.instance.addPostFrameCallback((_) {
      diaperProvider.loadOfflineData().then((_) async {
        if(diaperProvider.brands.isEmpty || diaperProvider.diaperTypes.isEmpty){
          await diaperProvider.loadCatalog(doNotifyListeners: true);
        }

        diaperProvider.loadChange(doNotifyListeners: true).then((_) {
          if (littleBoyKey.currentState != null) {
            littleBoyKey.currentState!.setState(() {});
          }
        });
        if(mounted) {
          searchApprovedProposals();
        }
      });
      showDialogWarningWeb(context);
      generateReleaseDialog(context).then((_){});
      Future.delayed(Duration.zero, () {
        createTutorialCreateCustomDiaper(context);
        createTutorialSubmitOfficialDiaper(context);
      });
    });

    initDeepLinks();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    super.didChangeAppLifecycleState(state);
    if (state == AppLifecycleState.resumed) {
      //DiaperProvider dp = Provider.of(context, listen: false);
      //loadDataSpecificTab(dp).then((_){}).catchError((_){});
    }
  }

  @override
  void dispose() {
    if (_linkSubscription != null && !_linkSubscription!.isPaused) {
      _linkSubscription?.cancel();
      _linkSubscription = null;
    }
    super.dispose();
  }

  Widget getBody() {
    switch (selectedIndex) {
      case 1:
        /*if (diaperProvider.diaperTypes
            .where((element) => element.official == false)
            .isEmpty) {
          Future.delayed(
              const Duration(milliseconds: 0), showTutorialCreateCustoms);
        } else {
          Future.delayed(
              const Duration(milliseconds: 0), showTutorialSubmitDiaper);
        }*/
        return _catalogPage;
      case 2:
        return _historyPage;
      case 3:
        return _statsPage;
      case 4:
        return _settingsPage;
      default:
        selectedIndex = 0;
        return _stockPage;
    }
  }

  FloatingActionButton? getFloatingActionButtton(BuildContext context) {
    //developer.log("floating button ${selectedIndex}");
    if (selectedIndex != 1) {
      return null;
    }

    return FloatingActionButton(
      key: createCustomsFBKey,
      onPressed: () {
        Navigator.pushNamed(context, CustomTypePage.routeName);
      },
      tooltip: I18N.current.catalog_add_custom_diaper_tooltip,
      child: const Icon(Icons.add),
    );
  }

  @override
  Widget build(BuildContext context) {
    var diaperProvider = Provider.of<DiaperProvider>(context);
    Widget title = const Text(appBarTitle);

    if (selectedIndex == 0) {
      var totalDiaper = 0;
      var totalBooster = 0;
      for (var c in diaperProvider.currentStock) {
        if (c.type.type == DTType.booster) {
          totalBooster += c.total();
        } else {
          totalDiaper += c.total();
        }
      }
      if ((totalDiaper + totalBooster) > 0) {
        var subItems = [];
        if (totalDiaper > 0) {
          subItems.add(I18N.current.count_diaper(totalDiaper));
        }
        if (totalBooster > 0) {
          subItems.add(I18N.current.count_booster(totalBooster));
        }

        title = Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            title,
            if(subItems.isNotEmpty)
            Text(
              subItems.join(', '),
              style: const TextStyle(fontSize: 14),
            )
          ],
        );
      }
    }

    return Scaffold(
      appBar: SearcheableAppBar(
        title: title,
        enableSearch: selectedIndex == 1,
        actions: getActions(),
        searchController: searchCatalogControler,
      ),
      primary: true,
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        currentIndex: selectedIndex,
        items: [
          BottomNavigationBarItem(
              icon: const Icon(Icons.apps_rounded), label: I18N.current.home),
          BottomNavigationBarItem(
              icon: Icon(key: keyCatalogButton, Icons.list),
              label: I18N.current.catalog),
          BottomNavigationBarItem(
              icon: const Icon(Icons.baby_changing_station),
              label: I18N.current.history),
          BottomNavigationBarItem(
              icon: const Icon(Icons.line_axis), label: I18N.current.stats),
          BottomNavigationBarItem(
              icon: const Icon(Icons.settings), label: I18N.current.settings),
        ],
        onTap: (index) {
          setSelectedIndex(context, index);
        },
      ),
      resizeToAvoidBottomInset: false,
      body: Column(
        children: [
          Expanded(child: getBody()),
          CurrentWearingWidget(key: littleBoyKey),
        ],
      ),
      floatingActionButton: getFloatingActionButtton(context),
      floatingActionButtonLocation: _endFloatFabLocation,
    );
  }

  void setSelectedIndex(BuildContext context, int index) {
    DiaperProvider diaperProvider = Provider.of(context, listen: false);
    var oldIndex = selectedIndex;

    selectedIndex = index;
    setState(() {});
    loadDataSpecificTab(diaperProvider).then((_){
      if (diaperProvider.currentChange == null) {
        NotificationProvider np =
        Provider.of<NotificationProvider>(context, listen: false);
        np.hasChangeNotification(requestPermission: false).then((has) async {
          if (has) {
            await np.cancelChangeNotification(requestPermission: false);
          }
        });
      }
    }).catchError((_){});

    Sentry.addBreadcrumb(Breadcrumb(category: "navigation", data: {
      "to": index,
      "from": oldIndex,
      "state": "bottomNavigationBarIndexChange"
    }));
  }

  Future<void> loadDataSpecificTab(DiaperProvider diaperProvider) async {
    if (selectedIndex == 0) {
      await diaperProvider.loadStock(doNotifyListeners: false);
      await diaperProvider.loadSharedStockAccess(doNotifyListeners: false);
    }
    await diaperProvider.loadChange(doNotifyListeners: false);
    if(selectedIndex == 2){
      await diaperProvider.loadSharedHistoryAccess(doNotifyListeners: false);
    }
    if(SettingsProvider.instance.hasCloud() && (lastFullLoad == null || DateTime.now().difference(lastFullLoad!).inMinutes.abs() > 5)) {
      developer.log("Load catalog data");
      lastFullLoad = DateTime.now();
      await diaperProvider.loadCatalog(doNotifyListeners: false);
    }else if(diaperProvider.brands.isEmpty || diaperProvider.diaperTypes.isEmpty){
      await diaperProvider.loadCatalog(doNotifyListeners: false);
    }
    diaperProvider.doNotifyListeners();
  }

  List<Widget> getActions() {
    List<Widget> actions = [];

    if (selectedIndex == 0) {
      actions.addAll(getActionsStock());
    } else if (selectedIndex == 1) {
      actions.addAll(getActionsCatalog());
    } else if (selectedIndex == 2) {
      actions.addAll(getActionsHistory());
    }
    return actions;
  }
}
