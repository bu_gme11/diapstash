import 'package:diap_stash/common.dart';
import 'package:diap_stash/components/stats_card_widget.dart';
import 'package:diap_stash/components/stats_period_widget.dart';
import 'package:diap_stash/object/interfaces.dart';
import 'package:diap_stash/providers/diaper_provider.dart';

import 'package:diap_stash/util.dart';
import 'package:diap_stash/utils/data_stats_source.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:screenshot/screenshot.dart';
import 'package:universal_io/io.dart';

class StatsPage extends StatefulWidget {
  static TextStyle contentStyle = const TextStyle(fontSize: 18);
  static TextStyle titleStyle =
      const TextStyle(fontSize: 20, fontWeight: FontWeight.bold);
  static TextStyle headerStyle =
      const TextStyle(fontSize: 18, fontWeight: FontWeight.bold);
  static Divider divider = const Divider(height: 40, indent: 50, endIndent: 50);

  const StatsPage({super.key});

  @override
  State<StatsPage> createState() => _StatsPageState();
}

class _StatsPageState extends State<StatsPage> with TickerProviderStateMixin {
  StatsPeriod period = StatsPeriod.all;
  DateTimeRange? range;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    DiaperProvider dp = Provider.of(context, listen: true);
    var changes = dp.changes
        .where((element) => element.endTime != null)
        .toList(growable: false);

    if (period == StatsPeriod.custom && range != null) {
      var start = range!.start.copyWith(hour: 0, minute: 0, second: 0);
      var end = range!.end.copyWith(hour: 23, minute: 59, second: 59);
      changes = changes
          .where((c) => c.startTime.isAfter(start) && c.endTime!.isBefore(end))
          .toList();
    }

    changes.sort((a, b) => a.startTime.compareTo(b.startTime));

    Map<DiaperType, List<Change>> mapTypeChange = {};
    Map<DiaperBrand, List<Change>> mapBrandChange = {};
    var totalDiaper = 0;
    var totalBooster = 0;

    for (var change in changes) {
      for (var diaper in change.diapers) {
        mapTypeChange.update(diaper.type, (value) => value..add(change),
            ifAbsent: () => [change]);
        if (diaper.type.brand != null) {
          mapBrandChange.update(
              diaper.type.brand!, (value) => value..add(change),
              ifAbsent: () => [change]);
        }
        if (diaper.type.type == DTType.booster) {
          totalBooster++;
        } else {
          totalDiaper++;
        }
      }
    }

    if (changes.isEmpty) {
      return Column(
        children: [
          StatsPeriodWidget(
            statsPeriod: period,
            range: range,
            onChange: (p, r) {
              setState(() {
                range = r;
                period = p;
              });
            },
          ),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(I18N.current.stats_not_available),
              ],
            ),
          ),
        ],
      );
    }
    return Column(
      children: [
        StatsPeriodWidget(
            statsPeriod: period,
            range: range,
            onChange: (p, r) {
              setState(() {
                range = r;
                period = p;
              });
            }),
        Expanded(
          child: SingleChildScrollView(
              child: Column(children: [
            buildCards(
                changes, mapBrandChange, mapTypeChange, totalDiaper, totalBooster),
            StatsPage.divider,
            Text(I18N.current.stats_per_type, style: StatsPage.titleStyle),
            buildTables(changes, mapBrandChange, mapTypeChange)
          ])),
        ),
      ],
    );
  }

  Widget buildCards(
      final List<Change> changes,
      final Map<DiaperBrand, List<Change>> mapBrandChange,
      final Map<DiaperType, List<Change>> mapTypeChange,
      final int totalDiaper,
      final int totalBooster) {
    // ignore: unused_local_variable
    bool hasStateDefined =
        changes.where((element) => element.state != null).isNotEmpty;
    int countDry =
        changes.where((element) => element.state == ChangeState.dry).length;
    int countWet =
        changes.where((element) => element.state == ChangeState.wet).length;
    int countMessy =
        changes.where((element) => element.state == ChangeState.messy).length;
    int countWetMessy = changes
        .where((element) => element.state == ChangeState.wetMessy)
        .length;

    var countChanges = changes.length;

    double ratioDry = 0;
    double ratioWet = 0;
    double ratioMessy = 0;
    double ratioWetMessy = 0;
    if (countChanges > 0) {
      ratioDry = (countDry / countChanges) * 100;
      ratioWet = (countWet / countChanges) * 100;
      ratioMessy = (countMessy / countChanges) * 100;
      ratioWetMessy = (countWetMessy / countChanges) * 100;
    }

    List<Duration> durationsStike = [];
    List<Duration> outside = [];

    var start = changes[0].startTime;

    if (period == StatsPeriod.custom && range != null) {
      if (start.isAfter(range!.start)) {
        var difference = start.difference(range!.start);
        outside.add(difference);
      }
    }

    List<double> changesPrices = [];

    var change = changes[0];

    if(change.price != null){
      changesPrices.add(change.price!);
    }

    for (var i = 1; i < changes.length; i++) {
      var currentChange = changes[i];

      if (currentChange.price != null) {
        changesPrices.add(currentChange.price!);
      }

      var difference = currentChange.startTime.difference(change.endTime!);
      if (difference.inMinutes >= 1) {
        durationsStike.add(change.endTime!.difference(start));
        outside.add(difference);
        start = currentChange.startTime;
      }
      change = currentChange;
    }

    durationsStike.add(change.endTime!.difference(start));

    if (period == StatsPeriod.custom &&
        range != null &&
        change.endTime != null &&
        change.endTime!.isBefore(range!.end)) {
      var difference = range!.end.difference(change.endTime!);
      outside.add(difference);
    }

    double? avgChangesPrices;
    double? totalChangePrices;
    if (changesPrices.isNotEmpty) {
      totalChangePrices =
          changesPrices.reduce((value, element) => value += element);
      avgChangesPrices = totalChangePrices / changesPrices.length;
    }
    NumberFormat currencyFormat = NumberFormat.simpleCurrency();
    try {
      currencyFormat = NumberFormat.simpleCurrency(locale: Platform.localeName);
    } catch (_) {}

    Duration outsideSum = sumDuration(outside);

    double? ratioInDiaper;

    DateTime firstTime = changes.first.startTime;
    DateTime? endTime = changes.last.endTime;
    if(period == StatsPeriod.custom && range != null){
      firstTime = range!.start;
      endTime = range!.end;
    }

    if (endTime != null) {
      Duration totalTimeInDiaper = sumDuration(durationsStike);
      Duration totalUsageApp = endTime.difference(firstTime);
      if (totalUsageApp.inSeconds != 0) {
        ratioInDiaper =
            (totalTimeInDiaper.inSeconds / totalUsageApp.inSeconds.abs()) * 100;
      }
    }

    var diaperTypesList = mapTypeChange.keys.toList(growable: false);
    diaperTypesList
        .sort((a, b) => mapTypeChange[b]!.length - mapTypeChange[a]!.length);

    var diaperTypesListWithoutBooster = diaperTypesList
        .where((element) => element.type == DTType.diaper)
        .toList();

    var diaperBrandList = mapBrandChange.keys.toList(growable: false);
    diaperBrandList
        .sort((a, b) => mapBrandChange[b]!.length - mapBrandChange[a]!.length);

    double? avgWetness;
    var wetnessChanges = changes
        .where((element) => element.wetness != null)
        .map((e) => e.wetness!)
        .toList();
    if (wetnessChanges.isNotEmpty) {
      avgWetness =
          wetnessChanges.reduce((a, b) => a + b) / wetnessChanges.length;
    }

    return StaggeredGrid.count(
      crossAxisCount: 2,
      mainAxisSpacing: 5,
      crossAxisSpacing: 5,
      //direction: Axis.horizontal,
      children: [
        if (diaperTypesListWithoutBooster.isNotEmpty)
          StatsCard(
              title: I18N.current.stats_favorite_diaper,
              stats: diaperTypesListWithoutBooster.first.name),
        if (diaperBrandList.isNotEmpty)
          StatsCard(
              title: I18N.current.stats_favorite_brand,
              stats: diaperBrandList.first.name),
        StatsCard(
            title: I18N.current.stats_total_used_diaper, stats: "$totalDiaper"),
        if (changes.length != totalDiaper)
          StatsCard(
              title: I18N.current.stats_total_change,
              stats: "${changes.length}"),
        if (totalBooster > 0)
          StatsCard(
              title: I18N.current.stats_total_booster, stats: "$totalBooster"),
        StatsCard(
            title: I18N.current.stats_mean_in_diaper,
            stats: durationToString(meanDuration(durationsStike))),
        StatsCard(
            title: I18N.current.stats_max_in_diaper,
            stats: durationToString(maxDuration(durationsStike))),
        StatsCard(
            title: I18N.current.stats_total_in_diaper,
            stats: durationToString(sumDuration(durationsStike))),
        StatsCard(
            title: I18N.current.stats_total_without_diaper,
            stats: durationToString(outsideSum)),
        if (ratioInDiaper != null)
          StatsCard(
              title: I18N.current.stats_total_in_diaper_percentage,
              stats: "${ratioInDiaper.toStringAsFixed(2)}%"),
        if (changes.length != durationsStike.length)
          StatsCard(
              title: I18N.current.stats_mean_of_change,
              stats: durationToString(meanDuration(
                  changes.map(getDuration).toList(growable: false)))),
        if (changes.length != durationsStike.length)
          StatsCard(
              title: I18N.current.stats_max_of_change,
              stats: durationToString(maxDuration(
                  changes.map(getDuration).toList(growable: false)))),
        if (countDry > 0)
          StatsCard(
            title: I18N.current.change_state_dry,
            stats: "$countDry",
            subStats: ratioDry > 0 ? "${ratioDry.toPrecision(1)}%" : null,
          ),
        if (countWet > 0)
          StatsCard(
              title: I18N.current.change_state_wet,
              stats: "$countWet",
              subStats: ratioWet > 0 ? "${ratioWet.toPrecision(1)}%" : null),
        if (countMessy > 0)
          StatsCard(
              title: I18N.current.change_state_messy,
              stats: "$countMessy",
              subStats:
                  ratioMessy > 0 ? "${ratioMessy.toPrecision(1)}%" : null),
        if (countWetMessy > 0)
          StatsCard(
              title: I18N.current.change_state_wet_messy,
              stats: "$countWetMessy",
              subStats: ratioWetMessy > 0
                  ? "${ratioWetMessy.toPrecision(1)}%"
                  : null),
        if (avgWetness != null)
          StatsCard(
            title: I18N.current.stats_avg_wetness,
            stats: avgWetness.toStringAsPrecision(2),
          ),
        if (avgChangesPrices != null)
          StatsCard(
            title: I18N.current.stats_avg_change_price,
            stats: currencyFormat.format(avgChangesPrices),
          ),
        if (totalChangePrices != null)
          StatsCard(
            title: I18N.current.stats_total_change_cost,
            stats: currencyFormat.format(totalChangePrices),
          ),
      ],
    );
  }

  Widget buildTables(
      final List<Change> changes,
      final Map<DiaperBrand, List<Change>> mapBrandChange,
      final Map<DiaperType, List<Change>> mapTypeChange) {
    var diaperTypesList = mapTypeChange.keys.toList(growable: false);
    diaperTypesList
        .sort((a, b) => mapTypeChange[b]!.length - mapTypeChange[a]!.length);

    List<DataStats> dataStats = [];
    for (var type in diaperTypesList) {
      dataStats.add(DataStats(
          type: type,
          minDuration: minDuration(getDurations(mapTypeChange[type]!)),
          avgDuration: meanDuration(getDurations(mapTypeChange[type]!)),
          maxDuration: maxDuration(getDurations(mapTypeChange[type]!)),
          count: getDiaperCount(type, changes)));
    }

    int columnSorted = 3;
    bool sortAsc = false;

    return StatefulBuilder(
      builder: (context, setState) {
        return Container(
          constraints: BoxConstraints(
            maxWidth: MediaQuery.sizeOf(context).width,
            minWidth: MediaQuery.sizeOf(context).width - 100,
          ),
          child: FittedBox(
            child: DataTable(
              columnSpacing: 10,
              horizontalMargin: 10,
              dataRowMaxHeight: double.infinity,
              sortColumnIndex: columnSorted,
              sortAscending: sortAsc,
              columns: [
                DataColumn(
                  label: Expanded(
                      child:
                          Text(I18N.current.type, style: StatsPage.headerStyle)),
                  onSort: (columnIndex, ascending) {
                    setState(() {
                      columnSorted = columnIndex;
                      sortAsc = ascending;
                      var order = ascending ? 1 : -1;
                      dataStats.sort((a, b) {
                        return a.type.name.compareTo(b.type.name) * order;
                      });
                    });
                  },
                ),
                DataColumn(
                  numeric: false,
                  label: Expanded(
                      child: Text(I18N.current.stats_min,
                          style: StatsPage.headerStyle)),
                  onSort: (columnIndex, ascending) {
                    setState(() {
                      columnSorted = columnIndex;
                      sortAsc = ascending;
                      var order = ascending ? 1 : -1;
                      dataStats.sort((a, b) {
                        return a.minDuration.compareTo(b.minDuration) * order;
                      });
                    });
                  },
                ),
                DataColumn(
                  numeric: false,
                  label: Expanded(
                      child: Text(I18N.current.stats_mean,
                          style: StatsPage.headerStyle)),
                  onSort: (columnIndex, ascending) {
                    setState(() {
                      columnSorted = columnIndex;
                      sortAsc = ascending;
                      var order = ascending ? 1 : -1;
                      dataStats.sort((a, b) {
                        return a.avgDuration.compareTo(b.avgDuration) * order;
                      });
                    });
                  },
                ),
                DataColumn(
                  label: Expanded(
                      child: Text(I18N.current.stats_max,
                          style: StatsPage.headerStyle)),
                  onSort: (columnIndex, ascending) {
                    setState(() {
                      columnSorted = columnIndex;
                      sortAsc = ascending;
                      var order = ascending ? 1 : -1;
                      dataStats.sort((a, b) {
                        return a.maxDuration.compareTo(b.maxDuration) * order;
                      });
                    });
                  },
                ),
                DataColumn(
                  label: Expanded(
                    child: Text(I18N.current.stats_count,
                        style: StatsPage.headerStyle),
                  ),
                  numeric: true,
                  onSort: (columnIndex, ascending) {
                    setState(() {
                      columnSorted = columnIndex;
                      sortAsc = ascending;
                      var order = ascending ? 1 : -1;
                      dataStats.sort((a, b) {
                        return a.count.compareTo(b.count) * order;
                      });
                    });
                  },
                ),
              ],
              rows: DataStatsSource(dataStats).rows(),
            ),
          ),
        );
      },
    );
  }

  int getDiaperCount(DiaperType type, List<Change> changes) {
    return changes
        .map((element) => element.diapers.where((e) => e.type == type).length)
        .reduce((a, b) => a + b);
  }

  Duration getDuration(Change c) {
    return c.endTime!.difference(c.startTime);
  }

  List<Duration> getDurations(List<Change> changes) {
    return changes.map(getDuration).toList();
  }
}
