import 'package:diap_stash/common.dart';
import 'package:diap_stash/components/type_card_widget.dart';
import 'package:diap_stash/components/types/button_size_type_widget.dart';
import 'package:diap_stash/object/interfaces.dart';
import 'package:diap_stash/object/proposals.dart';
import 'package:diap_stash/providers/diaper_provider.dart';

class SubmitOfficialArgs {
  final DiaperType type;

  SubmitOfficialArgs({required this.type});
}

class SubmitOfficialPage extends StatefulWidget {
  static const String routeName = "SubmitOfficial";

  final DiaperType type;

  const SubmitOfficialPage({super.key, required this.type});

  @override
  State<StatefulWidget> createState() {
    return _StateSubmitOfficialPage();
  }
}

class _StateSubmitOfficialPage extends State<SubmitOfficialPage> {
  TextEditingController commentCtrl = TextEditingController();

  bool submitted = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    DiaperProvider dp = Provider.of<DiaperProvider>(context);

    var sizes = widget.type.availableSizes;
    if (widget.type.type == DTType.booster) {
      sizes = [I18N.current.booster];
    }
    var buttonsSize = sizes
        .map((e) => ButtonSizeTypeWidget(size: e, color: Colors.purple))
        .toList();

    return Scaffold(
        appBar: AppBar(
          title: Text(I18N.current.submit_type),
        ),
        body: Padding(
          padding: const EdgeInsets.all(5),
          child: Column(
            children: [
              TypeCardWidget(type: widget.type, buttonsSize: buttonsSize),
              TextField(
                controller: commentCtrl,
                keyboardType: TextInputType.multiline,
                maxLines: null,
                minLines: 3,
                decoration: InputDecoration(
                    label: Text(I18N.current.custom_type_submit_comments),
                    helperMaxLines: 5,
                    helperText:
                        I18N.current.custom_type_submit_comments_help_text),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  FutureBuilder(
                    future: dp.getProposalId(widget.type),
                    builder: (context, snapshot) {
                      if (snapshot.connectionState != ConnectionState.done ||
                          snapshot.data != null ||
                          submitted) {
                        return ElevatedButton(
                            onPressed: null, child: Text(I18N.current.submit));
                      }
                      return ElevatedButton(
                          onPressed: () async {
                            var scaffoldMessengerState =
                                ScaffoldMessenger.of(context);
                            var navigator =
                                Navigator.of(context);
                            setState(() {
                              submitted = true;
                            });
                            Proposal proposal = Proposal.fromType(
                                widget.type, commentCtrl.text);
                            var ok = await dp.sendProposal(proposal);
                            setState(() {
                              submitted = ok;
                              var snackBar = SnackBar(
                                content: Text(ok
                                    ? I18N.current.diaper_submited
                                    : I18N.current.error_when_proposal),
                              );
                              scaffoldMessengerState.showSnackBar(snackBar);
                              navigator.pop();
                              navigator.pop();
                            });
                          },
                          child: Text(I18N.current.submit));
                    },
                  )
                ],
              )
            ],
          ),
        ));
  }
}
