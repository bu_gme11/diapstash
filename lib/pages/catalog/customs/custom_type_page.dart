import 'package:diap_stash/common.dart';
import 'package:diap_stash/components/catalog/customs/proposal_status_widget.dart';
import 'package:diap_stash/components/dialogs/catalog_list_dialog.dart';
import 'package:diap_stash/components/dialogs/dialog_yes_no.dart';
import 'package:diap_stash/components/tutorials/create_customs.dart';
import 'package:diap_stash/components/tutorials/submit_official_type.dart';
import 'package:diap_stash/components/type_image_widget.dart';
import 'package:diap_stash/object/interfaces.dart';
import 'package:diap_stash/object/proposals.dart';
import 'package:diap_stash/pages/catalog/customs/submit_official_page.dart';
import 'package:diap_stash/pages/main_page.dart';
import 'package:diap_stash/providers/diaper_provider.dart';
import 'package:diap_stash/util.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
// ignore: depend_on_referenced_packages
import 'package:collection/collection.dart';
import 'dart:developer' as developer;

class CustomTypePageArgs {
  DiaperType? type;

  CustomTypePageArgs({this.type});
}

class CustomTypePage extends StatefulWidget {
  static const String routeName = "CustomType";
  static const String routeArgsName = "CustomTypeArgs";
  final DiaperType? type;

  const CustomTypePage({super.key, this.type});

  @override
  State<StatefulWidget> createState() {
    return _StateCustomTypePage();
  }
}

class _StateCustomTypePage extends State<CustomTypePage> {
  var formKey = GlobalKey<FormState>();

  var nameCtrl = TextEditingController();
  var imgCtrl = TextEditingController();
  var sizeCtrl = TextEditingController();
  var availableSize = ["M", "L"];
  var diaperType = DTType.diaper;
  var discontinued = false;
  DiaperStyle? diaperStyle = DiaperStyle.tabs;
  List<DiaperTarget> diaperTarget = [];
  DiaperBrand? selectedBrand;

  @override
  void initState() {
    var type = widget.type;
    if (type != null) {
      selectedBrand = type.brand;
      availableSize = [...type.availableSizes];
      nameCtrl.text = type.name;
      imgCtrl.text = type.image ?? "";
      diaperType = type.type;
      diaperStyle = type.style;
      diaperTarget = type.target.toList();
    }
    super.initState();
  }

  String? _imageError(String? value) {
    if (value != null && value.isNotEmpty) {
      var url = Uri.tryParse(value);
      if (url == null || !url.hasScheme || url.path.isEmpty) {
        return I18N.current.custom_type_diaper_image_error_invalid_url;
      }
      if (!['.png', ".jpg", ".jpeg"].any((e) => url.path.endsWith(e))) {
        return I18N.current.custom_type_diaper_image_error_image_only;
      }
    }
    return null;
  }

  bool isEdited() {
    if (widget.type == null) {
      return true;
    }
    var type = widget.type!;
    bool e = (selectedBrand != type.brand) ||
        (nameCtrl.text != type.name) ||
        (imgCtrl.text != (type.image ?? "")) ||
        !const ListEquality().equals(availableSize, type.availableSizes) ||
        diaperStyle != type.style ||
        !diaperTarget.equals(type.target);

    return e;
  }

  @override
  Widget build(BuildContext context) {
    DiaperProvider dp = Provider.of<DiaperProvider>(context);

    var brandsItems = dp.brands
        .map((e) => DropdownMenuItem(value: e, child: Text(e.name)))
        .sorted(
          (a, b) => (a.value?.name ?? "").compareTo(b.value?.name ?? ""),
        )
        .toList();

    bool canSave = nameCtrl.value.text.isNotEmpty &&
        (availableSize.isNotEmpty || diaperType == DTType.booster);

    return Scaffold(
        appBar: AppBar(
          title: Text(I18N.current.custom_type),
        ),
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(5),
            child: Form(
              key: formKey,
              child: Column(
                children: [
                  TextField(
                    key: createCustomsNameKey,
                    controller: nameCtrl,
                    onChanged: (_) {
                      setState(() {});
                    },
                    decoration: InputDecoration(
                      helperText: I18N.current.required_field,
                      helperStyle: const TextStyle(color: Colors.grey),
                      labelText: I18N.current.custom_type_diaper_name,
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Expanded(
                        child: DropdownButtonFormField(
                          decoration: InputDecoration(
                            labelText: I18N.current.custom_type_diaper_brand,
                            helperText:
                                I18N.current.custom_type_brand_not_mandatory,
                            helperStyle: const TextStyle(color: Colors.grey),
                          ),
                          value: selectedBrand,
                          items: brandsItems,
                          onChanged: (DiaperBrand? value) {
                            setState(() {
                              selectedBrand = value;
                            });
                          },
                          selectedItemBuilder: (_) => brandsItems
                              .map((e) => Text(e.value!.name))
                              .toList(),
                        ),
                      ),
                      if (selectedBrand != null)
                        Tooltip(
                          message: I18N.current.custom_type_diaper_remove_brand,
                          child: TextButton(
                            onPressed: () {
                              setState(() {
                                selectedBrand = null;
                              });
                            },
                            child: const Icon(Icons.highlight_remove_outlined),
                          ),
                        )
                    ],
                  ),
                  Row(children: [
                    Expanded(
                        child: TextFormField(
                      key: createCustomsImgKey,
                      controller: imgCtrl,
                      validator: _imageError,
                      decoration: InputDecoration(
                          labelText: I18N.current.custom_type_diaper_image_link,
                          hintText:
                              I18N.current.custom_type_diaper_image_link_hint),
                      onChanged: (val) async {
                        formKey.currentState!.validate();
                        await DefaultCacheManager().removeFile("diaper-0");
                        setState(() {});
                      },
                    )),
                    if (imgCtrl.value.text.isNotEmpty)
                      Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: TypeImageWidget(
                          type: DiaperType(
                              name: "",
                              image: imgCtrl.value.text,
                              id: 0,
                              official: false,
                              availableSizes: [],
                              target: [],
                              hidden: false,
                              discontinued: false),
                          width: 100,
                          height: 100,
                          useCache: false,
                        ),
                      )
                  ]),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text("${I18N.current.type} : "),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: ToggleButtons(
                              isSelected: [
                                for (var s in DTType.values) diaperType == s
                              ],
                              children: [
                                for (var s in DTType.values)
                                  Padding(
                                    padding: const EdgeInsets.all(8),
                                    child: Text(s.localizedNamed()),
                                  )
                              ],
                              onPressed: (index) {
                                setState(() {
                                  diaperType = DTType.values[index];
                                  if (diaperType != DTType.booster) {
                                    if (availableSize.isEmpty ||
                                        availableSize == []) {
                                      availableSize = ["M", "L"];
                                    }
                                  } else {
                                    availableSize = [];
                                  }
                                });
                              }),
                        ),
                      ],
                    ),
                  ),
                  if (diaperType != DTType.booster)
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text("${I18N.current.diaper_style} : "),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: ToggleButtons(
                                isSelected: [
                                  for (var s in DiaperStyle.values)
                                    diaperStyle == s
                                ],
                                children: [
                                  for (var s in DiaperStyle.values)
                                    Padding(
                                      padding: const EdgeInsets.all(8),
                                      child: Text(s.localizedNamed()),
                                    )
                                ],
                                onPressed: (index) {
                                  setState(() {
                                    diaperStyle = DiaperStyle.values[index];
                                  });
                                }),
                          ),
                        ],
                      ),
                    ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text("${I18N.current.diaper_target} : "),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: ToggleButtons(
                              isSelected: [
                                for (var s in DiaperTarget.values)
                                  diaperTarget.contains(s)
                              ],
                              children: [
                                for (var s in DiaperTarget.values)
                                  Padding(
                                    padding: const EdgeInsets.all(8),
                                    child: Text(s.localizedNamed()),
                                  )
                              ],
                              onPressed: (index) {
                                setState(() {
                                  var target = DiaperTarget.values[index];
                                  if (diaperTarget.contains(target)) {
                                    diaperTarget.remove(target);
                                  } else {
                                    diaperTarget.add(target);
                                  }
                                });
                              }),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text("${I18N.current.state_discontinued} : "),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: ToggleButtons(
                              isSelected: [
                                for (var s in discontinuedArray)
                                  discontinued == s
                              ],
                              children: [
                                for (var s in discontinuedArray)
                                  Padding(
                                    padding: const EdgeInsets.all(8),
                                    child: Text(
                                        s ? I18N.current.yes : I18N.current.no),
                                  )
                              ],
                              onPressed: (index) {
                                setState(() {
                                  var val = discontinuedArray[index];
                                  discontinued = val;
                                });
                              }),
                        ),
                      ],
                    ),
                  ),
                  //if (diaperType != DTType.booster)
                  TextField(
                    controller: sizeCtrl,
                    textInputAction: TextInputAction.go,
                    keyboardType: TextInputType.text,
                    onSubmitted: (val) {
                      var text = val.toUpperCase();
                      if (text.isNotEmpty && !availableSize.contains(text)) {
                        setState(() {
                          availableSize.add(text);
                          availableSize.sort(compareSize);
                          sizeCtrl.text = "";
                        });
                      }
                    },
                    decoration: InputDecoration(
                        helperText: diaperType != DTType.booster
                            ? I18N.current.required_field
                            : I18N.current.custom_type_size_booster_optional,
                        labelText: I18N.current.custom_type_diaper_add_size,
                        suffixIcon: TextButton(
                            onPressed: () {
                              setState(() {
                                if (sizeCtrl.text.isNotEmpty &&
                                    !availableSize.contains(sizeCtrl.text)) {
                                  availableSize.add(sizeCtrl.text);
                                  availableSize.sort(compareSize);
                                  sizeCtrl.text = "";
                                }
                                if (availableSize.contains("b")) {
                                  availableSize.remove("b");
                                }
                              });
                            },
                            child: const Icon(Icons.add))),
                  ),
                  Wrap(
                    direction: Axis.horizontal,
                    children: availableSize
                        .where((element) => element != "b")
                        .map(
                          (e) => Chip(
                            label: Text(e),
                            deleteIcon: const Icon(Icons.delete_outline),
                            onDeleted: () {
                              setState(() {
                                availableSize.remove(e);
                              });
                            },
                          ),
                        )
                        .toList(),
                  ),
                  SizedBox(
                    width: double.infinity,
                    child: Wrap(
                      alignment: WrapAlignment.spaceAround,
                      children: [
                        ElevatedButton(
                            onPressed: () {
                              setState(() {
                                Navigator.of(context).pop();
                              });
                            },
                            child: Text(I18N.current.cancel)),
                        if (widget.type != null)
                          ElevatedButton(
                            onPressed: () async {
                              DiaperProvider dp = Provider.of<DiaperProvider>(
                                  context,
                                  listen: false);
                              await dp.deleteCustomType(widget.type!,
                                  doNotifyListeners: true);
                              if (mounted) {
                                setState(() {
                                  Navigator.of(context).pushNamedAndRemoveUntil(
                                      MainPage.routeArgsName, (route) => false,
                                      arguments: 1);
                                });
                              }
                            },
                            style: ButtonStyle(
                                foregroundColor: MaterialStateColor.resolveWith(
                                    (states) => Colors.red)),
                            child: Text(I18N.current.delete),
                          ),
                        ElevatedButton(
                            onPressed: canSave && isEdited()
                                ? () async {
                                    var size = availableSize;
                                    if (diaperType == DTType.booster) {
                                      if (size.isEmpty) {
                                        size = ["b"];
                                      } else {
                                        if (size.contains("b") &&
                                            size.length > 1) {
                                          size.remove("b");
                                        }
                                      }
                                    }
                                    var type = DiaperType(
                                        id: widget.type?.id ?? -1,
                                        name: nameCtrl.text,
                                        availableSizes: size,
                                        brandCode: selectedBrand?.code,
                                        image: imgCtrl.text.isNotEmpty
                                            ? imgCtrl.text
                                            : null,
                                        brand: selectedBrand,
                                        type: diaperType,
                                        official: false,
                                        style: diaperStyle,
                                        target: diaperTarget,
                                        hidden: false,
                                        discontinued: discontinued);
                                    DiaperProvider dp =
                                        Provider.of<DiaperProvider>(context,
                                            listen: false);
                                    var scaffoldMessengerState =
                                        ScaffoldMessenger.of(context);
                                    var navigatorState = Navigator.of(context,
                                        rootNavigator: true);
                                    if (widget.type == null) {
                                      var nType = await dp.createType(type);
                                      if (nType == null) {
                                        scaffoldMessengerState
                                            .showSnackBar(SnackBar(
                                          content: Text(I18N.current
                                              .custom_type_error_create),
                                          backgroundColor: Colors.red,
                                        ));
                                      }
                                    } else {
                                      await dp.updateType(type);
                                      if (mounted && navigatorState.canPop()) {
                                        navigatorState.pop();
                                      } else {
                                        navigatorState.pushReplacementNamed(
                                            MainPage.routeName);
                                      }
                                    }
                                    if (mounted && navigatorState.canPop()) {
                                      navigatorState.pop();
                                    } else {
                                      navigatorState.pushReplacementNamed(
                                          MainPage.routeName);
                                    }
                                  }
                                : null,
                            child: Text(I18N.current.save))
                      ],
                    ),
                  ),
                  if (widget.type != null)
                    SizedBox(
                      width: double.infinity,
                      child: Wrap(
                        direction: Axis.horizontal,
                        alignment: WrapAlignment.center,
                        children: [
                          FutureBuilder(
                            future: dp.getProposalFromType(widget.type!),
                            builder: (context, snapshot) {
                              if (snapshot.data != null) {
                                if (snapshot.data?.state ==
                                    ProposalState.WAITING) {
                                  return Container();
                                }
                              }

                              return ElevatedButton(
                                  key: keySubmitOfficialDiaperSubmitButton,
                                  onPressed: isEdited()
                                      ? null
                                      : () {
                                          Navigator.pushNamed(context,
                                              SubmitOfficialPage.routeName,
                                              arguments: SubmitOfficialArgs(
                                                  type: widget.type!));
                                        },
                                  child: Text(I18N.current
                                      .custom_type_diaper_propose_as_offical));
                            },
                          ),
                          ElevatedButton(
                              onPressed: isEdited()
                                  ? null
                                  : () {
                                      showMergeDialog(showCustom: false);
                                    },
                              onLongPress: isEdited()
                                  ? null
                                  : () {
                                      showMergeDialog(showCustom: true);
                                    },
                              child: Text(I18N.current
                                  .custom_type_diaper_merge_to_catalog)),
                        ],
                      ),
                    ),
                  if (widget.type != null)
                    FutureBuilder(
                        future: dp.getProposalId(widget.type!),
                        builder: (context, snap) {
                          developer.log("proposal ${snap.connectionState}");
                          if (snap.data != null) {
                            return ProposalStatusWidget(proposalId: snap.data!);
                          }
                          return Container();
                        })
                ],
              ),
            ),
          ),
        ));
  }

  void showMergeDialog({required bool showCustom}) {
    DiaperProvider dp = Provider.of<DiaperProvider>(context, listen: false);

    showDialog(
        context: context,
        builder: (context) => Scaffold(
              backgroundColor: Colors.transparent,
              body: AlertDialog(
                title: Text(I18N.current.select_type),
                actions: [
                  TextButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: Text(I18N.current.cancel))
                ],
                content: SizedBox(
                  width: MediaQuery.sizeOf(context).width * 0.9,
                  child: buildCatalog(dp, all: showCustom,
                      selectDiaper: (type) async {
                    if (widget.type == type) {
                      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                          content:
                              Text(I18N.current.custom_type_merge_same_type),
                          backgroundColor: Colors.redAccent));
                      return;
                    }
                    ResultDialogYesNo? result = await showDialog(
                        context: context,
                        builder: (context) => DialogYesNo(
                            title: Text(I18N.current
                                .custom_type_diaper_merge_confirm_title),
                            content: Text(I18N.current
                                .custom_type_diaper_merge_confirm_text(
                                    widget.type!.name, type.name))));
                    if (result == ResultDialogYesNo.yes) {
                      await dp.transfertTypeToNewType(widget.type!, type,
                          doNotifyListeners: false);
                      await dp.removeType(widget.type!,
                          doNotifyListeners: false);
                      dp.doNotifyListeners();
                      if (mounted) {
                        Navigator.of(context).popUntil((route) =>
                            route.settings.name == MainPage.routeName ||
                            route.settings.name == MainPage.routeArgsName);
                      }
                    }
                  }, openSizeDialog: false),
                ),
              ),
            ));
  }
}
