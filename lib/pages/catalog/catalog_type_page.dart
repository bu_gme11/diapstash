import 'package:diap_stash/common.dart';
import 'package:diap_stash/components/catalog/catalog_type_page_content.dart';
import 'package:diap_stash/object/interfaces.dart';
import 'package:diap_stash/pages/catalog/customs/custom_type_page.dart';
import 'package:diap_stash/providers/diaper_provider.dart';

class CatalogTypePageArgs {
  final DiaperType type;

  const CatalogTypePageArgs({required this.type});
}

class CatalogTypePage extends StatefulWidget {
  static const routeName = "CatalogTypePage";

  final DiaperType type;

  const CatalogTypePage({super.key, required this.type});

  @override
  State<CatalogTypePage> createState() => _CatalogTypePageState();
}

class _CatalogTypePageState extends State<CatalogTypePage> {

  @override
  Widget build(BuildContext context) {
    DiaperProvider dp = Provider.of<DiaperProvider>(context, listen: true);

    return Scaffold(
      appBar: buildAppBar(context: context, dp: dp),
      body: SingleChildScrollView(
          child: CatalogTypePageContent(type: widget.type, isPage: true)),
    );
  }

  AppBar buildAppBar(
      {required BuildContext context, required DiaperProvider dp}) {
    var actions = <Widget>[];

    if (!widget.type.official) {
      actions.add(IconButton(
          onPressed: () {
            Navigator.of(context).pushNamed(CustomTypePage.routeArgsName,
                arguments: CustomTypePageArgs(type: widget.type));
          },
          icon: const Icon(Icons.edit),
          tooltip: I18N.current.custom_type_edit));
    }

    var isFavorite = dp.isFavoriteType(widget.type);
    actions.add(IconButton(
      onPressed: () async {
        if (isFavorite) {
          await dp.removeFromFavorite(widget.type, doNotifyListeners: true);
        } else {
          await dp.addToFavorite(widget.type, doNotifyListeners: true);
        }
      },
      icon:
          isFavorite ? const Icon(Icons.star) : const Icon(Icons.star_outline),
      tooltip: I18N.current.favorite,
    ));

    return AppBar(
      title: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(widget.type.name),
          if (widget.type.brand != null)
            Text(widget.type.brand!.name,
                style: Theme.of(context).textTheme.bodyMedium)
        ],
      ),
      actions: actions,
    );
  }
}
