import 'package:diap_stash/common.dart';
import 'package:diap_stash/pages/settings/main/settings_main_about.dart';
import 'package:diap_stash/pages/settings/main/settings_main_cloud.dart';
import 'package:diap_stash/pages/settings/main/settings_main_data.dart';
import 'package:diap_stash/pages/settings/main/settings_main_general.dart';
import 'package:diap_stash/pages/settings/main/settings_main_web.dart';
import 'package:diap_stash/providers/settings_provider.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_screen_lock/flutter_screen_lock.dart';
import 'package:local_auth/local_auth.dart';
import 'package:settings_ui/settings_ui.dart';

class SettingsPage extends StatefulWidget {
  static const routeName = 'Settings';

  const SettingsPage({super.key});

  @override
  State<SettingsPage> createState() => SettingsPageState();

  static settingsThemeData(BuildContext context) {
    return SettingsThemeData(
        settingsListBackground: Theme.of(context).scaffoldBackgroundColor);
  }
}

class SettingsPageState extends State<SettingsPage> {

  final TextEditingController thresholdLowStockCtrl = TextEditingController();

  @override
  void initState() {
    SettingsProvider sp = Provider.of<SettingsProvider>(context, listen: false);
    thresholdLowStockCtrl.text =
        "${sp.spInstance.getInt(SettingsProvider.keyThresholdLowStock) ?? 10}";
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SettingsList(
      sections: [
        buildGeneral(),
        if (kIsWeb) buildWeb(),
        if (!kIsWeb) buildSecurity(),
        buildCloud(),
        buildData(),
        buildAbout(),
        buildCredits(),
      ],
      lightTheme: SettingsPage.settingsThemeData(context),
      //platform: DevicePlatform.iOS,
    );
  }

  SettingsSection buildSecurity() {
    var settingsProvider = Provider.of<SettingsProvider>(context);

    final LocalAuthentication localAuthentication = LocalAuthentication();
    localAuthentication.isDeviceSupported();

    return SettingsSection(
      tiles: [
        SettingsTile.switchTile(
          initialValue: settingsProvider.spInstance.getBool("lock") ?? false,
          onToggle: (val) {
            if (val == true) {
              screenLockCreate(
                  context: context,
                  title: Text(I18N.current.lock_first_code),
                  confirmTitle: Text(I18N.current.lock_confirm_code),
                  cancelButton: Text(I18N.current.cancel),
                  onConfirmed: (code) async {
                    await settingsProvider.spInstance
                        .setString("lock-code", code);
                    await settingsProvider.spInstance.setBool("lock", true);
                    setState(() {
                      Navigator.of(context).pop();
                    });
                  });
            } else {
              settingsProvider.spInstance.setBool("lock", false).then((_) {
                setState(() {});
              });
            }
          },
          title: Text(I18N.current.settings_security_lock),
          leading: const Icon(Icons.lock),
        )
      ],
      title: Text(I18N.current.settings_security),
    );
  }
}
