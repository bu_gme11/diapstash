import 'dart:developer';

import 'package:diap_stash/common.dart';
import 'package:diap_stash/components/stock/diaper_stock_widget.dart';
import 'package:diap_stash/object/interfaces.dart';
import 'package:diap_stash/providers/diaper_provider.dart';

class StockPage extends StatefulWidget {
  const StockPage({super.key});

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  @override
  State<StockPage> createState() => _StockPageState();
}

class _StockPageState extends State<StockPage> with TickerProviderStateMixin {
  late TabController _tabController;
  late TabController _tabSharedController;

  SharedStockAccess? selectedSSA;

  @override
  void initState() {
    log("Init ??");
    super.initState();
    _tabController = TabController(length: 2, vsync: this);
    _tabSharedController = TabController(length: 0, vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    var diaperProvider = Provider.of<DiaperProvider>(context, listen: true);

    log("build stock page");

    if (diaperProvider.sharedStockAccess.isEmpty) {
      return displayStock(
          stocks: diaperProvider.currentStockSorted, readOnly: false);
    }

    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        SizedBox(
          height: 50,
          child: TabBar(
            controller: _tabController,
            tabs: [
              Tab(
                text: I18N.current.stock_tab_your,
              ),
              Tab(text: I18N.current.stock_tab_other)
            ],
          ),
        ),
        Expanded(
          child: Padding(
            padding: const EdgeInsets.all(0.0),
            child: TabBarView(
              controller: _tabController,
              children: [
                displayStock(
                    stocks: diaperProvider.currentStockSorted, readOnly: false),
                buildShared(diaperProvider),
              ],
            ),
          ),
        )
      ],
    );
  }

  Widget displayStock(
      {required List<DiaperStock> stocks,
      required bool readOnly,
      SharedStockAccess? ssa}) {
    return stocks.isEmpty
        ? Container(
            alignment: Alignment.center,
            child: Text(I18N.current.no_diaper_left),
          )
        : ListView.builder(
            itemCount: stocks.length,
            itemBuilder: (context, index) {
              var stock = stocks.elementAt(index);
              return DiaperStockWidget(
                  stock: stock, readOnly: readOnly, ssa: ssa);
            });
  }

  Widget buildShared(DiaperProvider diaperProvider) {
    var sharedStockAccess = diaperProvider.sharedStockAccess;
    if (_tabSharedController.length != sharedStockAccess.length) {
      _tabSharedController =
          TabController(length: sharedStockAccess.length, vsync: this);
    }

    return Column(children: [
      SizedBox(
        height: 35,
        child: TabBar(
          controller: _tabSharedController,
          tabs: [for (var ssa in sharedStockAccess) Text(ssa.fromName)],
          onTap: (i) {
            if (i != 0) {
              diaperProvider.loadSharedStockAccess(doRemote: true, doNotifyListeners: true);
              setState(() {});
            }
          },
        ),
      ),
      Expanded(
        child: TabBarView(controller: _tabSharedController, children: [
          for (var ssa in sharedStockAccess)
            FutureBuilder(
                future: diaperProvider.getSharedStock(ssa),
                builder: (context, snapshot) {
                  if (!snapshot.hasData) {
                    return const CircularProgressIndicator();
                  }
                  var stocks =
                      snapshot.data!.where(diaperProvider.filterStock).toList();
                  var sortedStock = diaperProvider.sortStock(stocks);
                  return displayStock(
                      stocks: sortedStock, readOnly: ssa.readOnly, ssa: ssa);
                })
        ]),
      )
    ]);
  }
}

/*


 */
