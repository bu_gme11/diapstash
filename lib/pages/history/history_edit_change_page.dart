import 'package:date_time_picker/date_time_picker.dart';
import 'package:diap_stash/components/dialogs/catalog_list_dialog.dart';
import 'package:diap_stash/components/dialogs/dialog_yes_no.dart';
import 'package:diap_stash/components/stock/diaper_stock_widget.dart';
import 'package:diap_stash/components/tag_editor_widget.dart';
import 'package:diap_stash/components/type_image_widget.dart';
import 'package:diap_stash/components/types/button_size_type_widget.dart';
import 'package:diap_stash/object/interfaces.dart';
import 'package:diap_stash/pages/main_page.dart';
import 'package:diap_stash/providers/diaper_provider.dart';
import 'package:diap_stash/common.dart';
import 'package:diap_stash/providers/settings_provider.dart';
import 'package:diap_stash/util.dart';
import 'package:diap_stash/utils/comma_formatter.dart';
import 'package:diap_stash/utils/numerical_range_formatter.dart';
import 'package:diap_stash/utils/water_drop_icons.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:sentry_flutter/sentry_flutter.dart';
import 'package:universal_io/io.dart';

// ignore: unused_import, depend_on_referenced_packages
import 'package:intl/date_symbol_data_local.dart';

class HistoryEditChangePage extends StatefulWidget {
  final Change change;
  final Change? previousChange;
  final bool creation;

  const HistoryEditChangePage(
      {super.key,
      required this.change,
      this.previousChange,
      this.creation = false});

  @override
  State<HistoryEditChangePage> createState() => _HistoryEditChangePageState();
}

class _HistoryEditChangePageState extends State<HistoryEditChangePage>
    with TickerProviderStateMixin {
  TextEditingController noteTextController = TextEditingController();
  double wetness = 0;

  @override
  void initState() {
    super.initState();
    if (widget.change.note != null) {
      noteTextController.text = widget.change.note!;
    }
    if (widget.change.wetness != null) {
      wetness = widget.change.wetness!;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.creation
            ? I18N.current.add_change
            : I18N.current.edit_change),
        actions: [
          if (!widget.creation)
            IconButton(
                onPressed: () {
                  _deleteChange(context);
                },
                icon: const Icon(Icons.delete),
                tooltip: I18N.current.delete)
        ],
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              buildDates(context: context),
              buildDiapers(context: context),
              buildState(context: context),

              TextField(
                minLines: 3,
                maxLines: null,
                controller: noteTextController,
                decoration: InputDecoration(
                  label: Text(I18N.current.change_note),
                ),
                onChanged: (val) {
                  widget.change.note = val;
                },
              ),
              TagEditorWidget(
                tags: widget.change.tags,
                onChanged: (tags) {
                  setState(() {
                    widget.change.tags = tags;
                  });
                },
              ),
            ],
          ),
        ),
      ),
    );
  }

  buildDates({required BuildContext context}) {
    DateTime pickerMinTime = DateTime(2020, 0, 0);
    DateTime pickerMaxTime = DateTime.now();
    pickerMaxTime.add(const Duration(days: 1));

    var alwaysUse24HourFormatOf =
        SettingsProvider.instance.use24HourFormat(context);

    var format = DateFormat.yMd(Platform.localeName).add_Hm_12h();

    return Column(
      children: [
        Text(I18N.current.dates,
            style: Theme.of(context).textTheme.headlineMedium),
        DateTimePicker(
          type: DateTimePickerType.dateTime,
          dateLabelText: I18N.current.startTime,
          dateMask: format.pattern,
          firstDate: pickerMinTime,
          lastDate: pickerMaxTime,
          locale: SettingsProvider.instance.getCurrentLocale(context),
          use24HourFormat: alwaysUse24HourFormatOf,
          initialValue: widget.change.startTime.toString(),
          onChanged: ((newValue) {
            DateTime? newDate = DateTime.tryParse(newValue);
            if (newDate != null) {
              setState(() {
                widget.change.startTime = newDate;
              });
            }
          }),
        ),
        if (widget.change.endTime != null)
          DateTimePicker(
            type: DateTimePickerType.dateTime,
            dateLabelText: I18N.current.endTime,
            dateMask: format.pattern,
            firstDate: pickerMinTime,
            lastDate: pickerMaxTime,
            locale: SettingsProvider.instance.getCurrentLocale(context),
            use24HourFormat: alwaysUse24HourFormatOf,
            initialValue: widget.change.endTime!.toString(),
            onChanged: (newValue) {
              DateTime? newDate = DateTime.tryParse(newValue);
              if (newDate != null) {
                setState(() {
                  widget.change.endTime = newDate;
                });
              }
            },
          ),
        if (widget.change.endTime == null)
          ElevatedButton(
              onPressed: () {
                widget.change.endTime = DateTime.now();
                setState(() {});
              },
              child: Text(I18N.current.remove_diaper_now)),
      ],
    );
  }

  buildDiapers({required BuildContext context}) {
    var formatCurrency = NumberFormat.simpleCurrency();
    try {
      formatCurrency = NumberFormat.simpleCurrency(locale: Platform.localeName);
    } catch (_) {}
    List<Widget> diaperCards = [];
    for (var diaper in widget.change.diapers) {
      var diaperCard = buildDiaperCard(
          context: context,
          diaper: diaper,
          formatCurrency: formatCurrency,
          showActions: true);
      diaperCards.add(diaperCard);
    }

    return Column(children: [
      Text(I18N.current.diapers,
          style: Theme.of(context).textTheme.headlineMedium),
      if (widget.change.price != null)
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text.rich(
            TextSpan(children: [
              TextSpan(
                  text: "${I18N.current.price_change_price} : ",
                  style: const TextStyle(fontWeight: FontWeight.bold)),
              TextSpan(text: formatCurrency.format(widget.change.price))
            ]),
          ),
        ),
      Column(children: diaperCards),
      ElevatedButton(
          onPressed: () {
            addDiaper(context: context);
          },
          child: Text(I18N.current.add_diaper)),
    ]);
  }

  buildState({required BuildContext context}){
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        children: [
          Text(I18N.current.change_state,
              textAlign: TextAlign.right,
              style: TextStyle(color: Colors.grey.shade700)),
          ToggleButtons(
              isSelected: [
                for (var s in ChangeState.values)
                  widget.change.state == s
              ],
              children: [
                for (var s in ChangeState.values)
                  Padding(
                    padding: const EdgeInsets.fromLTRB(4, 2, 4, 2),
                    child: Text(s.localizedNamed()),
                  )
              ],
              onPressed: (index) {
                setState(() {
                  widget.change.state = ChangeState.values[index];
                });
              }),
          if (widget.change.state?.isWet ?? false)
            Text(I18N.current.change_wetness_level,
                textAlign: TextAlign.right,
                style: TextStyle(color: Colors.grey.shade700)),
          if (widget.change.state?.isWet ?? false)
            Align(
              alignment: Alignment.center,
              child: RatingBar.builder(
                glow: false,
                itemBuilder: (context, index) => Icon(
                    WaterDrop.water_drop_index(index + 1),
                    color: _ratingColor(index),
                ),
                updateOnDrag: true,
                direction: Axis.horizontal,
                wrapAlignment: WrapAlignment.center,
                minRating: 0,
                maxRating: 5,
                itemCount: 5,
                itemSize: 48,
                itemPadding: const EdgeInsets.symmetric(
                    horizontal: 2.0, vertical: 5),
                allowHalfRating: false,
                initialRating: widget.change.wetness ?? 0,
                onRatingUpdate: (value) {
                  setState(() {
                    widget.change.wetness =
                    value == 0 ? null : value;
                  });
                },
              ),
            )
        ],
      ),
    );
  }

  Color _ratingColor(int index){
    if(kIsWeb){
      return ((widget.change.wetness ?? 0) > index) ? Colors.blueAccent: Colors.grey;
    }
    return Colors.blueAccent;
  }

  void _deleteChange(BuildContext context) {
    Sentry.addBreadcrumb(Breadcrumb(category: "navigation", data: const {
      "openDialog": "confirmDeleteHistory",
    }));
    showDialog(
        context: context,
        builder: (context) => AlertDialog(
              title: Text(I18N.current.confirm),
              content: Text(I18N.current.confirm_delete_change),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(I18N.current.cancel)),
                TextButton(
                    onPressed: () async {
                      DiaperProvider dp =
                          Provider.of<DiaperProvider>(context, listen: false);
                      await dp.removeChange(widget.change,
                          doNotifyListeners: true);
                      if (mounted) {
                        Navigator.of(context)
                            .popUntil((route) {
                          return route.settings.name != null &&
                              (route.settings.name == MainPage.routeName ||
                                  route.settings.name ==
                                      MainPage.routeArgsName);
                        });
                      }
                    },
                    child: Text(I18N.current.confirm))
              ],
            ));
  }

  askRemoveDiaper(
      {required BuildContext context,
      required DiaperChange diaper,
      required NumberFormat formatCurrency}) {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text(I18N.current.change_ask_remove_diaper),
            content: SizedBox(
                height: 120,
                child: buildDiaperCard(
                    context: context,
                    diaper: diaper,
                    formatCurrency: formatCurrency,
                    showActions: false)),
            actions: [
              TextButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: Text(I18N.current.no)),
              TextButton(
                  onPressed: () async {
                    setState(() {
                      widget.change.diapers.remove(diaper);
                      Navigator.of(context).pop();
                    });
                  },
                  child: Text(I18N.current.yes))
            ],
          );
        });
  }

  editDiaperChange(
      {required BuildContext context,
      required DiaperChange diaper,
      required NumberFormat formatCurrency}) {
    String? currencySymbol;
    currencySymbol = formatCurrency.currencySymbol;
    TextEditingController ctrl =
        TextEditingController(text: diaper.price?.toString());
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text(I18N.current.change_edit_diaper_change),
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                buildDiaperCard(
                    context: context,
                    diaper: diaper,
                    formatCurrency: formatCurrency,
                    showActions: false),
                TextFormField(
                  decoration: InputDecoration(
                      label: Text(I18N.current.price),
                      prefixText: currencySymbol),
                  controller: ctrl,
                  keyboardType: const TextInputType.numberWithOptions(
                      signed: false, decimal: true),
                  inputFormatters: [
                    CommaFormatter(),
                    FilteringTextInputFormatter.allow(
                        RegExp(r"^[0-9]+([.,][0-9]*)?")),
                    NumericalRangeFormatter(min: 0)
                  ],
                )
              ],
            ),
            actions: [
              TextButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: Text(I18N.current.cancel)),
              TextButton(
                  onPressed: () async {
                    setState(() {
                      var priceParsed = double.tryParse(
                          ctrl.value.text.replaceAll(r",", "."));
                      if (priceParsed != null) {
                        var index = widget.change.diapers.indexOf(diaper);
                        if (index != -1) {
                          widget.change.diapers[index] = DiaperChange(
                              type: diaper.type,
                              size: diaper.size,
                              change: diaper.change,
                              price: priceParsed);
                        }
                      }
                      Navigator.of(context).pop();
                    });
                  },
                  child: Text(I18N.current.save))
            ],
          );
        });
  }

  askUseStock(
      {required BuildContext context,
      required DiaperStock stock,
      required String size}) async {
    DiaperProvider dp = Provider.of<DiaperProvider>(context, listen: false);
    ResultDialogYesNo? result = await showDialog(
        context: context,
        builder: (context) =>
            DialogYesNo(title: Text(I18N.current.change_ask_use_stock)));
    if (result == ResultDialogYesNo.yes) {
      await useStock(stock, size, dp);
    }
  }

  Future<void> useStock(
      DiaperStock stock, String size, DiaperProvider dp) async {
    if (stock.count.containsKey(size)) {
      var count = stock.count[size]! - 1;
      if (count <= 0) {
        stock.count.remove(size);
      } else {
        stock.count[size] = count;
      }
    }
    stock.updatedAt = DateTime.now();
    await dp.saveStock(stock: stock, doNotifyListeners: true);
  }

  addDiaper({required BuildContext context}) {
    Sentry.addBreadcrumb(Breadcrumb(category: "navigation", data: const {
      "openDialog": "addDiaper",
    }));
    showDialog(
        context: context,
        builder: (context) {
          DiaperProvider dp =
              Provider.of<DiaperProvider>(context, listen: false);
          SettingsProvider sp =
              Provider.of<SettingsProvider>(context, listen: false);

          Widget listViewCatalog =
              _buildCatalogAddDiaper(context: context, dp: dp);
          Widget? listStock = _buildStockAddDiaper(context: context, dp: dp, sp: sp);
          var tabController =
              TabController(length: listStock != null ? 2 : 0, vsync: this);
          return AlertDialog(
            title: Text(I18N.current.select_type),
            content: SizedBox(
                width: MediaQuery.sizeOf(context).width * 0.8,
                child: Column(
                  children: [
                    if (listStock != null)
                      SizedBox(
                          height: 50,
                          child: TabBar(
                            controller: tabController,
                            tabs: [
                              Text(I18N.current.stock),
                              Text(I18N.current.catalog)
                            ],
                          )),
                    Expanded(
                      child: TabBarView(
                        controller: tabController,
                        children: [
                          if (listStock != null) listStock,
                          listViewCatalog
                        ],
                      ),
                    )
                  ],
                )),
            actions: [
              TextButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: Text(I18N.current.cancel)),
            ],
          );
        });
  }

  Widget _buildCatalogAddDiaper(
      {required BuildContext context, required DiaperProvider dp}) {
    return buildCatalog(dp,
        selectDiaperWithSize: (DiaperType type, String size) async {
      var found =
          dp.currentStock.where((element) => element.type == type).toList();
      double? price;
      if (found.isNotEmpty) {
        var stock = found.first;
        if (stock.count.containsKey(size) && stock.count[size]! > 0) {
          await askUseStock(context: context, stock: stock, size: size);
        }
        if (stock.prices.containsKey(size)) {
          price = stock.prices[size];
        }
      }
      setState(() {
        Navigator.of(context).pop(); //select size
        Navigator.of(context).pop(); //select type
        widget.change.addDiaper(type, size, price);
      });
    }, typeSize: 80, openSizeDialog: true);
  }

  Widget? _buildStockAddDiaper(
      {required BuildContext context,
      required DiaperProvider dp,
      required SettingsProvider sp}) {
    var stocks = dp.currentStock;
    if (stocks.isEmpty) return null;

    var navigatorState = Navigator.of(context);
    return ListView.builder(
        itemCount: stocks.length,
        itemBuilder: (context, index) {
          var stock = stocks[index];
          return DiaperStockWidget(
            stock: stock,
            readOnly: true,
            onSelect: (size) async {
              useStock(stock, size, dp).then((_) {});
              setState(() {
                if (navigatorState.canPop()) {
                  navigatorState.pop();
                }
                double? price;
                if (stock.prices.containsKey(size)) {
                  price = stock.prices[size];
                }
                widget.change.addDiaper(stock.type, size, price);
              });
            },
            imageSize: 75,
            nameSize: 20,
            brandSize: 16,
          );
        });
  }

  Card buildDiaperCard(
      {required BuildContext context,
      required DiaperChange diaper,
      required NumberFormat formatCurrency,
      required bool showActions}) {
    var sizeName = diaper.size;
    if (diaper.type.type == DTType.booster && diaper.size == "b") {
      sizeName = I18N.current.booster;
    }
    return Card(
        child: Row(
      children: [
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: TypeImageWidget(type: diaper.type, width: 75, height: 75),
        ),
        Expanded(
          child: Column(
            children: [
              Padding(
                  padding: const EdgeInsets.all(5),
                  child: Text(
                    diaper.type.name,
                    style: Theme.of(context).textTheme.titleLarge,
                    textAlign: TextAlign.center,
                  )),
              ButtonSizeTypeWidget(
                  size: sizeName, color: Colors.purple, small: true),
              if (diaper.price != null)
                Text(formatCurrency.format(diaper.price))
            ],
          ),
        ),
        if (showActions)
          Column(
            children: [
              IconButton(
                  onPressed: () {
                    editDiaperChange(
                        context: context,
                        diaper: diaper,
                        formatCurrency: formatCurrency);
                  },
                  icon: const Icon(Icons.edit)),
              if (widget.change.diapers.length > 1)
                IconButton(
                    onPressed: () {
                      if (widget.change.diapers.length > 1) {
                        askRemoveDiaper(
                            context: context,
                            diaper: diaper,
                            formatCurrency: formatCurrency);
                      }
                    },
                    icon: const Icon(Icons.delete)),
            ],
          )
      ],
    ));
  }
}
