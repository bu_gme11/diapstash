import 'dart:developer';

import 'package:azlistview/azlistview.dart';
import 'package:diap_stash/common.dart';
import 'package:diap_stash/components/catalog/catalog_brand_widget.dart';
import 'package:diap_stash/components/catalog/catalog_type_page_content.dart';
import 'package:diap_stash/components/tutorials/submit_official_type.dart';
import 'package:diap_stash/object/interfaces.dart';
import 'package:diap_stash/pages/main_page.dart';
import 'package:diap_stash/providers/settings_provider.dart';
import 'package:diap_stash/util.dart';
import 'package:fuzzy/fuzzy.dart';

import '../providers/diaper_provider.dart';

class CatalogPage extends StatefulWidget {
  final TextEditingController searchController;

  const CatalogPage({super.key, required this.searchController});

  @override
  State<StatefulWidget> createState() => _CatalogPageState();
}

class _CatalogPageState extends State<CatalogPage>
    with TickerProviderStateMixin {
  late TabController _controller;
  DiaperType? selectedType;
  ScrollController asideScrollCtrl = ScrollController();


  @override
  void initState() {
    super.initState();
    _controller = TabController(length: 2, vsync: this);
    searchCatalogControler.addListener(onSearchChange);
  }

  @override
  void deactivate() {
    super.deactivate();
    searchCatalogControler.removeListener(onSearchChange);
  }

  onSearchChange() {
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    if (selectedType != null && useMobileLayout(context)) {
      selectedType = null;
    }
    return Row(
      children: [
        Flexible(flex: 2, child: buildList(context)),
        if (selectedType != null && !useMobileLayout(context))
          Flexible(
              flex: 3,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: buildTabletContent(context),
              ))
      ],
    );
  }

  Widget buildTabletContent(BuildContext context) {
    if (selectedType == null) {
      return const Text("No type selected");
    }
    return SingleChildScrollView(
        controller: asideScrollCtrl,
        child: CatalogTypePageContent(type: selectedType!, isPage: false));
  }

  Widget buildList(BuildContext context) {
    var diaperProvider = Provider.of<DiaperProvider>(context, listen: true);
    var settingsProvider = Provider.of<SettingsProvider>(context, listen: true);
    var filterDiaperType = settingsProvider.filterDiaperType;
    var filterDiaperTarget = settingsProvider.filterDiaperTarget;
    var filterDiaperStyle = settingsProvider.filterDiaperStyle;
    var filterDiscontinued = settingsProvider.filterDiscontinued;
    var types = diaperProvider.diaperTypes
        .where((t) =>
            !t.hidden &&
            t.canShow(filterDiaperType, filterDiaperTarget, filterDiaperStyle,
                filterDiscontinued))
        .toList();

    //var hasSearch = false;
    var searchText = searchCatalogControler.value.text;

    if (searchText.isNotEmpty) {
      var fuzzy = Fuzzy(types,
          options: FuzzyOptions(distance: 100, threshold: 0.25, keys: [
            WeightedKey<DiaperType>(
                name: "name", getter: (item) => item.name, weight: 1),
            WeightedKey<DiaperType>(
                name: "brand_name",
                getter: (item) => item.brand?.name ?? '',
                weight: 0.5),
            WeightedKey<DiaperType>(
                name: "brand_code",
                getter: (item) => item.brand?.code ?? '',
                weight: 0.1)
          ]));
      var searchResults = fuzzy.search(searchText);
      types = searchResults.map((e) => e.item).toList();
      //hasSearch = true;
    }

    var brandsOfficial = diaperProvider.brands
        .where((brand) =>
            types.any((type) => type.brand == brand && type.official))
        .toList();

    var brandsCustoms = diaperProvider.brands
        .where((brand) =>
            types.any((type) => type.brand == brand && !type.official))
        .toList();

    var customDiaperWithoutBrand = types
        .where((element) => element.brand == null && !element.official)
        .toList();

    brandsOfficial.sort((a, b) => a.name.compareTo(b.name));
    brandsCustoms.sort((a, b) => a.name.compareTo(b.name));

    var officialDiaperWithoutBrand = types
        .where((element) => element.brand == null && element.official)
        .toList();
    if (officialDiaperWithoutBrand.isNotEmpty) {
      brandsOfficial.add(DiaperBrand.otherBrand());
    }

    if (customDiaperWithoutBrand.isNotEmpty) {
      brandsCustoms.add(DiaperBrand.otherBrand());
    }

    var noDiaperFoundWidget = Container(
      alignment: Alignment.center,
      child: Text(I18N.current.no_diaper_found),
    );

    //var officialIndexBarData = S
    if (diaperProvider.favoritesTypes.isNotEmpty) {
      if(diaperProvider.favoritesTypes.where((e) => types.contains(e)).isNotEmpty) {
        brandsOfficial.insert(0, DiaperBrand.favoriteBrand());
      }
    }

    var selectDiaper = useMobileLayout(context)
        ? null
        : (type) {
            setState(() {
              selectedType = type;
              try {
                asideScrollCtrl.jumpTo(0);
              }catch(e){
                //nothing
              }
            });
          };

    const indexBarOptions = IndexBarOptions(
      needRebuild: true,
      ignoreDragCancel: true,
      downTextStyle: TextStyle(fontSize: 12, color: Colors.white),
      downItemDecoration:
          BoxDecoration(shape: BoxShape.circle, color: Colors.orangeAccent),
      indexHintWidth: 120 / 2,
      indexHintHeight: 100 / 2,
      indexHintAlignment: Alignment.centerRight,
      indexHintChildAlignment: Alignment(-0.25, 0.0),
      indexHintOffset: Offset(-20, 0),
    );
    Widget listViewCatalogOfficial = AzListView(
        data: brandsOfficial,
        //shrinkWrap: true,
        itemCount: brandsOfficial.length,
        indexBarData: SuspensionUtil.getTagIndexList(brandsOfficial),
        indexBarOptions: indexBarOptions,
        itemBuilder: (context, index) {
          var brand = brandsOfficial.elementAt(index);
          return CatalogBrandWidget(
              brand: brand,
              official: true,
              types: types,
              selectDiaper: selectDiaper,
              openSizeDialog: false);
        });

    if (brandsOfficial.isEmpty) {
      listViewCatalogOfficial = noDiaperFoundWidget;
    }

    Widget listViewCatalogCustoms = AzListView(
        data: brandsCustoms,
        indexBarData: SuspensionUtil.getTagIndexList(brandsCustoms),
        indexBarOptions: indexBarOptions,
        itemCount: brandsCustoms.length,
        itemBuilder: (context, index) {
          var brand = brandsCustoms.elementAt(index);
          return CatalogBrandWidget(
              brand: brand,
              official: false,
              types: types,
              selectDiaper: selectDiaper,
              openSizeDialog: false);
        });

    if (brandsCustoms.isEmpty) {
      listViewCatalogCustoms = noDiaperFoundWidget;
    }

    bool hasCustom = diaperProvider.diaperTypes
        .where((element) => !element.official)
        .isNotEmpty;

    var tablength = hasCustom ? 2 : 1;
    if (_controller.length != tablength) {
      log("Change tab controller");
      _controller = TabController(length: tablength, vsync: this);
    }

    return Column(crossAxisAlignment: CrossAxisAlignment.stretch, children: [
      if (hasCustom)
        SizedBox(
          height: 50,
          child: TabBar(
            key: keySubmitOfficialDiaperTabBar,
            controller: _controller,
            tabs: [
              Tab(text: I18N.current.catalog_official_diaper),
              if (hasCustom)
                Tab(
                    key: keySubmitOfficialDiaperTab,
                    text: I18N.current.catalog_custom_diaper),
            ],
          ),
        ),
      Expanded(
        child: Padding(
            padding: const EdgeInsets.all(5),
            child: TabBarView(controller: _controller, children: [
              listViewCatalogOfficial,
              if (hasCustom) listViewCatalogCustoms,
            ])),
      )
    ]);
  }
}
