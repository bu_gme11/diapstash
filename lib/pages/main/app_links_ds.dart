part of "../main_page.dart";

extension AppLinksDS on MainPageState {
  Future<void> initDeepLinks() async {
    _appLinks = AppLinks();
    // Check initial link if app was in cold state (terminated)
    final appLink = await _appLinks.getInitialAppLink();
    if (appLink != null) {
      developer.log('getLatestAppLink: $appLink');
      // ignore: use_build_context_synchronously
      await openAppLink(context, appLink);
    }

    if (!kIsWeb) {
      // Handle link when app is in warm state (front or background)
      _linkSubscription = _appLinks.uriLinkStream.listen((uri) async {
        await openAppLink(context, uri);
      });
    }
  }
}

openAppLink(BuildContext context, Uri uri) async {
  developer.log("open key $uri");
  var segments = uri.pathSegments.toList();
  if (uri.host.endsWith("diapstash.com")) {
    if (segments.length > 1) {
      switch (segments[0]) {
        case "history-sharing":
          await openAppLinkHistorySharing(context, uri);
          break;
        case "stock-sharing":
          await openAppLinkStockSharing(context, uri);
          break;
        case "cloud-sync":
          await openAppLinkCloudSync(context, uri);
          break;
      }
    }
  }
}

openAppLinkCloudSync(BuildContext context, Uri uri) async {
  SettingsProvider sp = Provider.of<SettingsProvider>(context, listen: false);
  if (sp.hasCloud()) {
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      content: Text(I18N.current.settings_cloud_already_enable),
      backgroundColor: Colors.red,
    ));
    return;
  }

  var all = Uri.decodeFull(uri.toString());
  var segments = Uri.parse(all).pathSegments;
  String qrCodeData =
      segments[1].replaceAll("%%%", "\$").replaceAll("___", "\$");
  ResultDialogYesNo? result = await showDialog(
      context: context,
      builder: (context) => DialogYesNo(
          title: Text(I18N.current.cloud_sync_app_links_title),
          content: Text(I18N.current.cloud_sync_app_links_content)));
  if (result == ResultDialogYesNo.yes) {
    sp.cloudFromQRCode(qrCodeData).then((ok) async {
      if (ok) {
        var dp = Provider.of<DiaperProvider>(context, listen: false);
        await dp.sendDataToCloud();
        await dp.loadData();
      } else {
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Text(I18N.current.settings_cloud_failed),
          backgroundColor: Colors.red,
        ));
      }
    }).catchError((err) {
      if (err is RangeError) {
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Text(I18N.current.settings_cloud_invalid_link),
          backgroundColor: Colors.red,
        ));
      }
    });
  }
}
