part of "../main_page.dart";

extension MainPageTutorial on MainPageState {
  void showTutorialCreateCustoms() {
    if (mounted) {
      var settingsProvider =
          Provider.of<SettingsProvider>(context, listen: false);
      var dp = Provider.of<DiaperProvider>(context, listen: false);
      if ((settingsProvider.spInstance.getBool("tuto_create_customs") ??
              false) ==
          false) {
        if (dp.currentStock
            .where((element) => element.total() > 0)
            .isNotEmpty) {
          tutorialCreateCustomDiaperMark.show(context: context);
        }
      }
    }
  }

  void showTutorialSubmitDiaper() {
    if (mounted) {
      var settingsProvider =
          Provider.of<SettingsProvider>(context, listen: false);
      if ((settingsProvider.spInstance.getBool("tuto_submit_custom") ??
              false) ==
          false) {
        tutorialSubmitOfficialDiaperMark.show(context: context);
      }
    }
  }
}
