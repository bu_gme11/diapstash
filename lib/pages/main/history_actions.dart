part of "../main_page.dart";

extension HistorySharing on MainPageState {
  List<Widget> getActionsHistory() {
    List<Widget> actions = [];
    DiaperProvider dp = Provider.of<DiaperProvider>(context, listen: true);
    SettingsProvider sp = Provider.of<SettingsProvider>(context, listen: false);
    NotificationProvider np = Provider.of<NotificationProvider>(context, listen: false);

    if (dp.changes.isNotEmpty) {
      actions.add(IconButton(
          onPressed: sp.hasCloud()
              ? () {
                  _showDialogAskTokenName();
                }
              : null,
          tooltip: sp.hasCloud()
              ? I18N.current.history_sharing
              : I18N.current.history_sharing_enable_cloud,
          icon: const Icon(Icons.share)));
    }

    actions.add(IconButton(
        onPressed: () {
          Sentry.addBreadcrumb(
              Breadcrumb(category: "navigation", data: const {
                "openDialog": "addChange",
              }));
          var change = Change(startTime: DateTime.now());
          Navigator.of(context).push(MaterialPageRoute(builder: (context) {
            return HistoryEditChangePage(change: change, creation: true);
          }), ).then((value) async {
            if(change.diapers.isEmpty) return;
            dp.changes.add(change);
            await dp.saveSpecificChanges(specificChanges: [change], doNotifyListeners: true);
            await np.computeNotification(dp.currentChange);
          });
        },
        icon: const Icon(Icons.add)));

    return actions;
  }

  _showDialogAskTokenName() {
    DialogAskText(
        title: Text(I18N.current.history_sharing),
        label: Text(
            I18N.current.history_sharing_name_of_the_share),
        bottom:
            Text(I18N.current.history_sharing_create_warning),
        okButton:
            Text(I18N.current.history_sharing_create_share),
        cancelButton: Text(I18N.current.cancel),
        cancelCallback: (context, _) {
          Navigator.pop(context);
        },
        okCallback: (context, nameCtrl) async {
          DiaperProvider dp =
              Provider.of<DiaperProvider>(context, listen: false);
          var scaffoldMessengerState = ScaffoldMessenger.of(context);
          var navigatorState = Navigator.of(context);
          var sharing = await dp.createSharingHistory(nameCtrl.value.text);

          if (sharing != null) {
            await Share.share(sharing.shareText(),
                subject:
                    I18N.current.history_sharing_text_subject);
          } else {
            scaffoldMessengerState.showSnackBar(SnackBar(
              content: Text(
                  I18N.current.history_sharing_failed_create),
            ));
          }
          navigatorState.pop();
        }).showAskTextDialog(context);
  }


}

openAppLinkHistorySharing(BuildContext context, Uri uri) async {
  var segments = uri.pathSegments.toList();
  var id = int.parse(segments[1]);
  var securityToken = segments[2];

  DiaperProvider dp = Provider.of<DiaperProvider>(context, listen: false);
  dp.validateSharingHistoryAccess(id, securityToken).then((valid) async {
    if (valid) {
      DialogAskText(
          title: Text(I18N.current.history_sharing),
          label: Text(I18N.current
              .history_sharing_name_of_the_share),
          okButton:
          Text(I18N.current.history_sharing_add_share),
          cancelButton: Text(I18N.current.cancel),
          cancelCallback: (context, _) {
            Navigator.pop(context);
          },
          okCallback: (context, nameCtrl) async {
            Navigator.pop(context);
            var name = nameCtrl.value.text;

            var err = await dp.linkSharingHistory(id, securityToken, name);

            if (err != null) {
              var errStr = "";
              switch (err) {
                case "already_linked":
                  errStr = I18N.current
                      .history_sharing_failed_already_linked;
                  break;
                default:
                  errStr = I18N.current.unknown_error;
              }
              scaffoldMessengerKey.currentState?.showSnackBar(SnackBar(
                  content: Text(errStr), backgroundColor: Colors.redAccent));
            }
          }).showAskTextDialog(context);
    } else {
      scaffoldMessengerKey.currentState?.showSnackBar(SnackBar(
          content: Text(
              I18N.current.history_sharing_failed_invalid),
          backgroundColor: Colors.redAccent));
    }
  });
}
