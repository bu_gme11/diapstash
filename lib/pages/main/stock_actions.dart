part of "../main_page.dart";

extension StockActions on MainPageState {
  List<Widget> getActionsStock() {
    DiaperProvider dp = Provider.of<DiaperProvider>(context, listen: false);
    SettingsProvider sp = Provider.of<SettingsProvider>(context, listen: false);
    List<Widget> actions = [];

    if (sp.allowRandomStock) {
      var randomEnabled = dp.currentStock
          .where((element) =>
              element.type.type != DTType.booster &&
              element.count.entries.where((e) => e.value > 0).isNotEmpty)
          .isNotEmpty;

      actions.add(IconButton(
          onPressed: randomEnabled
              ? () {
                  showDialogRandom();
                }
              : null,
          icon: const FaIcon(FontAwesomeIcons.dice), tooltip: I18N.current.select_diaper_random,));
    }
    actions.add(PopupMenuButton(
      icon: const Icon(Icons.sort),
      tooltip: I18N.current.sort,
      position: PopupMenuPosition.under,
      itemBuilder: (BuildContext context) {
        SettingsProvider sp =
            Provider.of<SettingsProvider>(context, listen: false);
        return [
          PopupMenuItem(
              value: "name",
              child: Row(
                children: [
                  if (sp.sortType == "name")
                    Icon(
                        sp.sortAsc ? Icons.arrow_upward : Icons.arrow_downward),
                  Text(I18N.current.sort__by_name),
                ],
              )),
          PopupMenuItem(
              value: "count",
              child: Row(
                children: [
                  if (sp.sortType == "count")
                    Icon(
                        sp.sortAsc ? Icons.arrow_upward : Icons.arrow_downward),
                  Text(I18N.current.sort__by_count),
                ],
              )),
        ];
      },
      onSelected: (selected) {
        developer.log(selected);

        // ignore: invalid_use_of_protected_member
        stockPageKey.currentState!.setState(() {
          var settingsProvider =
              Provider.of<SettingsProvider>(context, listen: false);
          settingsProvider.updateSort(selected);
        });
      },
    ));

    getActionSharingStock(dp, actions);
    return actions;
  }

  void getActionSharingStock(DiaperProvider dp, List<Widget> actions) {
    var currentStockSorted = dp.currentStockSorted;
    var shareImage = !kIsWeb &&
        currentStockSorted.isNotEmpty &&
        currentStockSorted.any((element) => element.total() > 0);
    actions.add(PopupMenuButton(
      icon: const Icon(Icons.share),
      tooltip: I18N.current.share,
      position: PopupMenuPosition.under,
      itemBuilder: (context) {
        return [
          if (shareImage)
            PopupMenuItem(
              value: "image",
              child: Text(I18N.current.stock_share_in_image),
            ),
          PopupMenuItem(
            value: "friend",
            enabled: SettingsProvider.instance.hasCloud(),
            child: Text(I18N.current.stock_share_with_friend),
          )
        ];
      },
      onSelected: (value) {
        switch (value) {
          case "image":
            _shareStockAsImage();
            break;
          case "friend":
            _showDialogAskTokenName();
            break;
        }
      },
    ));
  }

  _showDialogAskTokenName() {
    showDialog(
        context: context,
        builder: (context) {
          var readOnly = true;
          var controller = TextEditingController();
          return StatefulBuilder(builder: (context, setState) {
            bool canSave = controller.text.isNotEmpty;

            return AlertDialog(
              title: Text(I18N.current.stock_sharing),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(I18N.current.cancel)),
                TextButton(
                    onPressed: canSave
                        ? () async {
                            developer
                                .log("${controller.value.text} $readOnly");
                            DiaperProvider dp = Provider.of<DiaperProvider>(
                                context,
                                listen: false);
                            var navigatorState =
                                Navigator.of(context);
                            var sharing = await dp.createSharingStock(
                                controller.value.text, readOnly);

                            if (sharing != null) {
                              await Share.share(sharing.shareText(),
                                  subject: I18N
                                      .current.history_sharing_text_subject);
                            } else {
                              // ignore: use_build_context_synchronously
                              ScaffoldMessenger.of(context)
                                  .showSnackBar(SnackBar(
                                content: Text(
                                    I18N.current.history_sharing_failed_create),
                              ));
                            }
                            navigatorState.pop();
                          }
                        : null,
                    child: Text(I18N.current.stock_sharing_create_share))
              ],
              content: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  TextField(
                    controller: controller,
                    decoration: InputDecoration(
                        label:
                            Text(I18N.current.stock_sharing_name_of_the_share),
                        hintText: I18N.current.stock_sharing_create_warning),
                    onChanged: (val) {
                      setState(() {});
                    },
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(I18N.current.stock_sharing_read_only),
                      Switch(
                          value: readOnly,
                          onChanged: (val) {
                            setState(() {
                              readOnly = val;
                            });
                          })
                    ],
                  ),
                  Text(I18N.current.stock_sharing_read_only_hint,
                      style: const TextStyle(fontSize: 12)),
                  Text(controller.value.text)
                ],
              ),
            );
          });
        });
  }

  void _shareStockAsImage() {
    DiaperProvider dp = Provider.of<DiaperProvider>(context, listen: false);
    var currentStockSorted = dp.currentStockSorted;
    if (currentStockSorted.isNotEmpty &&
        currentStockSorted.any((element) => element.total() > 0)) {
      Navigator.pushNamed(context, "ShareStock");
    }
  }

  showDialogRandom() {
    DiaperProvider dp = Provider.of(context, listen: false);
    var random = Random();

    var stocks = dp.currentStock
        .where((element) =>
            element.type.type != DTType.booster &&
            element.count.entries.where((e) => e.value > 0).isNotEmpty)
        .toList();

    DiaperStock getRandomStock() {
      var rand = random.nextInt(stocks.length);
      return stocks[rand];
    }

    String getRandomSize(DiaperStock stock) {
      var sizes = stock.count.entries.where((e) => e.value > 0).toList();
      var rand = random.nextInt(sizes.length);
      return sizes[rand].key;
    }

    DiaperStock? stock;
    String? size;

    DiaperStock randomizer = getRandomStock();

    Timer? randomizerTimer;

    RestartableTimer? selectStock;

    int seconds = 3;
    if(stocks.length == 1){
      seconds = 1;
    }

    showDialog(
      context: context,
      builder: (ctx) {
        return StatefulBuilder(
          builder: (ctx, setState) {
            selectStock ??= RestartableTimer(Duration(seconds: seconds), () {
              if(ctx.mounted) {
                setState(() {
                  stock = getRandomStock();
                  size = getRandomSize(stock!);
                });
              }
            });
            randomizerTimer ??=
                Timer.periodic(const Duration(milliseconds: 150), (timer) {
                  if(ctx.mounted) {
                    setState(() {
                      randomizer = getRandomStock();
                    });
                  }else if(randomizerTimer!.isActive){
                    randomizerTimer!.cancel();
                  }
            });

            return AlertDialog(
              title: Text(I18N.current.select_diaper_random),
              content: Column(mainAxisSize: MainAxisSize.min, children: [
                if (stock != null)
                  TypeImageWidget(type: stock!.type, width: 100, height: 100)
                else
                  TypeImageWidget(
                      type: randomizer.type, width: 100, height: 100),
                if (stock != null)
                  Text(
                    stock!.type.name,
                    style: const TextStyle(
                        fontSize: 24, fontWeight: FontWeight.bold),
                  ),
                if (stock != null && stock?.type.brand != null)
                  Text(
                    stock!.type.brand!.name,
                    style: const TextStyle(fontSize: 20, color: Colors.grey),
                  ),
                if (size != null)
                  ButtonSizeTypeWidget(color: Colors.purple, size: size!)
              ]),
              actions: [
                TextButton(
                  onPressed: () {
                    if (randomizerTimer != null) {
                      randomizerTimer!.cancel();
                    }
                    if (selectStock != null && selectStock!.isActive) {
                      selectStock!.cancel();
                    }
                    Navigator.of(context).pop();
                  },
                  child: Text(I18N.current.close),
                ),
                TextButton(
                  onPressed: stock != null
                      ? () {
                          if (selectStock != null && !selectStock!.isActive) {
                            setState(() {
                              stock = null;
                              size = null;
                            });
                            selectStock!.reset();
                          }
                        }
                      : null,
                  child: Text(I18N.current.random_again),
                ),
                TextButton(
                  onPressed: stock != null && size != null
                      ? () async {
                          if (randomizerTimer != null) {
                            randomizerTimer!.cancel();
                          }
                          if (selectStock != null && selectStock!.isActive) {
                            selectStock!.cancel();
                          }
                          Navigator.of(context).pop();
                          await useStock(
                              context: context,
                              stock: stock!,
                              size: size!,
                              ssa: null,
                              onDone: () {});
                        }
                      : null,
                  child: Text(I18N.current.use),
                ),
              ],
            );
          },
        );
      },
    );
  }
}

openAppLinkStockSharing(BuildContext context, Uri uri) async {
  var segments = uri.pathSegments.toList();
  var id = int.parse(segments[1]);
  var securityToken = segments[2];

  DiaperProvider dp = Provider.of<DiaperProvider>(context, listen: false);
  dp.validateSharingStockAccess(id, securityToken).then((valid) async {
    if (valid != null) {
      DialogAskText(
          title: Text(I18N.current.stock_sharing),
          label: Text(I18N.current.stock_sharing_name_of_the_share),
          okButton: Text(I18N.current.stock_sharing_add_share),
          cancelButton: Text(I18N.current.cancel),
          cancelCallback: (context, _) {
            Navigator.pop(context);
          },
          okCallback: (context, nameCtrl) async {
            Navigator.of(context).pop();
            var name = nameCtrl.value.text;

            var err = await dp.linkSharingStock(id, securityToken, name);

            if (err != null) {
              var errStr = "";
              switch (err) {
                case "already_linked":
                  errStr = I18N.current.stock_sharing_failed_already_linked;
                  break;
                default:
                  errStr = I18N.current.unknown_error;
              }
              scaffoldMessengerKey.currentState?.showSnackBar(SnackBar(
                  content: Text(errStr), backgroundColor: Colors.redAccent));
            }
          }).showAskTextDialog(context);
    } else {
      scaffoldMessengerKey.currentState?.showSnackBar(SnackBar(
          content: Text(I18N.current.stock_sharing_failed_invalid),
          backgroundColor: Colors.redAccent));
    }
  });
}
