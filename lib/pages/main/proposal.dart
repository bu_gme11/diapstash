// ignore_for_file: invalid_use_of_protected_member

part of "../main_page.dart";

extension MainPageProposal on MainPageState {
  void searchApprovedProposals() {
    DiaperProvider dp = Provider.of<DiaperProvider>(context, listen: false);
    SettingsProvider sp = Provider.of<SettingsProvider>(context, listen: false);

    if(sp.spInstance.containsKey(SettingsProvider.keyLastsubcheck)) {
      var lastCheckStr = sp.spInstance.getString(
          SettingsProvider.keyLastsubcheck);
      if (lastCheckStr != null) {
        var lastCheck = DateTime.parse(lastCheckStr);
        if (DateTime
            .now()
            .difference(lastCheck)
            .inMinutes < 1) {
          developer.log("sub check wait");
          return;
        }
      }
    }

    developer.log("Search subs");
    dp.getOwnProposals().then((subs) async {
      if (subs.isEmpty) return;

      var accepted = subs
          .where((element) =>
              element.state == ProposalState.APPROVED &&
              element.original_type != null &&
              element.result_type != null)
          .toList();

      if (accepted.isNotEmpty) {
        Sentry.addBreadcrumb(Breadcrumb(
            category: "navigation",
            data: const {
              "openDialog": "dialogMerge",
            }
        ));
        showDialog(
            context: context,
            builder: (context) {
              return AlertDialog(
                  title: Text(I18N.current
                      .proposal_merge_dialog_title),
                  scrollable: true,
                  content: Column(mainAxisSize: MainAxisSize.min, children: [
                    Text(I18N.current
                        .proposal_merge_dialog_text1),
                    Text(I18N.current
                        .proposal_merge_dialog_text2),
                    for (var sub in accepted)
                      Card(
                        child: Column(
                          children: [
                            TypeCardWidget(
                                type: sub.original_type!, buttonsSize: const []),
                            const Icon(Icons.arrow_downward),
                            TypeCardWidget(
                                type: sub.result_type!, buttonsSize: const []),
                          ],
                        ),
                      )
                  ]),
                  actionsAlignment: MainAxisAlignment.spaceAround,
                  actions: [
                    TextButton(
                        onPressed: () async {
                          for (Proposal sub in accepted) {
                            if (sub.id != null) {
                              await (dp.database.delete(
                                      dp.database.diaperTypeProposalTable)
                                    ..where((tbl) =>
                                        tbl.proposal_id.equals(sub.id!)))
                                  .go();
                            }
                          }
                          setState(() {
                            Navigator.of(context).pop();
                          });
                        },
                        child: Text(I18N.current
                            .proposal_merge_dialog_dont_merge)),
                    TextButton(
                        onPressed: () {
                          sp.spInstance.setString(
                              SettingsProvider.keyLastsubcheck, DateTime.now().toString());
                          setState(() {
                            Navigator.of(context).pop();
                          });
                        },
                        child: Text(I18N.current.later)),
                    TextButton(
                        onPressed: () async {
                          var navigatorState = Navigator.of(context);
                          await dp.mergeProposals(accepted, doNotifyListeners: false);
                          for (var sub in accepted) {
                            await dp.removeType(sub.original_type!, doNotifyListeners: false);
                            await (dp.database.delete(dp.database.diaperTypeProposalTable)..where((tbl) => tbl.proposal_id.equals(sub.id!))).go();
                          }
                          await dp.saveTypes();
                          dp.doNotifyListeners();
                          if(mounted) {
                            setState(() {
                              navigatorState.pop();
                            });
                          }
                        },
                        child: Text(I18N.current
                            .proposal_merge_dialog_merge))
                  ],
                );
            });
      } else {
        sp.spInstance.setString(
            SettingsProvider.keyLastsubcheck, DateTime.now().toString());
      }
    });
  }
}
