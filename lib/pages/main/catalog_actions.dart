part of "../main_page.dart";

extension CatalogActions on MainPageState {

  List<Widget> getActionsCatalog() {
    List<Widget> actions = [];

    actions.add(PopupMenuButton(
        icon: const Icon(Icons.filter_alt_outlined),
        tooltip: I18N.current.filter,
        position: PopupMenuPosition.under,
        itemBuilder: (BuildContext context) {
          return [PopupMenuItem(value: "name", child: getCatalogFilter())];
        }));

    return actions;
  }

}
