import 'package:diap_stash/common.dart';
import 'package:diap_stash/providers/settings_provider.dart';

extension I18N on AppLocalizations {

  static AppLocalizations? _currentLocalization;

  static const List<Locale> translatedLocales = [Locale("en"), Locale("fr"), Locale("es"), Locale("nl"), Locale("de"), Locale("it"), Locale("ko")];

  static AppLocalizations get current {
    if (_currentLocalization == null) {
      _localization.then((value) => _currentLocalization = value);
    }else{
      try {
        var locale = Locale(SettingsProvider.instance.language);
        if(_currentLocalization != null && _currentLocalization!.localeName != locale.toString()){
          _localization.then((value) => _currentLocalization = value);
        }
      }catch(e){
        //nothing
      }
    }

    assert(_currentLocalization != null,
    'Localizations delegate was null. Try to call S.preheat() in your app\'s main method before trying to access S.current.');
    return _currentLocalization!;
  }

  static Future<void> preheat() async {
    _currentLocalization = await _localization;
  }

  static Future<AppLocalizations> get _localization {
    final widgetsBinding = WidgetsFlutterBinding.ensureInitialized();
    final preferredLocales = widgetsBinding.platformDispatcher.locales.toList();
    const supportedLocales = AppLocalizations.supportedLocales;

    try {
      if (SettingsProvider.instance.language != "system") {
        var locale = Locale(SettingsProvider.instance.language);
        preferredLocales.insert(0, locale);
      }
    }catch(e){
      //nothing
    }


    var locale = basicLocaleListResolution(preferredLocales, supportedLocales);

    return AppLocalizations.delegate.load(locale);
  }
}