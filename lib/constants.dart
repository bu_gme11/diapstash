// ignore_for_file: unused_import

import 'package:diap_stash/common.dart';
import 'package:flutter/foundation.dart';
import 'package:settings_ui/settings_ui.dart';

const devURL = String.fromEnvironment("DEV_URL", defaultValue: "http://10.0.1.71:8083");
const String appBarTitle= "DiapStash";

const bool isProduction = kReleaseMode;
//const bool isProduction = true; //BE CAREFULL TO DONT HAVE CLOUD SYNC ENABLE WITH USER DATA !
const baseUrl = isProduction ? "https://diapstash.com" : devURL;

const websiteURL = "https://diapstash.com/";
const catalogURL = "https://diapstash.com/catalog";
const privacyURL = "https://diapstash.com/diapstash/privacy.html";
const redditURL = "https://reddit.com/r/DiapStash";
const crowdinURL = "https://crowdin.com/project/diapstash";
const gitlabURL = "https://gitlab.com/DiapStash";

const taskChangeNotification =
    "fr.diapstash.diap_stash.task.change-notification";

const bgTaskPort = "fr.diapstash.diap_stash.main";

// ignore: constant_identifier_names
const ColorDS = Color(0xFF47299b);


const discontinuedArray = [false, true];
