import 'dart:async';
import 'dart:ui';

import 'package:diap_stash/components/initialiaze_widget.dart';
import 'package:diap_stash/common.dart';
import 'package:diap_stash/database/database.dart';
import 'package:diap_stash/pages/catalog/catalog_type_page.dart';
import 'package:diap_stash/pages/catalog/customs/submit_official_page.dart';
import 'package:diap_stash/pages/catalog/customs/custom_type_page.dart';
import 'package:diap_stash/pages/sharing/stock_sharing_page.dart';
import 'package:diap_stash/providers/diaper_provider.dart';
import 'package:diap_stash/providers/notification_provider.dart';
import 'package:diap_stash/providers/settings_provider.dart';
import 'package:diap_stash/utils/sentry_connectivity.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localized_locales/flutter_localized_locales.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:sentry_flutter/sentry_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:universal_io/io.dart';
import 'package:variable_app_icon/variable_app_icon.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'lock.dart';
import 'pages/main_page.dart';
import 'dart:developer' as developer;
import 'package:flutter/foundation.dart' show kDebugMode, kIsWeb, kReleaseMode;

// ignore: depend_on_referenced_packages
import 'package:intl/date_symbol_data_local.dart';

import 'package:diap_stash/backgroundfetch/backgroundfetch.dart'
    if (dart.library.io) 'package:diap_stash/backgroundfetch/backgroundfetch_app.dart';

final GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();
final GlobalKey<ScaffoldMessengerState> scaffoldMessengerKey =
    GlobalKey<ScaffoldMessengerState>();

FutureOr<SentryEvent?> beforeSendFlutter(SentryEvent event,
    {dynamic hint}) async {
  return (kDebugMode && !kIsWeb) ? null : event;
}

final MyDatabase database = MyDatabase();

Future<void> initProviders() async {
  var platform = await PackageInfo.fromPlatform();
  final DiaperProvider dp = DiaperProvider(
      database: database,
      versionCode: platform.buildNumber,
      versionName: platform.version);
  late SettingsProvider settingsProvider;
  var sharedPreferences = await SharedPreferences.getInstance();
  settingsProvider = SettingsProvider(
      spInstance: sharedPreferences,
      versionCode: platform.buildNumber,
      versionName: platform.version);
  dp.addSettingsProvider(settingsProvider);
  // ignore: unused_local_variable
  NotificationProvider np = NotificationProvider(database: database);
}

Future<void> main() async {
  if (!kIsWeb) {
    WidgetsFlutterBinding.ensureInitialized();
    DartPluginRegistrant.ensureInitialized();
    ByteData data =
        await PlatformAssetBundle().load('assets/ca/lets-encrypt-r3.pem');
    SecurityContext.defaultContext
        .setTrustedCertificatesBytes(data.buffer.asUint8List());
  }

  developer.log("Use server $baseUrl");

  VariableAppIcon.iOSDefaultAppIcon = "AppIcon"; // default ios icon
  VariableAppIcon.androidAppIconIds = ["appicon.DEFAULT", "appicon.DIAPER"];

  initializeDateFormatting(Platform.localeName);

  await initProviders();

  await SentryFlutter.init(
    (options) async {
      options.dsn =
          'https://42f0201348cb4dc4f02a4c93c6070742@sentry.diapstash.com/2';
      // Set tracesSampleRate to 1.0 to capture 100% of transactions for performance monitoring.
      // We recommend adjusting this value in production.
      options.attachViewHierarchy = true;
      options.tracesSampleRate = 1.0;
      options.environment = kReleaseMode ? "production" : "debug";
      options.beforeSend = beforeSendFlutter;
      options.addIntegration(SentryConnectivity());

      var platform = await PackageInfo.fromPlatform();
      options.dist = platform.buildNumber;
      if (Platform.isIOS) {
        options.release =
            "fr.diapstash.diap_stash@${platform.version}+${platform.buildNumber}";
      } else if (kIsWeb) {
        options.release =
            "diap_stash@${platform.version}+${platform.buildNumber}";
      }
    },
    appRunner: () {
      runApp(MultiProvider(providers: [
        ChangeNotifierProvider.value(value: SettingsProvider.instance),
        ChangeNotifierProvider.value(value: NotificationProvider.instance),
        ChangeNotifierProvider.value(value: DiaperProvider.instance),
      ], child: const MyApp()));
    },
  );
}

class MyApp extends StatefulWidget {
  const MyApp({super.key});

  @override
  State<MyApp> createState() => _AppState();
}

class _AppState extends State<MyApp> with Lock, WidgetsBindingObserver {
  @override
  void initState() {
    super.initState();
    developer.log("init");
    WidgetsBinding.instance.addObserver(this);

    Sentry.configureScope((scope) {
      SettingsProvider sp = SettingsProvider.instance;
      scope.setExtra("hasCloud", sp.hasCloud() ? "true" : "false");
      scope.setExtra(SettingsProvider.keyUserId, sp.getUserId() ?? "");
      if (sp.hasCloud()) {
        scope.setUser(SentryUser(id: sp.getUserId()));
      }
    });

    setupBackgroundTask().then((_) {}).catchError((error) {
      developer.log("Error on setup backgroud", error: error);
    });
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) async {
    super.didChangeAppLifecycleState(state);
    developer.log("set State $state");
    NotificationProvider np =
        Provider.of<NotificationProvider>(context, listen: false);
    DiaperProvider dp = Provider.of<DiaperProvider>(context, listen: false);
    if (state == AppLifecycleState.paused ||
        state == AppLifecycleState.hidden) {
      setState(() {
        relock();
      });
    } else if (state == AppLifecycleState.resumed) {
      await tryShowScreenLock();
      dp.loadData();
      np.loadNotification();
    }
    if (await np.hasChangeNotification(requestPermission: false) &&
        dp.currentChange == null) {
      await np.cancelChangeNotification(requestPermission: false);
    }
  }

  Locale? locale;

  @override
  Widget build(BuildContext context) {
    SettingsProvider sp = Provider.of<SettingsProvider>(context, listen: true);
    ThemeMode themeMode = getThemeMode(sp);
    getLocale(sp, context);

    return MaterialApp(
      scaffoldMessengerKey: scaffoldMessengerKey,
      title: appBarTitle,
      debugShowCheckedModeBanner: false,
      theme: ThemeData(useMaterial3: true),
      darkTheme: ThemeData.dark(useMaterial3: true),
      themeMode: themeMode,
      navigatorKey: navigatorKey,
      routes: {
        MainPage.routeName: (_) => const MainPage(),
        InitializeWidget.routeName: (_) => const InitializeWidget(),
        CustomTypePage.routeName: (_) => const CustomTypePage(),
        "ShareStock": (_) => const StockSharingPage()
      },
      onGenerateRoute: (settings) {
        var routes = {
          CustomTypePage.routeArgsName: (_) {
            var args = settings.arguments as CustomTypePageArgs;
            return CustomTypePage(type: args.type);
          },
          MainPage.routeArgsName: (_) {
            int index = settings.arguments! as int;
            return MainPage(selectedIndex: index);
          },
          SubmitOfficialPage.routeName: (_) {
            SubmitOfficialArgs args = settings.arguments! as SubmitOfficialArgs;
            return SubmitOfficialPage(type: args.type);
          },
          CatalogTypePage.routeName: (_) {
            var args = settings.arguments! as CatalogTypePageArgs;
            return CatalogTypePage(type: args.type);
          },
        };
        if (!routes.containsKey(settings.name)) {
          return null;
        }
        WidgetBuilder builder = routes[settings.name]!;
        return MaterialPageRoute(builder: (ctx) => builder(ctx));
      },
      initialRoute: InitializeWidget.routeName,
      locale: locale,
      localizationsDelegates: const [
        LocaleNamesLocalizationsDelegate(),
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
        AppLocalizations.delegate,
      ],
      supportedLocales: I18N.translatedLocales,
      navigatorObservers: [SentryNavigatorObserver()],
    );
  }

  void getLocale(SettingsProvider sp, BuildContext context) {
    try {
      Locale? newLocale;
      if (sp.language != "system") {
        newLocale = Locale(sp.language);
      }
      developer.log("getLocal ${locale?.languageCode} ${newLocale?.languageCode}");
      if (newLocale != locale) {
        rebuildAllChildren(context);
      }
      locale = newLocale;
    } catch (e) {
      developer.log("Bla bla", error: e);
    }
  }

  ThemeMode getThemeMode(SettingsProvider sp) {
    var themeMode = ThemeMode.system;
    var themeSettings = sp.spInstance.getString("theme") ?? "no";
    switch (themeSettings) {
      case "light":
        themeMode = ThemeMode.light;
        break;
      case "dark":
        themeMode = ThemeMode.dark;
        break;
    }
    developer.log("theme: ${themeMode.name} ($themeSettings)");
    return themeMode;
  }

  void rebuildAllChildren(BuildContext context) {
    void rebuild(Element el) {
      el.markNeedsBuild();
      el.visitChildren(rebuild);
    }

    (context as Element).visitChildren(rebuild);
  }
}
