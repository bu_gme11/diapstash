// ignore_for_file: invalid_use_of_visible_for_testing_member, invalid_use_of_protected_member

import 'dart:convert';
import 'dart:developer' as developer;

import 'package:diap_stash/database/database.dart';
import 'package:diap_stash/object/interfaces.dart';
import 'package:diap_stash/providers/diaper_provider.dart';
import 'package:diap_stash/constants.dart' as constants;
import 'package:diap_stash/providers/notification_provider.dart';
import 'package:drift/drift.dart';
import 'package:http/http.dart' as http;
import 'package:logging/logging.dart';
import 'package:sentry_flutter/sentry_flutter.dart';

extension DiaperProviderChange on DiaperProvider {
  List<String> get tags {
    Set<String> tags = {};
    for (var c in changes) {
      if (c.tags != null) {
        tags.addAll(c.tags!);
      }
    }
    return tags.toList()..sort((a, b) => a.compareTo(b));
  }

  Future<void> loadChange(
      {bool doRemote = true,
      bool setupNotification = true,
      required bool doNotifyListeners}) async {
    developer.log("Load Change doRemote=$doRemote");
    try {
      var mapsChange = await database.select(database.changeTable).get();
      for (var map in mapsChange) {
        var change = Change.fromDB(map);
        if (!changes.contains(change)) {
          changes.add(change);
        } else {
          var index = changes.indexOf(change);
          if (changes[index].updatedAt != null && change.updatedAt != null) {
            if (changes[index].updatedAt!.isBefore(change.updatedAt!)) {
              changes[index] = change;
            }
          }
        }
      }
      var maps = await database.select(database.diaperChangeTable).get();
      for (var map in maps) {
        var types = diaperTypes.where((element) => element.id == map.type_id);
        if (types.isEmpty) continue;
        var type = types.first;
        var search = changes.where((element) => element.id == map.change_id);
        if (search.isEmpty) {
          continue;
        }
        var change = search.first;
        var diaperChange = DiaperChange(
            type: type, size: map.size, change: change, price: map.price);
        if (!change.diapers.contains(diaperChange)) {
          change.diapers.add(diaperChange);
        }
      }
      if (doRemote) {
        await loadChangeRemote(doNotifyListeners: false);
      }
      changes.sort((a, b) => b.startTime.compareTo(a.startTime));

      if (setupNotification &&
          currentChange != null &&
          NotificationProvider.instance != null) {
        NotificationProvider.instance!
            .setupNotifications(currentChange!, requestPermission: false);
      }
    } catch (e, s) {
      developer.log("Error when load change", error: e);
      Sentry.captureException(e, stackTrace: s);
    }

    if (doNotifyListeners) {
      notifyListeners();
    }
  }

  Future<void> loadChangeRemote({required bool doNotifyListeners}) async {
    if (settingsProvider.hasCloud()) {
      developer.log("loadChangeRemote");
      var start = DateTime.now();
      var userId = settingsProvider.getUserId();
      if (userId != null) {
        var uri = Uri.parse("${constants.baseUrl}/rest/changes");
        var sentrySpan =
            Sentry.startTransaction("loadSharedHistory()", "loadSharedHistory");
        try {
          var res = await http.get(uri,
              headers: cloudHeaders(headers: {}, span: sentrySpan));
          var changesSaved = <Change>[];
          if (res.statusCode == 200) {
            var data = jsonDecode(res.body) as List<dynamic>;

            for (var rawChange in data) {
              var possibilites =
                  changes.where((element) => element.id == rawChange["id"]);
              if (possibilites.isEmpty) {
                var change = Change.fromCloud(rawChange, this);
                changes.add(change);
                changesSaved.add(change);
              } else {
                var change = Change.fromCloud(rawChange, this);
                var toSearch = possibilites.first;
                var index = changes.indexOf(toSearch);
                var realChange = changes[index];

                bool useRemote = true;

                if (realChange.updatedAt != null && change.updatedAt != null) {
                  if (realChange.updatedAt!.isAfter(change.updatedAt!)) {
                    useRemote = false;
                  }
                }

                if (useRemote) {
                  realChange.startTime = change.startTime;
                  realChange.endTime = change.endTime;
                  realChange.diapers = change.diapers;
                  realChange.note = change.note;
                  realChange.state = change.state;
                  realChange.tags = change.tags;
                  realChange.updatedAt = change.updatedAt;
                  changesSaved.add(change);
                }
              }
            }
          }
          await saveChanges(doRemote: false, doNotifyListeners: doNotifyListeners);
          var notInRemote =
              changes.where((c) => !changesSaved.contains(c)).toList();
          await saveSpecificChangesRemote(notInRemote);
        } catch (err, s) {
          Sentry.captureException(err, withScope: (scope) {
            scope.span = sentrySpan;
          }, stackTrace: s);
          developer.log("Fail when cloud load change",
              level: Level.SEVERE.value, error: err);
        } finally {
          sentrySpan.finish();
        }
      }
      developer.log(
          "loadChangeRemote - done ${DateTime.now().difference(start).inSeconds}");
    }
  }

  saveChanges({bool doRemote = true, required bool doNotifyListeners}) async {
    return await saveSpecificChanges(
        specificChanges: changes, doRemote: doRemote, doNotifyListeners: doNotifyListeners);
  }

  saveSpecificChanges(
      {required List<Change?> specificChanges, bool doRemote = true, required bool doNotifyListeners}) async {
    List<Change> changes = specificChanges
        .where((element) => element != null)
        .map((e) => e!)
        .toList(growable: false);
    for (var change in changes.toList(growable: false)) {
      var entity = ChangeTableCompanion.insert(
          id: change.id != null ? Value(change.id!) : const Value.absent(),
          startTime: Value(change.startTime
              .copyWith(second: 0, millisecond: 0, microsecond: 0)
              .toUtc()
              .toIso8601String()),
          endTime: change.endTime != null
              ? Value(change.endTime!
                  .copyWith(second: 0, millisecond: 0, microsecond: 0)
                  .toUtc()
                  .toIso8601String())
              : const Value.absent(),
          note: change.note != null ? Value(change.note) : const Value.absent(),
          tags: change.tags != null ? Value(change.tags) : const Value.absent(),
          wetness: change.wetness != null
              ? Value(change.wetness)
              : const Value.absent(),
          state:
              change.state != null ? Value(change.state) : const Value.absent(),
          updatedAt: change.updatedAt != null
              ? Value(change.updatedAt)
              : const Value.absent());
      var id = await database
          .into(database.changeTable)
          .insert(entity, mode: InsertMode.insertOrReplace);
      if (change.id != null) {
        var index = changes.indexOf(change);
        changes[index] = change;
      } else {
        change.id = id;
      }
      var changeDiapers = change.diapers.toList(growable: false);
      
      await database.diaperChangeTable.deleteWhere((tbl) => tbl.change_id.equals(id));
      for (var diaper in changeDiapers) {
        await database
            .into(database.diaperChangeTable)
            .insert(diaper.toDB(), mode: InsertMode.insertOrReplace);
      }
    }

    if (doRemote) {
      await saveSpecificChangesRemote(specificChanges);
    }
    if (doNotifyListeners) {
      notifyListeners();
    }
  }

  saveChangesRemote() async {
    return await saveSpecificChangesRemote(changes);
  }

  saveSpecificChangesRemote(List<Change?> specificChanges) async {
    if (settingsProvider.hasCloud() && specificChanges.isNotEmpty) {
      var sentrySpan = Sentry.startTransaction(
          "saveSpecificChangesRemote()", "saveSpecificChangesRemote");
      developer.log("saveSpecificChangesRemote");
      var start = DateTime.now();
      var userId = settingsProvider.getUserId()!;
      List<Change> changes = specificChanges
          .where((element) => element != null)
          .map((e) => e!)
          .toList(growable: false);
      var maps = {"changes": changes.map((c) => c.toCloud(userId)).toList()};

      var uri = Uri.parse("${constants.baseUrl}/rest/changes");
      try {
        await http.post(uri,
            headers: cloudHeaders(
                headers: {"Content-Type": "application/json"},
                span: sentrySpan),
            body: jsonEncode(maps));
      } catch (err, s) {
        Sentry.captureException(err, withScope: (scope) {
          scope.span = sentrySpan;
          scope.setExtra("change_maps", maps);
        }, stackTrace: s);
        developer.log("Fail when cloud save changes",
            level: Level.SEVERE.value, error: err);
      } finally {
        sentrySpan.finish();
      }
      developer.log(
          "saveSpecificChangesRemote Done ${DateTime.now().difference(start).inSeconds}");
    }
  }

  removeChange(Change change, {required bool doNotifyListeners}) async {
    if (changes.remove(change)) {
      await (database.delete(database.diaperChangeTable)
            ..where((tbl) => tbl.change_id.equals(change.id!)))
          .go();
      await (database.delete(database.changeTable)
            ..where((tbl) => tbl.id.equals(change.id!)))
          .go();
      await removeChangeRemote(change);
      //await saveChanges();
      if (doNotifyListeners) {
        notifyListeners();
      }
    }
  }

  removeChangeRemote(Change change) async {
    if (settingsProvider.hasCloud()) {
      var sentrySpan =
          Sentry.startTransaction("removeChangeRemote()", "removeChangeRemote");
      var uri = Uri.parse("${constants.baseUrl}/rest/changes");
      try {
        await http.delete(uri,
            headers: cloudHeaders(
                headers: {"Content-Type": "application/json"},
                span: sentrySpan),
            body: jsonEncode(change.toCloud(settingsProvider.getUserId()!)));
      } catch (err, s) {
        Sentry.captureException(err, withScope: (scope) {
          scope.span = sentrySpan;
        }, stackTrace: s);
        developer.log("Fail when cloud save changes",
            level: Level.SEVERE.value, error: err);
      } finally {
        sentrySpan.finish();
      }
    }
  }

  resetChangeHistory({bool doRemote = true, required bool doNotifyListeners}) async {
    await database.diaperChangeTable.deleteAll();
    await database.changeTable.deleteAll();
    if (doRemote) {
      await resetChangeHistoryRemote();
    }
    changes.clear();
    await loadChange(doRemote: doRemote, doNotifyListeners: false);
    if (doNotifyListeners) {
      notifyListeners();
    }
  }

  resetChangeHistoryRemote() async {
    if (settingsProvider.hasCloud()) {
      var sentrySpan = Sentry.startTransaction(
          "resetChangeHistoryRemote()", "resetChangeHistoryRemote");
      var uri = Uri.parse("${constants.baseUrl}/rest/changes/clear");
      try {
        await http.post(
          uri,
          headers: cloudHeaders(
              headers: {"Content-Type": "application/json"}, span: sentrySpan),
        );
      } catch (err, s) {
        Sentry.captureException(err, withScope: (scope) {
          scope.span = sentrySpan;
        }, stackTrace: s);
        developer.log("Fail when cloud clear changes",
            level: Level.SEVERE.value, error: err);
      } finally {
        sentrySpan.finish();
      }
    }
  }
}
