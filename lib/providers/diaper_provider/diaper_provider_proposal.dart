// ignore_for_file: invalid_use_of_visible_for_testing_member, invalid_use_of_protected_member

import 'dart:convert';
import 'dart:developer' as developer;

import 'package:diap_stash/database/database.dart';
import 'package:diap_stash/object/interfaces.dart';
import 'package:diap_stash/object/proposals.dart';
import 'package:diap_stash/providers/diaper_provider.dart';
import 'package:diap_stash/constants.dart' as constants;
import 'package:drift/drift.dart';
import 'package:http/http.dart' as http;
import 'package:sentry_flutter/sentry_flutter.dart';

extension DiaperProviderProposal on DiaperProvider {
  Future<bool> sendProposal(Proposal proposal) async {
    var result = false;
    var sentrySpan = Sentry.startTransaction("sendProposal()", "sendProposal");
    try {
      var uri = Uri.parse("${constants.baseUrl}/rest/v2/proposal/submit/type?from=app");
      var res = await http.put(uri,
          headers: cloudHeaders(
              headers: {"Content-Type": "application/json"}, span: sentrySpan),
          body: jsonEncode(proposal.toRest()));

      if (res.statusCode == 200) {
        if (proposal.original_type_id != null) {
          var map = jsonDecode(res.body);
          await database.into(database.diaperTypeProposalTable).insert(
              DiaperTypeProposalTableData(
                  type_id: proposal.original_type_id!, proposal_id: map["id"]), mode: InsertMode.replace);
        }
        result = true;
      }
    } catch (e, s) {
      developer.log("Error on submit proposal", error: e);
      developer.log(e.toString());
      Sentry.captureException(e, stackTrace: s, withScope: (scope) {
        scope.span = sentrySpan;
      });
    } finally {
      sentrySpan.finish();
    }
    return result;
  }

  Future<Proposal?> getProposalFromType(DiaperType type) async {
    var res = await (database.select(database.diaperTypeProposalTable)
      ..where((tbl) => tbl.type_id.equals(type.id)))
        .getSingleOrNull();
    if (res != null) {
      var id =  res.proposal_id;
      return await getProposal(id);
    }
    developer.log("No sub id");
    return null;
  }

  Future<String?> getProposalId(DiaperType type) async {
    var res = await (database.select(database.diaperTypeProposalTable)
          ..where((tbl) => tbl.type_id.equals(type.id)))
        .getSingleOrNull();
    if (res != null) {
      return res.proposal_id;
    }
    developer.log("No sub id");
    return null;
  }

  Future<Proposal?> getProposal(String id) async {
    var uri = Uri.parse("${constants.baseUrl}/rest/v2/proposal/$id");
    var sentrySpan = Sentry.startTransaction("sendProposal()", "sendProposal");
    try {
      var res = await http.get(uri,
          headers: cloudHeaders(headers: {}, span: sentrySpan));

      if (res.statusCode == 200) {
        var map = jsonDecode(res.body);
        var sub = Proposal.fromRest(map, this);
        return sub;
      }else if (res.statusCode == 404){
        await (database.delete(database.diaperTypeProposalTable)..where((tbl) => tbl.proposal_id.equals(id))).go();
      }
    } catch (e, s) {
      developer.log("Error when get proposal from remote", error: e);
      developer.log(e.toString());
      Sentry.captureException(e, stackTrace: s, withScope: (scope) {
        scope.span = sentrySpan;
      });
    } finally {
      sentrySpan.finish();
    }

    return null;
  }

  Future<List<Proposal>> getOwnProposals() async {
    List<Proposal> list = [];

    var ids = "";

    var data = await database.select(database.diaperTypeProposalTable).get();

    if (data.isEmpty) {
      return list;
    }

    for (var item in data) {
      var id = item.proposal_id;
      if (ids.isNotEmpty) {
        ids += "&";
      }
      ids += "id=$id";
    }

    var sentrySpan =
        Sentry.startTransaction("getOwnProposals()", "getOwnProposals");
    try {
      var uri = Uri.parse("${constants.baseUrl}/rest/v2/proposal/many");

      List<String> ids = data.map((e) => e.proposal_id).toList();
      var body = {
        "ids" : ids
      };

      var res = await http.post(uri, body: jsonEncode(body),
          headers: cloudHeaders(headers: {}, span: sentrySpan));

      if (res.statusCode == 200) {
        List<dynamic> maps = jsonDecode(res.body);
        for (var map in maps) {
          var sub = Proposal.fromRest(map as Map<String, dynamic>, this);
          list.add(sub);
        }
      }
    } catch (e, s) {
      Sentry.captureException(e, stackTrace: s, withScope: (scope) {
        scope.span = sentrySpan;
      });
    } finally {
      sentrySpan.finish();
    }
    return list;
  }

  Future<void> mergeProposals(List<Proposal> accepted, {required bool doNotifyListeners}) async {
    var subs = accepted
        .where((element) =>
            element.result_type != null && element.original_type != null)
        .toList();
    for (var sub in subs) {
      await transfertTypeToNewType(sub.original_type!, sub.result_type!, doNotifyListeners: false);
    }
    if(doNotifyListeners){
      notifyListeners();
    }
  }
}
