// ignore_for_file: invalid_use_of_visible_for_testing_member, invalid_use_of_protected_member

import 'dart:convert';
import 'dart:developer' as developer;

import 'package:diap_stash/database/database.dart';
import 'package:diap_stash/object/interfaces.dart';
import 'package:diap_stash/providers/diaper_provider.dart';
import 'package:diap_stash/constants.dart' as constants;
import 'package:drift/drift.dart';
import 'package:http/http.dart' as http;
import 'package:sentry_flutter/sentry_flutter.dart';

extension DiaperProviderFavorite on DiaperProvider {
  loadFavorites({bool doRemote = true, required bool doNotifyListeners}) async {
    var favs = await database.select(database.favoriteTypeTable).get();
    for (var fav in favs) {
      var found = diaperTypes.where((element) => element.id == fav.type_id);
      if (found.isNotEmpty) {
        var type = found.first;
        favoritesTypes.add(type);
      }
    }
    if (doRemote) {
      await loadRemoteFavorites(doNotifyListeners: false);
    }
    if (doNotifyListeners) {
      notifyListeners();
    }
  }

  loadRemoteFavorites({required bool doNotifyListeners}) async {
    if (settingsProvider.hasCloud()) {
      var sentrySpan = Sentry.startTransaction(
          "loadRemoteFavorites()", "loadRemoteFavorites");
      try {
        var uri = Uri.parse("${constants.baseUrl}/rest/favorites");
        var res = await http.get(uri,
            headers: cloudHeaders(headers: {}, span: sentrySpan));
        if (res.statusCode == 200) {
          var maps = jsonDecode(res.body) as List<dynamic>;
          for (var map in maps) {
            var typeId = map["id"];
            var found = diaperTypes.where((element) => element.id == typeId);
            if (found.isNotEmpty) {
              var type = found.first;
              favoritesTypes.add(type);
              await database.into(database.favoriteTypeTable).insert(
                  FavoriteTypeTableData(type_id: type.id),
                  mode: InsertMode.insertOrIgnore);
            }
          }
        }
        await sendAllRemoteFavorites();
      } catch (e, s) {
        developer.log("Error when loading fav from cloud $e");
        Sentry.captureException(e, stackTrace: s, withScope: (scope) {
          scope.span = sentrySpan;
        });
      } finally {
        sentrySpan.finish();
      }
    }

    if (doNotifyListeners) {
      notifyListeners();
    }
  }

  addToFavorite(DiaperType type,
      {bool doRemote = true, required bool doNotifyListeners}) async {
    if (!favoritesTypes.contains(type)) {
      favoritesTypes.add(type);
      await database.into(database.favoriteTypeTable).insert(
          FavoriteTypeTableData(type_id: type.id),
          mode: InsertMode.insertOrIgnore);
    }
    if (doRemote) {
      await addRemoteFavorites(type);
    }
    if (doNotifyListeners) {
      notifyListeners();
    }
  }

  addRemoteFavorites(DiaperType type) async {
    if (settingsProvider.hasCloud()) {
      var sentrySpan =
          Sentry.startTransaction("addRemoteFavorites()", "addRemoteFavorites");
      try {
        var uri = Uri.parse("${constants.baseUrl}/rest/favorites");
        var res = await http.put(uri,
            body: jsonEncode({"type_id": type.id}),
            headers: cloudHeaders(
                headers: {"Content-Type": "application/json"},
                span: sentrySpan));
        if (res.statusCode == 200) {}
      } catch (e, s) {
        developer.log("Error when add fav from cloud $e");
        Sentry.captureException(e, stackTrace: s, withScope: (scope) {
          scope.span = sentrySpan;
        });
      } finally {
        sentrySpan.finish();
      }
    }
  }

  sendAllRemoteFavorites() async {
    if (settingsProvider.hasCloud() && favoritesTypes.isNotEmpty) {
      var sentrySpan = Sentry.startTransaction(
          "sendAllRemoteFavorites()", "sendAllRemoteFavorites");
      try {
        var uri = Uri.parse("${constants.baseUrl}/rest/favorites");
        var res = await http.post(uri,
            body: jsonEncode(favoritesTypes.map((e) => e.id).toList()),
            headers: cloudHeaders(
                headers: {"Content-Type": "application/json"},
                span: sentrySpan));
        if (res.statusCode == 200) {}
      } catch (e, s) {
        developer.log("Error when send all fav from cloud $e");
        Sentry.captureException(e, stackTrace: s, withScope: (scope) {
          scope.span = sentrySpan;
        });
      } finally {
        sentrySpan.finish();
      }
    }
  }

  removeFromFavorite(DiaperType type,
      {required bool doNotifyListeners, bool doRemote = true}) async {
    if (favoritesTypes.contains(type)) {
      favoritesTypes.remove(type);
      await (database.delete(database.favoriteTypeTable)
            ..where((tbl) => tbl.type_id.equals(type.id)))
          .go();
    }
    if (doRemote) {
      await removeRemoteFavorites(type);
    }
    if (doNotifyListeners) {
      notifyListeners();
    }
  }

  removeRemoteFavorites(DiaperType type) async {
    if (settingsProvider.hasCloud()) {
      var sentrySpan = Sentry.startTransaction(
          "removeRemoteFavorites()", "removeRemoteFavorites");
      try {
        var uri = Uri.parse("${constants.baseUrl}/rest/favorites");
        var res = await http.delete(uri,
            body: jsonEncode({"type_id": type.id}),
            headers: cloudHeaders(
                headers: {"Content-Type": "application/json"},
                span: sentrySpan));
        if (res.statusCode == 200) {}
      } catch (e, s) {
        developer.log("Error when remove fav from cloud $e");
        Sentry.captureException(e, stackTrace: s, withScope: (scope) {
          scope.span = sentrySpan;
        });
      } finally {
        sentrySpan.finish();
      }
    }
  }

  int compareFavoriteType(DiaperType a, DiaperType b) {
    bool aFav = favoritesTypes.contains(a);
    bool bFav = favoritesTypes.contains(b);

    if (aFav == bFav) {
      return 0;
    } else if (aFav) {
      return -10000;
    } else {
      return 10000;
    }
  }

  bool isFavoriteType(DiaperType type) {
    return favoritesTypes.where((element) => element == type).isNotEmpty;
  }
}
