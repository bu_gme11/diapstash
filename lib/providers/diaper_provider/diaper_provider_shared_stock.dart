// ignore_for_file: invalid_use_of_visible_for_testing_member, invalid_use_of_protected_member

import 'dart:convert';
import 'dart:developer' as developer;

import 'package:diap_stash/object/interfaces.dart';
import 'package:diap_stash/providers/diaper_provider.dart';
import 'package:diap_stash/constants.dart' as constants;
import 'package:drift/drift.dart';
import 'package:http/http.dart' as http;
import 'package:sentry_flutter/sentry_flutter.dart';

extension DiaperProviderSharedStock on DiaperProvider {
  Future<List<SharedStockAccess>> loadSharedStockAccess(
      {bool doRemote = true, required bool doNotifyListeners}) async {
    var fromDB = await database.sharedStockAccessTable.select().get();
    for (var sha in fromDB) {
      var found =
          sharedStockAccess.indexWhere((element) => element.id == sha.id);
      var valid = await validateSharingStockAccess(sha.id, sha.securityToken);
      if (valid == null) {
        if (found != -1) {
          sharedStockAccess.removeAt(found);
          developer.log("SHA revoked ? ${sha.id}");
          await (database.sharedStockAccessTable.delete()
                ..where((tbl) => tbl.id.equals(sha.id)))
              .go();
        }
        continue;
      }
      if (found == -1) {
        sharedStockAccess.add(valid);
      } else {
        await (database.update(database.sharedStockAccessTable)
              ..where((tbl) => tbl.id.equals(sha.id)))
            .write(valid.toDB());
        sharedStockAccess[found] = valid;
      }
    }

    if (doRemote && settingsProvider.hasCloud()) {
      var sentrySpan = Sentry.startTransaction(
          "loadSharedStockAccess()", "loadSharedStockAccess");
      try {
        var uri =
            Uri.parse("${constants.baseUrl}/rest/shared-stock/list/access");
        var res = await http.get(uri,
            headers: cloudHeaders(headers: {}, span: sentrySpan));
        if (res.statusCode == 200) {
          var maps = jsonDecode(res.body);
          for (var map in maps) {
            var sha = SharedStockAccess.fromRest(map["sharedStock"]);
            var found =
                sharedStockAccess.indexWhere((element) => element.id == sha.id);
            if (found == -1) {
              sharedStockAccess.add(sha);
            } else {
              sharedStockAccess[found] = sha;
              await (database.update(database.sharedStockAccessTable)
                    ..where((tbl) => tbl.id.equals(sha.id)))
                  .write(sha.toDB());
            }
          }
        }
      } catch (e, s) {
        developer.log("Error when loading SHA from cloud $e");
        Sentry.captureException(e, stackTrace: s, withScope: (scope) {
          scope.span = sentrySpan;
        });
      } finally {
        sentrySpan.finish();
      }
    }
    if (doNotifyListeners) {
      notifyListeners();
    }
    return sharedStockAccess;
  }

  Future<SharedStockAccess?> validateSharingStockAccess(
      int id, String securityToken) async {
    SharedStockAccess? ok;
    var sentrySpan = Sentry.startTransaction(
        "validateSharingStockAccess()", "validateSharingStockAccess");
    try {
      var uri =
          Uri.parse("${constants.baseUrl}/rest/shared-stock/validate/access");
      var res = await http.post(uri,
          body: {"id": id.toString(), "securityToken": securityToken},
          headers: cloudHeaders(headers: {}, span: sentrySpan));
      if (res.statusCode == 200) {
        var map = jsonDecode(res.body);
        if (map["ok"] == true) {
          ok = SharedStockAccess.fromRest(map["sharedstock"]);
        }
      }
    } catch (e, s) {
      developer.log("Error when validate SHA from cloud $e");
      Sentry.captureException(e, stackTrace: s, withScope: (scope) {
        scope.span = sentrySpan;
      });
    } finally {
      sentrySpan.finish();
    }
    return ok;
  }

  Future<SharedStock?> createSharingStock(String name, bool readOnly) async {
    var sentrySpan =
        Sentry.startTransaction("createSharingStock()", "createSharingStock");
    SharedStock? result;
    try {
      var uri = Uri.parse("${constants.baseUrl}/rest/shared-stock/generate");
      var res = await http.put(uri,
          body: jsonEncode({"toName": name, "readonly": readOnly}),
          headers: cloudHeaders(
              headers: {"Content-Type": "application/json"}, span: sentrySpan));
      var map = jsonDecode(res.body);
      var ss = SharedStock.fromRest(map);
      sharedStocks.add(ss);
      result = ss;
    } catch (e, s) {
      developer.log("Failed to create sharing stock $e}");
      Sentry.captureException(e, stackTrace: s, withScope: (scope) {
        scope.span = sentrySpan;
      });
    } finally {
      sentrySpan.finish();
    }

    return result;
  }

  Future<List<SharedStock>> loadSharedStock() async {
    sharedStocks.clear();
    var sentrySpan =
        Sentry.startTransaction("loadSharedStock()", "loadSharedStock");
    try {
      var uri = Uri.parse("${constants.baseUrl}/rest/shared-stock/list/mines");
      var res = await http.get(uri,
          headers: cloudHeaders(headers: {}, span: sentrySpan));
      if (res.statusCode == 200) {
        var maps = jsonDecode(res.body);
        for (var map in maps) {
          var sha = SharedStock.fromRest(map);
          var found = sharedStocks.where((element) => element.id == sha.id);
          if (found.isEmpty) {
            sharedStocks.add(sha);
          }
        }
      }
    } catch (e, s) {
      developer.log("Error when loading SHA from cloud $e");
      Sentry.captureException(e, stackTrace: s, withScope: (scope) {
        scope.span = sentrySpan;
      });
    } finally {
      sentrySpan.finish();
    }

    //developer.log("ok");
    return sharedStocks;
  }

  Future<bool> revokeSharingStockAccess(int id, String securityToken) async {
    var sentrySpan = Sentry.startTransaction(
        "revokeSharingStockAccess()", "revokeSharingStockAccess");
    bool ok = false;
    try {
      var uri = Uri.parse("${constants.baseUrl}/rest/shared-stock/revoke");
      var res = await http.post(uri,
          body: {"id": id.toString(), "securityToken": securityToken},
          headers: cloudHeaders(headers: {}, span: sentrySpan));
      if (res.statusCode == 200) {
        var map = jsonDecode(res.body);
        if (map["ok"] == true) {
          await (database.delete(database.sharedHistoryAccessTable)
                ..where((tbl) => tbl.id.equals(id)))
              .go();
          sharedStocks.removeWhere((element) => element.id == id);
          sharedStockAccess.removeWhere((element) => element.id == id);
          ok = true;
        }
      }
    } catch (e, s) {
      developer.log("Error when validate SHA from cloud $e");
      Sentry.captureException(e, stackTrace: s, withScope: (scope) {
        scope.span = sentrySpan;
      });
    } finally {
      sentrySpan.finish();
    }
    return ok;
  }

  Future<List<DiaperStock>> getSharedStock(SharedStockAccess sha) async {
    List<DiaperStock> stocks = [];
    var sentrySpan =
        Sentry.startTransaction("getSharedStock()", "getSharedStock");
    try {
      var uri =
          Uri.parse("${constants.baseUrl}/rest/shared-stock/stock/lookup");
      var res = await http.post(uri,
          body: {"id": sha.id.toString(), "securityToken": sha.securityToken},
          headers: cloudHeaders(headers: {}, span: sentrySpan));
      if (res.statusCode == 200) {
        var maps = jsonDecode(res.body) as Map<String, dynamic>;
        if (maps.containsKey("user")) {
          for (var map in maps["user"]["stocks"]) {
            var types = diaperTypes.where((t) => t.id == map["diaperTypeId"]);
            if (types.isEmpty) continue;
            var type = types.first;
            var stock = DiaperStock(
              type: type,
              count: Map<String, int>.from(map["counts"]),
              prices: Map<String, num>.from(map["prices"])
                  .map((key, value) => MapEntry(key, value.toDouble())),
            );
            stocks.add(stock);
          }
        }
      }
    } catch (e, s) {
      developer.log("Error when loading SHA changes from cloud $e");
      Sentry.captureException(e, stackTrace: s, withScope: (scope) {
        scope.span = sentrySpan;
      });
    } finally {
      sentrySpan.finish();
    }

    return stocks;
  }

  Future<String?> linkSharingStock(
      int id, String securityToken, String fromName) async {
    var sentrySpan =
        Sentry.startTransaction("linkSharingStock()", "linkSharingStock");
    String? error = "error";
    try {
      var uri = Uri.parse("${constants.baseUrl}/rest/shared-stock/link/access");
      var res = await http.post(uri,
          body: {
            "id": id.toString(),
            "securityToken": securityToken,
            "fromName": fromName
          },
          headers: cloudHeaders(headers: {}, span: sentrySpan));
      if (res.statusCode == 200) {
        var map = jsonDecode(res.body) as Map<String, dynamic>;
        if (map["ok"] == true) {
          var shId = map["sh_id"] as int;
          var readonly = map["readonly"] ?? true;
          var ssa = SharedStockAccess(
              id: shId,
              securityToken: securityToken,
              fromName: fromName,
              readOnly: readonly);

          await database
              .into(database.sharedStockAccessTable)
              .insert(ssa.toDB());

          sharedStockAccess.add(ssa);
          error = null;
        } else {}
      }
    } catch (e, s) {
      developer.log("Error when link SSA from cloud $e");
      Sentry.captureException(e, stackTrace: s, withScope: (scope) {
        scope.span = sentrySpan;
      });
    } finally {
      sentrySpan.finish();
    }
    return error;
  }

  Future<void> updateSharingStock(SharedStock data) async {
    var found = sharedStocks.isEmpty
        ? -1
        : sharedStocks.indexWhere((s) => s.id == data.id);

    var span =
        Sentry.startTransaction("updateSharingStock", "updateSharingStock");

    try {
      var uri = Uri.parse("${constants.baseUrl}/rest/shared-stock/update");
      var res = await http.post(uri,
          body: jsonEncode(data.toRest()),
          headers: cloudHeaders(
              headers: {'Content-Type': 'application/json'}, span: span));
      if (res.statusCode == 200) {
        var map = jsonDecode(res.body) as Map<String, dynamic>;
        var sharedStock = SharedStock.fromRest(map);
        if (found != -1) {
          sharedStocks[found] = sharedStock;
        }
      }
    } catch (e, s) {
      developer.log("Failed when update SharingStock", error: e);
      Sentry.captureException(e, stackTrace: s, withScope: (scope) {
        scope.span = span;
      });
    } finally {
      span.finish();
    }

    notifyListeners();
  }

  Future<bool> useSharingStockRemote(SharedStockAccess ssa, DiaperStock stock,
      String size, ISentrySpan? parentSpan) async {
    bool ok = false;
    ISentrySpan spanUseStock;
    if (parentSpan != null) {
      spanUseStock = parentSpan.startChild("useSharingStockRemote");
    } else {
      spanUseStock = Sentry.startTransaction(
          "useSharingStockRemote", "useSharingStockRemote");
    }

    try {
      var uri = Uri.parse("${constants.baseUrl}/rest/shared-stock/stock/use");
      var res = await http.post(uri,
          body: jsonEncode({
            "id": ssa.id,
            "securityToken": ssa.securityToken,
            "type_id": stock.type.id,
            "size": size
          }),
          headers: cloudHeaders(
              headers: {'Content-Type': 'application/json'},
              span: spanUseStock));

      if (res.statusCode == 200) {
        var map = jsonDecode(res.body);
        if (map["ok"] == true) {
          stock.count = Map<String, int>.from(map["stock"]["counts"]);
          ok = true;
        }
      }
    } catch (e, s) {
      developer.log("Failed to use shared stock", error: e);
      Sentry.captureException(e, stackTrace: s, withScope: (scope) {
        scope.span = spanUseStock;
      });
    } finally {
      spanUseStock.finish();
    }
    return ok;
  }

  Future<Change?> useSharingStock(SharedStockAccess ssa, DiaperStock stock,
      String size, double? price, {required bool doNotifyListeners}) async {
    var span = Sentry.startTransaction("useSharingStock", "useSharingStock");

    var ok = await useSharingStockRemote(ssa, stock, size, span);

    if (!ok) {
      return null;
    }

    var changeTime = DateTime.now();
    var changeEdited = <Change>[];

    changes.where((change) => change.endTime == null).forEach((change) {
      change.endTime = changeTime;
      change.updatedAt = DateTime.now();
      changeEdited.add(change);
    });

    var change = Change(startTime: changeTime);
    change.addDiaper(stock.type, size, price);
    changes.add(change);
    changes.sort((a, b) => b.startTime.compareTo(a.startTime));
    changeEdited.add(change);
    await saveSpecificChanges(specificChanges: changeEdited, doNotifyListeners: false);

    if(doNotifyListeners){
      notifyListeners();
    }
    return change;
  }
}
