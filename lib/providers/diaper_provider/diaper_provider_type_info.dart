// ignore_for_file: invalid_use_of_visible_for_testing_member, invalid_use_of_protected_member

import 'dart:convert';
import 'dart:developer' as developer;

import 'package:diap_stash/object/interfaces.dart';
import 'package:diap_stash/providers/diaper_provider.dart';
import 'package:drift/drift.dart';
import 'package:logging/logging.dart';
import 'package:sentry_flutter/sentry_flutter.dart';
import 'package:http/http.dart' as http;
import 'package:diap_stash/constants.dart' as constants;

extension DiaperProviderTypeInfo on DiaperProvider {
  loadDiaperTypeInfo({bool doRemote = true, required bool doNotifyListeners}) async {
    var raw = await database.typeInfoTable.select().get();

    for (var r in raw) {
      var typeId = r.typeId;
      var found = diaperTypes.where((element) => element.id == typeId);
      if (found.isEmpty) continue;
      var type = found.first;

      var info = TypeInfo(
          type: type, thresholdLowStock: r.thresholdLowStock, note: r.note);
      typesInfo.add(info);
    }
    if (doRemote) {
      await loadDiaperTypeInfoRemote(doNotifyListeners: false);
    }

    if (doNotifyListeners) {
      notifyListeners();
    }
  }

  loadDiaperTypeInfoRemote({required bool doNotifyListeners}) async {
    if (!settingsProvider.hasCloud()) return;

    var uriListType =
        Uri.parse("${constants.baseUrl}/rest/user-diaper-type-info");
    var sentrySpan = Sentry.startTransaction(
        "loadDiaperTypeInfoRemote()", "loadDiaperTypeInfoRemote");
    try {
      var res = await http.get(uriListType,
          headers: cloudHeaders(headers: {}, span: sentrySpan));
      if (res.statusCode == 200) {
        var rawTypes = jsonDecode(res.body) as List<dynamic>;
        for (var map in rawTypes) {
          var typeId = map['typeId'];
          var found = diaperTypes.where((element) => element.id == typeId);
          if (found.isEmpty) continue;
          var type = found.first;

          var info = TypeInfo(
              type: type,
              thresholdLowStock: map["thresholdLowStock"],
              note: map["note"]);

          var index =
              typesInfo.indexWhere((element) => element.type.id == typeId);
          if (index == -1) {
            typesInfo.add(info);
          } else {
            typesInfo[index] = info;
          }
        }
      }
    } catch (err, s) {
      Sentry.captureException(err, stackTrace: s, withScope: (scope) {
        scope.span = sentrySpan;
      });
      developer.log("Fail when load type info",
          level: Level.SEVERE.value, error: err);
    } finally {
      sentrySpan.finish();
    }
    if (doNotifyListeners) {
      notifyListeners();
    }
  }

  saveDiaperTypeInfo({bool doRemote = true, required bool doNotifyListeners}) async {
    for (var info in typesInfo) {
      await database
          .into(database.typeInfoTable)
          .insert(info.toDB(), mode: InsertMode.insertOrReplace);
    }

    if (doRemote) {
      await saveTypeInfosRemote(doNotifyListeners: doNotifyListeners);
    }
  }

  saveTypeInfosRemote({required bool doNotifyListeners}) async {
    if (!settingsProvider.hasCloud()) return;

    var uri = Uri.parse("${constants.baseUrl}/rest/user-diaper-type-info/many");
    var sentrySpan =
        Sentry.startTransaction("saveTypeInfosRemote()", "saveTypeInfosRemote");
    try {
      var res = await http.post(uri,
          headers: cloudHeaders(
              headers: {"Content-Type": "application/json"}, span: sentrySpan),
          body: jsonEncode(typesInfo.map((e) => e.toRest()).toList()));
      if (res.statusCode == 200) {}
    } catch (err, s) {
      Sentry.captureException(err, stackTrace: s, withScope: (scope) {
        scope.span = sentrySpan;
      });
      developer.log("Fail when save multi type info",
          level: Level.SEVERE.value, error: err);
    } finally {
      sentrySpan.finish();
    }
  }

  saveTypeInfoRemote(TypeInfo info) async {
    if (!settingsProvider.hasCloud()) return;

    var uri = Uri.parse("${constants.baseUrl}/rest/user-diaper-type-info");
    var sentrySpan =
        Sentry.startTransaction("saveTypeInfoRemote()", "saveTypeInfoRemote");
    try {
      var body = jsonEncode(info.toRest());
      var res = await http.post(uri,
          headers: cloudHeaders(
              headers: {"Content-Type": "application/json"}, span: sentrySpan),
          body: body);
      if (res.statusCode == 200) {}
    } catch (err, s) {
      Sentry.captureException(err, stackTrace: s, withScope: (scope) {
        scope.span = sentrySpan;
      });
      developer.log("Fail when save type info",
          level: Level.SEVERE.value, error: err);
    } finally {
      sentrySpan.finish();
    }
  }

  TypeInfo getTypeInfo(DiaperType type) {
    return typesInfo.firstWhere((element) => element.type == type,
        orElse: () => TypeInfo(type: type));
  }

  saveTypeInfo(TypeInfo info, {bool doRemote = true}) async {
    var index = typesInfo.indexWhere((element) => element.type == info.type);
    if (index == -1) {
      typesInfo.add(info);
    } else {
      typesInfo[index] = info;
    }

    await database
        .into(database.typeInfoTable)
        .insert(info.toDB(), mode: InsertMode.insertOrReplace);

    if (doRemote) {
      await saveTypeInfoRemote(info);
    }
  }

  deleteTypeInfo(TypeInfo info, {bool doRemote = true}) async {
    var indexTypeInfo =
        typesInfo.indexWhere((element) => element.type == info.type);
    if (indexTypeInfo >= 0) {
      await (database.delete(database.typeInfoTable)
            ..where((tbl) => tbl.typeId.equals(info.type.id)))
          .go();
      typesInfo.removeAt(indexTypeInfo);
    }
  }

  deleteTypeInfoRemote(TypeInfo info) async {
    if (!settingsProvider.hasCloud()) return;

    var uri = Uri.parse("${constants.baseUrl}/rest/user-diaper-type-info");
    var sentrySpan = Sentry.startTransaction(
        "deleteTypeInfoRemote()", "deleteTypeInfoRemote");
    try {
      var body = jsonEncode(info.toRest());
      var res = await http.delete(uri,
          headers: cloudHeaders(
              headers: {"Content-Type": "application/json"}, span: sentrySpan),
          body: body);
      if (res.statusCode == 200) {}
    } catch (err, s) {
      Sentry.captureException(err, stackTrace: s, withScope: (scope) {
        scope.span = sentrySpan;
      });
      developer.log("Fail when delete type info",
          level: Level.SEVERE.value, error: err);
    } finally {
      sentrySpan.finish();
    }
  }

  bool needToBeSaved(TypeInfo info) {
    if (typesInfo.where((element) => element.type == info.type).isNotEmpty) {
      return true;
    }
    return info.thresholdLowStock != null || info.note != null;
  }
}
