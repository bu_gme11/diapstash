// ignore_for_file: invalid_use_of_visible_for_testing_member, invalid_use_of_protected_member

import 'dart:convert';
import 'dart:developer' as developer;

import 'package:diap_stash/object/interfaces.dart';
import 'package:diap_stash/providers/diaper_provider.dart';
import 'package:diap_stash/constants.dart' as constants;
import 'package:drift/drift.dart';
import 'package:http/http.dart' as http;
import 'package:logging/logging.dart';
import 'package:sentry_flutter/sentry_flutter.dart';

extension DiaperProviderType on DiaperProvider {
  loadBrands({bool doRemote = true}) async {
    var maps = await database.select(database.diaperBrandTable).get();

    for (var map in maps) {
      var brand = DiaperBrand.fromDB(map);
      if (!brands.contains(brand)) {
        brands.add(brand);
      }
    }

    if (doRemote) {
      await loadBrandRemote();
    }
    developer.log("Load ${brands.length} brands");
  }

  Future<void> loadBrandRemote() async {
    var uri = Uri.parse("${constants.baseUrl}/rest/diaper-brand");
    var span = Sentry.startTransaction("loadBrandRemote", "loadBrandRemote");
    try {
      var res = await http.get(uri, headers: cloudHeaders(headers: {}, span: span));
      if (res.statusCode == 200) {
        var rawBrands = jsonDecode(res.body) as List<dynamic>;
        for (var raw in rawBrands) {
          var brand = DiaperBrand.fromMap(raw);
          var index = brands.indexOf(brand);
          if (index == -1) {
            brands.add(brand);
          } else {
            brands[index] = brand;
          }
          await database
              .into(database.diaperBrandTable)
              .insert(brand.toDB(), mode: InsertMode.insertOrReplace);
        }
      }
    } catch (err, s) {
      Sentry.captureException(err, stackTrace: s, withScope: (scope){
        scope.span = span;
      });
      developer.log("Fail when cloud load brand",
          level: Level.SEVERE.value, error: err);
    }finally{
      span.finish();
    }
  }

  loadTypes({bool doRemote = true, required bool doNotifyListeners}) async {
    try {
      var maps = await database.select(database.diaperTypeTable).get();
      for (var map in maps) {
        try {
          final type = DiaperType.fromDB(map);
          if (type.brand == null && type.brandCode != null) {
            var brandFound =
                brands.where((element) => element.code == type.brandCode);
            if (brandFound.isNotEmpty) {
              type.brand = brandFound.first;
            }
          }
          if (!diaperTypes.contains(type)) {
            diaperTypes.add(type);
          } else {
            var index = diaperTypes.indexOf(type);
            diaperTypes[index] = type;
          }
        } catch (err, s) {
          Sentry.captureException(err, stackTrace: s);
          developer.log("Fail to load type",
              level: Level.SEVERE.value, error: err);
        }
      }
    } catch (err, s) {
      Sentry.captureException(err, stackTrace: s);
      developer.log("Fail to load type", level: Level.SEVERE.value, error: err);
    }

    if (doRemote) {
      await loadTypesRemote(doNotifyListeners: false);
    }

    if(doNotifyListeners) {
      notifyListeners();
    }
  }

  Future<void> loadTypesRemote({required bool doNotifyListeners}) async {

    List<Map<String, dynamic>> replacedBy = [];

    var uriListType = Uri.parse("${constants.baseUrl}/rest/diaper-type");
    var sentrySpan =
        Sentry.startTransaction("loadTypesRemote()", "loadTypesRemote");
    try {
      var res = await http.get(uriListType,
          headers: cloudHeaders(headers: {}, span: sentrySpan));
      if (res.statusCode == 200) {
        var rawTypes = jsonDecode(res.body) as List<dynamic>;
        for (var raw in rawTypes) {
          Map<String, dynamic> map = raw;

          if(map.containsKey("replacedById") && map["replacedById"] != null){
            replacedBy.add(map);
            int id = map["id"];
            await (database.delete(database.diaperTypeTable)
              ..where((tbl) => tbl.id.equals(id))).go();
            continue;
          }


          final type = DiaperType.fromMap(map);
          var brandFound =
              brands.where((element) => element.code == type.brandCode);
          if (brandFound.isNotEmpty) {
            type.brand = brandFound.first;
          }
          if (!diaperTypes.contains(type)) {
            diaperTypes.add(type);
            await database
                .into(database.diaperTypeTable)
                .insert(type.toDB(), mode: InsertMode.insertOrReplace);
          } else {
            var index = diaperTypes.indexOf(type);
            diaperTypes[index] = type;
            await (database.update(database.diaperTypeTable)
                  ..where((tbl) => tbl.id.equals(type.id)))
                .write(type.toDB());
          }
        }
      }
    } catch (err, s) {
      Sentry.captureException(err, stackTrace: s);
      developer.log("Fail when cloud load type",
          level: Level.SEVERE.value, error: err);
    } finally {
      sentrySpan.finish();
    }

    try {
      if (replacedBy.isNotEmpty) {
        for (var map in replacedBy) {
          var type = DiaperType.fromMap(map);
          if (diaperTypes.contains(type)) {
            int replacedTypeId = map["replacedById"];
            var found = diaperTypes.where((element) =>
            replacedTypeId == element.id);
            if (found.isNotEmpty) {
              var replacedType = found.first;
              transfertTypeToNewType(type, replacedType, doNotifyListeners: false);
            }
            diaperTypes.remove(type);
          }
        }
      }
    }catch(e, s){
      Sentry.captureException(e, stackTrace: s);
      developer.log("Error when replace type", error: e);
    }

    if(doNotifyListeners){
      notifyListeners();
    }

  }

  saveTypes() async {
    developer.log("Save types");
    for (var type in diaperTypes.toList()) {
      if (type.id == -999) {
        //skip unkown type
        continue;
      }
      await (database.update(database.diaperTypeTable)
            ..where((tbl) => tbl.id.equals(type.id)))
          .write(type.toDB());
    }
  }

  Future<DiaperType?> createType(DiaperType type) async {
    if (settingsProvider.hasCloud()) {
      var nType = await createTypeToCloud(type);
      if(nType != null) {
        await loadTypes(doNotifyListeners: true);
      }
      return nType;
    } else {
      var row = await database
          .customSelect(
              "select min(id) - 1 as n_id from ${database.diaperTypeTable.tableName}")
          .getSingle();
      var nId = (row.data["n_id"] as int?) ?? 0;
      if (nId > -10) {
        nId = -10;
      }
      type.id = nId;
      await database
          .into(database.diaperTypeTable)
          .insert(type.toDB(), mode: InsertMode.insertOrReplace);
      await loadTypes(doNotifyListeners: true);
      return type;
    }
  }

  Future<DiaperType?> createTypeToCloud(DiaperType type) async {
    var uri = Uri.parse("${constants.baseUrl}/rest/diaper-type/user");
    var sentrySpan =
        Sentry.startTransaction("createTypeToCloud()", "createTypeToCloud");
    DiaperType? returnType;
    try {
      var res = await http.put(uri,
          headers: cloudHeaders(
              headers: {"Content-Type": "application/json"}, span: sentrySpan),
          body: jsonEncode(type.toMap()));

      if (res.statusCode == 200) {
        var map = jsonDecode(res.body);
        type = DiaperType.fromMap(map);
        var brandFound =
            brands.where((element) => element.code == map["brand_code"]);
        if (brandFound.isNotEmpty) {
          type.brand = brandFound.first;
        }
        /*diaperTypes.add(type);
      await saveTypes();*/
        returnType = type;
      }
    } catch (err, s) {
      Sentry.captureException(err, stackTrace: s);
      developer.log("Fail when cloud create diaper",
          level: Level.SEVERE.value, error: err);
    } finally {
      sentrySpan.finish();
    }
    return returnType;
  }

  updateType(DiaperType type) async {
    if (settingsProvider.hasCloud()) {
      var sentrySpan = Sentry.startTransaction("updateType()", "updateType");
      var uri = Uri.parse("${constants.baseUrl}/rest/diaper-type/user");
      try {
        var res = await http.post(uri,
            headers: cloudHeaders(
                headers: {"Content-Type": "application/json"},
                span: sentrySpan),
            body: jsonEncode(type.toMap()));

        if (res.statusCode == 200) {
          var map = jsonDecode(res.body);
          var typeBD = DiaperType.fromMap(map);
          var brandFound =
              brands.where((element) => element.code == map["brand_code"]);
          if (brandFound.isNotEmpty) {
            typeBD.brand = brandFound.first;
          }
          await (database.update(database.diaperTypeTable)
                ..where((tbl) => tbl.id.equals(typeBD.id)))
              .write(typeBD.toDB());
          await loadTypes(doNotifyListeners: true);
        }
      } catch (err, s) {
        Sentry.captureException(err, stackTrace: s, withScope: (scope) {
          scope.span = sentrySpan;
        });
        developer.log("Fail when cloud save changes",
            level: Level.SEVERE.value, error: err);
      } finally {
        sentrySpan.finish();
      }
    }else{
      await (database.update(database.diaperTypeTable)
        ..where((tbl) => tbl.id.equals(type.id)))
          .write(type.toDB());
      await loadTypes(doNotifyListeners: true);
    }
  }

  deleteCustomType(DiaperType type, {bool doRemote = true, required bool doNotifyListeners}) async {
    diaperTypes.remove(type);
    await (database.delete(database.diaperTypeTable)
          ..where((tbl) => tbl.id.equals(type.id)))
        .go();
    await (database.delete(database.diaperTypeProposalTable)
          ..where((tbl) => tbl.type_id.equals(type.id)))
        .go();
    await (database.delete(database.diaperChangeTable)
          ..where((tbl) => tbl.type_id.equals(type.id)))
        .go();
    if (settingsProvider.hasCloud() && doRemote) {
      var uri = Uri.parse("${constants.baseUrl}/rest/diaper-type/user");
      var sentrySpan =
          Sentry.startTransaction("deleteCustomType()", "deleteCustomType");
      try {
        await http.delete(uri,
            headers: cloudHeaders(
                headers: {"Content-Type": "application/json"},
                span: sentrySpan),
            body: jsonEncode(type.toMap()));

        await loadTypes(doNotifyListeners: false);
      } catch (err, s) {
        Sentry.captureException(err, stackTrace: s, withScope: (scope) {
          scope.span = sentrySpan;
        });
        developer.log("Fail when cloud save changes",
            level: Level.SEVERE.value, error: err);
      } finally {
        sentrySpan.finish();
      }
    }
    await loadChange(doNotifyListeners: false);
    if (settingsProvider.hasCloud() && doRemote) {
      await saveChangesRemote();
    }
    if (doNotifyListeners) {
      notifyListeners();
    }
  }

  sendCustomTypeToCloud({required bool doNotifyListeners}) async {
    if (settingsProvider.hasCloud()) {
      var typesToTransfert = diaperTypes
          .where((element) => !element.official && element.id < 0)
          .toList();

      for (var type in typesToTransfert) {
        var newType = await createTypeToCloud(type);
        if (newType != null) {
          await transfertTypeToNewType(type, newType, doNotifyListeners: false);
        }
        await removeType(type, doNotifyListeners: doNotifyListeners);
      }
    }
    if(doNotifyListeners){
      notifyListeners();
    }
  }

  transfertCloudCustomTypeToLocal({required bool doNotifyListeners}) async {
    var customsTypes = diaperTypes
        .where((element) => element.official == false && element.id > 0)
        .toList(growable: false);

    for (var custom in customsTypes) {
      var type = DiaperType.fromMap(custom.toMap());
      var nType = await createType(type);
      if (nType != null) {
        await transfertTypeToNewType(custom, nType, doNotifyListeners: false, doRemote: false);
        await removeType(custom, doNotifyListeners: false);
      } else {
        developer.log("Error when create type when transfering cloud to local");
        Sentry.captureMessage(
            "Error when create type when transfering cloud to local",
            level: SentryLevel.debug);
      }
    }
    if(doNotifyListeners){
      notifyListeners();
    }
  }

  Future<void> removeType(DiaperType type, {required bool doNotifyListeners}) async {
    diaperTypes.remove(type);
    await (database.delete(database.diaperTypeTable)
          ..where((tbl) => tbl.id.equals(type.id)))
        .go();

    if(doNotifyListeners){
      notifyListeners();
    }
  }

  Future<void> transfertTypeToNewType(
      DiaperType type, DiaperType newType, {required bool doNotifyListeners, bool doRemote = true}) async {
    if (!diaperTypes.contains(newType)) {
      diaperTypes.add(newType);
    }

    var indexStock =
        currentStock.indexWhere((element) => element.type.id == type.id);
    if (indexStock >= 0) {
      var stock = currentStock[indexStock];
      await removeStock(stock, doNotifyListeners: false, doRemote: doRemote);
      for (var count in stock.count.entries) {
        if (count.value > 0) {
          addStock(newType, count.key, count.value, doNotifyListeners: false, doRemote: doRemote);
        }
      }
    }
    //await saveStocks();

    var changesWithType =
        changes.where((element) => element.diapers.any((d) => d.type == type));
    var changedChanges = <Change>[];
    for (var change in changesWithType) {
      for (var i = 0; i < change.diapers.length; i++) {
        var diaper = change.diapers[i];
        if (diaper.type == type) {
          var dc =
              DiaperChange(type: newType, size: diaper.size, change: change);
          change.diapers[i] = dc;
          changedChanges.add(change);
          await (database.delete(database.diaperChangeTable)
                ..where((tbl) =>
                    tbl.type_id.equals(type.id) &
                    tbl.change_id.equals(change.id!)))
              .go();
        }
      }
    }
    await saveSpecificChanges(specificChanges: changedChanges, doNotifyListeners: false, doRemote: doRemote);

    var indexTypeInfo = typesInfo.indexWhere((element) => element.type == type);
    if (indexTypeInfo >= 0) {
      var info = typesInfo[indexTypeInfo];
      await deleteTypeInfo(info);
      var newTypeInfo = TypeInfo(
          type: newType,
          thresholdLowStock: info.thresholdLowStock,
          note: info.note);
      await saveTypeInfo(newTypeInfo, doRemote: doRemote);
    }

    if (isFavoriteType(type)) {
      await addToFavorite(newType, doNotifyListeners: false, doRemote: doRemote);
      await removeFromFavorite(type, doNotifyListeners: false, doRemote: doRemote);
    }

    if(doNotifyListeners){
      notifyListeners();
    }
  }

  DiaperType? getTypeById(int id){
    var found = diaperTypes.where((element) => element.id == id);
    if(found.isNotEmpty){
      return found.first;
    }
    return null;
  }

  Future<void> checkDeletedDiaperType({required bool doNotifyListeners}) async {
    if(settingsProvider.hasCloud()){
      var uri = Uri.parse("${constants.baseUrl}/rest/diaper-type/user/deleted");
      var sentrySpan =
      Sentry.startTransaction("checkDeletedDiaperType()", "checkDeletedDiaperType");

      try{
        var res = await http.get(uri, headers: cloudHeaders(headers: {}, span: sentrySpan));
        if(res.statusCode == 200){
          var json = jsonDecode(res.body);
          List<Map<String, dynamic>> lst = List.from(json);
          for(var item in lst){
            var typeId = item["diaperTypeId"] as int;
            var found = diaperTypes.where((element) => element.id == typeId);
            if(found.isNotEmpty){
              await deleteCustomType(found.first, doNotifyListeners: false);
            }
          }
        }
      }catch(e, s){
        Sentry.captureException(e, stackTrace: s, withScope: (scope) {
          scope.span = sentrySpan;
        });
        developer.log("Error when replace type", error: e);
      }finally{
        sentrySpan.finish();
      }
      if(doNotifyListeners){
        notifyListeners();
      }
    }
  }

}
