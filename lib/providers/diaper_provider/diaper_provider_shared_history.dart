// ignore_for_file: invalid_use_of_visible_for_testing_member, invalid_use_of_protected_member

import 'dart:convert';
import 'dart:developer' as developer;

import 'package:diap_stash/object/interfaces.dart';
import 'package:diap_stash/providers/diaper_provider.dart';
import 'package:diap_stash/constants.dart' as constants;
import 'package:drift/drift.dart';
import 'package:http/http.dart' as http;
import 'package:sentry_flutter/sentry_flutter.dart';

extension DiaperProviderSharedHistory on DiaperProvider {
  Future<List<SharedHistory>> loadSharedHistory() async {
    sharedHistories.clear();
    var sentrySpan =
        Sentry.startTransaction("loadSharedHistory()", "loadSharedHistory");
    try {
      var uri =
          Uri.parse("${constants.baseUrl}/rest/shared-history/list/mines");
      var res = await http.get(uri,
          headers: cloudHeaders(headers: {}, span: sentrySpan));
      if (res.statusCode == 200) {
        var maps = jsonDecode(res.body);
        for (var map in maps) {
          var sha = SharedHistory.fromRest(map);
          var found = sharedHistories.where((element) => element.id == sha.id);
          if (found.isEmpty) {
            sharedHistories.add(sha);
          }
        }
      }
    } catch (e, s) {
      developer.log("Error when loading SHA from cloud $e");
      Sentry.captureException(e, stackTrace: s, withScope: (scope) {
        scope.span = sentrySpan;
      });
    } finally {
      sentrySpan.finish();
    }

    return sharedHistories;
  }

  Future<List<SharedHistoryAccess>> loadSharedHistoryAccess(
      {bool doRemote = true, required bool doNotifyListeners}) async {
    var fromDB = await database.sharedHistoryAccessTable.select().get();
    for (var sha in fromDB) {
      var found =
          sharedHistoriesAccess.where((element) => element.id == sha.id);
      if (found.isEmpty) {
        var valid =
            await validateSharingHistoryAccess(sha.id, sha.securityToken);
        if (valid) {
          sharedHistoriesAccess.add(SharedHistoryAccess.fromDb(sha));
        } else {
          developer.log("SHA revoked ? ${sha.id}");
          await (database.sharedHistoryAccessTable.delete()
                ..where((tbl) => tbl.id.equals(sha.id)))
              .go();
        }
      }
    }
    if (doRemote && settingsProvider.hasCloud()) {
      var sentrySpan = Sentry.startTransaction(
          "loadSharedHistoryAccess()", "loadSharedHistoryAccess");
      try {
        var uri =
            Uri.parse("${constants.baseUrl}/rest/shared-history/list/access");
        var res = await http.get(uri,
            headers: cloudHeaders(headers: {}, span: sentrySpan));
        if (res.statusCode == 200) {
          var maps = jsonDecode(res.body);
          for (var map in maps) {
            var sha = SharedHistoryAccess.fromRest(map["sharedHistory"]);
            var found =
                sharedHistoriesAccess.where((element) => element.id == sha.id);
            if (found.isEmpty) {
              sharedHistoriesAccess.add(sha);
            }
          }
        }
      } catch (e, s) {
        developer.log("Error when loading SHA from cloud $e");
        Sentry.captureException(e, stackTrace: s, withScope: (scope) {
          scope.span = sentrySpan;
        });
      } finally {
        sentrySpan.finish();
      }
    }
    if (doNotifyListeners) {
      notifyListeners();
    }
    return sharedHistoriesAccess;
  }

  Future<bool> validateSharingHistoryAccess(
      int id, String securityToken) async {
    bool ok = false;
    var sentrySpan = Sentry.startTransaction(
        "validateSharingHistoryAccess()", "validateSharingHistoryAccess");
    try {
      var uri =
          Uri.parse("${constants.baseUrl}/rest/shared-history/validate/access");
      var res = await http.post(uri,
          body: {"id": id.toString(), "securityToken": securityToken},
          headers: cloudHeaders(headers: {}, span: sentrySpan));
      if (res.statusCode == 200) {
        var map = jsonDecode(res.body);
        ok = map["ok"] == true;
      }
    } catch (e, s) {
      developer.log("Error when validate SHA from cloud $e");
      Sentry.captureException(e, stackTrace: s, withScope: (scope) {
        scope.span = sentrySpan;
      });
    } finally {
      sentrySpan.finish();
    }
    return ok;
  }

  Future<bool> revokeSharingHistoryAccess(int id, String securityToken) async {
    var sentrySpan = Sentry.startTransaction(
        "revokeSharingHistoryAccess()", "revokeSharingHistoryAccess");
    bool ok = false;
    try {
      var uri = Uri.parse("${constants.baseUrl}/rest/shared-history/revoke");
      var res = await http.post(uri,
          body: {"id": id.toString(), "securityToken": securityToken},
          headers: cloudHeaders(headers: {}, span: sentrySpan));
      if (res.statusCode == 200) {
        var map = jsonDecode(res.body);
        if (map["ok"] == true) {
          await (database.delete(database.sharedHistoryAccessTable)
                ..where((tbl) => tbl.id.equals(id)))
              .go();
          sharedHistories.removeWhere((element) => element.id == id);
          sharedHistoriesAccess.removeWhere((element) => element.id == id);
          ok = true;
        }
      }
    } catch (e, s) {
      developer.log("Error when validate SHA from cloud $e");
      Sentry.captureException(e, stackTrace: s, withScope: (scope) {
        scope.span = sentrySpan;
      });
    } finally {
      sentrySpan.finish();
    }
    return ok;
  }

  Future<String?> linkSharingHistory(
      int id, String securityToken, String fromName) async {
    String? error = "error";
    var sentrySpan =
        Sentry.startTransaction("linkSharingHistory()", "linkSharingHistory");
    try {
      var uri =
          Uri.parse("${constants.baseUrl}/rest/shared-history/link/access");
      var res = await http.post(uri,
          body: {
            "id": id.toString(),
            "securityToken": securityToken,
            "fromName": fromName
          },
          headers: cloudHeaders(headers: {}, span: sentrySpan));
      if (res.statusCode == 200) {
        var map = jsonDecode(res.body) as Map<String, dynamic>;
        if (map["ok"] == true) {
          var shId = map["sh_id"] as int;
          var sha = SharedHistoryAccess(shId, securityToken, fromName);

          await database
              .into(database.sharedHistoryAccessTable)
              .insert(sha.toDB());

          sharedHistoriesAccess.add(sha);
          error = null;
        } else {}
      }
    } catch (e, s) {
      developer.log("Error when link SHA from cloud $e");
      Sentry.captureException(e, stackTrace: s, withScope: (scope) {
        scope.span = sentrySpan;
      });
    } finally {
      sentrySpan.finish();
    }
    return error;
  }

  Future<SharedHistory?> createSharingHistory(String name) async {
    var sentrySpan = Sentry.startTransaction(
        "createSharingHistory()", "createSharingHistory");
    SharedHistory? result;
    try {
      var uri = Uri.parse("${constants.baseUrl}/rest/shared-history/generate");
      var res = await http.put(uri,
          body: {"toName": name},
          headers: cloudHeaders(headers: {}, span: sentrySpan));
      var map = jsonDecode(res.body);
      var sh = SharedHistory.fromRest(map);
      sharedHistories.add(sh);
      result = sh;
    } catch (e, s) {
      developer.log("Failed to create sharing history $e}");
      Sentry.captureException(e, stackTrace: s, withScope: (scope) {
        scope.span = sentrySpan;
      });
    } finally {
      sentrySpan.finish();
    }

    return result;
  }

  Future<List<Change>> getSharedHistory(SharedHistoryAccess sha) async {
    List<Change> changes = [];
    var sentrySpan =
        Sentry.startTransaction("getSharedHistory()", "getSharedHistory");
    try {
      var uri =
          Uri.parse("${constants.baseUrl}/rest/shared-history/history/lookup");
      var res = await http.post(uri,
          body: {"id": sha.id.toString(), "securityToken": sha.securityToken},
          headers: cloudHeaders(headers: {}, span: sentrySpan));
      if (res.statusCode == 200) {
        var maps = jsonDecode(res.body) as Map<String, dynamic>;
        if (maps.containsKey("user")) {
          for (var map in maps["user"]["changes"]) {
            var change = Change.fromCloud(map, this);
            changes.add(change);
          }
        }
      }
    } catch (e, s) {
      developer.log("Error when loading SHA changes from cloud $e");
      Sentry.captureException(e, stackTrace: s, withScope: (scope) {
        scope.span = sentrySpan;
      });
    } finally {
      sentrySpan.finish();
    }

    return changes;
  }
}
