// ignore_for_file: invalid_use_of_visible_for_testing_member, invalid_use_of_protected_member

import 'dart:convert';
import 'dart:developer' as developer;

import 'package:diap_stash/object/interfaces.dart';
import 'package:diap_stash/object/rating.dart';
import 'package:diap_stash/providers/diaper_provider.dart';
import 'package:diap_stash/constants.dart' as constants;
import 'package:drift/drift.dart';
import 'package:http/http.dart' as http;
import 'package:logging/logging.dart';
import 'package:sentry_flutter/sentry_flutter.dart';
import 'package:uuid/uuid.dart';

extension DiaperProviderRating on DiaperProvider {
  loadCommunityRating({required bool doNotifyListeners}) async {
    var uri = Uri.parse("${constants.baseUrl}/rest/rating/community");
    var span =
        Sentry.startTransaction("loadCommunityRating", "loadCommunityRating");
    try {
      var res =
          await http.get(uri, headers: cloudHeaders(headers: {}, span: span));
      if (res.statusCode == 200) {
        await _parseRatingCommunity(res, span);
      }
    } catch (err, s) {
      Sentry.captureException(err, stackTrace: s, withScope: (scope) {
        scope.span = span;
      });
      developer.log("Fail when to load community rating",
          level: Level.SEVERE.value, error: err);
    } finally {
      span.finish();
    }

    if (doNotifyListeners) {
      notifyListeners();
    }
  }

  Future<void> loadCommunityRatingForType(
      {required DiaperType type, required bool doNotifyListeners}) async {
    var uri =
        Uri.parse("${constants.baseUrl}/rest/rating/community/type/${type.id}");
    var span = Sentry.startTransaction(
        "loadCommunityRatingForType", "loadCommunityRatingForType");
    try {
      var res =
          await http.get(uri, headers: cloudHeaders(headers: {}, span: span));
      if (res.statusCode == 200) {
        await _parseRatingCommunity(res, span);
      }
    } catch (err, s) {
      Sentry.captureException(err, stackTrace: s, withScope: (scope) {
        scope.span = span;
      });
      developer.log("Fail when to load community rating for type $type",
          level: Level.SEVERE.value, error: err);
    } finally {
      span.finish();
    }

    if (doNotifyListeners) {
      notifyListeners();
    }
  }

  loadUserRating(
      {bool doRemote = true, required bool doNotifyListeners}) async {
    List<UserRating> loaded = [];
    var rows = await database.userRatingTable.select().get();
    for (var row in rows) {
      var type =
          diaperTypes.where((element) => element.id == row.typeId).firstOrNull;
      if (type == null) {
        continue;
      }
      var criteria = RatingCriteria.values.byName(row.criteria);
      var uR = UserRating(
          id: row.id,
          type: type,
          criteria: criteria,
          note: row.note,
          updatedAt: row.updatedAt);
      loaded.add(uR);
    }
    userRatings.clear();
    userRatings.addAll(loaded);
    if (doRemote) {
      await loadRemoteUserRating(doNotifyListeners: false);
    }
    if (doNotifyListeners) {
      notifyListeners();
    }
  }

  loadRemoteUserRating({required bool doNotifyListeners}) async {
    if(!settingsProvider.hasCloud()) return;

    var uri = Uri.parse("${constants.baseUrl}/rest/rating/user");
    var span = Sentry.startTransaction("loadUserRating", "loadUserRating");
    try {
      var res =
          await http.get(uri, headers: cloudHeaders(headers: {}, span: span));
      if (res.statusCode == 200) {
        await _parseRemoteUserRating(res);
      }
    } catch (err, s) {
      Sentry.captureException(err, stackTrace: s, withScope: (scope) {
        scope.span = span;
      });
      developer.log("Fail when to load community rating",
          level: Level.SEVERE.value, error: err);
    } finally {
      span.finish();
    }

    if (doNotifyListeners) {
      notifyListeners();
    }
  }

  _parseRatingCommunity(http.Response res, ISentrySpan span) async {
    var rawRating = jsonDecode(res.body) as List<dynamic>;
    for (var raw in rawRating) {
      try {
        var rawTypeId = (raw["type_id"] as int);
        var foundType =
        diaperTypes.where((element) => element.id == rawTypeId);
        if (foundType.isEmpty) {
          developer.log("load overall rating: no type id=$rawTypeId",
              level: Level.SEVERE.value);
          continue;
        }
        var type = foundType.first;
        var criteria = RatingCriteria.values.byName(raw["criteria"]);
        var rating = Rating(
            type: type,
            criteria: criteria,
            note: double.parse("${raw["note"]}"));
        var index = communityRatings.indexWhere(
                (element) =>
            element.type == type && element.criteria == criteria);
        if (index >= 0) {
          communityRatings[index] = rating;
        } else {
          communityRatings.add(rating);
        }
      }catch(e, s){
        developer.log("Error on parse community rating $raw", error: e);
        developer.log(e.toString());
        Sentry.captureException(e, stackTrace: s, withScope: (scope) {
           scope.extra["raw_data"] = raw;
           scope.span = span;
        });
        rethrow;
      }
    }
  }

  _parseRemoteUserRating(http.Response res) async {
    var rawRating = jsonDecode(res.body) as List<dynamic>;
    for (var raw in rawRating) {
      var foundType =
          diaperTypes.where((element) => element.id == (raw["type_id"] as int));
      if (foundType.isEmpty) {
        developer.log("load overall rating: no type id=${raw["type_id"]}",
            level: Level.SEVERE.value);
        continue;
      }
      var type = foundType.first;
      var criteria = RatingCriteria.values.byName(raw["criteria"]);
      var rating = UserRating(
          id: raw["id"],
          type: type,
          criteria: criteria,
          note: double.parse("${raw["note"]}"),
          updatedAt: DateTime.parse(raw["updatedAt"]));
      var index = userRatings.indexWhere(
          (element) => element.type == type && element.criteria == criteria);
      var dbData = rating.toDB();
      if (index >= 0) {
        var uR = userRatings[index];
        if (uR.updatedAt.isBefore(rating.updatedAt)) {
          userRatings[index] = rating;
          if (dbData != null) {
            await (database.update(database.userRatingTable)
                  ..where((tbl) => tbl.id.equals(dbData.id)))
                .write(dbData);
          }
        }
      } else {
        userRatings.add(rating);
        if (dbData != null) {
          await database
              .into(database.userRatingTable)
              .insert(dbData, mode: InsertMode.insertOrReplace);
        }
      }
    }
  }

  linkUserRatingToCloudSync({required bool doNotifyListeners}) async {
    var uri = Uri.parse("${constants.baseUrl}/rest/rating/link-cloud-sync");
    var span =
    Sentry.startTransaction("linkUserRatingToCloudSync", "linkUserRatingToCloudSync");
    try {

      var body = {
        "ids": userRatings.where((e) => e.id != null).map((e) => e.id).toList()
      };

      var res =
      await http.post(uri, body: jsonEncode(body), headers: cloudHeaders(headers: {
        'Content-Type': 'application/json'
      }, span: span));
      if (res.statusCode == 200) {
        userRatings.clear();
        await database.userRatingTable.deleteAll();
        await _parseRemoteUserRating(res);

      }
    } catch (err, s) {
      Sentry.captureException(err, stackTrace: s, withScope: (scope) {
        scope.span = span;
      });
      developer.log("Fail when to load community rating",
          level: Level.SEVERE.value, error: err);
    } finally {
      span.finish();
    }


    if(doNotifyListeners){
      notifyListeners();
    }
  }

  unlinkUserRatingToCloudSync({required bool recreateRating, required bool doNotifyListeners}) async {
    var uri = Uri.parse("${constants.baseUrl}/rest/rating/unlink-cloud-sync");
    var span =
    Sentry.startTransaction("unlinkUserRatingToCloudSync", "unlinkUserRatingToCloudSync");
    try {

      var body = {
        "recreateRating": recreateRating
      };

      var res =
      await http.post(uri, body: jsonEncode(body), headers: cloudHeaders(headers: {
        'Content-Type': 'application/json'
      }, span: span));
      if (res.statusCode == 200) {
        userRatings.clear();
        await database.userRatingTable.deleteAll();
        await _parseRemoteUserRating(res);
      }
    } catch (err, s) {
      Sentry.captureException(err, stackTrace: s, withScope: (scope) {
        scope.span = span;
      });
      developer.log("Fail when to load community rating",
          level: Level.SEVERE.value, error: err);
    } finally {
      span.finish();
    }

    if(doNotifyListeners){
      notifyListeners();
    }
  }

  Future<UserRating?> sendRating(
      {required DiaperType type,
      required RatingCriteria criteria,
      required double note,
      required bool doNotifyListeners,
      bool reloadCommunity = true}) async {
    var rating = userRatings.firstWhere(
        (element) => element.type == type && element.criteria == criteria,
        orElse: () => UserRating(
            id: null,
            type: type,
            criteria: criteria,
            note: note,
            updatedAt: DateTime.now()));
    rating = rating.copyWith(note: note, updatedAt: DateTime.now());

    if(!settingsProvider.hasCloud() && !settingsProvider.sendRating){
      if(rating.id == null){
        rating = rating.copyWith(id: const Uuid().v4());
      }
      await (database.into(database.userRatingTable).insertOnConflictUpdate(rating.toDB()!));

      var index = userRatings.indexWhere((element) => element.type == type && element.criteria == criteria);
      if(index == -1){
        userRatings.add(rating);
      }else{
        userRatings[index] = rating;
      }
      if(doNotifyListeners){
        notifyListeners();
      }
      return rating;
    }

    var uri = Uri.parse("${constants.baseUrl}/rest/rating/set");
    var span = Sentry.startTransaction("sendRating", "sendRating");

    UserRating? resRating;

    try {
      var body = {
        "id": rating.id,
        "type_id": rating.type.id,
        "criteria": rating.criteria.name,
        "note": rating.note
      };

      var res = await http.post(uri,
          body: jsonEncode(body),
          headers: cloudHeaders(
              headers: {"content-type": "application/json"}, span: span));
      if (res.statusCode == 200) {
        var raw = jsonDecode(res.body);
        resRating = rating.copyWith(id: raw["id"], updatedAt: DateTime.parse(raw["updatedAt"]));
        
      }
    } catch (err, s) {
      Sentry.captureException(err, stackTrace: s, withScope: (scope) {
        scope.span = span;
      });
      developer.log("Fail send note", level: Level.SEVERE.value, error: err);
    } finally {
      span.finish();
    }

    if (resRating != null) {
      var index = userRatings.indexWhere(
          (e) => e.type == rating.type && e.criteria == rating.criteria);
      var dbData = resRating.toDB();
      if (index >= 0) {
        userRatings[index] = resRating;
      } else {
        userRatings.add(resRating);
      }
      if (dbData != null) {
        await database
            .into(database.userRatingTable)
            .insert(dbData, mode: InsertMode.insertOrReplace);
      }
      if (reloadCommunity) {
        await loadCommunityRatingForType(
            type: resRating.type, doNotifyListeners: doNotifyListeners);
      }
    }

    return resRating;
  }

  Rating? getCommunityRating(
      {required DiaperType type, required RatingCriteria criteria}) {
    var found = communityRatings.where(
        (element) => element.type == type && element.criteria == criteria);
    return found.firstOrNull;
  }

  UserRating? getUserRating(
      {required DiaperType type, required RatingCriteria criteria}) {
    var found = userRatings.where(
            (element) => element.type == type && element.criteria == criteria);
    return found.firstOrNull;
  }
}
