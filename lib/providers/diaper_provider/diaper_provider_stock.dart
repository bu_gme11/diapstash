// ignore_for_file: invalid_use_of_visible_for_testing_member, invalid_use_of_protected_member

import 'dart:convert';
import 'dart:developer' as developer;

import 'package:diap_stash/object/interfaces.dart';
import 'package:diap_stash/providers/diaper_provider.dart';
import 'package:diap_stash/constants.dart' as constants;
import 'package:diap_stash/utils/stocks.dart';
import 'package:drift/drift.dart';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:logging/logging.dart';
import 'package:sentry_flutter/sentry_flutter.dart';

extension DiaperProviderStock on DiaperProvider {
  List<DiaperStock> get currentStockSorted {
    var currentStock = this
        .currentStock
        .where(filterStock)
        .toList();

    return sortStock(currentStock);
  }

  bool filterStock(DiaperStock stock){
    if(stock.count.isEmpty){
      return false;
    }
    if (stock.count.values.contains(DiaperStock.infinity)) {
      return true;
    }
    if (settingsProvider.keepZeroStockDiaper == "always") {
      return true;
    }
    if (settingsProvider.keepZeroStockDiaper == "favorite" &&
        isFavoriteType(stock.type)) {
      return true;
    }
    return stock.total() > 0;
  }

  List<DiaperStock> sortStock(List<DiaperStock> stock) {
    //var stock = stockIn.toList();
    switch (settingsProvider.sortType) {
      case "name":
        if (settingsProvider.sortAsc) {
          stock.sort((a, b) =>
              a.type.name.compareTo(b.type.name) +
              compareFavoriteType(a.type, b.type));
        } else {
          stock.sort((a, b) =>
              b.type.name.compareTo(a.type.name) +
              compareFavoriteType(a.type, b.type));
        }
        break;
      case "count":
        if (settingsProvider.sortAsc) {
          stock.sort((a, b) =>
              a.totalSort().compareTo(b.totalSort()) +
              compareFavoriteType(a.type, b.type));
        } else {
          stock.sort((a, b) =>
              b.totalSort().compareTo(a.totalSort()) +
              compareFavoriteType(a.type, b.type));
        }
        break;
    }
    return stock;
  }

  Future<void> loadStock({bool doRemote = true, required bool doNotifyListeners}) async {
    try {
      var maps = await database.select(database.diaperStockTable).get();
      for (var map in maps) {
        var types = diaperTypes.where((t) => t.id == map.type_id);
        if (types.isEmpty) continue;
        var type = types.first;
        var stock = DiaperStock(
            type: type,
            count: Map<String, int>.from(jsonDecode(map.counts)),
            prices: Map<String, double>.from(jsonDecode(map.prices)),
            updatedAt: map.updatedAt);
        if (!currentStock.contains(stock)) {
          currentStock.add(stock);
        } else {
          var index = currentStock.indexOf(stock);
          if (index >= 0) {
            var old = currentStock[index];
            if (old.updatedAt != null && stock.updatedAt != null) {
              if (stock.updatedAt!.isAfter(old.updatedAt!)) {
                currentStock[index] = stock;
              }
            } else if (stock.updatedAt != null) {
              currentStock[index] = stock;
            }
          }
        }
      }
    } catch (e, s) {
      Sentry.captureException(e, stackTrace: s);
      developer.log("Error on load stock", error: e);
      developer.log(e.toString());
    }

    if (doRemote) {
      await loadStockRemote(doNotifyListeners: false);
    }

    if (doNotifyListeners) {
      notifyListeners();
    }
  }

  Future<void> loadStockRemote({required bool doNotifyListeners}) async {
    if (settingsProvider.hasCloud()) {
      DateTime start = DateTime.now();
      var sentrySpan =
          Sentry.startTransaction("loadStockRemote()", "loadStockRemote");
      var userId = settingsProvider.getUserId();
      if (userId != null) {
        var uri = Uri.parse("${constants.baseUrl}/rest/diaper-stock");

        try {
          var res = await http.get(uri,
              headers: cloudHeaders(headers: {}, span: sentrySpan));
          if (res.statusCode == 200) {
            var data = jsonDecode(res.body) as List<dynamic>;

            var stockRemote = [];
            for (var e in data) {
              var elem = e as Map<String, dynamic>;
              var types =
                  diaperTypes.where((t) => t.id == elem["diaperTypeId"]);
              if (types.isEmpty) continue;
              var type = types.first;
              var rawPrice = Map<String, num>.from(elem["prices"]);
              Map<String, double> prices = rawPrice.map((key, value) => MapEntry(key, value.toDouble()));
              var stock = DiaperStock(
                  type: type,
                  count: Map<String, int>.from(elem["counts"]),
                  prices: prices);
              stockRemote.add(stock);
              if (elem.containsKey("updatedAt")) {
                var updatedAtStr = elem["updatedAt"];
                if (updatedAtStr != null) {
                  var updatedAt = DateTime.tryParse(updatedAtStr);
                  if (updatedAt != null) {
                    stock.updatedAt = updatedAt.toLocal();
                  }
                }
              }

              if (!currentStock.contains(stock)) {
                currentStock.add(stock);
                await saveStock(stock: stock, doRemote: false, doNotifyListeners: doNotifyListeners);
              } else {
                bool useRemote = true;
                var index = currentStock.indexOf(stock);
                var currentStockItem = currentStock[index];
                var selfDate = currentStockItem.updatedAt;
                if (stock.type.id == 116) {
                  developer.log(
                      "load ${stock.type.id} $selfDate ${stock.updatedAt} ${currentStockItem.count} ${stock.count}");
                }
                if (selfDate != null &&
                    stock.updatedAt != null &&
                    selfDate.isAfter(stock.updatedAt!)) {
                  useRemote = false;
                }

                if (useRemote) {
                  currentStock[index].count = stock.count;
                  if (stock.updatedAt != null) {
                    currentStock[index].updatedAt = stock.updatedAt;
                  }
                  saveStock(stock: stock, doRemote: false, doNotifyListeners: false);
                } else {
                  var selfCount = currentStockItem.count;
                  if(!mapEquals(selfCount, stock.count)) {
                    await saveStockRemote(stock: stock);
                  }else{ //juste la date qui a changé ?
                    developer.log("Dont rewrite stock ${stock.type.id} count similar");
                    currentStock[index].count = stock.count;
                    currentStock[index].prices = stock.prices;
                    if (stock.updatedAt != null) {
                      currentStock[index].updatedAt = stock.updatedAt;
                    }
                    saveStock(stock: stock, doRemote: false, doNotifyListeners: false);
                  }
                }
              }
            }
            var stocksNotInRemote = currentStock
                .where((element) => !stockRemote.contains(element))
                .toList();
            for (var stock in stocksNotInRemote) {
              await saveStockRemote(stock: stock);
            }
            //await saveStocks();
          }
        } catch (err, s) {
          Sentry.captureException(err, stackTrace: s, withScope: (scope) {
            scope.span = sentrySpan;
          });
          developer.log("Fail when cloud load stock",
              level: Level.SEVERE.value, error: err);
        } finally {
          sentrySpan.finish();
        }
      }
      DateTime end = DateTime.now();
      Duration d = end.difference(start);
      developer.log("loadRemote $d ${d.inSeconds} ${d.inMilliseconds}");
      if (doNotifyListeners) {
        notifyListeners();
      }
    }
  }

  saveStocks({bool doRemote = true, required bool doNotifyListeners}) async {
    for (var stock in currentStock.toList(growable: false)) {
      await database
          .into(database.diaperStockTable)
          .insert(stock.toDB(), mode: InsertMode.insertOrReplace);
    }

    if (doRemote) {
      await saveStocksRemote();
    }
    if (doNotifyListeners) {
      notifyListeners();
    }
  }

  saveStock({required DiaperStock stock, bool doRemote = true, required bool doNotifyListeners}) async {
    await database
        .into(database.diaperStockTable)
        .insert(stock.toDB(), mode: InsertMode.insertOrReplace);

    if (doRemote) {
      await saveStockRemote(stock: stock);
    }
    if (doNotifyListeners) {
      notifyListeners();
    }
  }

  Future<void> saveStockRemote({required DiaperStock stock}) async {
    if (settingsProvider.hasCloud()) {
      var uri = Uri.parse("${constants.baseUrl}/rest/diaper-stock");
      var sentrySpan =
          Sentry.startTransaction("saveStockRemote()", "saveStockRemote");
      try {
        var data = stock.toMapRest();
        var res = await http.post(uri,
            headers: cloudHeaders(
                headers: {"Content-Type": "application/json"},
                span: sentrySpan),
            body: jsonEncode(data));
        if (res.statusCode == 200) {
          developer.log("save ok stock ${stock.type.id} ${stock.count}");
        }
      } catch (err, s) {
        Sentry.captureException(err, stackTrace: s, withScope: (scope) {
          scope.span = sentrySpan;
        });
        developer.log("Fail when cloud save",
            level: Level.SEVERE.value, error: err);
      } finally {
        sentrySpan.finish();
      }
    } else {
      developer.log("cannot save stock, cloud disabled",
          level: Level.WARNING.value);
    }
  }

  Future<void> saveStocksRemote() async {
    if (settingsProvider.hasCloud()) {
      var uri = Uri.parse("${constants.baseUrl}/rest/diaper-stock");
      var sentrySpan =
          Sentry.startTransaction("saveStocksRemote()", "saveStocksRemote");
      try {
        for (var stock in currentStock.toList(growable: false)) {
          var data = stock.toMapRest();
          var res = await http.post(uri,
              headers: cloudHeaders(
                  headers: {"Content-Type": "application/json"},
                  span: sentrySpan),
              body: jsonEncode(data));
          if (res.statusCode == 200) {
            //developer.log("save ok stock");
          }
        }
      } catch (err, s) {
        Sentry.captureException(err, stackTrace: s, withScope: (scope) {
          scope.span = sentrySpan;
        });
        developer.log("Fail when cloud save",
            level: Level.SEVERE.value, error: err);
      } finally {
        sentrySpan.finish();
      }
    } else {
      developer.log("cannot save stock, cloud disabled",
          level: Level.WARNING.value);
    }
  }

  Future<Change> use(DiaperStock stock, String size,
      {bool doRemote = true, required bool doNotifyListeners}) async {
    bool needSaveStockRemote = false;
    if (stock.count.containsKey(size)) {
      removeDiaperOnStock(stock: stock, size: size, dp: this);
      if (stock.count.isEmpty) {
        await removeStock(stock, doNotifyListeners: false);
      } else {
        stock.updatedAt = DateTime.now();
        await saveStock(stock: stock, doRemote: false, doNotifyListeners: false);
        needSaveStockRemote = true;
      }
    }

    var changeTime = DateTime.now();

    List<Change> changesToSave = [];

    changes.where((change) => change.endTime == null).forEach((change) {
      change.endTime = changeTime;
      change.updatedAt = DateTime.now();
      changesToSave.add(change);
    });

    double? price;
    if(stock.prices.containsKey(size)){
      price = stock.prices[size];
    }

    var change = Change(startTime: changeTime);
    change.addDiaper(stock.type, size, price);
    changes.add(change);
    changes.sort((a, b) => b.startTime.compareTo(a.startTime));
    changesToSave.add(change);
    await saveSpecificChanges(specificChanges: changesToSave, doRemote: false, doNotifyListeners: false);

    if (doNotifyListeners) {
      notifyListeners();
    }

    if (needSaveStockRemote && doRemote) {
      await saveStockRemote(stock: stock);
    }
    if (doRemote) {
      await saveSpecificChangesRemote(changesToSave);
    }

    return change;
  }

  removeStock(DiaperStock stock, {required bool doNotifyListeners, bool doRemote = true}) async {
    currentStock.remove(stock);

    developer.log("remove ${stock.type.id}");

    await (database.delete(database.diaperStockTable)
          ..where((tbl) => tbl.type_id.equals(stock.type.id)))
        .go();

    if(doRemote) {
      await removeStockRemote(stock);
    }
    if (doNotifyListeners) {
      notifyListeners();
    }
  }

  Future<void> removeStockRemote(DiaperStock stock) async {
    if (settingsProvider.hasCloud()) {
      var sentrySpan =
          Sentry.startTransaction("saveChangesRemote()", "saveChangesRemote");
      var uri = Uri.parse("${constants.baseUrl}/rest/diaper-stock");
      try {
        var data = stock.toMapRest();
        var res = await http.delete(uri,
            headers: cloudHeaders(
                headers: {"Content-Type": "application/json"},
                span: sentrySpan),
            body: jsonEncode(data));
        if (res.statusCode == 200) {
          developer.log("save ok remove stock remote");
        }
      } catch (err, s) {
        Sentry.captureException(err, stackTrace: s, withScope: (scope) {
          scope.span = sentrySpan;
        });
        developer.log("Fail when cloud remove",
            level: Level.SEVERE.value, error: err);
      } finally {
        sentrySpan.finish();
      }
    }
  }

  addStock(DiaperType type, String size, int count, {required bool doNotifyListeners, bool doRemote = true}) async {
    var findStock = currentStock.where((s) => s.type == type);

    if (findStock.isEmpty) {
      var stock = DiaperStock(type: type, count: {size: count}, prices: {});
      currentStock.add(stock);
      await saveStock(stock: stock, doNotifyListeners: doNotifyListeners, doRemote: doRemote);
    } else {
      var stock = findStock.first;
      if (stock.count.containsKey(size)) {
        stock.count[size] = stock.count[size]! + count;
      } else {
        stock.count[size] = count;
      }
      stock.updatedAt = DateTime.now();
      await saveStock(stock: stock, doNotifyListeners: doNotifyListeners, doRemote: doRemote);
    }
  }

  setPrice(DiaperType type, String size, double? price, {required bool doNotifyListeners}) async {
    var findStock = currentStock.where((s) => s.type == type);

    if (findStock.isEmpty && price != null) {
      var stock = DiaperStock(type: type, count: {size: 0}, prices: {size: price});
      currentStock.add(stock);
      await saveStock(stock: stock, doNotifyListeners: doNotifyListeners);
    } else if(findStock.isNotEmpty) {
      var stock = findStock.first;
      if(price != null) {
        stock.prices[size] = price;
      }else if(!stock.prices.containsKey(size)){
        return;
      }
      stock.updatedAt = DateTime.now();
      await saveStock(stock: stock, doNotifyListeners: doNotifyListeners);
    }
  }

  updateStock(DiaperType type, String size, int count, {required bool doNotifyListeners}) async {
    var findStock = currentStock.where((s) => s.type == type);

    if (findStock.isNotEmpty) {
      var stock = findStock.first;
      stock.count[size] = count;
      stock.updatedAt = DateTime.now();
      await saveStock(stock: stock, doNotifyListeners: false);
    }

    if (doNotifyListeners) {
      notifyListeners();
    }
  }
}
