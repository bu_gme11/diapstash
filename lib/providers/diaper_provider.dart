import 'package:diap_stash/common.dart';
import 'package:diap_stash/database/database.dart';
import 'package:diap_stash/object/interfaces.dart';
import 'package:diap_stash/object/rating.dart';
import 'package:diap_stash/providers/diaper_provider/diaper_provider_rating.dart';
import 'package:diap_stash/providers/settings_provider.dart';
import 'package:sentry_flutter/sentry_flutter.dart';
import 'dart:developer' as developer;
import 'package:http/http.dart' as http;
export 'package:diap_stash/providers/diaper_provider/diaper_provider_shared_stock.dart';
import 'package:diap_stash/providers/diaper_provider/diaper_provider_shared_stock.dart';

export 'package:diap_stash/providers/diaper_provider/diaper_provider_shared_history.dart';
import 'package:diap_stash/providers/diaper_provider/diaper_provider_shared_history.dart';

export 'package:diap_stash/providers/diaper_provider/diaper_provider_favorite.dart';
import 'package:diap_stash/providers/diaper_provider/diaper_provider_favorite.dart';

export 'package:diap_stash/providers/diaper_provider/diaper_provider_proposal.dart';

export 'package:diap_stash/providers/diaper_provider/diaper_provider_change.dart';
import 'package:diap_stash/providers/diaper_provider/diaper_provider_change.dart';

export 'package:diap_stash/providers/diaper_provider/diaper_provider_type.dart';
import 'package:diap_stash/providers/diaper_provider/diaper_provider_type.dart';

export 'package:diap_stash/providers/diaper_provider/diaper_provider_stock.dart';
import 'package:diap_stash/providers/diaper_provider/diaper_provider_stock.dart';

export 'package:diap_stash/providers/diaper_provider/diaper_provider_type_info.dart';
import 'package:diap_stash/providers/diaper_provider/diaper_provider_type_info.dart';

class DiaperProvider extends ChangeNotifier {
  static late DiaperProvider instance;

  final String versionName;
  final String versionCode;
  final MyDatabase database;
  late SettingsProvider settingsProvider;

  final List<DiaperBrand> brands = [];
  final List<DiaperType> diaperTypes = [];
  final List<DiaperStock> currentStock = [];
  final List<Change> changes = [];
  final List<SharedHistory> sharedHistories = [];
  final List<SharedStock> sharedStocks = [];
  final List<SharedHistoryAccess> sharedHistoriesAccess = [];
  final List<SharedStockAccess> sharedStockAccess = [];
  final List<TypeInfo> typesInfo = [];
  final List<Rating> communityRatings = [];
  final List<UserRating> userRatings = [];

  final Set<DiaperType> favoritesTypes = {};

  DiaperProvider({required this.database, required this.versionCode, required this.versionName}) {
    instance = this;
    /*loadOfflineData().then((_) async {
      await loadData();
    });*/
  }

  addSettingsProvider(SettingsProvider sp) {
    settingsProvider = sp;
    return this;
  }

  Change? get currentChange {
    if (changes.isEmpty) return null;
    var currents = changes.where((element) => element.endTime == null);
    if (currents.isNotEmpty) return currents.first;
    return null;
  }

  Future<void> loadOfflineData() async {
    await loadBrands(doRemote: false);
    await loadTypes(doRemote: false, doNotifyListeners: false);
    await loadStock(doRemote: false, doNotifyListeners: false);
    await loadChange(doRemote: false, doNotifyListeners: false);
    await loadFavorites(doRemote: false, doNotifyListeners: true);
    await loadDiaperTypeInfo(doRemote: false, doNotifyListeners: false);
    await loadSharedHistoryAccess(doRemote: false, doNotifyListeners: false);
    await loadSharedStockAccess(doRemote: false, doNotifyListeners: false);
    await loadUserRating(doRemote: false, doNotifyListeners: false);
    notifyListeners();
  }

  Future<void> loadData() async {
    developer.log("-> Load Data");
    await loadBrands();
    await loadTypes(doNotifyListeners: false);
    await loadStock(doNotifyListeners: false);
    await loadFavorites(doNotifyListeners: false);
    await loadDiaperTypeInfo(doNotifyListeners: false);
    await loadChange(doNotifyListeners: false);
    await loadSharedHistoryAccess(doNotifyListeners: false);
    await loadSharedStockAccess(doNotifyListeners: false);
    await sendCustomTypeToCloud(doNotifyListeners: false);
    await checkDeletedDiaperType(doNotifyListeners: false);
    await loadUserRating(doNotifyListeners: false);
    await loadCommunityRating(doNotifyListeners: false);
    notifyListeners();
    developer.log("<- Load Data");
  }

  Future<void> loadCatalog({bool doNotifyListeners = true}) async {
    await loadBrands();
    await loadTypes(doNotifyListeners: doNotifyListeners);
    await loadFavorites(doNotifyListeners: doNotifyListeners);
    await loadDiaperTypeInfo(doNotifyListeners: doNotifyListeners);
  }

  Future<void> sendDataToCloud() async {
    await sendCustomTypeToCloud(doNotifyListeners: false);
    await sendAllRemoteFavorites();
    await saveTypeInfosRemote(doNotifyListeners: false);
    await linkUserRatingToCloudSync(doNotifyListeners: false);
    notifyListeners();
  }

  Map<String, String> cloudHeaders(
      {required Map<String, String> headers, required ISentrySpan span}) {
    var userId = settingsProvider.getUserId();
    var secureToken = settingsProvider.spInstance.getString(SettingsProvider.keySecureToken);
    if (userId != null) {
      headers["X-USER-ID"] = userId;
      span.setData("userId", userId);
    }
    if (secureToken != null) {
      headers["X-SECURE-TOKEN"] = secureToken;
    }

    headers["X-DS-VERSION-CODE"] = versionCode;
    headers["X-DS-VERSION-NAME"] = versionName;
    //headers["Connection"] = "Keep-Alive";

    var sentryTrace = span.toSentryTrace();
    headers[sentryTrace.name] = sentryTrace.value;
    return headers;
  }

  Future<void> deleteCloudUser() async {
    var span =
    Sentry.startTransaction("deleteCloudUser", "deleteCloudUser");
    try {
      var uri = Uri.parse("$baseUrl/rest/user/delete");
      await http.delete(uri, headers: cloudHeaders(headers: {}, span: span));
    } catch (e) {
      developer.log("Error when disable cloud $e");
      Sentry.captureException(e, withScope: (scope) {
        scope.span = span;
      });
    }finally{
      span.finish();
    }
  }

  unlinkCloudSync({required bool dropCloudSync, required bool doNotifyListeners}) async {
    await unlinkUserRatingToCloudSync(recreateRating: !dropCloudSync, doNotifyListeners: false);
    await transfertCloudCustomTypeToLocal(doNotifyListeners: false);
    if(dropCloudSync){
      await deleteCloudUser();
    }
    if(doNotifyListeners){
      super.notifyListeners();
    }
  }

  doNotifyListeners() {
    developer.log("Notify listener");
    super.notifyListeners();
  }

}
