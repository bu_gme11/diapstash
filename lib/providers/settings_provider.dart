import 'dart:convert';
import 'dart:developer';

import 'package:diap_stash/common.dart';
import 'package:diap_stash/main.dart';
import 'package:diap_stash/object/interfaces.dart';
import 'package:diap_stash/utils/period.dart';
import 'package:flutter/foundation.dart';
import 'package:logging/logging.dart';
import 'package:sentry_flutter/sentry_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

class SettingsProvider extends ChangeNotifier {
  static const keyCloud = "cloud";
  static const keyUserId = "userId";
  static const keyThresholdLowStock = "thresholdLowStock";
  static const keySecureToken = "secureToken";
  static const keyPredefinedBagSize = "predefinedBagSize";
  static const keyKeepZeroStockDiaper = "keepZeroStockDiaper";
  static const keySortStockAsc = "sort_stock_asc";
  static const keySortStockType = "sort_stock_type";
  static const keyAllowInfiniteStock = "allowInfiniteStock";
  static const keyLanguage = "language";
  static const keyFilterTarget = "filterTarget";
  static const keyFilterStyle = "filterStyles";
  static const keyFilterType = "filterTypes";
  static const keyFilterDiscontinued = "filterDiscontinued";
  static const keyNotificationNightMode = "notificationNightMode";
  static const keyNotificationNightModePeriod = "notificationNightModePeriod";
  static const keyNotificationNightModeNotifications = "notificationNightModeNotifications";
  static const keyNotificationNightModeNotificationPropagate = "notificationNightModeNotificationPropagate";
  static const keyWhatsNewHide = "whatsNewHide";
  static const keyWhatsNewLast = "whatsNewLast";
  static const keyAllowRandomStock = "randomStock";
  static const keyUse24HourFormat = "use24HourFormat";
  static const keyLastsubcheck = "lastsubcheck";
  static const keySendRating = "sendRating";

  late SharedPreferences spInstance;

  static late SettingsProvider instance;

  final String versionName;
  final String versionCode;

  SettingsProvider({required this.spInstance, required this.versionName, required this.versionCode, }) {
    SettingsProvider.instance = this;
  }

  bool hasCloud() {
    return spInstance.getBool(SettingsProvider.keyCloud) ?? false;
  }

  String? getUserId() {
    return spInstance.getString(SettingsProvider.keyUserId);
  }

  int get thresholdLowStock {
    return spInstance.getInt(SettingsProvider.keyThresholdLowStock) ?? 5;
  }

  bool get allowInfiniteStock {
    return spInstance.getBool(keyAllowInfiniteStock) ?? false;
  }

  bool get notificationNightMode {
    return spInstance.getBool(keyNotificationNightMode) ?? false;
  }

  Map<String, Map<String, String>> get notificationNightModeNotifications {

    Map<String, Map<String, String>> map = {};

    var raw = spInstance.getString(keyNotificationNightModeNotifications);
    if(raw != null){
      var json = jsonDecode(raw);
      Map<String, Map<String, dynamic>> map1 = Map.from(json);
      map = map1.map((key, value) => MapEntry(key, Map<String, String>.from(value)));
      //map = Map.castFrom(json);
    }
    return map;
  }

  bool get notificationNightPropagate {
    return spInstance.getBool(keyNotificationNightModeNotificationPropagate) ?? false;
  }

  Period get notificationNightPeriod {
    var val = spInstance.getString(keyNotificationNightModePeriod);
    if(val != null){
      return Period.fromJSON(val);
    }
    return Period(start: null, end: null);
  }

  setCloud(bool? val) async {
    log("set persistant ${val.toString()}");

    if (val ?? false) {
      try {
        var uri = Uri.parse("$baseUrl/rest/user/init");
        var res = await http.post(uri, headers: {
          "X-DS-VERSION-CODE": versionCode,
          "X-DS-VERSION-NAME": versionName
        });
        if (res.statusCode == 200) {
          var data = jsonDecode(res.body);
          await spInstance.setBool(SettingsProvider.keyCloud, true);
          await spInstance.setString(
              SettingsProvider.keyUserId, data["id"].toString());
          await spInstance.setString(
              SettingsProvider.keySecureToken, data["secureToken"].toString());
          notifyListeners();
        }
      } catch (e) {
        log("Error when disable cloud $e");
        Sentry.captureException(e);
      }
    } else {
      await spInstance.setBool(SettingsProvider.keyCloud, false);
      await spInstance.remove(SettingsProvider.keyUserId);
      await spInstance.remove(SettingsProvider.keySecureToken);
      notifyListeners();
    }
  }

  Map<String, String> cloudHeaders(Map<String, String> headers) {
    var userId = getUserId();
    var secureToken = spInstance.getString(SettingsProvider.keySecureToken);
    if (userId != null) {
      headers["X-USER-ID"] = userId;
    }
    if (secureToken != null) {
      headers["X-SECURE-TOKEN"] = secureToken;
    }
    headers["X-DS-VERSION-CODE"] = versionCode;
    headers["X-DS-VERSION-NAME"] = versionName;
    return headers;
  }

  Future<bool> cloudFromQRCode(String code) async {
    var part = code.split("\$");
    var userId = part[0];
    var secureToken = part[1];
    try {
      var uri = Uri.parse("$baseUrl/rest/user/join");
      var res = await http.post(uri,
          headers: {
            "Content-Type": "application/json",
            "X-DS-VERSION-CODE": versionCode,
            "X-DS-VERSION-NAME": versionName
          },
          body: jsonEncode({"userId": userId, "secureToken": secureToken}));
      if (res.statusCode == 200) {
        var data = jsonDecode(res.body);
        if (data["join"] == true) {
          spInstance.setBool(SettingsProvider.keyCloud, true);
          spInstance.setString(SettingsProvider.keyUserId, userId);
          spInstance.setString(SettingsProvider.keySecureToken, secureToken);
          notifyListeners();
          log("joined");
          return true;
        } else {
          log("Fail to join", level: Level.WARNING.value);
        }
      }
    } catch (e) {
      log("Error when join user", level: Level.SEVERE.value);
    }
    return false;
  }

  String getQRCodeData() {
    return "${spInstance.getString(SettingsProvider.keyUserId)}\$${spInstance.getString(SettingsProvider.keySecureToken)}";
  }

  String get sortType {
    return spInstance.getString(SettingsProvider.keySortStockType) ?? "count";
  }

  bool get sortAsc {
    return spInstance.getBool(SettingsProvider.keySortStockAsc) ?? false;
  }

  List<int> get predefinedBagSize {
    var rawList =
        spInstance.getStringList(SettingsProvider.keyPredefinedBagSize);
    if (rawList == null) {
      return [2, 10, 40];
    }
    return rawList.map((e) => int.parse(e)).toList();
  }

  Future<bool> setPredefinedBagSize(List<int> sizes) async {
    var res = await spInstance.setStringList(
        SettingsProvider.keyPredefinedBagSize,
        sizes.map((e) => "$e").toList(growable: false));
    notifyListeners();
    return res;
  }

  String get keepZeroStockDiaper =>
      spInstance.getString(keyKeepZeroStockDiaper) ?? 'never';

  List<DiaperTarget> get filterDiaperTarget {
    var lst = spInstance.getStringList(keyFilterTarget);
    if (lst != null) {
      return lst.map((e) => DiaperTarget.fromRest(e)).toList();
    }
    return DiaperTarget.values.toList();
  }

  List<DiaperStyle> get filterDiaperStyle {
    var lst = spInstance.getStringList(keyFilterStyle);
    if (lst != null) {
      return lst.map((e) => DiaperStyle.fromRest(e)).toList();
    }
    return DiaperStyle.values.toList();
  }

  List<DTType> get filterDiaperType {
    var lst = spInstance.getStringList(keyFilterType);
    if (lst != null) {
      return lst.map((e) => DTType.fromRest(e)).toList();
    }
    return DTType.values.toList();
  }

  List<bool> get filterDiscontinued {
    var lst = spInstance.getStringList(keyFilterDiscontinued);
    if (lst != null) {
      return lst.map((e) => e == "true").toList();
    }
    return [true, false];
  }

  String get keepZeroStockDiaperI18N {
    switch (keepZeroStockDiaper) {
      case "always":
        return I18N.current.settings_general_keep_zero_stock_always;
      case "favorite":
        return I18N.current.settings_general_keep_zero_stock_only_favorite;
      case "never":
        return I18N.current.settings_general_keep_zero_stock_never;
      default:
        return keepZeroStockDiaper;
    }
  }

  bool get whatNewHide {
    return spInstance.getBool(keyWhatsNewHide) ?? false;
  }

  int? get whatNewLast {
    return spInstance.getInt(keyWhatsNewLast);
  }

  bool get allowRandomStock {
    return spInstance.getBool(keyAllowRandomStock) ?? true;
  }

  bool get sendRating {
    return spInstance.getBool(keySendRating) ?? true;
  }

  Future<bool> setKeepZeroStockDiaper(String value) async {
    var res = await spInstance.setString(keyKeepZeroStockDiaper, value);
    notifyListeners();
    return res;
  }

  String get language => spInstance.getString(keyLanguage) ?? 'system';

  Future<bool> setLanguage(String value) async {
    var res = await spInstance.setString(keyLanguage, value);
    notifyListeners();
    return res;
  }

  Future<bool> setFilterDiaperTarget(List<DiaperTarget> values) async {
    var res = await spInstance.setStringList(keyFilterTarget,
        values.map((e) => e.restValue).toList(growable: false));
    notifyListeners();
    return res;
  }

  Future<bool> setFilterDiaperStyle(List<DiaperStyle> values) async {
    var res = await spInstance.setStringList(keyFilterStyle,
        values.map((e) => e.restValue).toList(growable: false));
    notifyListeners();
    return res;
  }

  Future<bool> setFilterDiaperType(List<DTType> values) async {
    var res = await spInstance.setStringList(keyFilterType,
        values.map((e) => e.restValue).toList(growable: false));
    notifyListeners();
    return res;
  }

  Future<bool> setFilterDiscontinued(List<bool> values) async {
    var res = await spInstance.setStringList(keyFilterDiscontinued,
        values.map((e) => e.toString()).toList(growable: false));
    notifyListeners();
    return res;
  }

  Future<bool> setNotificationNightMode(bool val) async {
    var res = await spInstance.setBool(keyNotificationNightMode, val);
    notifyListeners();
    return res;
  }


  Future<bool> setNotificationNightModeNotificationPropagate(bool val) async {
    var res = await spInstance.setBool(keyNotificationNightModeNotificationPropagate, val);
    notifyListeners();
    return res;
  }
  Future<bool> setNotificationNightModePeriod(Period val) async {
    var json = val.toJSON();
    var res = await spInstance.setString(keyNotificationNightModePeriod, json);
    notifyListeners();
    return res;
  }

  Future<bool> setNotificationNightModeNotifications(Map<String, Map<String, String>> val) async {
    var res = await spInstance.setString(keyNotificationNightModeNotifications, jsonEncode(val));
    notifyListeners();
    return res;
  }


  Future<bool> setWhatsNewHide(bool val) async {
    var res = await spInstance.setBool(keyWhatsNewHide, val);
    notifyListeners();
    return res;
  }

  Future<bool> setWhatsNewLast(int val) async {
    var res = await spInstance.setInt(keyWhatsNewLast, val);
    notifyListeners();
    return res;
  }

  Future<bool> setAllowRandomStock(bool val) async {
    var res = await spInstance.setBool(keyAllowRandomStock, val);
    notifyListeners();
    return res;
  }

  Future<bool> setSendRating(bool val) async {
    var res = await spInstance.setBool(keySendRating, val);
    notifyListeners();
    return res;
  }

  updateSort(String selected) async {
    if (sortType == selected) {
      await spInstance.setBool(SettingsProvider.keySortStockAsc, !sortAsc);
    } else {
      await spInstance.setBool(SettingsProvider.keySortStockAsc, selected == "count" ? false : true);
      await spInstance.setString(SettingsProvider.keySortStockType, selected);
    }
  }

  Locale getCurrentLocale(BuildContext context){
    var locale = Localizations.localeOf(context);
    if(language != "system"){
      locale = Locale(language);
    }
    return locale;
  }

  bool use24HourFormat([BuildContext? context]) {
    String? rawVal = spInstance.getString(keyUse24HourFormat);
    bool? val;
    if(rawVal != null){
      val = bool.tryParse(rawVal);
    }
    if(val == null && !kIsWeb){
      try {
        BuildContext? ctx = context ?? navigatorKey.currentContext;
        if (ctx != null) {
          var alwaysUse24HourFormat = MediaQuery.of(ctx).alwaysUse24HourFormat;
          val = alwaysUse24HourFormat;
        }
      }catch(_){}
    }
    return val ?? true;
  }

}
