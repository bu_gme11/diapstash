// ignore_for_file: use_build_context_synchronously

import 'dart:developer';

import 'package:diap_stash/common.dart';
import 'package:diap_stash/database/database.dart';
import 'package:diap_stash/object/interfaces.dart';
import 'package:diap_stash/object/notifications.dart';
import 'package:diap_stash/providers/diaper_provider.dart';
import 'package:diap_stash/providers/settings_provider.dart';
import 'package:diap_stash/util.dart';
import 'package:diap_stash/utils/period.dart';
import 'package:drift/drift.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:sentry_flutter/sentry_flutter.dart';

// ignore: depend_on_referenced_packages
import 'package:timezone/data/latest_all.dart' as tz;

// ignore: depend_on_referenced_packages
import 'package:timezone/timezone.dart' as tz;

const String darwinNotificationCategoryText = 'textCategory';

const offsetNotificationChangeId = 64;

class NotificationProvider extends ChangeNotifier {
  static const String nightModeStartWearing = "start_wearing";
  static const String nightModeStartNoWearing = "start_no_wearing";
  static const String nightModeEndWearing = "end_wearing";

  static const int _idNotificationStartWearing = 10;
  static const int _idNotificationStartNoWearing = 11;
  static const int _idNotificationEndWearing = 12;

  final MyDatabase database;

  static NotificationProvider? instance;

  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
      FlutterLocalNotificationsPlugin();

  List<DSNotification> notifications = [];

  bool alreadyAskPerms = false;

  NotificationProvider({required this.database}) {
    instance = this;

    if (kIsWeb) return;

    log("NotificationProvider constructor");

    tz.initializeTimeZones();
    initializeLocalNotification();
    loadNotification(setupNotification: false);
    //log("4");
  }

  void initializeLocalNotification() async {
    log("Notif init");
    const AndroidInitializationSettings initializationSettingsAndroid =
        AndroidInitializationSettings('@mipmap/ic_diaper');

    const List<DarwinNotificationCategory> darwinNotificationCategories = [
      DarwinNotificationCategory(darwinNotificationCategoryText, actions: [])
    ];

    const DarwinInitializationSettings initializationSettingsIOS =
        DarwinInitializationSettings(
            notificationCategories: darwinNotificationCategories);
    const InitializationSettings initializationSettings =
        InitializationSettings(
            android: initializationSettingsAndroid,
            iOS: initializationSettingsIOS);
    var ok = await flutterLocalNotificationsPlugin
        .initialize(initializationSettings,
            onDidReceiveNotificationResponse: (NotificationResponse details) {
      log("onDidReceiveNotificationResponse : ${details.id} ${details.payload}");
    });

    log("notif init = ${ok.toString()}");
  }

  loadNotification({bool setupNotification = true}) async {
    //log("loadNotification");

    var data = await database.notificationTable.select().get();
    notifications =
        data.map((e) => DSNotification.fromDB(e)).toList(growable: true);
    notifyListeners();
    if (setupNotification && await Permission.notification.isGranted) {
      if (notifications.isNotEmpty) {
        if (DiaperProvider.instance.currentChange != null) {
          await setupNotifications(DiaperProvider.instance.currentChange!,
              loadNotifications: false);
        }
      }
      await setupNotificationNightMode();
    }
  }

  saveNotification(DSNotification notification) async {
    await (database.notificationTable.update()
          ..where((tbl) => tbl.id.equals(notification.id)))
        .write(notification.toDB());
    await loadNotification();
  }

  deleteNotification(DSNotification notification) async {
    notifications.remove(notification);
    await (database.notificationTable.delete()
          ..where((tbl) => tbl.id.equals(notification.id)))
        .go();

    cancelNotification(notification);
    notifyListeners();
  }

  Future<DSNotification> createNotification(
      {required NotificationType type,
      required Duration? duration,
      required String title,
      required String description,
      required int? typeId}) async {
    duration ??= const Duration(hours: 6);
    var insertData = NotificationTableCompanion.insert(
        type: type, duration: duration.inMinutes);
    var id = await (database.notificationTable.insert()).insert(insertData);
    var n = DSNotification(
        id: id,
        type: type,
        duration: duration,
        title: title,
        description: description,
        typeId: typeId);
    //var n = DSNotification.fromDB(data);
    notifications.add(n);
    notifyListeners();
    return n;
  }

  NotificationDetails changeNotificationDetail() {
    var iconSettings =
        SettingsProvider.instance.spInstance.getString("icon") ?? "discret";
    var icon = '@drawable/logo_soft_transparent';
    if (iconSettings == 'diaper') {
      icon = '@drawable/logo_diaper_transparent';
    }

    return NotificationDetails(
        android: AndroidNotificationDetails(
            'fr.diapstash.diap_stash.change', 'change',
            icon: icon,
            //category: AndroidNotificationCategory.reminder,
            //visibility: NotificationVisibility.private,
            colorized: false,
            color: Colors.white),
        iOS: const DarwinNotificationDetails(
            categoryIdentifier: darwinNotificationCategoryText));
  }

  Future<bool> askPermissions({bool requestPermission = true}) async {
    if (await Permission.notification.isDenied) {
      if (requestPermission) {
        Sentry.addBreadcrumb(Breadcrumb(
            level: SentryLevel.debug,
            message: "Request notification perms",
            data: {"stackTrace": StackTrace.current}));
        var status = await Permission.notification.request();
        if (!status.isGranted) {
          log("No perms, no notification");
          return false;
        }
      } else {
        return false;
      }
    }
    return true;
  }

  setupNotifications(Change change,
      {bool loadNotifications = true, bool requestPermission = true}) async {
    if (kIsWeb) return;

    var diapers =
        change.diapers.where((element) => element.type.type == DTType.diaper);
    if (diapers.isEmpty) return;
    DiaperType diaperType = diapers.first.type;

    if (loadNotifications) {
      loadNotification(setupNotification: false);
    }

    //log("Setup notification for change");

    if (!await askPermissions(requestPermission: requestPermission)) return;

    SettingsProvider sp = SettingsProvider.instance;

    await cancelChangeNotification();
    var now = tz.TZDateTime.now(tz.local);

    var notificationsForType =
        notifications.where((element) => element.typeId == diaperType.id);
    var notificationsDefault =
        notifications.where((element) => element.typeId == null);

    List<DSNotification> notificationsToUse = [];

    if (notificationsForType.isNotEmpty) {
      notificationsToUse = notificationsForType.toList(growable: false);
    } else if (notificationsDefault.isNotEmpty) {
      notificationsToUse = notificationsDefault.toList(growable: false);
    } else {
      return;
    }

    Period? nightModePeriod = sp.notificationNightMode
        ? sp.notificationNightPeriod.isValid
            ? sp.notificationNightPeriod
            : null
        : null;
    bool propagate = sp.notificationNightPropagate;

    bool canExact = false;
    if(!requestPermission){
      canExact = await Permission.scheduleExactAlarm.isGranted;
    }else{
      canExact = (await Permission.scheduleExactAlarm.request()).isGranted;
    }

    for (var n in notificationsToUse) {
      var dateTime = change.startTime.add(n.duration).copyWith(second: 0);
      var tzDateTime = tz.TZDateTime.from(dateTime, tz.local);
      if (tzDateTime.isBefore(now)) continue;

      Duration durationToShow = n.duration;

      if (nightModePeriod != null) {
        if (nightModePeriod.isInclude(dateTime.toLocal())) {
          if (propagate) {
            durationToShow = change.startTime.difference(dateTime);
            tzDateTime =
                tz.TZDateTime.from(nightModePeriod.endFromNow!, tz.local);
          } else {
            continue;
          }
        }
      }

      log("schedule ${n.id} at ${tzDateTime.toIso8601String()}");

      String title = n.type.localizedTitle();
      String desc = n.type.localizedDescription(n.duration);

      if (n.type == NotificationType.CUSTOM) {
        title = n.title.replaceAll("%time%", durationToString(durationToShow));
        desc = n.description
            .replaceAll("%time%", durationToString(durationToShow));
      }

      await flutterLocalNotificationsPlugin.zonedSchedule(
          n.id + offsetNotificationChangeId,
          title,
          desc,
          tzDateTime,
          changeNotificationDetail(),
          uiLocalNotificationDateInterpretation:
              UILocalNotificationDateInterpretation.wallClockTime,
          androidScheduleMode: canExact ? AndroidScheduleMode.exactAllowWhileIdle : AndroidScheduleMode.inexactAllowWhileIdle,
          matchDateTimeComponents: DateTimeComponents.dateAndTime);
    }
    await _logSetupedNotifications();
  }

  Future<void> _logSetupedNotifications() async {
    var list =
        await flutterLocalNotificationsPlugin.pendingNotificationRequests();
    log("${list.length} setup notification");
  }

  Future<void> setupNotificationNightMode() async {
    if (kIsWeb) return;

    await cancelNotificationNightMode();

    if (!(await Permission.notification.isGranted)) return;

    SettingsProvider sp = SettingsProvider.instance;
    DiaperProvider dp = DiaperProvider.instance;
    Period period = sp.notificationNightPeriod;
    var now = tz.TZDateTime.now(tz.local);

    if (sp.notificationNightMode && period.isValid) {
      var notifications = sp.notificationNightModeNotifications;

      if (dp.currentChange == null &&
          notifications.containsKey(nightModeStartNoWearing)) {
        var title = notifications[nightModeStartNoWearing]!['title']!;
        var desc = notifications[nightModeStartNoWearing]!['description']!;
        var notificationTime =
            tz.TZDateTime.from(period.startFromNow!, tz.local);

        if (notificationTime.isAfter(now)) {
          log("Setup notification night mode start no wearing");

          await flutterLocalNotificationsPlugin.zonedSchedule(
              _idNotificationStartNoWearing,
              title,
              desc,
              notificationTime,
              changeNotificationDetail(),
              uiLocalNotificationDateInterpretation:
                  UILocalNotificationDateInterpretation.wallClockTime,
              androidScheduleMode: AndroidScheduleMode.inexactAllowWhileIdle,
              matchDateTimeComponents: DateTimeComponents.dateAndTime);
        }
      } else if (dp.currentChange != null &&
          notifications.containsKey(nightModeStartWearing)) {
        var title = notifications[nightModeStartWearing]!['title']!;
        var desc = notifications[nightModeStartWearing]!['description']!;
        var notificationTime =
            tz.TZDateTime.from(period.startFromNow!, tz.local);

        if (notificationTime.isAfter(now)) {
          log("Setup notification night mode start wearing");

          await flutterLocalNotificationsPlugin.zonedSchedule(
              _idNotificationStartWearing,
              title,
              desc,
              notificationTime,
              changeNotificationDetail(),
              uiLocalNotificationDateInterpretation:
                  UILocalNotificationDateInterpretation.wallClockTime,
              androidScheduleMode: AndroidScheduleMode.inexactAllowWhileIdle,
              matchDateTimeComponents: DateTimeComponents.dateAndTime);
        }
      }

      if (dp.currentChange != null &&
          notifications.containsKey(nightModeEndWearing)) {
        var title = notifications[nightModeEndWearing]!['title']!;
        var desc = notifications[nightModeEndWearing]!['description']!;
        var notificationTime = tz.TZDateTime.from(period.endFromNow!, tz.local);

        if (notificationTime.isAfter(now)) {
          log("Setup notification night mode end wearing");

          await flutterLocalNotificationsPlugin.zonedSchedule(
              _idNotificationEndWearing,
              title,
              desc,
              notificationTime,
              changeNotificationDetail(),
              uiLocalNotificationDateInterpretation:
                  UILocalNotificationDateInterpretation.wallClockTime,
              androidScheduleMode: AndroidScheduleMode.inexactAllowWhileIdle,
              matchDateTimeComponents: DateTimeComponents.dateAndTime);
        }
      }
    }
    await _logSetupedNotifications();
  }

  Future<void> cancelNotificationNightMode() async {
    var list =
        await flutterLocalNotificationsPlugin.pendingNotificationRequests();
    const ids = [
      _idNotificationStartNoWearing,
      _idNotificationStartWearing,
      _idNotificationEndWearing
    ];

    list.where((e) {
      return ids.contains(e.id);
    }).forEach((element) async {
      await flutterLocalNotificationsPlugin.cancel(element.id);
    });
  }

  Future<void> cancelNotification(DSNotification notification) async {
    var list =
        await flutterLocalNotificationsPlugin.pendingNotificationRequests();

    list
        .where((e) => e.id == notification.id + offsetNotificationChangeId)
        .forEach((element) async {
      await flutterLocalNotificationsPlugin.cancel(element.id);
    });
  }

  Future<void> notificationPreview(DSNotification n) async {
    if (kIsWeb || notifications.isEmpty || !await askPermissions()) return;

    String title = n.type.localizedTitle();
    String desc = n.type.localizedDescription(n.duration);

    if (n.type == NotificationType.CUSTOM) {
      title = n.title.replaceAll("%time%", durationToString(n.duration));
      desc = n.description.replaceAll("%time%", durationToString(n.duration));
    }

    await flutterLocalNotificationsPlugin.show(
      n.id + 256,
      title,
      desc,
      changeNotificationDetail(),
    );
  }

  Future<void> cancelChangeNotification({bool requestPermission = true}) async {
    if (kIsWeb ||
        notifications.isEmpty ||
        !await askPermissions(requestPermission: requestPermission)) return;
    var list =
        await flutterLocalNotificationsPlugin.pendingNotificationRequests();
    list
        .where((e) => notifications
            .where((n) => (n.id + offsetNotificationChangeId) == e.id)
            .isNotEmpty)
        .forEach((element) async {
      await flutterLocalNotificationsPlugin.cancel(element.id);
    });
  }

  Future<bool> hasChangeNotification({bool requestPermission = true}) async {
    if (kIsWeb ||
        notifications.isEmpty ||
        !await askPermissions(requestPermission: requestPermission)) {
      return false;
    }
    var list =
        await flutterLocalNotificationsPlugin.pendingNotificationRequests();
    return list
        .where((e) => notifications
            .where((n) => (n.id + offsetNotificationChangeId) == e.id)
            .isNotEmpty)
        .isNotEmpty;
  }

  Future<dynamic> computeNotification(Change? currentChange) async {
    if(currentChange == null){
      hasChangeNotification(requestPermission: false).then((has){
        if(has){
          cancelChangeNotification(requestPermission: false);
        }
      });
    }else{
      await setupNotifications(currentChange);
    }
    await setupNotificationNightMode();
  }

}
