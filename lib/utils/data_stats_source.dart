import 'package:diap_stash/common.dart';
import 'package:diap_stash/object/interfaces.dart';
import 'package:diap_stash/pages/stats_page.dart';
import 'package:diap_stash/util.dart';

class DataStatsSource extends DataTableSource {
  final List<DataStats> statsList;

  DataStatsSource(this.statsList);

  @override
  DataRow? getRow(int index) {
    var stats = statsList[index];
    return getRowStats(stats);
  }

  DataRow getRowStats(DataStats stats) {
    var index = statsList.indexOf(stats);
    var row = DataRow.byIndex(index: index, cells: [
      DataCell(Container(
        constraints: const BoxConstraints(maxWidth: 150),
        child: Text(stats.type.name,
            maxLines: 2,
            softWrap: true,
            overflow: TextOverflow.ellipsis,
            style: StatsPage.contentStyle,
            textAlign: TextAlign.left),
      )),
      DataCell(Center(
        child: Text(durationToString(stats.minDuration),
            maxLines: 1,
            softWrap: false,
            overflow: TextOverflow.fade,
            style: StatsPage.contentStyle,
            textAlign: TextAlign.center),
      )),
      DataCell(Center(
        child: Text(durationToString(stats.avgDuration),
            maxLines: 1,
            softWrap: false,
            overflow: TextOverflow.fade,
            style: StatsPage.contentStyle,
            textAlign: TextAlign.center),
      )),
      DataCell(Center(
        child: Text(durationToString(stats.maxDuration),
            maxLines: 1,
            overflow: TextOverflow.fade,
            style: StatsPage.contentStyle,
            textAlign: TextAlign.center),
      )),
      DataCell(Text("${stats.count}",
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
          style: StatsPage.contentStyle,
          textAlign: TextAlign.right)),
    ]);
    return row;
  }

  List<DataRow> rows() {
    return statsList.map((e) => getRowStats(e)).toList();
  }

  @override
  bool get isRowCountApproximate => false;

  @override
  int get rowCount => statsList.length;

  @override
  int get selectedRowCount => 0;
}

class DataStats {
  final DiaperType type;
  final Duration minDuration;
  final Duration avgDuration;
  final Duration maxDuration;
  final int count;

  const DataStats(
      {required this.type,
      required this.minDuration,
      required this.avgDuration,
      required this.maxDuration,
      required this.count});
}
