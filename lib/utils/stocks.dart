import 'dart:developer';

import 'package:diap_stash/common.dart';
import 'package:diap_stash/components/history/edit_change.dart';
import 'package:diap_stash/object/interfaces.dart';
import 'package:diap_stash/providers/diaper_provider.dart';
import 'package:diap_stash/providers/notification_provider.dart';
import 'package:diap_stash/providers/settings_provider.dart';
import 'package:sentry_flutter/sentry_flutter.dart';

Future<void> useStock(
    {required BuildContext context,
    required DiaperStock stock,
    required String size,
    required SharedStockAccess? ssa,
    required void Function() onDone}) async {
  if (ssa != null && ssa.readOnly) {
    return;
  }

  DiaperType type = stock.type;

  var scaffoldMessengerState = ScaffoldMessenger.of(context);
  var dp = Provider.of<DiaperProvider>(context, listen: false);
  var np = Provider.of<NotificationProvider>(context, listen: false);

  switch (type.type) {
    case DTType.diaper:
      var oldChange = dp.currentChange;
      Change change;

      if (ssa == null) {
        change = await dp.use(stock, size, doRemote: false, doNotifyListeners: true);
      } else {
        double? price;
        if(stock.prices.containsKey(size)){
          price = stock.prices[size];
        }
        var changeSharing = await dp.useSharingStock(ssa, stock, size, price, doNotifyListeners: true);
        if (changeSharing == null) {
          scaffoldMessengerState.showSnackBar(SnackBar(
            content: Text(I18N.current.stock_sharing_error_use),
            backgroundColor: Colors.red,
          ));
          return;
        }
        //removeDiaperOnStock(stock: stock, size: size, dp: dp);
        change = changeSharing;
      }

      if (context.mounted) {
        await dialogAskEditChange(
            context: context,
            change: change,
            onDone: () async {
              onDone();
              try{
                await np.setupNotifications(change);
                await np.setupNotificationNightMode();
              }catch(e, s){
                Sentry.captureMessage("Error setup notification after useStock", level: SentryLevel.error);
                Sentry.captureException(e, stackTrace: s);
                log("Error setup notification after useStock", error: e);
                log(e.toString());
              }
              try {
                await dp.saveStockRemote(stock: stock);
                await dp.saveSpecificChanges(specificChanges: [change, oldChange], doNotifyListeners: true);
              }catch(e, s){
                Sentry.captureMessage("Error on save stock & change remote after useStock", level: SentryLevel.error);
                Sentry.captureException(e, stackTrace: s);
                log("Error on save stock & change remote after useStock", error: e);
                log(e.toString());
              }
            },
            previousChange: oldChange,
            canUndo: true,
            onUndo: () async {
              stock.count.update(size, (value) => value+1, ifAbsent: () => 1);
              await dp.saveStock(stock: stock, doNotifyListeners: false);
              await dp.removeChange(change, doNotifyListeners: true);
              if(oldChange != null){
                oldChange.endTime = null;
                oldChange.updatedAt = DateTime.now();
                await dp.saveSpecificChanges(specificChanges: [oldChange], doNotifyListeners: true);
              }
            });
      }
      break;
    case DTType.booster:
      if (dp.currentChange == null) {
        scaffoldMessengerState.showSnackBar(SnackBar(
          content: Text(I18N.current.stock_use_booster_without_change),
          backgroundColor: Colors.red,
        ));
      } else {
        // ignore: use_build_context_synchronously
        await useBooster(
            context: context,
            stock: stock,
            type: type,
            size: size,
            ssa: ssa,
            scaffoldMessengerState: scaffoldMessengerState);
      }
      break;
  }
}

Future<void> useBooster(
    {required BuildContext context,
    required DiaperStock stock,
    required DiaperType type,
    required String size,
    required SharedStockAccess? ssa,
    required ScaffoldMessengerState scaffoldMessengerState}) async {
  SettingsProvider sp = Provider.of(context, listen: false);
  DiaperProvider dp = Provider.of(context, listen: false);
  if (ssa == null) {
    var count = stock.count[size]!;
    if (count != DiaperStock.infinity) {
      stock.count[size] = count - 1;
    }
    if (sp.keepZeroStockDiaper == "never" ||
        (sp.keepZeroStockDiaper == "favorite" && !dp.isFavoriteType(type))) {}
    if (stock.total() == 0) {
      await dp.removeStock(stock, doNotifyListeners: true);
      return;
    }
    stock.updatedAt = DateTime.now();
    await dp.saveStock(stock: stock, doNotifyListeners: true);
  } else {
    var ok = await dp.useSharingStockRemote(ssa, stock, size, null);
    if (!ok) {
      return;
    }
  }


  double? price;
  if(stock.prices.containsKey(size)){
    price = stock.prices[size];
  }

  var change = dp.currentChange!;
  change.addDiaper(stock.type, size, price);
  change.updatedAt = DateTime.now();
  await dp.saveChanges(doRemote: false, doNotifyListeners: true);
  await dp.saveSpecificChangesRemote([change]);
  scaffoldMessengerState.showSnackBar(SnackBar(
    content: Text(I18N.current.stock_use_booster_added_change),
  ));
}

removeDiaperOnStock(
    {required DiaperStock stock,
    required String size,
    required DiaperProvider dp}) {
  var left = stock.count[size]!;
  if (left != DiaperStock.infinity) {
    left--;
    stock.count[size] = left;
    var keepZeroDiaper = SettingsProvider.instance.keepZeroStockDiaper;
    if (keepZeroDiaper == "never" ||
        (keepZeroDiaper == "favorite" && !dp.isFavoriteType(stock.type))) {
      if (left == 0) {
        stock.count.remove(size);
      }
    }
    stock.updatedAt = DateTime.now();
  }
}
