import 'dart:convert';

import 'package:diap_stash/common.dart';

// ignore: depend_on_referenced_packages
import 'package:intl/intl.dart';

var _format = DateFormat.Hm();

class Period {
  DateTime? start;
  DateTime? end;

  Period({required this.start, required this.end});

  bool get isValid => start != null && end != null;

  String? get startString => startFormated(_format);

  String? get endString => endFormated(_format);

  String? startFormated(DateFormat format) => start != null ? format.format(start!) : "";
  String? endFormated(DateFormat format) => end != null ? format.format(end!) : "";

  DateTime? get endFromNow {
    if (!isValid) return null;
    var e = DateTime.now().copyWith(hour: end!.hour, minute: end!.minute, second: 0);
    if (start!.isAfter(end!)) {
      e = e.add(const Duration(days: 1));
    }
    return e;
  }

  DateTime? get startFromNow {
    if (!isValid) return null;
    var s = DateTime.now().copyWith(hour: start!.hour, minute: start!.minute, second: 0);
    return s;
  }

  toJSON() {
    return jsonEncode({
      "start": start != null ? _format.format(start!) : null,
      "end": end != null ? _format.format(end!) : null
    });
  }

  factory Period.fromJSON(String json) {
    Map<String, dynamic> map = jsonDecode(json);
    DateTime? start;
    DateTime? end;
    if (map.containsKey("start") && map["start"] != null) {
      var str = map["start"]!;
      start = _format.parse(str);
    }
    if (map.containsKey("end") && map["end"] != null) {
      var str = map["end"]!;
      end = _format.parse(str);
    }
    return Period(start: start, end: end);
  }

  bool isInclude(DateTime dateTime) {
    if (!isValid) return false;

    var time = DateTime(
        start!.year, start!.month, start!.day, dateTime.hour, dateTime.minute, 0);
    var s = start!;
    var e = end!;
    if (e.isBefore(s)) {
      e = e.add(const Duration(days: 1));
    }
    return time.isAfter(s) && time.isBefore(e);
  }
}
