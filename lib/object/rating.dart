import 'package:diap_stash/common.dart';
import 'package:diap_stash/database/database.dart';
import 'package:diap_stash/object/interfaces.dart';

enum RatingCriteria {
  overall,
  durability,
  noise,
  comfort,
  thickness;

  String get title {
    switch(this){
      case overall:
        return I18N.current.rating_overall;
      case durability:
        return I18N.current.rating_durability;
      case noise:
        return I18N.current.rating_noise;
      case comfort:
        return I18N.current.rating_comfort;
      case thickness:
        return I18N.current.rating_thickness;
      default:
        return "";
    }
  }

  RatingBottomLegend? get bottomLegend {
    switch(this){
      case noise:
        return RatingBottomLegend(left: I18N.current.rating_noise_left, right:  I18N.current.rating_noise_right);
      case thickness:
        return RatingBottomLegend(left: I18N.current.rating_thickness_left, right:  I18N.current.rating_thickness_right);
      default:
        return null;
    }
  }



}

class RatingBottomLegend {
  final String left;
  final String right;

  const RatingBottomLegend({required this.left, required this.right});
}

class Rating {
  final DiaperType type;
  final RatingCriteria criteria;
  final double note;

  Rating({required this.type, required this.criteria, required this.note});
}

class UserRating extends Rating {
  final String? id;
  final DateTime updatedAt;

  UserRating(
      {required this.id,
      required super.type,
      required super.criteria,
      required super.note,
      required this.updatedAt});

  UserRatingTableData? toDB() {
    if (id == null) {
      return null;
    }
    return UserRatingTableData(
        id: id!,
        typeId: type.id,
        criteria: criteria.name,
        note: note,
        updatedAt: updatedAt);
  }

  UserRating copyWith({String? id, double? note, DateTime? updatedAt}) {
    return UserRating(
        id: id ?? this.id,
        type: type,
        criteria: criteria,
        note: note ?? this.note,
        updatedAt: updatedAt ?? this.updatedAt);
  }
}
