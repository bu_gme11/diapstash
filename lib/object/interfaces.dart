// ignore_for_file: hash_and_equals

import 'dart:core';
import 'dart:convert' as convert;

import 'package:azlistview/azlistview.dart';
import 'package:diap_stash/common.dart';
import 'package:diap_stash/database/database.dart';
import 'package:diap_stash/providers/diaper_provider.dart';

class User {
  final String id;
  final String name;
  final String secureToken;

  const User(this.id, this.name, this.secureToken);

  saveStore() {}
}

class DiaperBrand extends ISuspensionBean {
  static const tableName = "DiaperBrand";
  final String code;
  final String name;
  final String? image;

  DiaperBrand({required this.code, required this.name, this.image});

  Map<String, dynamic> toMap() {
    return {'name': name, 'code': code, 'image': image};
  }

  DiaperBrandTableData toDB() {
    return DiaperBrandTableData(code: code, name: name, image: image);
  }

  factory DiaperBrand.fromMap(Map<String, dynamic> map) {
    return DiaperBrand(
        code: map["code"], name: map["name"], image: map["image"]);
  }

  factory DiaperBrand.fromDB(DiaperBrandTableData map) {
    return DiaperBrand(code: map.code, name: map.name, image: map.image);
  }

  // ignore: non_constant_identifier_names
  factory DiaperBrand.favoriteBrand() =>
      DiaperBrand(code: "_FAVORITE", name: I18N.current.favorite);

  factory DiaperBrand.otherBrand() =>
      DiaperBrand(code: "OTHER", name: I18N.current.brand_other);

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is DiaperBrand &&
          runtimeType == other.runtimeType &&
          code == other.code;

  @override
  int get hashCode => code.hashCode;

  @override
  String getSuspensionTag() {
    if (code == "_FAVORITE") {
      return "☆";
    } else if (code == "OTHER") {
      return "+";
    }
    return name[0];
  }
}

enum DTType {
  diaper(restValue: "DIAPER"),
  booster(restValue: "BOOSTER");

  const DTType({required this.restValue});

  final String restValue;

  String localizedNamed() {
    switch (this) {
      case diaper:
        return I18N.current.diaper;
      case booster:
        return I18N.current.booster;
    }
  }

  String localizedNamedPlurial() {
    switch (this) {
      case diaper:
        return I18N.current.diapers;
      case booster:
        return I18N.current.boosters;
    }
  }

  static DTType fromRest(String name) {
    return DTType.values.firstWhere((element) => element.restValue == name,
        orElse: () => DTType.diaper);
  }
}

enum DiaperStyle {
  tabs(restValue: "TABS"),
  pullups(restValue: "PULLUP");

  const DiaperStyle({required this.restValue});

  final String restValue;

  String localizedNamed() {
    switch (this) {
      case tabs:
        return I18N.current.style_tabs;
      case pullups:
        return I18N.current.style_pullup;
    }
  }

  static DiaperStyle fromRest(String name) {
    return DiaperStyle.values.firstWhere((element) => element.restValue == name,
        orElse: () => DiaperStyle.tabs);
  }
}

enum DiaperTarget {
  abdl(restValue: "ABDL"),
  medical(restValue: "MEDICAL"),
  youth(restValue: "YOUTH");

  const DiaperTarget({required this.restValue});

  final String restValue;

  String localizedNamed() {
    switch (this) {
      case abdl:
        return I18N.current.target_abdl;
      case medical:
        return I18N.current.target_medical;
      case youth:
        return I18N.current.target_youth;
    }
  }

  static DiaperTarget fromRest(String name) {
    return DiaperTarget.values.firstWhere(
        (element) => element.restValue == name,
        orElse: () => DiaperTarget.abdl);
  }

  static DiaperTarget parse(String name) {
    return DiaperTarget.values.firstWhere(
            (element) => element.name == name,
        orElse: () => DiaperTarget.abdl);
  }
}

class DiaperType {
  static const tableName = "DiaperType";
  int id;
  DiaperBrand? brand;
  String? brandCode;
  final String name;
  final String? image;
  final List<String> availableSizes;
  final bool official;
  final DTType type;
  final DiaperStyle? style;
  final List<DiaperTarget> target;
  final bool hidden;
  final bool discontinued;

  DiaperType({
    required this.id,
    required this.name,
    this.image,
    required this.availableSizes,
    required this.official,
    this.brand,
    this.brandCode,
    this.type = DTType.diaper,
    this.style,
    required this.target,
    required this.hidden,
    required this.discontinued,
  }) {
    if (brand != null && brandCode == null) {
      brandCode = brand!.code;
    }
  }

  String get fullName {
    String fn = name;
    if (brand != null) {
      fn += " (${brand!.name})";
    }
    return fn;
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'name': name,
      'image': image,
      'availableSizes': availableSizes,
      'official': official,
      'brand_code': brand?.code,
      "type": type.restValue,
      "style": style?.restValue,
      "target": target.map((e) => e.restValue).toList(),
      "hidden": hidden,
      "discontinued": discontinued
      //'brand': brand?.toMap()
    };
  }

  DiaperTypeTableData toDB() {
    var rawTarget =
        convert.jsonEncode(target.map((e) => e.name).toList(growable: false));
    return DiaperTypeTableData(
        id: id,
        brand_code: brandCode,
        name: name,
        availableSizes: convert.jsonEncode(availableSizes),
        official: official,
        image: image,
        type: type,
        style: style,
        target: rawTarget,
        hidden: hidden,
        discontinued: discontinued);
  }

  factory DiaperType.fromDB(DiaperTypeTableData map) {
    List<DiaperTarget> target = [];
    if (map.target != null) {
      target = List<String>.from(convert.jsonDecode(map.target!))
          .map((e) => DiaperTarget.parse(e))
          .toList();
    }

    return DiaperType(
        id: map.id,
        name: map.name,
        image: map.image,
        availableSizes:
            List<String>.from(convert.jsonDecode(map.availableSizes!)),
        official: map.official,
        brandCode: map.brand_code,
        type: map.type,
        style: map.style,
        target: target,
        hidden: map.hidden,
        discontinued: map.discontinued);
  }

  factory DiaperType.fromMap(Map<String, dynamic> map) {
    DTType type = DTType.diaper;
    if (map.containsKey("type")) {
      type = DTType.fromRest(map["type"]);
    }

    DiaperStyle? style;
    if (map.containsKey("style") && map["style"] != null) {
      style = DiaperStyle.fromRest(map["style"]);
    }

    List<DiaperTarget> target = [];
    if (map.containsKey("target")) {
      List<String> mapTarget = List<String>.from(map["target"]);
      target = mapTarget.map((e) => DiaperTarget.fromRest(e)).toList();
    }

    return DiaperType(
        id: map["id"],
        name: map["name"],
        image: map["image"],
        brandCode: map["brand_code"],
        availableSizes: List<String>.from(map["availableSizes"]),
        official: map["official"],
        type: type,
        style: style,
        target: target,
        hidden: map["hidden"],
        discontinued: map["discontinued"]);
  }

  factory DiaperType.unknown() {
    return DiaperType(
        id: -999,
        name: I18N.current.type_unknown,
        type: DTType.diaper,
        availableSizes: [],
        official: false,
        target: [],
        hidden: false,
        discontinued: false);
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is DiaperType && runtimeType == other.runtimeType && id == other.id;

  @override
  int get hashCode => id.hashCode;

  canShow(List<DTType> filterDiaperType, List<DiaperTarget> filterDiaperTarget,
      List<DiaperStyle> filterDiaperStyle, List<bool> filterDiscontinued) {
    if (filterDiaperType.isNotEmpty && !filterDiaperType.contains(type)) {
      return false;
    }

    if (target.isNotEmpty &&
        filterDiaperTarget.isNotEmpty &&
        filterDiaperTarget.where((t) {
          return target.contains(t);
        }).isEmpty) {
      return false;
    }

    if (style != null &&
        filterDiaperStyle.isNotEmpty &&
        !filterDiaperStyle.contains(style)) {
      return false;
    }

    if (filterDiscontinued.isNotEmpty &&
        !filterDiscontinued.contains(discontinued)) {
      return false;
    }

    return true;
  }

  int compareTo(DiaperType b) {
    if (brand != null && b.brand != null) {
      if (brand != b.brand) {
        return brand!.name.compareTo(b.brand!.name);
      }
    } else if (brand != null) {
      return 1;
    } else if (b.brand != null) {
      return -1;
    }
    return name.compareTo(b.name);
  }
}

class DiaperStock {
  static const tableName = "DiaperStock";
  static const infinity = -888;
  final DiaperType type;
  Map<String, int> count = {};
  Map<String, double> prices = {};
  DateTime? updatedAt;

  DiaperStock(
      {required this.type,
      required this.count,
      required this.prices,
      this.updatedAt}) {
    updatedAt ??= DateTime.now();
  }

  toMap() {
    return {
      "type": type.toMap(),
      "count": convert.jsonEncode(count),
      "prices": convert.jsonEncode(prices)
    };
  }

  toMapRest() {
    var counts = {};
    for (var entry in count.entries) {
      counts[entry.key] = entry.value.toString();
    }
    return {"diaperTypeId": type.id, "counts": count, "prices": prices};
  }

  DiaperStockTableData toDB() {
    return DiaperStockTableData(
        type_id: type.id,
        counts: convert.jsonEncode(count),
        prices: convert.jsonEncode(prices),
        updatedAt: updatedAt);
  }

  int total() {
    var sum = 0;
    for (var val in count.values) {
      if(val > 0) {
        sum += val;
      }
    }
    return sum;
  }

  double totalSort() {
    var sum = 0.0;
    for (var val in count.values) {
      if (val == DiaperStock.infinity) {
        sum += double.infinity;
      } else {
        sum += val;
      }
    }
    return sum;
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is DiaperStock &&
          runtimeType == other.runtimeType &&
          type == other.type;

  @override
  int get hashCode => type.hashCode;
}

enum ChangeState {
  dry(restValue: "DRY"),
  wet(restValue: "WET", isWet: true),
  messy(restValue: "MESSY", isMessy: true),
  wetMessy(restValue: "WET_MESSY", isWet: true, isMessy: true);

  const ChangeState(
      {required this.restValue, this.isWet = false, this.isMessy = false});

  final String restValue;
  final bool isWet;
  final bool isMessy;

  String localizedNamed() {
    switch (this) {
      case dry:
        return I18N.current.change_state_dry;
      case wet:
        return I18N.current.change_state_wet;
      case messy:
        return I18N.current.change_state_messy;
      case wetMessy:
        return I18N.current.change_state_wet_messy;
    }
  }
}

class Change {
  static const tableName = "Change";
  int? id;
  DateTime startTime;
  DateTime? endTime;
  List<DiaperChange> diapers = <DiaperChange>[];
  ChangeState? state;
  String? note;
  List<String>? tags = [];
  double? wetness;
  DateTime? updatedAt;

  Change(
      {this.id,
      required this.startTime,
      this.endTime,
      this.state,
      this.note,
      this.tags,
      this.wetness,
      this.updatedAt}) {
    updatedAt ??= DateTime.now();
  }

  factory Change.fromDB(ChangeTableData map) {
    return Change(
        id: map.id,
        startTime: DateTime.parse(map.startTime!)
            .toLocal()
            .copyWith(second: 0, millisecond: 0, microsecond: 0),
        endTime: map.endTime != null
            ? DateTime.parse(map.endTime!)
                .toLocal()
                .copyWith(second: 0, millisecond: 0, microsecond: 0)
            : null,
        note: map.note,
        state: map.state,
        tags: map.tags,
        wetness: map.wetness,
        updatedAt: map.updatedAt);
  }

  double? get price {
    double? p;
    for (var dc in diapers) {
      if (dc.price != null) {
        if (p != null) {
          p += dc.price!;
        } else {
          p = dc.price!;
        }
      }
    }
    return p;
  }

  addDiaper(DiaperType type, String size, double? price) {
    var dc = DiaperChange(type: type, size: size, change: this, price: price);
    diapers.add(dc);
  }

  ChangeTableData toDB() {
    return ChangeTableData(
        id: id ?? -1,
        startTime: startTime.toUtc().toIso8601String(),
        endTime: endTime?.toUtc().toIso8601String(),
        note: note,
        state: state,
        tags: tags,
        wetness: wetness,
        updatedAt: updatedAt);
  }

  Map<String, dynamic> toCloud(String userId) {
    var diapers = this.diapers.map((d) => d.toCloud(userId)).toList();
    return {
      "id": id,
      "startTime": startTime.toUtc().toIso8601String(),
      "endTime": endTime?.toUtc().toIso8601String(),
      "user_id": userId,
      "diapers": diapers,
      "note": note,
      "state": state?.restValue,
      "tags": tags,
      "wetness": wetness
    };
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is Change && runtimeType == other.runtimeType && id == other.id;

  factory Change.fromCloud(Map<String, dynamic> rawChange, DiaperProvider dp) {
    ChangeState? state;

    if (rawChange.containsKey("state")) {
      var rawState = rawChange["state"] as String?;
      switch (rawState) {
        case "DRY":
          state = ChangeState.dry;
          break;
        case "WET":
          state = ChangeState.wet;
          break;
        case "MESSY":
          state = ChangeState.messy;
          break;
        case "WET_MESSY":
          state = ChangeState.wetMessy;
          break;
      }
    }

    List<String>? tags;
    if (rawChange.containsKey("tags")) {
      tags = List<String>.from(rawChange["tags"]);
    }

    double? wetness;

    DateTime? updatedAt;
    if (rawChange.containsKey("updatedAt")) {
      var updatedAtStr = rawChange["updatedAt"];
      if (updatedAtStr != null) {
        updatedAt = DateTime.tryParse(updatedAtStr);
        if (updatedAt != null) {
          updatedAt = updatedAt.toLocal();
        }
      }
    }

    if (rawChange.containsKey("wetness") && rawChange["wetness"] != null) {
      var rawVal = rawChange["wetness"];
      if (rawVal is int || rawVal is double) {
        wetness = rawVal.toDouble();
      }
    }

    Change change = Change(
        id: rawChange["id"]!,
        startTime: DateTime.parse(rawChange["startTime"]!)
            .toLocal()
            .copyWith(second: 0, millisecond: 0, microsecond: 0),
        note: rawChange["note"],
        state: state,
        tags: tags,
        wetness: wetness,
        updatedAt: updatedAt);
    if (rawChange.containsKey("endTime") && rawChange["endTime"] != null) {
      change.endTime = DateTime.parse(rawChange["endTime"])
          .toLocal()
          .copyWith(second: 0, millisecond: 0, microsecond: 0);
    }
    for (Map<String, dynamic> rawDiaper in rawChange["diapers"]) {
      var types = dp.diaperTypes.where((t) => t.id == rawDiaper["type_id"]);
      var type = types.isNotEmpty ? types.first : DiaperType.unknown();
      var count = rawDiaper["count"] as int;
      double? price;
      if (rawDiaper.containsKey("price") && rawDiaper["price"] != null) {
        price = double.tryParse("${rawDiaper["price"]}");
      }
      for (var i = 0; i < count; i++) {
        change.addDiaper(type, rawDiaper["size"], price);
      }
    }
    return change;
  }

  List<dynamic> toCSV() {
    var data = [
      startTime.toIso8601String(),
      endTime?.toIso8601String(),
      (tags ?? []).join(','),
      state?.restValue ?? "",
      price?.toString() ?? ""
    ];
    for (var d in diapers) {
      data.addAll([
        d.type.brand?.code,
        d.type.brand?.name,
        d.type.name,
        d.size,
        d.price?.toString() ?? ""
      ]);
    }
    return data;
  }
}

class DiaperChange {
  static const tableName = "DiaperChange";
  final DiaperType type;
  final String size;
  final Change change;
  final double? price;

  DiaperChange(
      {required this.type,
      required this.size,
      required this.change,
      this.price});

  DiaperChangeTableData toDB() {
    return DiaperChangeTableData(
        change_id: change.id!, type_id: type.id, size: size, price: price);
  }

  Map<String, dynamic> toCloud(String userId) {
    return {
      "type_id": type.id,
      "size": size,
      "change_id": change.id,
      "user_id": userId,
      "price": price
    };
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is DiaperChange &&
          runtimeType == other.runtimeType &&
          type == other.type &&
          change == other.change &&
          size == other.size;
}

class SharedHistory {
  final int id;
  final String securityToken;
  final String toName;
  final String state;

  SharedHistory(this.id, this.securityToken, this.toName, this.state);

  factory SharedHistory.fromRest(Map<String, dynamic> map) {
    return SharedHistory(
        map["id"], map["securityToken"], map["toName"], map["state"]);
  }

  String shareText() {
    return "${I18N.current.history_sharing_text}: \n\n https://diapstash.com/history-sharing/$id/$securityToken";
  }
}

class SharedHistoryAccess {
  final int id;
  final String securityToken;
  final String fromName;

  SharedHistoryAccess(this.id, this.securityToken, this.fromName);

  factory SharedHistoryAccess.fromRest(Map<String, dynamic> map) {
    return SharedHistoryAccess(
        map["id"], map["securityToken"], map["fromName"]);
  }

  factory SharedHistoryAccess.fromDb(SharedHistoryAccessTableData data) {
    return SharedHistoryAccess(data.id, data.securityToken, data.fromName);
  }

  SharedHistoryAccessTableData toDB() {
    return SharedHistoryAccessTableData(
        id: id, securityToken: securityToken, fromName: fromName);
  }
}

class SharedStock {
  final int id;
  final String securityToken;
  final String toName;
  final String state;
  bool readOnly;

  SharedStock(
      {required this.id,
      required this.securityToken,
      required this.toName,
      required this.state,
      this.readOnly = false});

  factory SharedStock.fromRest(Map<String, dynamic> map) {
    return SharedStock(
        id: map["id"],
        securityToken: map["securityToken"],
        toName: map["toName"],
        state: map["state"],
        readOnly: map["readonly"] ?? true);
  }

  Map<String, dynamic> toRest() {
    return {
      "id": id,
      "securityToken": securityToken,
      "toName": toName,
      "state": state,
      "readonly": readOnly
    };
  }

  String shareText() {
    return "${I18N.current.stock_sharing_text}: \n\n https://diapstash.com/stock-sharing/$id/$securityToken";
  }
}

class SharedStockAccess {
  final int id;
  final String securityToken;
  final String fromName;
  final bool readOnly;

  SharedStockAccess(
      {required this.id,
      required this.securityToken,
      required this.fromName,
      this.readOnly = true});

  factory SharedStockAccess.fromRest(Map<String, dynamic> map) {
    return SharedStockAccess(
        id: map["id"],
        securityToken: map["securityToken"],
        fromName: map["fromName"] ?? "",
        readOnly: map["readonly"] as bool);
  }

  factory SharedStockAccess.fromDb(SharedStockAccessTableData data) {
    return SharedStockAccess(
        id: data.id,
        securityToken: data.securityToken,
        fromName: data.fromName,
        readOnly: data.readonly);
  }

  SharedStockAccessTableData toDB() {
    return SharedStockAccessTableData(
        id: id,
        securityToken: securityToken,
        fromName: fromName,
        readonly: readOnly);
  }
}

class TypeInfo {
  final DiaperType type;
  int? thresholdLowStock;
  String? note;

  TypeInfo({required this.type, this.thresholdLowStock, this.note});

  Map<dynamic, dynamic> toRest() {
    return {
      "typeId": type.id,
      "thresholdLowStock": thresholdLowStock,
      "note": note
    };
  }

  TypeInfoTableData toDB() {
    return TypeInfoTableData(
        typeId: type.id, thresholdLowStock: thresholdLowStock, note: note);
  }
}

class WhatsNewLog {
  final int buildCode;
  final String releaseName;
  final String title;
  final String description;
  final String? image;
  final Map<String, Map<String, String>> messages;

  WhatsNewLog(
      {required this.buildCode,
      required this.releaseName,
      required this.title,
      required this.description,
      required this.image,
      required this.messages});
}
