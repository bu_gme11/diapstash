// ignore_for_file: constant_identifier_names, non_constant_identifier_names

import 'dart:developer';

import 'package:diap_stash/object/interfaces.dart';
import 'package:diap_stash/providers/diaper_provider.dart';

enum ProposalState {

  WAITING,
  APPROVED,
  REJECTED;
}

class Proposal {
  final String? id;
  final String name;
  final String? brand_code;

  DiaperBrand? brand;
  final String? image;
  final List<String> availableSizes;

  final DTType type;

  final ProposalState state;
  final String? comment;
  final String? reason;

  final DiaperStyle? style;
  final List<DiaperTarget> target;



  final int? original_type_id;
  DiaperType? original_type;

  final int? result_type_id;
  DiaperType? result_type;

  Proposal({this.id,
    required this.name,
    this.brand_code,
    this.image,
    required this.availableSizes,
    required this.type,
    required this.state,
    this.comment,
    this.reason,
    this.style,
    required this.target,
    this.original_type_id,
    this.original_type,
    this.result_type_id});


  toRest() {
    var map =  {
      "name": name,
      "brand_code": brand_code,
      "image": image,
      "availableSizes": availableSizes,
      "type": type.restValue,
      "comment": comment,
      "originalTypeId": original_type_id,
      "style": style?.restValue,
      "target": target.map((e) => e.restValue).toList(growable: false)
    };

    if(id != null){
      map["id"] = id;
    }

    return map;
  }

  factory Proposal.fromType(DiaperType type, String? comment){
    return Proposal(
      name: type.name,
      brand_code: type.brandCode,
      image: type.image,
      availableSizes: type.availableSizes,
      type: type.type,
      comment: comment,
      state: ProposalState.WAITING,
      original_type: type,
      original_type_id: type.id,
      style: type.style,
      target: type.target
    );
  }

  factory Proposal.fromRest(Map<String, dynamic> map, DiaperProvider dp){
    log(map.toString());
    var typeProposal = Map<String, dynamic>.from(map["typeProposal"]);
    List<String> availableSizes = (typeProposal['availableSizes'] as List<dynamic>).map((item) => item as String).toList();
    var proposal = Proposal(
        id: map["id"],
        name: typeProposal["name"],
        brand_code: typeProposal["brand_code"],
        availableSizes: availableSizes,
        state: ProposalState.values.byName(map["state"]),
        type: DTType.fromRest(typeProposal["type"]),
        style: typeProposal.containsKey("style") && typeProposal["style"] != null ? DiaperStyle.fromRest(typeProposal["style"]) : null,
        target: List<String>.from(typeProposal["target"]).map((e) => DiaperTarget.fromRest(e)).toList(),
        comment: typeProposal["comment"],
        reason: map["reason"],
        original_type_id: typeProposal["original_type_id"],
        result_type_id: typeProposal["result_type_id"],
    );

    if(proposal.brand_code != null){
      var possibilities = dp.brands.where((element) => element.code == proposal.brand_code!);
      if(possibilities.isNotEmpty){
        proposal.brand = possibilities.first;
      }
    }
    if(proposal.original_type_id != null){
      var possibilities = dp.diaperTypes.where((element) => element.id == proposal.original_type_id!);
      if(possibilities.isNotEmpty){
        proposal.original_type = possibilities.first;
      }
    }
    if(proposal.result_type_id != null){
      var possibilities = dp.diaperTypes.where((element) => element.id == proposal.result_type_id!);
      if(possibilities.isNotEmpty){
        proposal.result_type = possibilities.first;
      }
    }

    return proposal;
  }

}
