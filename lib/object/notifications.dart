// ignore_for_file: constant_identifier_names

import 'package:diap_stash/common.dart';
import 'package:diap_stash/database/database.dart';
import 'package:diap_stash/util.dart';

enum NotificationType {
  REMINDER,
  FORGET,
  REMINDER_DISCRET,
  FORGET_DISCRET,
  CUSTOM;

  String localizedName() {
    switch (this) {
      case REMINDER:
        return I18N.current.notification_type_reminder;
      case FORGET:
        return I18N.current.notification_type_forget;
      case REMINDER_DISCRET:
        return I18N.current.notification_type_reminder_discret;
      case FORGET_DISCRET:
        return I18N.current.notification_type_forget_discret;
      case CUSTOM:
        return I18N.current.notification_type_custom;
    }
  }

  String localizedTitle() {
    switch (this) {
      case REMINDER:
        return I18N.current.notification_title_reminder;
      case FORGET:
        return I18N.current.notification_title_forget;
      case REMINDER_DISCRET:
        return I18N.current.notification_title_reminder_discret;
      case FORGET_DISCRET:
        return I18N.current.notification_title_forget_discret;
      case CUSTOM:
        return "";
    }
  }

  String localizedDescription(Duration duration) {
    switch (this) {
      case REMINDER:
        return I18N.current
            .notification_desc_reminder(durationToString(duration));
      case FORGET:
        return I18N.current
            .notification_desc_forget(durationToString(duration));
      case REMINDER_DISCRET:
        return I18N.current
            .notification_desc_reminder_discret(durationToString(duration));
      case FORGET_DISCRET:
        return I18N.current
            .notification_desc_forget_discret(durationToString(duration));
      case CUSTOM:
        return "";
    }
  }
}

class DSNotification {
  int id;
  NotificationType type;
  Duration duration;
  String title;
  String description;
  int? typeId;

  DSNotification(
      {required this.id,
      required this.type,
      required this.duration,
      required this.title,
      required this.description,
      this.typeId});

  factory DSNotification.fromDB(NotificationTableData data) {
    return DSNotification(
        id: data.id,
        type: data.type,
        duration: Duration(minutes: data.duration),
        title: data.title,
        description: data.description,
        typeId: data.typeId
    );
  }

  NotificationTableData toDB() {
    return NotificationTableData(
        id: id,
        type: type,
        duration: duration.inMinutes,
        title: title,
        description: description,
        typeId: typeId
    );
  }
}
