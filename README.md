# DiapStash
DiapStash © 2023 by RanTigr is licensed under CC BY-NC-ND 4.0

> **Distribution of the application outside the official channels (Android Play Store & https://app.diapstash.com) is strictly forbidden, including for a modified version of the application!**

[Website](https://diapstash.com) - [Android App](https://play.google.com/store/apps/details?id=fr.diapstash.diap_stash) - [Web App (_unstable_)](https://app.diapstash.com)

[Reddit community](https://reddit.com/r/diapstash/) - [Help to translate on Crowdin](https://crowdin.com/project/diapstash) - [Gitlab](https://gitlab.com/diapstash)

Track your stash
Diaper Stock Management for ABDL and incontient

The application allows the tracking of the stock of diapers, the tracking of the history of diaper changes.

Created by [RanTigr](https://linktr.ee/rantigr).


## Development / Contributing

### Requirements

* flutter : latest stable version
* cider : https://pub.dev/packages/cider

### Guidelines

* Each development must be entered in the [changelog](#changelog).
* Each character string must be in the translation. The source of the translation is the English translation file [app_en.arb](./lib/l10n/app_en.arb)
* Each modification to the internal database schema must be the subject of a migration. (cf : `lib/database/database.dart` method `migrate`)

### Dev

Development requires a local version of the backend (sources not yet available).

To use your local backend, you need to define the `DEV_URL` variable (e.g. `http://127.0.0.1:8083`).
To do this, add a to the flutter commands `--dart-define=DEV_URL=http://<your local ip>:8083`.

Before start app, you need to [build the databases files](#build-database-files)

### Commands

### Build database files
```
flutter pub run build_runner build
```

### Download translation

[Help to translate on Crowdin](https://crowdin.com/project/diapstash)

```
crowdin download --verbose --export-only-approved --skip-untranslated-strings -l en -l fr -l es-ES -l nl -l de -l ko -l it
```

### Changelog

Each feature or patch must be added to the changelog.

Please use [cider](https://pub.dev/packages/cider) to generate changelog : `cider log <fixed|added|changed> "<informations>"`.

### Git

#### Branches

* `main` : The `main` branch are the "production" branch. It corresponds to the version deployed on https://app.diapstash.com and normally delivered on android.
* `dev` : This is the development branch linked to the major production version. It contains any patches or features not yet delivered.
* `v1.<minor>`: These are the development branches for future versions. It contains the features that will compose the next version. They evolve in parallel with the `dev` branch.

#### Naming convention for commits

DiapStash repositories use [Conventional Commits](https://www.conventionalcommits.org/en/v1.0.0/). Please respect it !


## Roadmap

### 1.17

* [ ] Rating (local or public ?)

### Not yet Planned

* [ ] Share wearing as image
* [ ] Full discrete mode (hide picture?)

### Done

[See history roadmap](./HISTORY_ROADMAP.md)