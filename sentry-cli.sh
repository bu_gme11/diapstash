#! /bin/bash

package="fr.diapstash.diap_stash"
version=$(yq '.version' pubspec.yaml)

sentryRelease="${package}@${version}"

echo "Preparing version : $sentryRelease";
read -p "Press [Enter] key to start..."

set -x
sentry-cli releases new $sentryRelease
git push
sentry-cli releases set-commits $sentryRelease --local
# sentry-cli releases finalize $sentryRelease