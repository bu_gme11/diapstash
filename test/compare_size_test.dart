// This is a basic Flutter widget test.
//
// To perform an interaction with a widget in your test, use the WidgetTester
// utility in the flutter_test package. For example, you can send tap and scroll
// gestures. You can also use WidgetTester to find child widgets in the widget
// tree, read text, and verify that the values of widget properties are correct.

import 'package:diap_stash/util.dart';
import 'package:flutter_test/flutter_test.dart';


void main() {
  test("compareSize", (){
    expect(0, compareSize("S", "S"));
    expect(-1, compareSize("XS", "S"));
    expect(1, compareSize("S", "XS"));
    expect(1, compareSize("XL", "L"));
    expect(1, compareSize("XXL", "XL"));
    expect(-1, compareSize("XXS", "XXL"));
    expect(0, compareSize("XXS", "2XS"));
    expect(-1, compareSize("12XS", "2XL"));
    expect(0, compareSize("XXL", "2XL"));
    expect(-1, compareSize("12XS", "XXS"));
    expect(1, compareSize("S/M", "M/L"));
    expect(-1, compareSize("M/L", "S/M"));
    expect(0, compareSize("M/L", "M/L"));
  });
}
