## 1.17.11+182 - 2023-12-11
### Added
- Can undo remove change

## 1.17.10+181 - 2023-12-05
### Added
- Stats: add total change price

### Fixed
- Fix svg of water drop on web/ios

## 1.17.9+180 - 2023-12-01
### Fixed
- Set price when add diaper when edit change

## 1.17.8+179 - 2023-11-30
### Added
- Access rating from stock dialog
- Add Gitlab and /catalog links on settings > about

### Changed
- Rewrite DialogYesNo to return enum via navigator
- Rewrite notifcation tile in settings

## 1.17.6+177 - 2023-11-26
### Changed
- Rewrite proposal api

## 1.17.5+176 - 2023-11-18
### Fixed
- Fix diaper count with infinite stock

## 1.17.4+175 - 2023-11-17
### Changed
- Show propagate change time dialog with after prev change & before time next time

### Fixed
- Hide favorite in catalog if not favorite type match the filter
- Stats: fix avg change price calc
- Fix currency symbol to use device local
- Delete history wasn't deleted
- Show propagate after edit when just start a change from stock

## 1.17.2+173 - 2023-11-14
### Changed
- Show type information aside the catalog list on web & tablets

### Fixed
- Show threshold low stock settings on web

## 1.17.0+170 - 2023-11-09
### Added
- Rating of diaper types

### Changed
- Alert when you try merge with same type
- Type information on catalog are now a subpage instead of a dialog
- Edit and create change are now a subpage instead of dialog

## 1.16.9+169 - 2023-10-22
### Added
- Cloud-Sync : ask confirmation before use cloud-sync link

### Changed
- Optimize cloud sync deactivation

## 1.16.8+167 - 2023-10-20
### Fixed
- Stats: Fix total time in diaper calc

## 1.16.7+166 - 2023-10-16
### Fixed
- Resetup notification on use

## 1.16.7+165 - 2023-10-16
### Added
- Stats :Add min time on detailed table

### Changed
- Stats: Calc percentage in diaper on selected period

### Fixed
- Save favorite from cloud
- Load right targets from internal database

## 1.16.6+164 - 2023-10-13
### Changed
- Stats : always display period selector

### Fixed
- Stats: update ui when history change

## 1.16.5+163 - 2023-10-10
### Changed
- Reduce notifyListeners on diaper provider

## 1.16.4+162 - 2023-10-09
### Changed
- Upgrade deps

## 1.16.3+161 - 2023-10-07
### Added
- Export price in history csv
- Can edit diaper price in change
- Export stock as CSV

## 1.16.2+159 - 2023-10-05
### Fixed
- Stats: The period selector had disappeared

## 1.16.1+158 - 2023-10-05
### Fixed
- Allow comma in price field

## 1.16.0+157 - 2023-10-03
### Added
- Stats : add ratio by state
- Diaper price

### Changed
- Stats: Change detailed table
- Default sort are now count descending

### Fixed
- Don't show edit stock dialog on read-only shared stock
- Apply "Keep diapers with no stock" settings on shared stock
- Fix bugs when activating cloud sync via QR Code

## 1.15.14+152 - 2023-09-25
### Added
- Notification : indicate when notification are not allowed

## 1.15.13+151 - 2023-09-22
### Fixed
- Fix 12h/24h time format on web. 24h forced by default

## 1.15.12+150 - 2023-09-21
### Added
- 12 hour time format
- Can force time format

### Fixed
- Improve stock merge difference between cloud-sync's servers and device stock

## 1.15.11+149 - 2023-09-17
### Changed
- Request new permissions on android 14 for exact time notification

### Fixed
- Fix save old change end time when set state & wetness when remote slowly respond

## 1.15.10+148 - 2023-09-13
### Fixed
- Fix double icon on app menu

## 1.15.9+147 - 2023-09-11
### Added
- Stats: Add average wetness level
- Fix load wetness from cloud

### Changed
- Notifications no longer sent at the exact time for compatibility with Android 14 (probably temporary)

## 1.15.8+146 - 2023-09-06
### Added
- Stats: Add average wetness level
- Fix load wetness from cloud

## 1.15.7+145 - 2023-09-05
### Fixed
- Always display infinite stock

## 1.15.5+143 - 2023-09-03
### Changed
- Allow define custom size for custom booster
- Allow add custom size to stock for official booster

### Fixed
- Remove seconds and bellow units on change start/end time
- Fix calc stats  outside time of diaper with custom period

## 1.15.4+141 - 2023-09-01
### Fixed
- Fix

## 1.15.3+140 - 2023-09-01
### Fixed
- Fix

## 1.15.2+139 - 2023-08-31
### Changed
- Ask biometrics immediately if available

### Fixed
- Fix double unlock with biometrisc

## 1.15.1+138 - 2023-08-30
### Fixed
- Fix parsing wetness level with cloud-sync or shared history
- - Fix color of wet icon in dark mode


## 1.15.0+135 - 2023-08-30
### Added
- Select diaper randomly
- Custom stats period
- Wetness level

### Fixed
- Limit Whats news to current version

## 1.14.6+134 - 2023-08-28
### Added
- Allow export history in csv on web

## 1.14.5+133 - 2023-08-26
### Fixed
- Sort stock on add diaper on change dialog
- Handle defined booster size if default are in availables sizes
- Edit target on custom type enable save button
- Fix edit custom type when cloud-sync is disable

## 1.14.4+129 - 2023-08-24
### Added
- Add Korean translations

## 1.14.3+127 - 2023-08-22
### Fixed
- Fix duplicate custom type when disable cloud sync
- Sort your stash when using sharing stock
- Don't calc streak in current wearing widget if having many change without end time
