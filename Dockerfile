#Stage 1 - Install dependencies and build the app
FROM debian:12 AS builder

# Install flutter dependencies
RUN apt-get update
RUN apt-get install --no-install-recommends -y curl git unzip libgconf-2-4 gdb libstdc++6 libglu1-mesa fonts-droid-fallback python3 python3-pip jq
# lib32stdc++6
RUN pip3 install yq --break-system-packages
RUN apt-get clean

# Clone the flutter repo
RUN git clone -b stable https://github.com/flutter/flutter.git /usr/local/flutter

RUN curl -sL https://sentry.io/get-cli/ | bash

# Set flutter path
# RUN /usr/local/flutter/bin/flutter doctor -v
ENV PATH="/usr/local/flutter/bin:/usr/local/flutter/bin/cache/dart-sdk/bin:${PATH}"

# Run flutter doctor

# Enable flutter web
#RUN flutter channel master
RUN flutter config --enable-web
#RUN flutter upgrade

RUN flutter doctor -v


WORKDIR /app/
COPY . /app/

ARG SENTRY_AUTH_TOKEN=""

ARG CI_COMMIT_REF_SLUG=dev

#RUN flutter doctor -v
RUN flutter clean
RUN flutter build web --source-maps --dart-define=Dart2jsOptimization=O1

RUN echo "fr.diapstash.diap_stash@$(yq -r .version pubspec.yaml)"
RUN bash .docker/upload_sourcemaps.sh

FROM nginx

COPY .docker/nginx.conf /etc/nginx/conf.d/default.conf
ENV PORT 8080
ENV HOST 0.0.0.0
EXPOSE 8080


COPY --from=builder /app/build/web /usr/share/nginx/html

# CMD sh -c "envsubst '\$PORT' < /etc/nginx/conf.d/configfile.template > /etc/nginx/conf.d/default.conf && nginx -g 'daemon off;'"
